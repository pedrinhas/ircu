﻿using IRCUAssemblies.Utilizadores;
using IRCUAssemblies.Toolkit;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProxyAcessoDB.Types;

namespace ProxyAcessoDB
{
    public static class ProxyDB
    {
        public static class GIAF
        {
            public static IEnumerable<Carreira> Carreiras_LS(SqlConnection con)
            {
                var res = new List<Carreira>();

                using (var cmd = new SqlCommand("[stp_GIAF_Carreiras_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var carr = new Carreira()
                        {
                            ID = -1,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("descricao"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = true,
                            UtilizadorCriado = reader.GetString("utilizador_criacao"),
                            DataModificadoObject = reader.GetDateTime("data_alteracao"),
                            UtilizadorModificado = reader.GetString("utilizador_alteracao")
                        };

                        if(!reader.IsDBNull("grupoCarreira"))
                        {
                            carr.Grupo = new Carreira.GrupoCarreira() { Codigo = reader.GetString("grupoCarreira") };
                        }

                        var dCriado = reader.GetDateTime("data_criacao");
                        carr.DataCriadoObject = dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(carr);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<Carreira> Carreiras_LS(string cString)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Carreiras_LS(con);
                }
            }
            public static IEnumerable<Regime> Regimes_LS(SqlConnection con)
            {
                var regimes = new List<Regime>();

                using (var cmd = new SqlCommand("[stp_GIAF_Regimes_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    var reader = cmd.ExecuteReader();

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    while (reader.Read())
                    {
                        var reg = new Regime()
                        {
                            ID = -1,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            UtilizadorCriado = reader.GetString("UTILIZADOR_CRIACAO"),
                            UtilizadorModificado = reader.GetString("UTILIZADOR_ALTERACAO"),
                        };

                        var dataCriado = reader.GetDateTime("DATA_CRIACAO");
                        if (dataCriado != null && dataCriado.HasValue)
                        {
                            reg.DataCriadoObject = dataCriado.Value;
                        }
                        else
                        {
                            reg.DataCriadoObject = DateTime.MinValue;
                        }

                        var dataModificado = reader.GetDateTime("DATA_ALTERACAO");
                        if (dataModificado != null && dataModificado.HasValue)
                        {
                            reg.DataModificadoObject = dataModificado.Value;
                        }

                        regimes.Add(reg);
                    }

                    reader.Close();
                }

                return regimes;
            }
            public static IEnumerable<Regime> Regimes_LS(string cString)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Regimes_LS(con);
                }
            }
            public static IEnumerable<Carreira.GrupoCarreira> Carreiras_Grupos_LS(SqlConnection con)
            {
                var res = new List<Carreira.GrupoCarreira>();

                using (var cmd = new SqlCommand("[stp_GIAF_GrupoCarreiras_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var grupo = new Carreira.GrupoCarreira()
                        {
                            ID = -1,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("nome"),
                            Ativo = true,
                            UtilizadorCriado = reader.GetString("utilizador_criacao"),
                            DataModificadoObject = reader.GetDateTime("data_alteracao"),
                            UtilizadorModificado = reader.GetString("utilizador_alteracao")
                        };

                        var dCriado = reader.GetDateTime("data_criacao");
                        grupo.DataCriadoObject = dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(grupo);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<Carreira.GrupoCarreira> Carreiras_Grupos_LS(string cString)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Carreiras_Grupos_LS(con);
                }
            }
            public static IEnumerable<Entidade> Entidades_LS(SqlConnection con)
            {
                var res = new List<Entidade>();

                using (var cmd = new SqlCommand("[stp_GIAF_UO_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var grupo = new Entidade()
                        {
                            ID = -1,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            Sigla = reader.GetString("sigla"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = true,
                            UtilizadorCriado = reader.GetString("utilizador_criacao"),
                            DataModificadoObject = reader.GetDateTime("data_alteracao"),
                            UtilizadorModificado = reader.GetString("utilizador_alteracao")
                        };

                        var dCriado = reader.GetDateTime("data_criacao");
                        grupo.DataCriadoObject = dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(grupo);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<Entidade> Entidades_LS(string cString)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Entidades_LS(con);
                }
            }
            public static IEnumerable<Categoria> Categorias_LS(SqlConnection con)
            {
                var res = new List<Categoria>();

                using (var cmd = new SqlCommand("[stp_GIAF_Categorias_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var categoria = new Categoria()
                        {
                            ID = -1,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = true,
                            UtilizadorCriado = reader.GetString("utilizador_criacao"),
                            DataModificadoObject = reader.GetDateTime("data_alteracao"),
                            UtilizadorModificado = reader.GetString("utilizador_alteracao")
                        };

                        var dCriado = reader.GetDateTime("data_criacao");
                        categoria.DataCriadoObject = dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        var carreira = reader.GetString("carreira");
                        if (!string.IsNullOrWhiteSpace(carreira))
                        {
                            categoria.Carreira = new Carreira() { ID = -1, Codigo = carreira };
                        }

                        res.Add(categoria);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<Categoria> Categorias_LS(string cString)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Categorias_LS(con);
                }
            }
            public static IEnumerable<Situacao> Situacoes_LS(SqlConnection con)
            {
                var res = new List<Situacao>();

                using (var cmd = new SqlCommand("[stp_GIAF_Situacao_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var situacao = new Situacao()
                        {
                            ID = -1,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("nome"),
                            PRC = reader.GetString("prc"),
                            Ativo = true,
                            UtilizadorCriado = reader.GetString("utilizador_criacao"),
                            DataModificadoObject = reader.GetDateTime("data_alteracao"),
                            UtilizadorModificado = reader.GetString("utilizador_alteracao")
                        };

                        var dCriado = reader.GetDateTime("data_criacao");
                        situacao.DataCriadoObject = dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(situacao);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<Situacao> Situacoes_LS(string cString)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Situacoes_LS(con);
                }
            }
            public static IEnumerable<Perfil.TipoPerfil> TiposPerfil_LS(SqlConnection con)
            {
                var res = new List<Perfil.TipoPerfil>();

                using (var cmd = new SqlCommand("[stp_GIAF_Perfil_Tipo_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var tipo = new Perfil.TipoPerfil()
                        {
                            ID = -1,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            Sigla = reader.GetString("sigla"),
                            Descricao = reader.GetString("nome"),
                            Ativo = true,
                            UtilizadorCriado = reader.GetString("utilizador_criacao"),
                            DataModificadoObject = reader.GetDateTime("data_alteracao"),
                            UtilizadorModificado = reader.GetString("utilizador_alteracao")
                        };

                        var dCriado = reader.GetDateTime("data_criacao");
                        tipo.DataCriadoObject = dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(tipo);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<Perfil.TipoPerfil> TiposPerfil_LS(string cString)
            {
                using(var con = new SqlConnection(cString))
                {
                    return TiposPerfil_LS(con);
                }
            }
            public static string Categoria_CodigoTipoPerfil_S(SqlConnection con, string codigoCategoria)
            {
                var res = "";

                using (var cmd = new SqlCommand("[stp_GIAF_Categorias_TipoPerfil_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@codigoCategoria", System.Data.SqlDbType.NVarChar).Value = codigoCategoria;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == "" && reader.Read())
                    {
                        res = reader.GetString("tipoPerfil");
                    }

                    reader.Close();

                    return res;
                }
            }
            public static string Categoria_TipoPerfil_S(string cString, string codigoCategoria)
            { 
                using(var con = new SqlConnection(cString))
                {
                    return Categoria_CodigoTipoPerfil_S(con, codigoCategoria);
                }
            }

            public static IEnumerable<Utilizador> Funcionario_LS(SqlConnection con)
            {
                var res = new List<Utilizador>();

                using (var cmd = new SqlCommand("[stp_GIAF_Funcionario_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {

                        var user = (Utilizador)null;
                        var nif = reader.GetString("emp_num_fisc");
                        var onTheList = res.Select(x => x.NIF).Contains(nif);


                        if (onTheList)
                        {
                            //Já está na lista
                            user = res.Single(x => x.NIF == nif);
                        }
                        else
                        {
                            user = new Utilizador()
                            {
                                Nome = reader.GetString("nome"),
                                NIF = nif,
                                IUPI = reader.GetGuid("IUPI"),
                                Email = reader.GetString("email"),
                                ContaNoGIAF = true,
                                UtilizadorCriado = reader.GetString("utilizador_criacao"),
                                DataModificadoObject = reader.GetDateTime("data_alteracao"),
                                UtilizadorModificado = reader.GetString("utilizador_alteracao")
                        };

                        var dCriado = reader.GetDateTime("data_criacao");
                        user.DataCriadoObject = dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                            //res.Add(user);
                        }

                        var perfil = new Perfil()
                        {
                            ID = -1,
                            NumeroMecanografico = reader.GetString("nMec")
                        };


                        //AQUI
                        if (!string.IsNullOrWhiteSpace(user.Email))
                        //if (!string.IsNullOrWhiteSpace(reader.GetString("email")) || reader.GetString("emp_cat_func") == "770000")
                        {
                            user.OriginalEmail = user.Email ?? "";
                            if (user.OriginalEmail.Contains("@utad.pt")) user.Username = user.OriginalEmail.Substring(0, user.OriginalEmail.IndexOf('@'));
                            

                            if (!reader.IsDBNull("emp_sit") && reader.GetString("emp_sit") != "0")
                            {
                                perfil.Situacao = new Situacao() { Codigo = reader.GetString("emp_sit") };
                            }

                            if (!reader.IsDBNull("emp_regime") && reader.GetString("emp_regime") != "0")
                            {
                                perfil.Regime = new Regime() { Codigo = reader.GetString("emp_regime") };
                            }

                            if (!reader.IsDBNull("emp_cat_func") && reader.GetString("emp_cat_func") != "0")
                            {
                                perfil.Categoria = new Categoria() { Codigo = reader.GetString("emp_cat_func") };
                            }

                            if (!reader.IsDBNull("tipoPerfil") && reader.GetString("tipoPerfil") != "0")
                            {
                                perfil.Tipo = new Perfil.TipoPerfil() { Codigo = reader.GetString("tipoPerfil") };
                            }

                            if (!reader.IsDBNull("emp_carreira") && reader.GetString("emp_carreira") != "0")
                            {
                                perfil.Carreira = new Carreira() { Codigo = reader.GetString("emp_carreira") };


                                if (!reader.IsDBNull("grupoCarreira") && reader.GetString("grupoCarreira") != "0")
                                {
                                    perfil.Carreira.Grupo = new Carreira.GrupoCarreira() { Codigo = reader.GetString("grupoCarreira") };
                                }
                            }

                            if (!reader.IsDBNull("uo") && reader.GetString("uo") != "0")
                            {
                                perfil.Entidade = new Entidade() { Codigo = reader.GetString("uo") };
                            }

                            perfil.DataSaidaObject = reader.GetDateTime("dataSaida");
                            perfil.DataEntradaObject = reader.GetDateTime("dataEntrada");

                            user.Perfil.Add(perfil);
                            if (!onTheList) res.Add(user);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<Utilizador> Funcionario_LS(string cString)
            {
                using(var con = new SqlConnection(cString))
                {
                    return Funcionario_LS(con);
                }
            }

            public static Utilizador Funcionario_S(SqlConnection con, string nMec)
            {
                var user = (Utilizador)null;

                using (var cmd = new SqlCommand("[stp_GIAF_Funcionario_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@numero", System.Data.SqlDbType.NVarChar).Value = nMec;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (user == null && reader.Read())
                    {
                        user = new Utilizador()
                        {
                            Nome = reader.GetString("nome"),
                            NIF = reader.GetString("emp_num_fisc"),
                            Email = reader.GetString("email"),
                            Morada = reader.GetString("emp_mor"),
                            CodPostal = reader.GetString("emp_cpostal"),
                            Localidade = reader.GetString("emp_res_frg"),
                            NumTelefoneFixo = reader.GetString("emp_tlf_1"),
                            NumTelemovel = reader.GetString("emp_tlf_movel"),
                            ContaNoGIAF = true,
                            UtilizadorCriado = reader.GetString("utilizador_criacao"),
                            DataModificadoObject = reader.GetDateTime("data_alteracao"),
                            UtilizadorModificado = reader.GetString("utilizador_alteracao")
                        };

                        var dCriado = reader.GetDateTime("data_criacao");
                        user.DataCriadoObject = dCriado.HasValue ? dCriado.Value : DateTime.MinValue;


                        var perfil = new Perfil()
                        {
                            ID = -1,
                            NumeroMecanografico = nMec
                        };


                        if (!string.IsNullOrWhiteSpace(reader.GetString("email")))
                        {
                            user.Username = reader.GetString("email").Substring(0, reader.GetString("email").IndexOf('@'));
                            user.OriginalEmail = reader.GetString("email");
                        }
                        if (!reader.IsDBNull("codSituacao"))
                        {
                            perfil.Situacao = new Situacao() { Codigo = reader.GetString("codSituacao"), Nome = reader.GetString("situacao"), Descricao = reader.GetString("situacao") };
                        }

                        if (!reader.IsDBNull("codRegime"))
                        {
                            perfil.Regime = new Regime() { Codigo = reader.GetString("codRegime"), Nome = reader.GetString("regime"), Descricao = reader.GetString("regime") };
                        }

                        if (!reader.IsDBNull("codCategoria"))
                        {
                            perfil.Categoria = new Categoria() { Codigo = reader.GetString("codCategoria"), Nome = reader.GetString("categoria"), Descricao = reader.GetString("categoria") };
                        }

                        if (!reader.IsDBNull("codTipoPerfil"))
                        {
                            perfil.Tipo = new Perfil.TipoPerfil() { Codigo = reader.GetString("codTipoPerfil"), Nome = reader.GetString("tipoPerfil"), Descricao = reader.GetString("tipoPerfil") };
                        }

                        if (!reader.IsDBNull("codCarreira"))
                        {
                            perfil.Carreira = new Carreira() { Codigo = reader.GetString("codCarreira"), Nome = reader.GetString("carreira"), Descricao = reader.GetString("carreira") };


                            if (!reader.IsDBNull("grupoCarreira"))
                            {
                                perfil.Carreira.Grupo = new Carreira.GrupoCarreira() { Codigo = reader.GetString("codGrupoCarreira"), Nome = reader.GetString("grupoCarreira"), Descricao = reader.GetString("grupoCarreira") };
                            }
                        }

                        perfil.Categoria.Carreira = perfil.Carreira;

                        if (!reader.IsDBNull("uo"))
                        {
                            perfil.Entidade = new Entidade() { Codigo = reader.GetString("uo"), Nome = reader.GetString("servico"), Descricao = reader.GetString("servico") };
                        }

                        perfil.DataSaidaObject = reader.GetDateTime("dataSaida");
                        perfil.DataEntradaObject = reader.GetDateTime("dataEntrada");

                        user.Perfil.Add(perfil);
                    }


                    reader.Close();

                    return user;
                }
            }
            public static Utilizador Funcionario_S(string cString, string nMec)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Funcionario_S(con, nMec);
                }

            }

            public static void Dados_U(SqlConnection con, string numFunc, string extensao, string gabinete)
            {
                using (var cmd = new SqlCommand("[stp_GIAF_Dados_U]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@empnum", System.Data.SqlDbType.VarChar).Value = numFunc;
                    cmd.Parameters.Add("@extensao", System.Data.SqlDbType.VarChar).Value = extensao;
                    cmd.Parameters.Add("@gabinete", System.Data.SqlDbType.VarChar).Value = gabinete.Substring(0, Math.Min(gabinete.Length, 20)); //O GIAF SÓ ARMAZENA 20 CHARS NA BD. O Pedro Não permite mais de 20 chars no site, mas assim assegura-se que ninguém consegue contornar

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Dados_U(string cString, string numFunc, string extensao, string gabinete)
            {
                using(var con = new SqlConnection(cString))
                {
                    Dados_U(con, numFunc, extensao, gabinete);
                }
            }
            public static void IUPI_U(SqlConnection con, string numFunc, Guid iupi)
            {
                using (var cmd = new SqlCommand("[stp_GIAF_IUPI_U]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@empnum", System.Data.SqlDbType.VarChar).Value = numFunc;
                    cmd.Parameters.Add("@iupi", System.Data.SqlDbType.VarChar).Value = iupi;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void IUPI_U(string cString, string numFunc, Guid iupi)
            {
                using(var con = new SqlConnection(cString))
                {
                    IUPI_U(con, numFunc, iupi);
                }
            }

            public static void IUPI_U_TabelaEntrada(SqlConnection con, IEnumerable<NMecIUPIType> nmecIUPIs)
            {
                using (var cmd = new SqlCommand("[stp_GIAF_IUPI_U_TabelaEntrada]", con))
                {
                    var dt = NMecIUPIType.CreateDataTable();

                    foreach (var nmeciupi in nmecIUPIs)
                    {
                        dt.Rows.Add(nmeciupi.ToDataRow());
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    var param = cmd.Parameters.AddWithValue("@perfis", dt);
                    param.SqlDbType = System.Data.SqlDbType.Structured;
                    param.TypeName = "dbo.UtilizadorPerfilType";

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void IUPI_U_TabelaEntrada(string cString, IEnumerable<NMecIUPIType> nmecIUPIs)
            {
                using(var con = new SqlConnection(cString))
                {
                    IUPI_U_TabelaEntrada(con, nmecIUPIs);
                }
            }
        }
    }
}
