﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProxyAcessoDB.Types
{
    public class NMecIUPIType
    {
        public string Empnum { get; set; }
        public string IUPI { get; set; }

        public object[] ToDataRow()
        {
            return new object[] { Empnum, IUPI };
        }
        public static DataTable CreateDataTable()
        {
            var dt = new DataTable();

            dt.Columns.Add("Empnum", typeof(string));
            dt.Columns.Add("IUPI", typeof(string));

            return dt;
        }
    }
}
