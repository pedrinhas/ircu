﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Script.Serialization;

namespace ErrorHandling
{
    public static class ErrorHandler
    {
        private static bool initd = false;

        private static string _appName = "";

        private static bool _logByEmail = false;
        private static bool _logByDB = false;

        private static string _from = "";
        private static string _smtp = "smtp.utad.pt";

        private static List<string> _to;
        private static List<string> _cc;
        private static List<string> _bcc;

        private static int _logLvl = 0;

        public static void Initialize(string appName, string smtp, string from, List<string> to, List<string> cc = null, List<string> bcc = null, int minLogLvl = 1, bool logByEmail = true, bool logByDB = false)
        {
            _appName = appName;

            _logLvl = minLogLvl;

            _logByEmail = logByEmail;
            _logByDB = logByDB;

            _from = from;
            _smtp = smtp;

            _to = to ?? new List<string>();
            _cc = cc ?? new List<string>();
            _bcc = bcc ?? new List<string>();


            initd = true;

            if(_logByEmail && !_logByDB)
            {
                if(string.IsNullOrWhiteSpace(_smtp) || _to.Count + _cc.Count + _bcc.Count == 0)
                {
                    initd = false;
                }
            }
        }

        private static Enums.Severity MinimumSeverity
        {
            get
            {
                if (_logLvl > (int)Enums.Severity.Critical)
                    return Enums.Severity.Critical;
                else if (_logLvl < (int)Enums.Severity.Info)
                    return Enums.Severity.Info;
                else return (Enums.Severity)_logLvl;
            }
        }

        public static bool LogError(Exception ex, Object obj = null, Enums.Severity severity = Enums.Severity.Error)
        {
            if (severity < MinimumSeverity) return false;

            bool email = false, db = false;
            Exception exObj = null;

            if(obj != null)
            {
                exObj = new Exception(new JavaScriptSerializer().Serialize(obj), ex);
            }

            if (_logByEmail) email = LogErrorEmail(exObj ?? ex, severity);
            else email = true; //caso os logs para email estiverem desligados, não dá false só porque não mandou email
            if(_logByDB) db = LogErrorDB(exObj ?? ex, severity);

            return email && db;
        }

        public static bool LogErrorEmail(List<string> to, string subject, string body, Exception ex, Enums.Severity severity = Enums.Severity.Error, List<string> cc = null, List<string> bcc = null)
        {
            if (severity < MinimumSeverity) return false;

            if (!string.IsNullOrWhiteSpace(body)) body = body.TrimEnd(new char[] { '\n', '\r' }) + string.Format("{0}{0}", Environment.NewLine);

            body += string.Format("IP{0}{1}{2}{0}{0}", Environment.NewLine, "\t", HttpContext.Current.Request.UserHostAddress);
            body += string.Format("URL{0}{1}{2}{0}{0}", Environment.NewLine, "\t", HttpContext.Current.Request.Url);
            body += string.Format("Http Status Code{0}{1}{2} - {3}{0}{0}", Environment.NewLine, "\t", HttpContext.Current.Response.StatusCode + "." + HttpContext.Current.Response.SubStatusCode, HttpContext.Current.Response.StatusDescription);

            body += string.Format("Request Headers{0}", Environment.NewLine);

            foreach (var header in HttpContext.Current.Request.Headers.AllKeys)
            {
                var val = HttpContext.Current.Request.Headers[header];

                body += string.Format("{1}{2}: {3}{0}", Environment.NewLine, "\t", header, val);
            }

            body += Environment.NewLine;

            body += string.Format("Response Headers{0}", Environment.NewLine);

            foreach (var header in HttpContext.Current.Response.Headers.AllKeys)
            {
                var val = HttpContext.Current.Response.Headers[header];

                body += string.Format("{1}{2}: {3}{0}", Environment.NewLine, "\t", header, val);
            }


            string exText = "";

            do
            {
                exText += string.Format("{0} - {1}", ex.GetType().ToString(), ex.Message);

                if (!string.IsNullOrWhiteSpace(ex.StackTrace)) exText += string.Format("{0}{1}StackTrace:{0}{1}{2}", Environment.NewLine, "\t", ex.StackTrace);
                if (ex.TargetSite != null && !string.IsNullOrWhiteSpace(ex.TargetSite.ToString())) exText += string.Format("{0}{0}{1}TargetSite:{0}{1}{2}{3}{3}", Environment.NewLine, "\t", ex.TargetSite, ex.InnerException == null ? "" : Environment.NewLine);

                ex = ex.InnerException;
            } while (ex != null);

            var bodyAll = "";

            if (string.IsNullOrWhiteSpace(body))
            {
                bodyAll = string.Format("Exceptions{0}{1}{2}", Environment.NewLine, "\t", exText);
            }
            else
            {
                bodyAll = string.Format("{2}{0}{0}Exceptions{0}{1}{3}", Environment.NewLine, "\t", body.TrimEnd(new char[] { '\n', '\r' }), exText);
            }

            return Toolkit.SendEmail(_smtp, _from, to, subject, bodyAll, cc, bcc);
        }
        public static bool LogErrorEmail(List<string> to, Exception ex, Enums.Severity severity = Enums.Severity.Error, List<string> cc = null, List<string> bcc = null)
        {
            if (severity < MinimumSeverity) return false;

            string subject = string.Format("[{2}] [{3}] Exception: {0} - {1}", ex.GetType().ToString(), ex.Message, severity.ToString(), _appName);

            return LogErrorEmail(to, subject, "", ex, severity, cc, bcc);
        }
        public static bool LogErrorEmail(Exception ex, Enums.Severity severity = Enums.Severity.Error)
        {
            if (severity < MinimumSeverity) return false;

            return LogErrorEmail(_to, ex, severity, _cc, _bcc);
        }

        public static bool LogErrorDB(Exception ex, Enums.Severity severity = Enums.Severity.Error)
        {
            if (severity < MinimumSeverity) return false;

            return true;
        }

        private static class Toolkit
        {
            public static bool SendEmail(string stmp, string from, List<string> to, string subject, string body, List<string> cc = null, List<string> bcc = null)
            {
                try
                {
                    MailMessage mail = new MailMessage(from, to.First());
                    foreach (var toAddress in to.Where(x => x != to.First()))
                    {
                        mail.To.Add(toAddress);
                    }
                    foreach (var ccAddress in cc)
                    {
                        mail.CC.Add(ccAddress);
                    }
                    foreach (var bccAddress in bcc)
                    {
                        mail.Bcc.Add(bccAddress);
                    }
                    SmtpClient client = new SmtpClient();
                    client.Port = 25;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Host = stmp;
                    mail.Subject = subject;
                    mail.Body = body;
                    client.Send(mail);
                }
                catch (Exception ex)
                {
                    return false;
                }

                return true;
            }
        }
    }
}