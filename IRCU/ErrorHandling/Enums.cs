﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ErrorHandling
{
    public static class Enums
    {
        public enum Severity
        {
            Info = 0,
            Warning = 1,
            Error = 2,
            Critical = 3
        }
    }
}