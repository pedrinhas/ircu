﻿namespace Tester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tbModifyPasswordUsername = new System.Windows.Forms.TextBox();
            this.gbModifyPassword = new System.Windows.Forms.GroupBox();
            this.bModifyPassword = new System.Windows.Forms.Button();
            this.tbModifyPasswordNewPW = new System.Windows.Forms.TextBox();
            this.tbModifyPasswordOldPW = new System.Windows.Forms.TextBox();
            this.gbModifyPassword.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(165, 226);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Test XML search";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(84, 226);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Query IDM";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // tbModifyPasswordUsername
            // 
            this.tbModifyPasswordUsername.Location = new System.Drawing.Point(6, 19);
            this.tbModifyPasswordUsername.Name = "tbModifyPasswordUsername";
            this.tbModifyPasswordUsername.Size = new System.Drawing.Size(100, 20);
            this.tbModifyPasswordUsername.TabIndex = 2;
            this.tbModifyPasswordUsername.Text = "cpereira";
            // 
            // gbModifyPassword
            // 
            this.gbModifyPassword.Controls.Add(this.tbModifyPasswordOldPW);
            this.gbModifyPassword.Controls.Add(this.bModifyPassword);
            this.gbModifyPassword.Controls.Add(this.tbModifyPasswordNewPW);
            this.gbModifyPassword.Controls.Add(this.tbModifyPasswordUsername);
            this.gbModifyPassword.Location = new System.Drawing.Point(13, 38);
            this.gbModifyPassword.Name = "gbModifyPassword";
            this.gbModifyPassword.Size = new System.Drawing.Size(194, 114);
            this.gbModifyPassword.TabIndex = 3;
            this.gbModifyPassword.TabStop = false;
            this.gbModifyPassword.Text = "Modificar password";
            // 
            // bModifyPassword
            // 
            this.bModifyPassword.Location = new System.Drawing.Point(112, 69);
            this.bModifyPassword.Name = "bModifyPassword";
            this.bModifyPassword.Size = new System.Drawing.Size(75, 23);
            this.bModifyPassword.TabIndex = 4;
            this.bModifyPassword.Text = "Modificar";
            this.bModifyPassword.UseVisualStyleBackColor = true;
            this.bModifyPassword.Click += new System.EventHandler(this.bModifyPassword_Click);
            // 
            // tbModifyPasswordNewPW
            // 
            this.tbModifyPasswordNewPW.Location = new System.Drawing.Point(6, 71);
            this.tbModifyPasswordNewPW.Name = "tbModifyPasswordNewPW";
            this.tbModifyPasswordNewPW.PasswordChar = '•';
            this.tbModifyPasswordNewPW.Size = new System.Drawing.Size(100, 20);
            this.tbModifyPasswordNewPW.TabIndex = 3;
            // 
            // tbModifyPasswordOldPW
            // 
            this.tbModifyPasswordOldPW.Location = new System.Drawing.Point(6, 45);
            this.tbModifyPasswordOldPW.Name = "tbModifyPasswordOldPW";
            this.tbModifyPasswordOldPW.PasswordChar = '•';
            this.tbModifyPasswordOldPW.Size = new System.Drawing.Size(100, 20);
            this.tbModifyPasswordOldPW.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.gbModifyPassword);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gbModifyPassword.ResumeLayout(false);
            this.gbModifyPassword.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox tbModifyPasswordUsername;
        private System.Windows.Forms.GroupBox gbModifyPassword;
        private System.Windows.Forms.Button bModifyPassword;
        private System.Windows.Forms.TextBox tbModifyPasswordNewPW;
        private System.Windows.Forms.TextBox tbModifyPasswordOldPW;
    }
}

