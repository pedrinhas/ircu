﻿using IRCUAssemblies.IDM;
using IRCUAssemblies.IDM.Helpers;
using IRCUAssemblies.LDAP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Tester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            string url = @"http://193.136.40.108:8080/idm/servlet/rpcrouter2",
                   user = "configurator",
                   pw = "configurator";

            LDAPTools.Initialize("LDAP://172.16.249.35");
            SPMLOperations.Initialize(url, user, pw);
            //LDAPTools.Initialize()
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string xml = @"<?xml version='1.0' encoding='UTF-8'?><SOAP-ENV:Envelope  xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/'><SOAP-ENV:Body><spml:searchResponse xmlns:spml='urn:oasis:names:tc:SPML:1:0' xmlns:dsml='urn:oasis:names:tc:DSML:2:0:core' result='urn:oasis:names:tc:SPML:1:0#success'><spml:operationalAttributes><dsml:attr name='session'><dsml:value>AAAHxgAARSQAAAemH4sIAAAAAAAAAK1cS1PjOBC+z69QFYectgjMBGamYKqAhN1UwcAm8zgbRzBaHCsr2wzZX79+xI5ktWR3mxt0f/q69WqpJTlny+zhHx6mLA7W/Hx0JeNH8ZSpIJVqxBQPVndxtD0fpSrjI5ZU2G9izWWWno/GIxYFSXoRhjxJCun56GhyNJ6cfjx5//FocjL68o6xs3sl4lBsgigp/s0FdyXLgj+ydLvJy3xPeG5MrM5HB/PpgekC4Nahg+ZitRbxn0pmmz1Z7prM4pSVOpGkBiusxNAXnjOvDQ8CY+iH4L9Z1U4V716AobnJjZelkppHk5CIWI6zySopmrBUsIWMuEmpy9GkV8EmeBCRSEWL1VCgae/UUxCL/4JUyNjkNTVo4nsZibDtayPEt2m2Eg7SlgpNveCJzFTYYt1LyYSsVMK0tQ5Pbg0r4oBaZBZRRiIqZsq1VGt7/lRSdIC45WnAir+MKLGXkiLO93kVfa3I0yiQATlfK6og8X2uRWJdiiG8yheplBuxUReho7kez2ihLG+VuFhRwQgBKyn07SltySmk1pS2FUNoWxPbpSaZ0Ke3KaTQGYukKUQNqc2qPTp1EYZqyiPeotJFBKr59Baia8Q4yo2SLyLJh3OLsyVHtV4cifjZbD1NhKMKkkQ8xS0yXYihm8XBQ2R2hi5CtZxILC5DhiFb8ILA4NJFGKr7vG1+S7WC97UOLWqq/QriJ878dvwgXNMkPO2w5sVgjM3XG6lSox90EXISyNCaBI0ItfhuislYJCRgOgRrUW2cx9uapmnTXMYaIbLHymXBYqyXCxKrvvK2mXUdjf1iH1DAzMOppyxSkAFQRyEHkjFIRV1cjY2vrcDQLu9vb1i1hawJdRGG6jKLnv0JvQeBNtR9fNAFQ5sEtsuWHF8Pe5djydGkwH7HklNJ2zsfUEcgh/dAsBLfytAWBtAQiK1tliVHkwI7JEuOb2Fgr2QrKGuMcz8AaQm7nIswFS+cLbdxyPw2+xcg5N0Nk5V76xpkTE+VjDAV7F+C1JOlvKM/IQxlJZu9hlG24qs6PCfm6VAXDD0B/C3rg5CasnO37IURpklPyz3R+PSgf817gOm967feC4vfGaB6AFOE0g79+wJRApvKODrf1uBqmIi4jLjDeMri+mnlXkA9PyNEpaY4FPbwdPNvQ5slz9oiEe4yODpLUownevkb+STa9utDfkBFafGK50oqxaOqumAfuGBDTMaF3yJ29LoDRTH4dXfVwL4WR1igNQhCPQ6AO8yDoCa/7JavH7hy7BYABOrgrrgA2DqO7SAdamI0qb/jEMmlH3q+ATcVgCAZqrakUb5DMy5mHEb9aPKgmCesOh7iK+2IxTNGvAVQbtTN6Bg2Tj3qmIaHmXKacGgxBn4GL9wO2zt+WIka+sV1ub4HLM9zB43y8lELry7i229dajFhdkKemfNzqO+LLGY6VdMobTmWdOeZsa9pidF95m8QNwLdIBqV0SC6HJeThDIORbmtc9egA4XvAZPO7IaWjpRV+ivjA+HrYrCZVTFVKGqRPLOLOIi2iUgc9fBA0LXQuYw6GApC8uDpBycA7b2Eh5KkjaLdmwy3524A1nONSfdcF2MovwXJs9dzNwDrucake66LCStfdxx1YUiRVEKN39KQiIvdUpoq8ZCl0Mh0YkjjpzleBYeRqSXVJk+3XJXQVFjqXaKwzNbrQG0BAzCAtmTWXGEA7idcGJKxHc8PIXfp8F/5KJVqa1l0ArFmjfzRa7cDSV79vEY9KKzBPYN73DgxWGNLvglUxSQf2TSrUkDLng+GHkD1bH0p3kC66+jFDTM65Wkgok6bJgwd64Ho54n63WjKm/p9XPS/rHfi0LXeh0tPZZ0gwhGNOTK8thEl8PmRHVt9uVInGu2AK9R6vOhZhHwS2NMXVBlSstTTkd543BtwOFbDLvRE446MnMHbdYrUtwDp8hyI585Pi/qWoDtiBPk+fvgKUPIBbyIwgL56zAJ/uQIrCfTmsb7Plg9JelkHGnNoqY8PvTWCEORlo3upGD7SFnzNVwLONxsNaQinKU+g0VvLKaT3XAm5EmFrU+adLr3KkGpY8ZXLtX/COoG4hSvvjy2bxyl/Up5btU4c/rn14ezVHQl9ENLCXIE7VmMQRDsC9s22PeotLHWeMv+b8WRIpZov0mBTLjXqAl6EPE4cdYGVuO8/VrsgCvE7tLidj3oRxUuW4tHoqqbyXLz2L0BZspb3M/wdZb9CQ9zJoxUPVPirvJLlr44HCR3oN3HgWkRpcSzcy74BHmb+4rFgQjnhLjL4jv4u3nd69329jR4+Q/pNi7eZku4A0Ac6yDTwjN+LGWQMeInvxQwyZv3SgQcxrPfsjxa8GIyxaZCvYD8DxX/JLF9n/s74/swV1KHI+UPWfI1d/TPAN3D8ejGlsbND82c9zu425RuYnSO3wab6q/p7Fqd5+vvMt0UbJ+U3EPPpiL0EUZZbO1jOlsuDD+Prj+PTow+TT9ez09Pxyec/TsZXp5PL45PPR5Pry/fHx+NPn6afJ5dH47q+FnlUPD3TXgA2Jq5knoHnq/KNiHm551SPQcibdjvc+Xt22FTj7HD3yyhf3v0PpXYIfSRFAADqaDh2fZPWl3t7kss+s60StP771A==</dsml:value></dsml:attr></spml:operationalAttributes><spml:searchResultEntry><spml:identifier type='urn:oasis:names:tc:SPML:1:0#GenericString'><spml:id>acolaco</spml:id></spml:identifier><spml:attributes><dsml:attr name='lhlocked'><dsml:value>false</dsml:value></dsml:attr><dsml:attr name='dis'><dsml:value>0</dsml:value></dsml:attr><dsml:attr name='MemberObjectGroupsIds'><dsml:value>#ID#9AE29A978E2542E3:754E17EF:1185F8B5A7F:-7D07</dsml:value></dsml:attr><dsml:attr name='id'><dsml:value>#ID#40F8071459FE7706:76D5A622:11DFC619D87:-1BB3</dsml:value></dsml:attr><dsml:attr name='res'><dsml:value>View Funcionarios Intranet</dsml:value><dsml:value>Table Utilizadores Intranet</dsml:value><dsml:value>Table Logins Intranet</dsml:value><dsml:value>...</dsml:value></dsml:attr><dsml:attr name='lastname'><dsml:value>Colaço</dsml:value></dsml:attr><dsml:attr name='lhdis'><dsml:value>false</dsml:value></dsml:attr><dsml:attr name='xmlSize'><dsml:value>5419</dsml:value></dsml:attr><dsml:attr name='hasCapabilities'><dsml:value>false</dsml:value></dsml:attr><dsml:attr name='firstname'><dsml:value>Aura Antunes</dsml:value></dsml:attr><dsml:attr name='MemberObjectGroups'><dsml:value>Top:Staff:DEP_32</dsml:value></dsml:attr><dsml:attr name='role'><dsml:value>STAFF_USER</dsml:value></dsml:attr><dsml:attr name='prov'><dsml:value>2</dsml:value></dsml:attr></spml:attributes></spml:searchResultEntry></spml:searchResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>";

            string xmlNoUser = @"<?xml version='1.0' encoding='UTF-8'?><SOAP-ENV:Envelope  xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/'><SOAP-ENV:Body><spml:searchResponse xmlns:spml='urn:oasis:names:tc:SPML:1:0' xmlns:dsml='urn:oasis:names:tc:DSML:2:0:core' result='urn:oasis:names:tc:SPML:1:0#success'><spml:operationalAttributes><dsml:attr name='session'><dsml:value>AAAHxgAARSQAAAemH4sIAAAAAAAAAK1cS1PjOBC+z69QFYectgjMBGamYKqAhN1UwcAm8zgbRzBaHCsr2wzZX79+xI5ktWR3mxt0f/q69WqpJTlny+zhHx6mLA7W/Hx0JeNH8ZSpIJVqxBQPVndxtD0fpSrjI5ZU2G9izWWWno/GIxYFSXoRhjxJCun56GhyNJ58fP/x9MPJ6WT05R1jZ/dKxKHYBFFS/JsL7kqWBX9k6XaTl/me8NyYWJ2PDubTA9MFwK1DB83Fai3iP5XMNnuy3DWZxSkrdSJJDVZYiaEvPGdeGx4ExtAPwX+zqp0q3r0AQ3OTGy9LJTWPJiERsRxnk1VSNGGpYAsZcZNSl6NJr4JN8CAikYoWq6FA096ppyAW/wWpkLHJa2rQxPcyEmHb10aIb9NsJRykLRWaesETmamwxbqXkglZqYRpax2e3BpWxAG1yCyijERUzJRrqdb2/Kmk6ABxy9OAFX8ZUWIvJUWc7/Mq+lqRp1EgA3K+VlRB4vtci8S6FEN4lS9SKTdioy5CR3M9ntFCWd4qcbGighECVlLo21PaklNIrSltK4bQtia2S00yoU9vU0ihMxZJU4gaUptVe3TqIgzVlEe8RaWLCFTz6S1E14hxlBslX0SSD+cWZ0uOar04EvGz2XqaCEcVJIl4iltkuhBDN4uDh8jsDF2EajmRWFyGDEO24AWBwaWLMFT3edv8lmoF72sdWtRU+xXET5z57fhBuKZJeNphzYvBGJuvN1KlRj/oIuQkkKE1CRoRavHdFJOxSEjAdAjWoto4j7c1TdOmuYw1QmSPlcuCxVgvFyRWfeVtM+s6GvvFPqCAmYdTT1mkIAOgjkIOJGOQirq4GhtfW4GhXd7f3rBqC1kT6iIM1WUWPfsTeg8Cbaj7+KALhjYJbJctOb4e9i7HkqNJgf2OJaeStnc+oI5ADu+BYCW+laEtDKAhEFvbLEuOJgV2SJYc38LAXslWUNYY534A0hJ2ORdhKl44W27jkPlt9i9AyLsbJiv31jXImJ4qGWEq2L8EqSdLeUd/QhjKSjZ7DaNsxVd1eE7M06EuGHoC+FvWByE1Zedu2QsjTJOelnui8elB/5r3ANN712+9Fxa/M0D1AKYIpR369wWiBDaVcXS+rcHVMBFxGXGH8ZTF9dPKvYB6fkaISk1xKOzh6ebfhjZLnrVFItxlcHSWpBhP9PI38km07deH/ICK0uIVz5VUikdVdcE+cMGGmIwLv0Xs6HUHimLw6+6qgX0tjrBAaxCEehwAd5gHQU1+2S1fP3Dl2C0ACNTBXXEBsHUc20E61MRoUn/HIZJLP/R8A24qAEEyVG1Jo3yHZlzMOIz60eRBMU9YdTzEV9oRi2eMeAug3Kib0TFsnHrUMQ0PM+U04dBiDPwMXrgdtnf8sBI19Ivrcn0PWJ7nDhrl5aMWXl3Et9+61GLC7IQ8M+fnUN8XWcx0qqZR2nIs6c4zY1/TEqP7zN8gbgS6QTQqo0F0OS4nCWUcinJb565BBwrfAyad2Q0tHSmr9FfGB8LXxWAzq2KqUNQieWYXcRBtE5E46uGBoGuhcxl1MBSE5MHTD04A2nsJDyVJG0W7Nxluz90ArOcak+65LsZQfguSZ6/nbgDWc41J91wXE1a+7jjqwpAiqYQav6UhERe7pTRV4iFLoZHpxJDGT3O8Cg4jU0uqTZ5uuSqhqbDUu0Rhma3XgdoCBmAAbcmsucIA3E+4MCRjO54fQu7S4b/yUSrV1rLoBGLNGvmj124Hkrz6eY16UFiDewb3uHFisMaWfBOoikk+smlWpYCWPR8MPYDq2fpSvIF019GLG2Z0ytNARJ02TRg61gPRzxP1u9GUN/X7uOh/We/EoWu9D5eeyjpBhCMac2R4bSNK4PMjO7b6cqVONNoBV6j1eNGzCPkksKcvqDKkZKmnI73xuDfgcKyGXeiJxh0ZOYO36xSpbwHS5TkQz52fFvUtQXfECPJ9/PAVoOQD3kRgAH31mAX+cgVWEujNY32fLR+S9LIONObQUh8femsEIcjLRvdSMXykLfiarwScbzYa0hBOU55Ao7eWU0jvuRJyJcLWpsw7XXqVIdWw4iuXa/+EdQJxC1feH1s2j1P+pDy3ap04/HPrw9mrOxL6IKSFuQJ3rMYgiHYE7Jtte9RbWOo8Zf4348mQSjVfpMGmXGrUBbwIeZw46gIrcd9/rHZBFOJ3aHE7H/UiipcsxaPRVU3luXjtX4CyZC3vZ/g7yn6FhriTRyseqPBXeSXLXx0PEjrQb+LAtYjS4li4l30DPMz8xWPBhHLCXWTwHf1dvO/07vt6Gz18hvSbFm8zJd0BoA90kGngGb8XM8gY8BLfixlkzPqlAw9iWO/ZHy14MRhj0yBfwX4Giv+SWb7O/J3x/ZkrqEOR84es+Rq7+meAb+D49WJKY2eH5s96nN1tyjcwO0dug031V/X3LE7z9PeZb4s2TspvIObTEXsJoiy3drCcLZcHH8bXH8enRx8mn65np6fjk89/nIyvTieXxyefjybXl++Pj8efPk0/Ty6PxnV9LfKoeHqmvQBsTFzJPAPPV+UbEfNyz6keg5A37Xa48/fssKnG2eHul1G+vPsf/yB2ViRFAACAqpX/oqStcc7Qq7ql5kXRRTylcA==</dsml:value></dsml:attr></spml:operationalAttributes></spml:searchResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>";

            var user = UtilizadorIDM.FromXML(xml);
            var noUser = UtilizadorIDM.FromXML(xmlNoUser);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var searchedUser = SPMLOperations.SearchUser("cpereira");
        }

        private void bModifyPassword_Click(object sender, EventArgs e)
        {
            var uname = tbModifyPasswordUsername.Text;
            var oldpwd = tbModifyPasswordOldPW.Text;
            var newpwd = tbModifyPasswordNewPW.Text;

            if (!string.IsNullOrWhiteSpace(uname) && !string.IsNullOrWhiteSpace(newpwd))
            {
                var res = SPMLOperations.ModifyPassword(uname, oldpwd, newpwd);
                if(res.Status == IRCUAssemblies.IDM.Helpers.SPMLResult.SPMLResultStatus.Success)
                {
                    MessageBox.Show("A password foi modificada com sucesso", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(string.Format("Não foi possível modificar a password.{0}Erro {1}: {2}", Environment.NewLine, res.ErrorCode, res.ErrorMessage), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
