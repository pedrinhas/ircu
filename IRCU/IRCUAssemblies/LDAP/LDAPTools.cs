﻿using System;
using System.DirectoryServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAssemblies.LDAP
{
    public static class LDAPTools
    {
        private static bool initd = false;
        private static string ldapAddr;

        public static void Initialize(string addr)
        {
            ldapAddr = addr;

            initd = true;
        }

        /*SEARCH USERNAME IN LDAP DIRECTORY*/
        public static bool Authenticate(string username, string password)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                throw new ArgumentException("O username é vazio");
            }
            else if (string.IsNullOrWhiteSpace(password))
            {
                throw new ArgumentException("A password é vazia");
            }
            else if (string.IsNullOrWhiteSpace(ldapAddr))
            {
                throw new ArgumentException("Não foi possível determinar o endereço do LDAP. Ver no Web.config");
            }


            //UID FOR ALUNOS
            String uid = "uid=" + username + ",OU=alunos,OU=organizacao,DC=utad,DC=pt";

            //ROOT FOR ALUNOS
            DirectoryEntry root = new DirectoryEntry(ldapAddr, uid, password, AuthenticationTypes.None);

            try
            {
                //ATTEMPT TO USE LDAP CONNECTION WITH ALUNOS ROOT
                object connected = root.NativeObject;
                //NO EXCEPTION, LOGIN SUCCESFUL WITH ALUNOS ROOT             
                return true;
            }
            catch (Exception)
            {
                //UID FOR FUNCIONARIOS
                String uidFuncionarios = "uid=" + username + ",OU=pessoas,OU=organizacao,DC=utad,DC=pt";

                //ROOT FOR FUNCIONARIOS
                DirectoryEntry root2 = new DirectoryEntry(ldapAddr, uidFuncionarios, password, AuthenticationTypes.None);

                try
                {
                    //ATTEMPT TO USE LDAP CONNECTION WITH FUNCIONARIOS ROOT
                    object connected = root2.NativeObject;
                    //NO EXCEPTION, LOGIN SUCCESFUL WITH FUNCIOINARIOS ROOT                 
                    return true;
                }
                catch (Exception)
                {
                    //EXCEPTION THROW, LOGIN FAILED               
                    return false;
                }

            }
        }


        #region Exchange

        public static class Exchange
        {
            private static bool initd = false;
            private static string ldapBaseAddr, ldapSrchAddr, ldapFilter, ldapUser, ldapPassword;
            private static List<SearchResult> all = null;

            public static void Initialize(string ldapSearchAddr, string fltr, string usr, string pwd)
            {
                ldapSrchAddr = ldapSearchAddr;
                ldapFilter = fltr;
                ldapUser = usr;
                ldapPassword = pwd;

                initd = true;
            }

            public static AliasResult GetAlias(string username)
            {
                if (!initd) throw new Exception("Inicializar o LDAPTools");

                var result = new AliasResult() { HasAlias = false };

                try
                {
                    #region Conexão e procura no LDAP
                    if (all == null)
                    {
                        var ldapQuery = string.Format(ldapSrchAddr, username);

                        DirectoryEntry root = new DirectoryEntry(ldapQuery, ldapUser, ldapPassword, AuthenticationTypes.Secure);

                        DirectorySearcher searcher = new DirectorySearcher(root);
                        searcher.PropertiesToLoad.Add("cn");
                        searcher.PropertiesToLoad.Add("proxyAddresses");
                        searcher.PropertiesToLoad.Add("mail");

                        searcher.PageSize = int.MaxValue;

                        searcher.Filter = string.Format(ldapFilter, username);

                        //searcher.Filter = tbFilter.Text;
                        //Abaixo é tudo já valores por defeito
                        searcher.SearchScope = SearchScope.Subtree;
                        searcher.SizeLimit = int.MaxValue;

                        all = searcher.FindAll().OfType<SearchResult>().ToList();
                    }

                    var res = all.SingleOrDefault(x => x.Properties.Contains("cn") && x.Properties["cn"][0].ToString() == username);
                    if (res == null) return result;
                    //if (res == null) throw new Exception("res a null"); //fds, que é este bruxedo?

                    #endregion

                    #region Tratar resposta do LDAP
                    if (res.Properties.Contains("cn") && res.Properties.Contains("proxyAddresses"))
                    {
                        var realName = res.Properties["cn"][0].ToString();
                        var emailDomain = "";
                        if (res.Properties.Contains("mail"))
                        {
                            var mail = res.Properties["mail"][0].ToString();
                            emailDomain = mail.Substring(mail.IndexOf('@'));

                            var proxies = res.Properties["proxyAddresses"];

                            foreach (string proxy in proxies)
                            {
                                if (proxy.EndsWith(emailDomain)) //@utad.pt em vez de @intra.utad.pt
                                {
                                    var p = proxy.ToLowerInvariant();

                                    var startIndex = p.IndexOf(':') + 1;

                                    var tmp = p.Substring(startIndex);

                                    if (mail != tmp)
                                    {
                                        result.OriginalEmail = mail;
                                        result.AliasedEmail = tmp;
                                        result.HasAlias = true;

                                        return result;
                                    }
                                }
                            }
                        }
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    var newEx = new Exception(string.Format("Erro ao obter informação do alias para o user {0}", username), ex);
                    throw newEx;
                }

                return result;
            }
            public static bool CheckUsernameExists(string username)
            {
                try
                {
                    var ldapQuery = string.Format(ldapSrchAddr, username);
                    DirectoryEntry root = new DirectoryEntry(ldapQuery, ldapUser, ldapPassword, AuthenticationTypes.Secure);

                    DirectorySearcher searcher = new DirectorySearcher(root);
                    searcher.PropertiesToLoad.Add("cn");
                    searcher.PropertiesToLoad.Add("proxyAddresses");
                    searcher.PropertiesToLoad.Add("mail");

                    //searcher.Filter = tbFilter.Text;
                    //Abaixo é tudo já valores por defeito
                    searcher.SearchScope = SearchScope.Subtree;
                    searcher.SizeLimit = int.MaxValue;

                    var res = searcher.FindOne();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }

            public class AliasResult
            {
                public bool HasAlias { get; set; }
                public string OriginalEmail { get; set; }
                public string AliasedEmail { get; set; }
            }
        }

        #endregion
    }
}
