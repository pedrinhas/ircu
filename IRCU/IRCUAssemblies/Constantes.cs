﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAssemblies
{
    public static class Constantes
    {
        public static string FormatoDateTime { get { return "yyyy-MM-dd HH:mm:ss"; } }
        public static string FormatoDateTimeData { get { return "yyyy-MM-dd"; } }
    }
}
