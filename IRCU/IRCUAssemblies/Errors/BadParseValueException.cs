﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAssemblies.Errors
{
    public class BadParseValueException : Exception
    {
        public BadParseValueException() : base() { }
        public BadParseValueException(string message) : base(message) { }
        public BadParseValueException(string message, string expectedType) : base(message)
        {
            ExpectedType = expectedType;
        }
        public string ExpectedType { get; set; }
    }
}
