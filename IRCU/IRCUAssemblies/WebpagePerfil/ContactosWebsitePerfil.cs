﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAssemblies.WebpagePerfil
{
    [DataContract]
    public class ContactosWebsitePerfil
    {
        public ContactosWebsitePerfil()
        {
            Flags = new List<ContactosWebsiteFlag>();
        }
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public List<ContactosWebsiteFlag> Flags { get; set; }
        [DataMember]
        public string UnidadeOrganica { get; set; }
        [DataMember]
        public string Categoria { get; set; }
        [DataMember]
        public string CodigoTipo { get; set; }
        [DataMember]
        public string NomeTipo { get; set; }
    }
}
