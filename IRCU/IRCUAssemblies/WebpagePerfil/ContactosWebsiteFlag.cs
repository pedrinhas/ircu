﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAssemblies.WebpagePerfil
{
    [DataContract]
    public class ContactosWebsiteFlag
    {
        [DataMember]
        public string Key { get; set; }
        [DataMember]
        public string Value { get; set; }

        public static ContactosWebsiteFlag ToContactosWebsiteFlag(Utilizadores.UtilizadorFlag uFlag)
        {
            if (uFlag.Flag == null) return null;

            return new ContactosWebsiteFlag()
            {
                Key = uFlag.Flag.Key,
                Value = uFlag.Value
            };
        }
    }
}
