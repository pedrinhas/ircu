﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAssemblies.WebpagePerfil
{
    [DataContract]
    public class ContactosWebsiteUser
    {
        public ContactosWebsiteUser()
        {
            this.Perfil = new List<ContactosWebsitePerfil>();
            this.Flags = new List<ContactosWebsiteFlag>();
            this.Contactos = new List<Contactos.Contacto>();
        }

        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string NomeUtilizador { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string NomePersonalizado { get; set; }
        [DataMember]
        public List<ContactosWebsiteFlag> Flags { get; set; }
        [DataMember]
        public Utilizadores.Visibilidade Visibilidade { get; set; }
        [DataMember]
        public List<Contactos.Contacto> Contactos { get; set; }
        [DataMember]
        public List<ContactosWebsitePerfil> Perfil { get; set; }
    }
}
