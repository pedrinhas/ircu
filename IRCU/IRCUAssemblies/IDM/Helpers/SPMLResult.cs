﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAssemblies.IDM.Helpers
{
    [DataContract]
    public class SPMLResult
    {
        public SPMLResult()
        {
            Status = SPMLResultStatus.Success;
        }
        public SPMLResult(string err)
        {
            Status = SPMLResultStatus.Failure;

            handleError(err);
        }
        public SPMLResult(string errorMessage, string errorCode) : this(errorMessage)
        {
            ErrorCode = errorCode;
        }

        [DataMember]
        public SPMLResultStatus Status { get; set; }
        [DataMember]
        public string ErrorMessage { get; set; }
        [DataMember]
        public string ErrorCode { get; set; }

        private void handleError(string SPMLError)
        {
            string str = SPMLError;
            if (SPMLError.IndexOf("com.waveset.exception.ItemNotFound") != -1)
            {
                this.ErrorMessage = SPMLConstants.ERROR_SPMLUSERNOTEXIST;
                this.ErrorCode = SPMLConstants.ERROR_SPMLUSERNOTEXIST_COD;
            }
            else if (SPMLError.IndexOf("<dsml:value>[LDAP: error code 19 - password in history]</dsml:value>") != -1)
            {
                this.ErrorMessage = SPMLConstants.ERROR_SPMLPWDHISTORY;
                this.ErrorCode = SPMLConstants.ERROR_SPMLPWDHISTORY_COD;
            }
            else if (SPMLError.IndexOf("com.waveset.exception.PolicyViolation") != -1)
            {
                this.ErrorMessage = SPMLConstants.ERROR_SPMLPWDVIOLATION;
                this.ErrorCode = SPMLConstants.ERROR_SPMLPWDVIOLATION_COD;
            }
            else
            {
                this.ErrorMessage = SPMLConstants.ERROR_SPMLDEFAULT;
                this.ErrorCode = SPMLConstants.ERROR_SPMLDEFAULT_COD;
            }
        }


        public enum SPMLResultStatus
        {
            Failure,
            Success
        }

        public static SPMLResult HandleXMLResult(string xml)
        {
            if(string.IsNullOrWhiteSpace(xml))
            {
                return new SPMLResult()
                {
                    Status = SPMLResultStatus.Failure,
                    ErrorCode = "-1",
                    ErrorMessage = "[UTAD] XML devolvido pelo IDM vazio"
                };
            }

            var res = new SPMLResult();
            try
            {
                int num1 = xml.IndexOf("result='urn:oasis:names:tc:SPML:1:0#");
                if (!num1.Equals(-1))
                {
                    int num2 = xml.IndexOf("'", num1 + "result='urn:oasis:names:tc:SPML:1:0#".Length);
                    if (xml.Substring(num1 + "result='urn:oasis:names:tc:SPML:1:0#".Length, num2 - (num1 + "result='urn:oasis:names:tc:SPML:1:0#".Length)).Equals("success"))
                    {
                        res = new SPMLResult();
                        res.Status = SPMLResultStatus.Success;
                    }
                    else
                    {
                        int num3 = xml.IndexOf("<spml:errorMessage>");
                        if (!num3.Equals(-1))
                        {
                            int num4 = xml.IndexOf("</spml:errorMessage>");
                            var SPMLError = xml.Substring(num3 + "<spml:errorMessage>".Length, num4 - (num3 + "<spml:errorMessage>".Length));
                            res.handleError(SPMLError);
                        }
                    }
                }
                else
                {
                    res.Status = SPMLResultStatus.Failure;
                }
            }
            catch
            {
            }

            return res;
        }
    }
}
