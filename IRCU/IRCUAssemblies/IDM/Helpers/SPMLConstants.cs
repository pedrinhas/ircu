﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAssemblies.IDM.Helpers
{
    public static class SPMLConstants
    {
        public const string ERROR_SPMLDEFAULT_COD = "0000";
        public const string ERROR_SPMLDEFAULT = "Erro genérico";

        public const string ERROR_SPMLLOGIN_COD = "0001";
        public const string ERROR_SPMLLOGIN = "Erro no pedido do token de autenticação no IDM";

        public const string ERROR_SPMLLOGOUT_COD = "0002";
        public const string ERROR_SPMLLOGOUT = "Erro no pedido do destruição do token de autenticação no IDM";

        public const string ERROR_SPMLCHGUSERPWD_COD = "0003";
        public const string ERROR_SPMLCHGUSERPWD = "Erro na alteração da password da conta";

        public const string ERROR_SPMLRSTUSERPWD_COD = "0004";
        public const string ERROR_SPMLRSTUSERPWD = "Erro no reset da password da conta";

        public const string ERROR_SPMLUSERNOTEXIST_COD = "0005";
        public const string ERROR_SPMLUSERNOTEXIST = "Utilizador não existe";


        public const string ERROR_SPMLPWDHISTORY_COD = "0006";
        public const string ERROR_SPMLPWDHISTORY = "Password no histórico - Deve inserir uma password diferente das duas últimas";

        public const string ERROR_SPMLPWDVIOLATION_COD = "0007";
        public const string ERROR_SPMLPWDVIOLATION = "Password não permitida - A password não respeita os requisitos mínimos quanto aos caracteres escolhidos";

        public const string ERROR_SPMLCHGUSERDATA_COD = "0008";
        public const string ERROR_SPMLCHGUSERDATA = "Erro na alteração da conta";

        public const string ERROR_SPMLBADARG_COD = "0009";
        public const string ERROR_SPMLBADARG = "Argumentos inválidos";

        public const string ERROR_SPMLCHANGEPWD_BADAUTH_COD = "0010";
        public const string ERROR_SPMLCHANGEPWD_BADAUTH = "Autenticação inválida";
    }
}
