﻿using IRCUAssemblies.LDAP;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace IRCUAssemblies.IDM.Helpers
{
    public class SPMLOperations
    {
        private static bool initd = false;
        private static string idm_url, idm_user, idm_password, idm_token;

        public static void Initialize(string url, string user, string pw)
        {
            if (!initd)
            {
                idm_url = url;
                idm_user = user;
                idm_password = pw;

                try
                {
                    idm_token = IDMLogin();
                }
                catch (Exception ex)
                {

                    throw;
                }

                initd = true;
            }
        }
        private static HttpWebRequest CreateRequest(string innerText)
        {
            var req = (HttpWebRequest)WebRequest.Create(idm_url);
            req.Method = "POST";
            req.ContentType = "text/xml";
            using(var writer = new StreamWriter(req.GetRequestStream()))
            {
                writer.Write(innerText);
            }

            return req;
        }

        private static string IDMLogin()
        {
            var xmlRequest = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body><spml:extendedRequest xmlns:dsml=\"urn:oasis:names:tc:DSML:2:0:core\" xmlns:spml=\"urn:oasis:names:tc:SPML:1:0\"><spml:operationIdentifier operationIDType=\"urn:oasis:names:tc:SPML:1:0#GenericString\"><spml:operationID>login</spml:operationID></spml:operationIdentifier><spml:attributes><dsml:attr name=\"accountId\"><dsml:value>{0}</dsml:value></dsml:attr><dsml:attr name=\"password\"><dsml:value>{1}</dsml:value></dsml:attr></spml:attributes></spml:extendedRequest></s:Body></s:Envelope>";
            xmlRequest = string.Format(xmlRequest, idm_user, idm_password);

            var req = CreateRequest(xmlRequest);
            var res = (HttpWebResponse)req.GetResponse();

            if(res.StatusCode == HttpStatusCode.OK)
            {
                var xmlResponse = "";
                using(var reader = new StreamReader(res.GetResponseStream()))
                {
                    xmlResponse = reader.ReadToEnd();
                }

                if(!string.IsNullOrWhiteSpace(xmlRequest))
                {
                    var doc = new XmlDocument();
                    doc.LoadXml(xmlResponse);

                    var extendedResponse = doc.GetElementsByTagName("spml:extendedResponse")[0];
                    var loggedIn = extendedResponse.Attributes["result"].InnerText.ToLowerInvariant().Contains("success");

                    if(loggedIn)
                    {
                        return extendedResponse.FirstChild.FirstChild.FirstChild.InnerText;
                    }
                }
            }

            return null;
        }
        private static SPMLResult IDMLogout()
        {
            var result = new SPMLResult();

            var xmlRequest = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><spml:extendedRequest xmlns:dsml=\"urn:oasis:names:tc:DSML:2:0:core\" xmlns:spml=\"urn:oasis:names:tc:SPML:1:0\"><spml:operationalAttributes><dsml:attr name=\"session\"></dsml:value></dsml:attr></spml:operationalAttributes><spml:operationIdentifier operationIDType=\"urn:oasis:names:tc:SPML:1:0#GenericString\"></spml:operationID></spml:operationIdentifier><spml:attributes><dsml:attr name=\"accountId\"></dsml:value></dsml:attr><dsml:attr name=\"password\"></dsml:value></dsml:attr></spml:attributes></spml:extendedRequest></soap:Body></soap:Envelope>";
            xmlRequest = string.Format(xmlRequest, idm_token, idm_user, idm_password);

            var req = CreateRequest(xmlRequest);
            var res = (HttpWebResponse)req.GetResponse();

            if(res.StatusCode == HttpStatusCode.OK)
            {
                using(var reader = new StreamReader(res.GetResponseStream()))
                {
                    if (res.StatusCode == HttpStatusCode.OK)
                    {
                        var xmlResponse = reader.ReadToEnd();

                        result = SPMLResult.HandleXMLResult(xmlResponse);
                    }
                    else
                    {
                        throw new WebException(string.Format("Ocorreu um erro ao aceder ao IDM. Status code {0}", res.StatusCode), null, WebExceptionStatus.UnknownError, res);
                    }
                }
            }

            return result;
        }

        public static UtilizadorIDM SearchUser(string username)
        {
            if (!initd) throw new InvalidOperationException("O RequestCreator não foi inicializado.");

            UtilizadorIDM user = null;

            try
            {
                string xmlRequest = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soap:Body><spml:searchRequest requestID=\"searchRequest\" xmlns:dsml=\"urn:oasis:names:tc:DSML:2:0:core\" xmlns:spml=\"urn:oasis:names:tc:SPML:1:0\"><spml:operationalAttributes><dsml:attr name=\"session\"><dsml:value>{0}</dsml:value></dsml:attr></spml:operationalAttributes><dsml:filter><dsml:equalityMatch name=\"accountId\"><dsml:value>{1}</dsml:value></dsml:equalityMatch></dsml:filter></spml:searchRequest></soap:Body></soap:Envelope>";

                xmlRequest = string.Format(xmlRequest, idm_token, username);

                var req = CreateRequest(xmlRequest);
                var res = (HttpWebResponse)req.GetResponse();

                using(var reader = new StreamReader(res.GetResponseStream()))
                {
                    if(res.StatusCode != HttpStatusCode.OK)
                    {
                        throw new WebException(string.Format("Ocorreu um erro ao aceder ao IDM. Status code {0}", res.StatusCode), null, WebExceptionStatus.UnknownError, res);
                    }

                    var content = reader.ReadToEnd();

                    user = UtilizadorIDM.FromXML(content);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return user;
        }
        public static SPMLResult ModifyPassword(string username, string oldPassword, string newPassword)
        {
            if (!initd) throw new InvalidOperationException("O RequestCreate não foi inicializado.");

            SPMLResult result = null;

            try
            {
                if(!LDAPTools.Authenticate(username, oldPassword))
                {
                    return new SPMLResult(SPMLConstants.ERROR_SPMLCHANGEPWD_BADAUTH, SPMLConstants.ERROR_SPMLCHANGEPWD_BADAUTH_COD);
                }
                if (!ValidatePassword(newPassword))
                {
                    return new SPMLResult(SPMLConstants.ERROR_SPMLPWDVIOLATION, SPMLConstants.ERROR_SPMLPWDVIOLATION_COD);
                }
                if(string.IsNullOrWhiteSpace(username))
                {
                    return new SPMLResult(SPMLConstants.ERROR_SPMLBADARG, SPMLConstants.ERROR_SPMLBADARG_COD);
                }

                string xmlRequest = "<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'><soap:Body><spml:extendedRequest xmlns:dsml='urn:oasis:names:tc:DSML:2:0:core' xmlns:spml='urn:oasis:names:tc:SPML:1:0'><spml:operationalAttributes><dsml:attr name='session'><dsml:value>{0}</dsml:value></dsml:attr></spml:operationalAttributes><spml:identifier type='urn:oasis:names:tc:SPML:1:0#UserIDAndOrDomainName'><spml:id>account</spml:id></spml:identifier><spml:operationIdentifier operationIDType='urn:oasis:names:tc:SPML:1:0#GenericString'><spml:operationID>changeUserPassword</spml:operationID></spml:operationIdentifier><spml:attributes><dsml:attr name='accountId'><dsml:value>{1}</dsml:value></dsml:attr><dsml:attr name='password'><dsml:value>{2}</dsml:value></dsml:attr></spml:attributes></spml:extendedRequest></soap:Body></soap:Envelope>";

                xmlRequest = string.Format(xmlRequest, idm_token, username, newPassword);

                var req = CreateRequest(xmlRequest);
                var res = (HttpWebResponse)req.GetResponse();

                using (var reader = new StreamReader(res.GetResponseStream()))
                {
                    if(res.StatusCode == HttpStatusCode.OK)
                    {
                        var xmlResponse = reader.ReadToEnd();

                        result = SPMLResult.HandleXMLResult(xmlResponse);
                    }
                    else
                    {
                        throw new WebException(string.Format("Ocorreu um erro ao aceder ao IDM. Status code {0}", res.StatusCode), null, WebExceptionStatus.UnknownError, res);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Ocorreu um erro a modificar a password do utilizador {0}", username), ex);
            }

            return result;
        }
        public static SPMLResult ModifyProperties(string username, Dictionary<string, string> properties)
        {
            if (!initd) throw new InvalidOperationException("O RequestCreate não foi inicializado.");

            SPMLResult result = null;

            try
            {
                if (string.IsNullOrWhiteSpace(username))
                {
                    return new SPMLResult(SPMLConstants.ERROR_SPMLBADARG, SPMLConstants.ERROR_SPMLBADARG_COD);
                }

                string defaultInnerRequest = "<dsml:modification name=\"{0}\" operation=\"replace\"><dsml:value>{1}</dsml:value></dsml:modification>";
                string innerRequest = "";

                foreach (var p in properties.Where(x => !string.IsNullOrWhiteSpace(x.Key))) //não deve ser preciso, mas é melhor fazer assim
                {
                    innerRequest = innerRequest + string.Format(defaultInnerRequest, p.Key, p.Value);
                }

                string xmlRequest = string.Format("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soap:Body><spml:modifyRequest requestID=\"modifyRequest\" xmlns:dsml=\"urn:oasis:names:tc:DSML:2:0:core\" xmlns:spml=\"urn:oasis:names:tc:SPML:1:0\"><spml:operationalAttributes><dsml:attr name=\"session\"><dsml:value>{0}</dsml:value></dsml:attr></spml:operationalAttributes><spml:identifier type=\"urn:oasis:names:tc:SPML:1:0#UserIDAndOrDomainName\"><spml:id>{1}</spml:id></spml:identifier><spml:modifications>{2}</spml:modifications></spml:modifyRequest></soap:Body></soap:Envelope>", idm_token, username, innerRequest);

                var req = CreateRequest(xmlRequest);
                var res = (HttpWebResponse)req.GetResponse();

                using (var reader = new StreamReader(res.GetResponseStream()))
                {
                    if (res.StatusCode == HttpStatusCode.OK)
                    {
                        var xmlResponse = reader.ReadToEnd();

                        result = SPMLResult.HandleXMLResult(xmlResponse);
                    }
                    else
                    {
                        throw new WebException(string.Format("Ocorreu um erro ao aceder ao IDM. Status code {0}", res.StatusCode), null, WebExceptionStatus.UnknownError, res);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Ocorreu um erro a modificar as propriedades do utilizador {0}", username), ex);
            }

            return result;
        }

        static bool ValidatePassword(string password)
        {
            int minLength = 9;
            int maxLength = 100;
            char[] allowedSpecial = new char[] { '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '|', '`', '-', '=', '\\', '{', '}', '[', ']', ':', '"', ';', '\'', '<', '>', '?', ',', '.', '/' };
            int minScore = 3;

            if (password == null) throw new ArgumentNullException();

            bool meetsLengthRequirements = password.Length >= minLength && password.Length <= maxLength;
            bool hasUpperCaseLetter = false;
            bool hasLowerCaseLetter = false;
            bool hasDecimalDigit = false;
            bool hasSpecial = false;

            if (meetsLengthRequirements)
            {
                foreach (char c in password)
                {
                    if (!hasUpperCaseLetter && char.IsUpper(c)) hasUpperCaseLetter = true;
                    else if (!hasLowerCaseLetter && char.IsLower(c)) hasLowerCaseLetter = true;
                    else if (!hasDecimalDigit && char.IsDigit(c)) hasDecimalDigit = true;
                    else if (!hasSpecial && allowedSpecial.Contains(c)) hasSpecial = true;
                }
            }

            int pwdScore = (hasUpperCaseLetter ? 1 : 0) + (hasLowerCaseLetter ? 1 : 0) + (hasDecimalDigit ? 1 : 0) + (hasSpecial ? 1 : 0);

            return pwdScore >= minScore;
        }

        public static string GetDisplayName(string username)
        {
            var xmlRequest = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soap:Body><spml:searchRequest requestID=\"searchRequest\" xmlns:dsml=\"urn:oasis:names:tc:DSML:2:0:core\" xmlns:spml=\"urn:oasis:names:tc:SPML:1:0\"><spml:operationalAttributes><dsml:attr name=\"session\"><dsml:value>{0}</dsml:value></dsml:attr></spml:operationalAttributes><spml:identifier type=\"urn:oasis:names:tc:SPML:1:0#UserIDAndOrDomainName\"><spml:id>{1}</spml:id></spml:identifier><spml:attributes><dsml:attr name='staff_v_displayname'></dsml:attr></spml:attributes></spml:searchRequest></soap:Body></soap:Envelope>";

            xmlRequest = string.Format(xmlRequest, idm_token, username);

            var req = CreateRequest(xmlRequest);
            var res = (HttpWebResponse)req.GetResponse();

            if (res.StatusCode == HttpStatusCode.OK)
            {
                var xmlResponse = "";
                using (var reader = new StreamReader(res.GetResponseStream()))
                {
                    xmlResponse = reader.ReadToEnd();
                }

                if (!string.IsNullOrWhiteSpace(xmlRequest))
                {
                    var doc = new XmlDocument();
                    doc.LoadXml(xmlResponse);

                    var attr = doc.GetElementsByTagName("spml:attributes");

                    if(attr.Count > 0) //exists
                    {
                        var dnAttr = attr[0].FirstChild;
                        if(dnAttr.FirstChild != null)
                        {
                            var dnValue = dnAttr.FirstChild;
                            if(dnValue!= null)
                            {
                                return dnValue.InnerText;
                            }
                        }
                    }
                }
            }

            return null;
        }

        #region old
        ///// <summary>
        ///// Tenta fazer um login semelhante ao da conta de sistema. É para mudar quanto houver melhor documentação
        ///// </summary>
        ///// <param name="username">Nome de utilizador</param>
        ///// <param name="password">Palavra passe</param>
        ///// <returns>Login bem sucedido ou não</returns>
        //[Obsolete("Este método deve ser revisto quando se encontrar documentação para o login no Sun Identity Manager 6.0 com SPML 1.0")]
        //public static bool LoginSucata(string username, string password)
        //{
        //    //wrong acct
        //    //Invalid Account ID

        //    //wrong pwd
        //    //

        //    var xmlRequest = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body><spml:extendedRequest xmlns:dsml=\"urn:oasis:names:tc:DSML:2:0:core\" xmlns:spml=\"urn:oasis:names:tc:SPML:1:0\"><spml:operationIdentifier operationIDType=\"urn:oasis:names:tc:SPML:1:0#GenericString\"><spml:operationID>login</spml:operationID></spml:operationIdentifier><spml:attributes><dsml:attr name=\"accountId\"><dsml:value>{0}</dsml:value></dsml:attr><dsml:attr name=\"password\"><dsml:value>{1}</dsml:value></dsml:attr></spml:attributes></spml:extendedRequest></s:Body></s:Envelope>";
        //    xmlRequest = string.Format(xmlRequest, username, password);

        //    var req = CreateRequest(xmlRequest);
        //    var res = (HttpWebResponse)req.GetResponse();

        //    if (res.StatusCode == HttpStatusCode.OK)
        //    {
        //        var xmlResponse = "";
        //        using (var reader = new StreamReader(res.GetResponseStream()))
        //        {
        //            xmlResponse = reader.ReadToEnd();
        //        }

        //        if (!string.IsNullOrWhiteSpace(xmlRequest))
        //        {
        //            var doc = new XmlDocument();
        //            doc.LoadXml(xmlResponse);

        //            var extendedResponse = doc.GetElementsByTagName("spml:extendedResponse")[0].FirstChild;

        //            if (loggedIn)
        //            {
        //                return extendedResponse.FirstChild.FirstChild.FirstChild.InnerText;
        //            }
        //        }
        //    }

        //    return null;
        //}
        #endregion
    }
}
