﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace IRCUAssemblies.IDM
{
    [DataContract]
    public class UtilizadorIDM
    {
        [DataMember]
        public string Identifier { get; set; }
        [DataMember]
        public bool LHLocked { get; set; }
        [DataMember]
        public int DIS { get; set; }
        [DataMember]
        public string MemberObjectGroupsIds { get; set; }
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public List<string> Res { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public bool LHDIS { get; set; }
        [DataMember]
        public int XMLSize { get; set; }
        [DataMember]
        public bool HasCapabilities { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string MemberObjectGroups { get; set; }
        [DataMember]
        public string Role { get; set; }
        [DataMember]
        public int Prov { get; set; }

        public static UtilizadorIDM FromXML(string xml)
        {
            UtilizadorIDM res = null;

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                var body = doc.GetElementsByTagName("SOAP-ENV:Body").Item(0);
                if (body != null)
                {
                    var searchResponse = body["spml:searchResponse"];
                    var searchResult = searchResponse["spml:searchResultEntry"];
                    if (searchResult != null)
                    {
                        res = new UtilizadorIDM();
                        res.Identifier = searchResult["spml:identifier"]["spml:id"].InnerText;
                        foreach (XmlNode attribute in searchResult["spml:attributes"].ChildNodes)
                        {
                            switch (attribute.Attributes["name"].InnerText.ToLowerInvariant())
                            {
                                case "lhlocked":
                                    res.LHLocked = Convert.ToBoolean(attribute["dsml:value"].InnerText);
                                    break;
                                case "dis":
                                    res.DIS = Convert.ToInt32(attribute["dsml:value"].InnerText);
                                    break;
                                case "MemberObjectGroupsIds":
                                    res.MemberObjectGroupsIds = attribute["dsml:value"].InnerText;
                                    break;
                                case "id":
                                    res.id = attribute["dsml:value"].InnerText;
                                    break;
                                case "res":
                                    var l = new List<string>();
                                    foreach (XmlNode r in attribute.ChildNodes)
                                    {
                                        l.Add(r.InnerText);
                                    }
                                    res.Res = l;
                                    break;
                                case "lastname":
                                    res.LastName = attribute["dsml:value"].InnerText;
                                    break;
                                case "lhdis":
                                    res.LHDIS = Convert.ToBoolean(attribute["dsml:value"].InnerText);
                                    break;
                                case "xmlSize":
                                    res.XMLSize = Convert.ToInt32(attribute["dsml:value"].InnerText);
                                    break;
                                case "hasCapabilities":
                                    res.HasCapabilities = Convert.ToBoolean(attribute["dsml:value"].InnerText);
                                    break;
                                case "firstname":
                                    res.FirstName = attribute["dsml:value"].InnerText;
                                    break;
                                case "MemberObjectGroups":
                                    res.MemberObjectGroups = attribute["dsml:value"].InnerText;
                                    break;
                                case "role":
                                    res.Role = attribute["dsml:value"].InnerText;
                                    break;
                                case "prov":
                                    res.Prov = Convert.ToInt32(attribute["dsml:value"].InnerText);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return res;
        }
    }
}
