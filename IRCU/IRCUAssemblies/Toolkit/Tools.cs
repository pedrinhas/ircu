﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace IRCUAssemblies.Toolkit
{
    public static class Tools
    {
        public static class Email
        {
            public static bool SendEmail(string stmp, string from, List<string> to, string subject, string body, List<string> cc = null, List<string> bcc = null)
            {
                try
                {
                    MailMessage mail = new MailMessage(from, to.First());
                    foreach (var toAddress in to.Where(x => x != to.First()))
                    {
                        mail.To.Add(toAddress);
                    }
                    foreach (var ccAddress in cc)
                    {
                        mail.CC.Add(ccAddress);
                    }
                    foreach (var bccAddress in bcc)
                    {
                        mail.Bcc.Add(bccAddress);
                    }
                    SmtpClient client = new SmtpClient();
                    client.Port = 25;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Host = stmp;
                    mail.Subject = subject;
                    mail.Body = body;
                    client.Send(mail);
                }
                catch (Exception ex)
                {
                    return false;
                }

                return true;
            }
        }
        [Obsolete("Usar as extensões", false)]
        public static class DB
        {
            public static string GetStringFromReader(System.Data.SqlClient.SqlDataReader r, int index)
            {
                return GetStringFromReader(r, r.GetName(index));
            }
            public static string GetStringFromReader(System.Data.SqlClient.SqlDataReader r, string colName)
            {
                return r.GetString(colName);
            }
            public static bool IsNull(System.Data.SqlClient.SqlDataReader r, string colName)
            {
                return r.IsDBNull(colName);
            }
        }
    }

    public static class Extensions
    {
        public static string GetString(this IDataRecord reader, string colName)
        {
            var idCol = reader.GetColumnID(colName);

            if (idCol > -1 && !reader.IsDBNull(idCol)) return reader.GetString(idCol);
            return null;
        }

        public static DateTime? GetDateTime(this IDataRecord reader, string colName)
        {
            var idCol = reader.GetColumnID(colName);

            if (idCol > -1 && !reader.IsDBNull(idCol)) return new DateTime?(reader.GetDateTime(idCol));
            return null;
        }

        public static int? GetInt32(this IDataRecord reader, string colName)
        {
            var idCol = reader.GetColumnID(colName);

            if (idCol > -1 && !reader.IsDBNull(idCol)) return reader.GetInt32(idCol);
            return null;
        }

        public static bool? GetBoolean(this IDataRecord reader, string colName)
        {
            var idCol = reader.GetColumnID(colName);

            if (idCol > -1 && !reader.IsDBNull(idCol)) return reader.GetBoolean(idCol);
            return null;
        }

        public static Guid? GetGuid(this IDataRecord reader, string colName)
        {
            var idCol = reader.GetColumnID(colName);

            if (idCol > -1 && !reader.IsDBNull(idCol)) return reader.GetGuid(idCol);
            return null;
        }

        public static bool HasColumn(this IDataRecord dr, string columnName)
        {
            return dr.GetColumnID(columnName) >= 0;
        }

        public static int GetColumnID(this IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return i;
            }
            return -1;
        }
        public static bool IsDBNull(this IDataRecord dr, string columnName)
        {
            var idCol = dr.GetColumnID(columnName);

            if (idCol > -1) return dr.IsDBNull(idCol);
            return true;
        }

        public static object[] GetAllValues(this IDataRecord dr)
        {
            var all = new object[dr.FieldCount];
            dr.GetValues(all);

            return all;
        }

        public static string FirstCharToUpper(this string input)
        {
            try
            {
                var res = "";
                var separators = new char[] { ' ', '-', '\'' };

                if (input == null) throw new ArgumentNullException("input");
                else if (input == "") throw new ArgumentException("input cannot be empty");
                else
                {
                    var separatorsInput = input.Where(x => separators.Contains(x)).ToArray();
                    var splitInput = input.Split(separators);

                    for (int i = 0; i < splitInput.Length; i++)
                    {
                        var thisName = splitInput[i];
                        var singleName = string.IsNullOrWhiteSpace(thisName) ? "" : thisName.Substring(0, 1).ToUpper() + thisName.Substring(1).ToLower();

                        res += singleName + (i < separatorsInput.Count() ? separatorsInput[i].ToString() : "");
                    }
                }

                return res.Trim();

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}