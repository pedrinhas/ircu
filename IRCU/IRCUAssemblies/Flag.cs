﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IRCUAssemblies.Utilizadores;
using System.Runtime.Serialization;

namespace IRCUAssemblies
{
    public class Flag : UtilizadoresObject
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Key { get; set; }
        [DataMember]
        public string Descricao { get; set; }

        public FlagUpdate ToFlagUpdate(string utilizadorEdicao)
        {
            return new FlagUpdate()
            {
                ID = this.ID,
                Key = this.Key,
                Descricao = this.Descricao,
                UtilizadorEdicao = utilizadorEdicao
            };
        }

        [DataContract]
        public class FlagUpdate
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public string Key { get; set; }
            [DataMember]
            public string Descricao { get; set; }
            [DataMember]
            public string UtilizadorEdicao { get; set; }
        }
    }
}
