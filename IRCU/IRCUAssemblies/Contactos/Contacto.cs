﻿using IRCUAssemblies.Utilizadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAssemblies.Contactos
{
    [DataContract]
    public class Contacto : IRCUAssemblies.Utilizadores.UtilizadoresObject
    {
        public Contacto()
        {
            Visibilidade = new Visibilidade() { ID = 3 }; //PUBLIC
        }
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int? PropertyID { get; set; }
        [DataMember]
        public int IDUtilizador { get; set; }
        [DataMember]
        public Guid IUPI { get; set; }
        [DataMember]
        public string Valor { get; set; }
        [DataMember]
        public Visibilidade Visibilidade { get; set; }
        [DataMember]
        public TipoContacto Tipo { get; set; }

        public override string ToString()
        {
            return string.Format("[{0} - {1}] {2}, {3}", ID, IDUtilizador, Tipo.Nome, Valor);
        }

        public ContactoUpdate ToContactoUpdate(string utilizadorEdicao)
        {
            return new ContactoUpdate()
            {
                ID = this.ID,
                PropertyID = this.PropertyID,
                IUPI = this.IUPI,
                IDTipo = this.Tipo.ID,
                Valor = this.Valor,
                Visibilidade = this.Visibilidade.ID,
                Ativo = this.Ativo,
                UtilizadorEdicao = utilizadorEdicao
            };
        }

        [DataContract]
        public class TipoContacto : IRCUAssemblies.Utilizadores.UtilizadoresObject
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public string Nome { get; set; }
            [DataMember]
            public string tmp_oldText { get; set; }
            [DataMember]
            public GrupoContacto Grupo { get; set; }

            public override string ToString()
            {
                return string.Format("[{0}] {1}, {2}", ID, Nome, Grupo.Nome);
            }
        }
        [DataContract]
        public class GrupoContacto : IRCUAssemblies.Utilizadores.UtilizadoresObject
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public string Nome { get; set; }

            public override string ToString()
            {
                return string.Format("[{0}] {1}", ID, Nome);
            }
        }

        [DataContract]
        public class ContactoUpdate
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public int? PropertyID { get; set; }
            [DataMember]
            public Guid IUPI { get; set; }
            [DataMember]
            public int IDTipo { get; set; }
            [DataMember]
            public string Valor { get; set; }
            [DataMember]
            public int Visibilidade { get; set; }
            [DataMember]
            public bool Ativo { get; set; }
            [DataMember]
            public string UtilizadorEdicao { get; set; }
        }
    }
}
