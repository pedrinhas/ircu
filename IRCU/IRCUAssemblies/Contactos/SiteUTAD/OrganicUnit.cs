﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IRCUAssemblies.Contactos.SiteUTAD
{
    [DataContract]
    public class OrganicUnit
    {
        public OrganicUnit()
        {
            Category = new List<Category>();
        }

        [DataMember]
        public string Name { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public List<Category> Category { get; set; }
    }
}