﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IRCUAssemblies.Contactos.SiteUTAD
{
    public class RawFromDB
    {
        public int id { get; set; }
        public int? idPerfil { get; set; }
        public string nomedeutilizador { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        public string uo { get; set; }
        public string categoria { get; set; }
        public string contactgroup { get; set; }
        public string contactlabel { get; set; }
        public string contactvalue { get; set; }
        public int visibilidadeContacto { get; set; }
        public string visibilidadeContactoNome { get; set; }
        public int visibilidadeUtilizador { get; set; }
        public string visibilidadeUtilizadorNome { get; set; }
        public int? visibilidadePerfil { get; set; }
        public string visibilidadePerfilNome { get; set; }
    }
}