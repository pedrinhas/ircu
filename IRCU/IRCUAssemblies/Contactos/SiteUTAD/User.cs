﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IRCUAssemblies.Contactos.SiteUTAD
{
    public class User
    {
        public User()
        {
            Profiles = new List<Profile>();
            ContactGroups = new List<ContactGroup>();
            Flags = new List<SiteUTADFlag>();
        }
        public string Username { get; set; }
        public string Name { get; set; }
        public int Visibility { get; set; }
        public List<SiteUTADFlag> Flags { get; set; }
        public List<Profile> Profiles { get; set; }
        public List<ContactGroup> ContactGroups { get; set; }
        [JsonIgnore]
        public int ID { get; set; }
    }
}