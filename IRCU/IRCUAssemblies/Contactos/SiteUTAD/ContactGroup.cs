﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IRCUAssemblies.Contactos.SiteUTAD
{
    public class ContactGroup
    {
        public ContactGroup()
        {
            Contacts = new List<Contact>();
        }
        public string Name { get; set; }
        public List<Contact> Contacts { get; set; }
    }
}