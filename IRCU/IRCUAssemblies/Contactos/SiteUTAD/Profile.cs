﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IRCUAssemblies.Contactos.SiteUTAD
{
    public class Profile
    {
        public Profile()
        {
            Flags = new List<SiteUTADFlag>();
        }
        public List<SiteUTADFlag> Flags { get; set; }
        public OrganicUnit OU { get; set; }
        [JsonIgnore]
        public int ID { get; set; }
    }
}
