﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IRCUAssemblies.Contactos.SiteUTAD
{
    public class Contact
    {
        public string Label { get; set; }
        public string Value { get; set; }
        public int Visibility { get; set; }
    }
}