﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IRCUAssemblies.Contactos.SiteUTAD
{
    [DataContract]
    public class Category
    {
        [DataMember]
        public string Name { get; set; }
    }
}