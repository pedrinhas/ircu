﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IRCUAssemblies.Utilizadores
{
    [DataContract]
    public class Carreira : UtilizadoresObject
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Codigo { get; set; }
        [DataMember]
        public string Nome { get; set; }
        [DataMember]
        public string Descricao { get; set; }
        [DataMember]
        public GrupoCarreira Grupo { get; set; }

        public override string ToString()
        {
            return string.Format("[{0} - {1}] {2}, {3}", ID, Codigo, Nome, Descricao);
        }

        public CarreiraUpdate ToCarreiraUpdate(string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            int? idGrupo = null;
            if (Grupo != null) idGrupo = Grupo.ID;
            return new CarreiraUpdate()
            {
                ID = this.ID,
                Codigo = this.Codigo,
                Nome = this.Nome,
                Descricao = this.Descricao,
                IDGrupo = idGrupo,
                Ativo = this.Ativo,
                UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, this.UtilizadorModificado) : utilizadorEdicao
            };
        }

        [DataContract]
        public class CarreiraUpdate
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public string Codigo { get; set; }
            [DataMember]
            public string Nome { get; set; }
            [DataMember]
            public string Descricao { get; set; }
            [DataMember]
            public int? IDGrupo { get; set; }
            [DataMember]
            public bool Ativo { get; set; }
            [DataMember]
            public string UtilizadorEdicao { get; set; }
        }

        [DataContract]
        public class GrupoCarreira : UtilizadoresObject
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public string Codigo { get; set; }
            [DataMember]
            public string Nome { get; set; }
            [DataMember]
            public string Descricao { get; set; }

            public override string ToString()
            {
                return string.Format("[{0} - {1}] {2}, {3}", ID, Codigo, Nome, Descricao);
            }

            public GrupoCarreiraUpdate ToGrupoCarreiraUpdate(string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
            {
                return new GrupoCarreiraUpdate()
                {
                    ID = this.ID,
                    Codigo = this.Codigo,
                    Nome = this.Nome,
                    Descricao = this.Descricao,
                    Ativo = this.Ativo,
                    UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, this.UtilizadorModificado) : utilizadorEdicao
                };
            }

            public class GrupoCarreiraUpdate
            {
                [DataMember]
                public int ID { get; set; }
                [DataMember]
                public string Codigo { get; set; }
                [DataMember]
                public string Nome { get; set; }
                [DataMember]
                public string Descricao { get; set; }
                [DataMember]
                public bool Ativo { get; set; }
                [DataMember]
                public string UtilizadorEdicao { get; set; }
            }
        }
    }
}