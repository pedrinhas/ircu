﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IRCUAssemblies.Utilizadores
{
    [DataContract]
    public class Regime : UtilizadoresObject
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Codigo { get; set; }
        [DataMember]
        public string Nome { get; set; }
        [DataMember]
        public string Descricao { get; set; }

        public override string ToString()
        {
            return string.Format("[{0} - {1}] {2}, {3}", ID, Codigo, Nome, Descricao);
        }

        public RegimeUpdate ToRegimeUpdate(string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            return new RegimeUpdate()
            {
                ID = this.ID,
                Codigo = this.Codigo,
                Nome = this.Nome,
                Descricao = this.Descricao,
                Ativo = this.Ativo,
                UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, this.UtilizadorModificado) : utilizadorEdicao
            };
        }

        [DataContract]
        public class RegimeUpdate
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public string Codigo { get; set; }
            [DataMember]
            public string Nome { get; set; }
            [DataMember]
            public string Descricao { get; set; }
            [DataMember]
            public bool Ativo { get; set; }
            [DataMember]
            public string UtilizadorEdicao { get; set; }
        }
    }
}