﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IRCUAssemblies.Utilizadores
{
    [DataContract]
    public class Utilizador : UtilizadoresObject
    {
        public Utilizador()
        {
            Perfil = new List<Perfil>();
            Visibilidade = new Visibilidade() { ID = 3 };
        }
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public Nullable<Guid> IUPI { get; set; }
        [DataMember]
        public string Nome { get; set; }
        [DataMember]
        public string NomePersonalizado { get; set; }
        [DataMember]
        public string NIF { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string AliasedEmail { get; set; }
        [DataMember]
        public string OriginalEmail { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public bool ContaNoGIAF { get; set; }
        [DataMember]
        public Visibilidade Visibilidade { get; set; }
        [DataMember]
        public List<UtilizadorFlag> Flags { get; set; }
        [DataMember]
        public List<Perfil> Perfil { get; set; }

        /// <summary>
        /// Não existe na BD. Calculado a partir dos valores da propriedade EmFuncoes dos perfis
        /// </summary>
        public bool EmFuncoes
        {
            get 
            {
                return Perfil != null && Perfil.Select(p => p.EmFuncoes).Contains(true);
            }
        }

        #region morada

        [DataMember]
        public string Morada { get; set; }
        [DataMember]
        public string CodPostal { get; set; }
        [DataMember]
        public string Localidade { get; set; }
        [DataMember]
        public string NumTelemovel { get; set; }
        [DataMember]
        public string NumTelefoneFixo { get; set; }

        #endregion

        public override string ToString()
        {
            return string.Format("[{0}] {1}, {2}", NIF, Nome, Email);
        }

        public UtilizadorUpdate ToUtilizadorUpdate(string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            return new UtilizadorUpdate()
            {
                ID = this.ID,
                IUPI = this.IUPI,
                Nome = this.Nome,
                NomePersonalizado = this.NomePersonalizado,
                NIF = this.NIF,
                Username = this.Username,
                OriginalEmail = this.OriginalEmail,
                AliasedEmail = this.AliasedEmail,
                ContaNoGIAF = this.ContaNoGIAF,
                Visibilidade = (this.Visibilidade ?? new Visibilidade() { ID = 3 }).ID,
                Email = this.Email,
                IDPerfil = this.Perfil.Select(x => x.ID),
                Ativo = this.Ativo,
                UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, this.UtilizadorModificado) : utilizadorEdicao
            };
        }

        [DataContract]
        public class UtilizadorUpdate
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public Nullable<Guid> IUPI { get; set; }
            [DataMember]
            public string Nome { get; set; }
            [DataMember]
            public string NomePersonalizado { get; set; }
            [DataMember]
            public string NIF { get; set; }
            [DataMember]
            public string Username { get; set; }
            [DataMember]
            public string OriginalEmail { get; set; }
            [DataMember]
            public string AliasedEmail { get; set; }
            [DataMember]
            public string Email { get; set; }
            [DataMember]
            public bool ContaNoGIAF { get; set; }
            [DataMember]
            public int Visibilidade { get; set; }
            [DataMember]
            public IEnumerable<int> IDPerfil { get; set; }
            [DataMember]
            public bool Ativo { get; set; }
            [DataMember]
            public string UtilizadorEdicao { get; set; }
        }
    }
}