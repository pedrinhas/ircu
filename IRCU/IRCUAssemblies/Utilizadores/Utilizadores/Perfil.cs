﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IRCUAssemblies.Utilizadores
{
    [DataContract]
    public class Perfil : UtilizadoresObject
    {
        [DataMember]
        public int ID { get; set; }
        /// <summary>
        /// Para o método de updade/insert
        /// </summary>
        [DataMember]
        public Nullable<Guid> IUPI { get; set; }
        [DataMember]
        public TipoPerfil Tipo { get; set; }
        [DataMember]
        public List<Cargo> Cargos { get; set; }
        [DataMember]
        public Categoria Categoria { get; set; }
        [DataMember]
        public Entidade Entidade { get; set; }
        [DataMember]
        public Carreira Carreira { get; set; }
        [DataMember]
        public Regime Regime { get; set; }
        [DataMember]
        public Situacao Situacao { get; set; }
        [DataMember]
        public List<UtilizadorFlag> Flags { get; set; }
        [DataMember]
        public Visibilidade Visibilidade { get; set; }
        [DataMember]
        public string NumeroMecanografico { get; set; }
        [DataMember]
        public string Email { get; set; }
        public DateTime? DataEntradaObject { get; set; }
        [DataMember]
        public string DataEntrada
        {
            get
            {
                if (DataEntradaObject != null) return DataEntradaObject.Value.ToString(Constantes.FormatoDateTimeData);
                else return null;
            }
            set
            {
                DateTime d = DateTime.MinValue;
                if (DateTime.TryParse(value, out d))
                {
                    DataEntradaObject = d;
                }
                else
                {
                    DataEntradaObject = null;
                }
            }
        }
        public DateTime? DataSaidaObject { get; set; }
        [DataMember]
        public string DataSaida
        {
            get
            {
                if (DataSaidaObject != null) return DataSaidaObject.Value.ToString(Constantes.FormatoDateTimeData);
                else return null;
            }
            set
            {
                DateTime d = DateTime.MinValue;
                if (DateTime.TryParse(value, out d))
                {
                    DataSaidaObject = d;
                }
                else
                {
                    DataSaidaObject = null;
                }
            }
        }

        /// <summary>
        /// Não existe na BD. Calculado apenas a partir da data de saida do perfil em particular
        /// </summary>
        public bool EmFuncoes
        {
            get
            {
                return DataSaidaObject == null || DataSaidaObject > DateTime.Now;
            }
        }

        public override string ToString()
        {
            return string.Format("[{0}] {1}", ID, Tipo.Nome);
        }

        [DataContract]
        public class TipoPerfil : UtilizadoresObject
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public string Codigo { get; set; }
            [DataMember]
            public string Sigla { get; set; }
            [DataMember]
            public string Nome { get; set; }
            [DataMember]
            public string Descricao { get; set; }

            public override string ToString()
            {
                return string.Format("[{0} - {1}] {2}, {3}", ID, Codigo, Nome, Descricao);
            }

            public TipoPerfilUpdate ToTipoPerfilUpdate(string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
            {
                return new TipoPerfilUpdate()
                {
                    ID = this.ID,
                    Codigo = this.Codigo,
                    Sigla = this.Sigla,
                    Nome = this.Nome,
                    Descricao = this.Descricao,
                    Ativo = this.Ativo,
                    UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, this.UtilizadorModificado) : utilizadorEdicao
                };
            }

            public class TipoPerfilUpdate
            {
                [DataMember]
                public int ID { get; set; }
                [DataMember]
                public string Codigo { get; set; }
                [DataMember]
                public string Sigla { get; set; }
                [DataMember]
                public string Nome { get; set; }
                [DataMember]
                public string Descricao { get; set; }
                [DataMember]
                public bool Ativo { get; set; }
                [DataMember]
                public string UtilizadorEdicao { get; set; }
            }
        }

        public PerfilUpdate ToPerfilUpdate(string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            var res = new PerfilUpdate()
            {
                ID = this.ID,
                IUPI = this.IUPI,
                NumeroMecanografico = this.NumeroMecanografico,
                UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, this.UtilizadorModificado) : utilizadorEdicao,
                DataEntrada = this.DataEntrada,
                DataSaida = this.DataSaida,
                Ativo = this.Ativo,
            };

            if (this.Tipo != null) res.IDTipo = this.Tipo.ID;
            if (this.Cargos != null) res.IDCargos = this.Cargos.Select(x => x.ID).ToList();
            if (this.Categoria != null) res.IDCategoria = this.Categoria.ID;
            if (this.Entidade != null) res.IDEntidade = this.Entidade.ID;
            if (this.Carreira != null) res.IDCarreira = this.Carreira.ID;
            if (this.Regime != null) res.IDRegime = this.Regime.ID;
            if (this.Situacao != null) res.IDSituacao = this.Situacao.ID;

            return res;
        }
        
        [DataContract]
        public class PerfilUpdate
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public Nullable<Guid> IUPI { get; set; }
            [DataMember]
            public string Username { get; set; }
            [DataMember]
            public string OriginalEmail { get; set; }
            [DataMember]
            public string AliasedEmail { get; set; }
            [DataMember]
            public int? IDTipo { get; set; }
            [DataMember]
            public IEnumerable<int> IDCargos { get; set; }
            [DataMember]
            public int? IDCategoria { get; set; }
            [DataMember]
            public int? IDEntidade { get; set; }
            [DataMember]
            public int? IDCarreira { get; set; }
            [DataMember]
            public int? IDRegime { get; set; }
            [DataMember]
            public int? IDSituacao { get; set; }
            [DataMember]
            public string NumeroMecanografico { get; set; }
            [DataMember]
            public string DataEntrada { get; set; }
            [DataMember]
            public string DataSaida { get; set; }
            [DataMember]
            public bool Ativo { get; set; }
            [DataMember]
            public string UtilizadorEdicao { get; set; }
        }
    }
}