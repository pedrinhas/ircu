﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAssemblies.Utilizadores
{
    [DataContract]
    public class FicheiroUtilizador : UtilizadoresObject
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public Utilizador Utilizador { get; set; }
        [DataMember]
        public Perfil Perfil { get; set; }
        [DataMember]
        public string SharePointURL { get; set; }
        [DataMember]
        public string SharePointList { get; set; }
        [DataMember]
        public string SharePointFolder { get; set; }
        [DataMember]
        public string Filename { get; set; }
        [DataMember]
        public TipoFicheiroUtilizador Tipo { get; set; }


        public FicheiroUtilizadorUpdate ToFicheiroUtilizadorUpdate(string utilizadorEdicao)
        {
            return new FicheiroUtilizadorUpdate()
            {
                ID = this.ID,
                IDUtilizador = Utilizador.ID,
                IDPerfil = Perfil == null ? (int?)null : Perfil.ID,
                SharePointURL = this.SharePointURL,
                SharePointList = this.SharePointList,
                SharePointFolder = this.SharePointFolder,
                Filename = this.Filename,
                IDTipo = Tipo.ID,
                Ativo = this.Ativo,
                UtilizadorEdicao = utilizadorEdicao
            };
        }

        [DataContract]
        public class FicheiroUtilizadorUpdate
        {

            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public int IDUtilizador { get; set; }
            [DataMember]
            public int? IDPerfil { get; set; }
            [DataMember]
            public string Filename { get; set; }
            [DataMember]
            public int IDTipo { get; set; }
            [DataMember]
            public string SharePointURL { get; set; }
            [DataMember]
            public string SharePointList { get; set; }
            [DataMember]
            public string SharePointFolder { get; set; }
            [DataMember]
            public bool Ativo { get; set; }
            [DataMember]
            public string UtilizadorEdicao { get; set; }
        }

        [DataContract]
        public class TipoFicheiroUtilizador : UtilizadoresObject
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public string Nome { get; set; }
            [DataMember]
            public string Descricao { get; set; }

            public override string ToString()
            {
                return string.Format("[{0}] {1}",  Nome, Descricao);
            }

            public TipoFicheiroUtilizadorUpdate ToTipoFicheiroUtilizadorUpdate(string utilizadorEdicao)
            {
                return new TipoFicheiroUtilizadorUpdate()
                {
                    ID = this.ID,
                    Nome = this.Nome,
                    Descricao = this.Descricao,
                    Ativo = this.Ativo,
                    UtilizadorEdicao = utilizadorEdicao
                };
            }

            [DataContract]
            public class TipoFicheiroUtilizadorUpdate
            {
                [DataMember]
                public int ID { get; set; }
                [DataMember]
                public string Nome { get; set; }
                [DataMember]
                public string Descricao { get; set; }
                [DataMember]
                public bool Ativo { get; set; }
                [DataMember]
                public string UtilizadorEdicao { get; set; }
            }
        }
    }
}
