﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IRCUAssemblies.Utilizadores
{
    [DataContract]
    public class Cargo : UtilizadoresObject
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public Entidade Entidade { get; set; }
        [DataMember]
        public TipoCargo Tipo { get; set; }
        [DataMember]
        public DateTime DataInicio { get; set; }
        [DataMember]
        public DateTime? DataFim { get; set; }

        public override string ToString()
        {
            return string.Format("[{0}] {1}, {2}", ID, Email, Tipo.Nome);
        }

        public CargoUpdate ToCargoUpdate(int idPerfil, string utilizadorEdicao)
        {
            return new CargoUpdate()
            {
                ID = this.ID,
                IDPerfil = idPerfil,
                IDEntidade = Entidade.ID,
                IDTipo = Tipo.ID,
                Email = this.Email,
                DataInicio = DataInicio.ToString(Constantes.FormatoDateTime),
                DataFim = DataFim.HasValue ? DataFim.Value.ToString(Constantes.FormatoDateTime) : null,
                Ativo = this.Ativo,
                UtilizadorEdicao = utilizadorEdicao
            };
        }

        [DataContract]
        public class CargoUpdate
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public string Email { get; set; }
            [DataMember]
            public int IDPerfil { get; set; }
            [DataMember]
            public int IDEntidade { get; set; }
            [DataMember]
            public int IDTipo { get; set; }
            [DataMember]
            public string DataInicio { get; set; }
            [DataMember]
            public string DataFim { get; set; }
            [DataMember]
            public bool Ativo { get; set; }
            [DataMember]
            public string UtilizadorEdicao { get; set; }
        }

        [DataContract]
        public class CargoFromListagem
        {
            [DataMember]
            public int IDCargo { get; set; }
            [DataMember]
            public int IDPerfil { get; set; }
            [DataMember]
            public int IDUtilizador { get; set; }
            [DataMember]
            public Guid IUPI { get; set; }
            [DataMember]
            public string UtilizadorNome { get; set; }
            [DataMember]
            public string UtilizadorUsername { get; set; }
            [DataMember]
            public string UtilizadorNumeroMecanografico { get; set; }
            [DataMember]
            public string UtilizadorEmail { get; set; }
            [DataMember]
            public string Email { get; set; }
            [DataMember]
            public int IDEntidade { get; set; }
            [DataMember]
            public string EntidadeSigla { get; set; }
            [DataMember]
            public string EntidadeNome { get; set; }
            [DataMember]
            public string EntidadeDescricao { get; set; }
            [DataMember]
            public int IDTipoCargo { get; set; }
            [DataMember]
            public string TipoCargoCodigo { get; set; }
            [DataMember]
            public string TipoCargoNome { get; set; }
            [DataMember]
            public string TipoCargoDescricao { get; set; }
            public DateTime DataInicioObject { get; set; }
            [DataMember]
            public string DataInicio
            {
                get
                {
                    return DataInicioObject.ToString(Constantes.FormatoDateTime);
                }
                set
                {
                    var d = DateTime.MinValue;
                    if (DateTime.TryParse(value, out d))
                    {
                        DataInicioObject = d;
                    }
                    else
                    {
                        DataInicioObject = DateTime.MinValue;
                    }
                }
            }
            public DateTime? DataFimObject { get; set; }
            [DataMember]
            public string DataFim
            {
                get
                {
                    if (DataFimObject != null && DataFimObject.HasValue) return DataFimObject.Value.ToString(Constantes.FormatoDateTime);
                    else return null;
                }
                set
                {
                    var d = DateTime.MinValue;
                    if (DateTime.TryParse(value, out d))
                    {
                        DataFimObject = d;
                    }
                    else
                    {
                        DataFimObject = null;
                    }
                }
            }
            [DataMember]
            public bool Ativo { get; set; }
            public DateTime DataCriadoObject { get; set; }
            [DataMember]
            public string DataCriado
            {
                get
                {
                    return DataCriadoObject.ToString(Constantes.FormatoDateTime);
                }
                set
                {
                    var d = DateTime.MinValue;
                    if (DateTime.TryParse(value, out d))
                    {
                        DataCriadoObject = d;
                    }
                    else
                    {
                        DataCriadoObject = DateTime.MinValue;
                    }
                }
            }
            [DataMember]
            public string UtilizadorCriado { get; set; }
            public DateTime? DataModificadoObject { get; set; }
            [DataMember]
            public string DataModificado
            {
                get
                {
                    if (DataModificadoObject != null && DataModificadoObject.HasValue) return DataModificadoObject.Value.ToString(Constantes.FormatoDateTime);
                    else return null;
                }
                set
                {
                    DateTime d = DateTime.MinValue;
                    if (DateTime.TryParse(value, out d))
                    {
                        DataModificadoObject = d;
                    }
                    else
                    {
                        DataModificadoObject = null;
                    }
                }
            }
            [DataMember]
            public string UtilizadorModificado { get; set; }
        }


        [DataContract]
        public class TipoCargo : UtilizadoresObject
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public string Codigo { get; set; }
            [DataMember]
            public string Nome { get; set; }
            [DataMember]
            public string Descricao { get; set; }

            public override string ToString()
            {
                return string.Format("[{0}] {1}, {2}", Codigo, Nome, Descricao);
            }

            public TipoCargoUpdate ToTipoCargoUpdate(string utilizadorEdicao)
            {
                return new TipoCargoUpdate()
                {
                    ID = this.ID,
                    Codigo = this.Codigo,
                    Nome = this.Nome,
                    Descricao = this.Descricao,
                    Ativo = this.Ativo,
                    UtilizadorEdicao = utilizadorEdicao
                };
            }

            [DataContract]
            public class TipoCargoUpdate
            {
                [DataMember]
                public int ID { get; set; }
                [DataMember]
                public string Codigo { get; set; }
                [DataMember]
                public string Nome { get; set; }
                [DataMember]
                public string Descricao { get; set; }
                [DataMember]
                public bool Ativo { get; set; }
                [DataMember]
                public string UtilizadorEdicao { get; set; }
            }
        }
    }
}