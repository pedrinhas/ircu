﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAssemblies.Utilizadores
{
    [DataContract]
    public class FicheiroCargo : UtilizadoresObject
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public Cargo Cargo { get; set; }
        [DataMember]
        public string SharePointURL { get; set; }
        [DataMember]
        public string SharePointList { get; set; }
        [DataMember]
        public string SharePointFolder { get; set; }
        [DataMember]
        public string Filename { get; set; }
        [DataMember]
        public TipoFicheiroCargo Tipo { get; set; }


        public FicheiroCargoUpdate ToFicheiroCargoUpdate(string utilizadorEdicao)
        {
            return new FicheiroCargoUpdate()
            {
                ID = this.ID,
                IDCargo = Cargo.ID,
                SharePointURL = this.SharePointURL,
                SharePointList = this.SharePointList,
                SharePointFolder = this.SharePointFolder,
                Filename = this.Filename,
                IDTipo = Tipo.ID,
                Ativo = this.Ativo,
                UtilizadorEdicao = utilizadorEdicao
            };
        }

        [DataContract]
        public class FicheiroCargoUpdate
        {

            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public int IDCargo { get; set; }
            [DataMember]
            public string Filename { get; set; }
            [DataMember]
            public int IDTipo { get; set; }
            [DataMember]
            public string SharePointURL { get; set; }
            [DataMember]
            public string SharePointList { get; set; }
            [DataMember]
            public string SharePointFolder { get; set; }
            [DataMember]
            public bool Ativo { get; set; }
            [DataMember]
            public string UtilizadorEdicao { get; set; }
        }

        [DataContract]
        public class TipoFicheiroCargo : UtilizadoresObject
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public string Nome { get; set; }
            [DataMember]
            public string Descricao { get; set; }

            public override string ToString()
            {
                return string.Format("[{0}] {1}",  Nome, Descricao);
            }

            public TipoFicheiroCargoUpdate ToTipoFicheiroCargoUpdate(string utilizadorEdicao)
            {
                return new TipoFicheiroCargoUpdate()
                {
                    ID = this.ID,
                    Nome = this.Nome,
                    Descricao = this.Descricao,
                    Ativo = this.Ativo,
                    UtilizadorEdicao = utilizadorEdicao
                };
            }

            [DataContract]
            public class TipoFicheiroCargoUpdate
            {
                [DataMember]
                public int ID { get; set; }
                [DataMember]
                public string Nome { get; set; }
                [DataMember]
                public string Descricao { get; set; }
                [DataMember]
                public bool Ativo { get; set; }
                [DataMember]
                public string UtilizadorEdicao { get; set; }
            }
        }
    }
}
