﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IRCUAssemblies.Utilizadores
{
    [DataContract]
    public class Entidade : UtilizadoresObject
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Codigo { get; set; }
        [DataMember]
        public string Sigla { get; set; }
        [DataMember]
        public string Nome { get; set; }
        [DataMember]
        public string Descricao { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public TipoEntidade Tipo { get; set; }
        [DataMember]
        public Entidade Parent { get; set; }

        public override string ToString()
        {
            return string.Format("[{0} - {1}] {2}, {3}", ID, Codigo, Nome, Descricao);
        }

        [DataContract]
        public class TipoEntidade : UtilizadoresObject
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public string Nome { get; set; }
            [DataMember]
            public string Descricao { get; set; }

            public override string ToString()
            {
                return string.Format("[{0}] {1}, {2}", ID, Nome, Descricao);
            }

            public TipoEntidadeUpdate ToTipoEntidadeUpdate(string utilizadorEdicao)
            {
                return new TipoEntidadeUpdate()
                {
                    ID = this.ID,
                    Nome = this.Nome,
                    Descricao = this.Descricao,
                    Ativo = this.Ativo,
                    UtilizadorEdicao = utilizadorEdicao
                };
            }

            [DataContract]
            public class TipoEntidadeUpdate
            {
                [DataMember]
                public int ID { get; set; }
                [DataMember]
                public string Nome { get; set; }
                [DataMember]
                public string Descricao { get; set; }
                [DataMember]
                public bool Ativo { get; set; }
                [DataMember]
                public string UtilizadorEdicao { get; set; }
            }
        }

        public EntidadeUpdate ToEntidadeUpdate(string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            var e = new EntidadeUpdate()
            {
                ID = this.ID,
                Codigo = this.Codigo,
                Sigla = this.Sigla,
                Nome = this.Nome,
                Descricao = this.Descricao,
                Email = this.Email,
                Ativo = this.Ativo,
                UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, this.UtilizadorModificado) : utilizadorEdicao
            };

            if (this.Parent != null) e.IDParent = this.Parent.ID;
            if (this.Tipo != null) e.IDTipo = this.Tipo.ID;

            return e;
        }

        [DataContract]
        public class EntidadeUpdate
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public string Codigo { get; set; }
            [DataMember]
            public string Sigla { get; set; }
            [DataMember]
            public string Nome { get; set; }
            [DataMember]
            public string Descricao { get; set; }
            [DataMember]
            public string Email { get; set; }
            [DataMember]
            public int? IDTipo { get; set; }
            [DataMember]
            public int? IDParent { get; set; }
            [DataMember]
            public bool Ativo { get; set; }
            [DataMember]
            public string UtilizadorEdicao { get; set; }
        }
    }
}