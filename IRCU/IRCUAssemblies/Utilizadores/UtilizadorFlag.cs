﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAssemblies.Utilizadores
{
    [DataContract]
    public class UtilizadorFlag : UtilizadoresObject
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int IDUtilizador { get; set; }
        [DataMember]
        public int? IDPerfil { get; set; }
        [DataMember]
        public Flag Flag { get; set; }
        [DataMember]
        public string Value { get; set; }

        public UtilizadorFlagUpdate ToUtilizadorFlagUpdate(string utilizadorEdicao)
        {
            return new UtilizadorFlagUpdate()
            {
                ID = this.ID,
                IDUtilizador = this.IDUtilizador,
                IDPerfil = this.IDPerfil,
                IDFlag = this.Flag.ID,
                Value = this.Value,
                Ativo = this.Ativo,
                UtilizadorEdicao = utilizadorEdicao
            };
        }

        [DataContract]
        public class UtilizadorFlagUpdate
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public int IDUtilizador { get; set; }
            [DataMember]
            public int? IDPerfil { get; set; }
            [DataMember]
            public int IDFlag { get; set; }
            [DataMember]
            public string Value { get; set; }
            [DataMember]
            public bool Ativo { get; set; }
            [DataMember]
            public string UtilizadorEdicao { get; set; }
        }
    }
}
