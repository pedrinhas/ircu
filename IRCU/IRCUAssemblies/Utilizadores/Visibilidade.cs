﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAssemblies.Utilizadores
{


    [DataContract]
    public class Visibilidade : IRCUAssemblies.Utilizadores.UtilizadoresObject
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Nome { get; set; }
        [DataMember]
        public string Descricao { get; set; }


        public VisibilidadeUpdate ToCarreiraUpdate(string utilizadorEdicao)
        {
            return new VisibilidadeUpdate()
            {
                ID = this.ID,
                Nome = this.Nome,
                Descricao = this.Descricao,
                Ativo = this.Ativo,
                UtilizadorEdicao = utilizadorEdicao
            };
        }

        [DataContract]
        public class VisibilidadeUpdate
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public string Nome { get; set; }
            [DataMember]
            public string Descricao { get; set; }
            [DataMember]
            public bool Ativo { get; set; }
            [DataMember]
            public string UtilizadorEdicao { get; set; }
        }
    }
}
