﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAssemblies.Utilizadores
{
    [DataContract]
    [KnownType(typeof(Cargo))]
    [KnownType(typeof(Cargo.TipoCargo))]
    [KnownType(typeof(Carreira))]
    [KnownType(typeof(Carreira.GrupoCarreira))]
    [KnownType(typeof(Categoria))]
    [KnownType(typeof(Contactos.Contacto))]
    [KnownType(typeof(Contactos.Contacto.TipoContacto))]
    [KnownType(typeof(Contactos.Contacto.GrupoContacto))]
    [KnownType(typeof(Entidade))]
    [KnownType(typeof(Entidade.TipoEntidade))]
    [KnownType(typeof(Perfil))]
    [KnownType(typeof(Perfil.TipoPerfil))]
    [KnownType(typeof(Regime))]
    [KnownType(typeof(Situacao))]
    [KnownType(typeof(Utilizador))]
    public class UtilizadoresObject
    {
        public UtilizadoresObject()
        {
            Ativo = true;
            DataCriadoObject = DateTime.MinValue;
            DataModificadoObject = null;
            UtilizadorCriado = "";
            UtilizadorModificado = "";
        }
        [DataMember]
        public bool Ativo { get; set; }
        public DateTime DataCriadoObject { get; set; }
        [DataMember]
        public string DataCriado
        {
            get
            {
                return DataCriadoObject.ToString(Constantes.FormatoDateTime);
            }
            set
            {
                var d = DateTime.MinValue;
                if (DateTime.TryParse(value, out d))
                {
                    DataCriadoObject = d;
                }
                else
                {
                    DataCriadoObject = DateTime.MinValue;
                }
            }
        }
        [DataMember]
        public string UtilizadorCriado { get; set; }
        public DateTime? DataModificadoObject { get; set; }
        [DataMember]
        public string DataModificado
        {
            get
            {
                if (DataModificadoObject != null && DataModificadoObject.HasValue) return DataModificadoObject.Value.ToString(Constantes.FormatoDateTime);
                else return null;
            }
            set
            {
                DateTime d = DateTime.MinValue;
                if (DateTime.TryParse(value, out d))
                {
                    DataModificadoObject = d;
                }
                else
                {
                    DataModificadoObject = null;
                }
            }
        }
        [DataMember]
        public string UtilizadorModificado { get; set; }
    }
}
