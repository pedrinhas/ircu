﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IRCUAssemblies.Utilizadores;
using System.Runtime.Serialization;

namespace IRCUAssemblies
{
    public class FlagDefault : UtilizadoresObject
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int IDFlag { get; set; }
        [DataMember]
        public int IDTipoPerfil { get; set; }
        [DataMember]
        public string ValorDefault { get; set; }

        public FlagDefaultUpdate ToFlagDefaultUpdate(string utilizadorEdicao)
        {
            return new FlagDefaultUpdate()
            {
                ID = this.ID,
                IDFlag = this.IDFlag,
                IDTipoPerfil = this.IDTipoPerfil,
                ValorDefault = this.ValorDefault,
                UtilizadorEdicao = utilizadorEdicao
            };
        }

        [DataContract]
        public class FlagDefaultUpdate
        {
            [DataMember]
            public int ID { get; set; }
            [DataMember]
            public int IDFlag { get; set; }
            [DataMember]
            public int IDTipoPerfil { get; set; }
            [DataMember]
            public string ValorDefault { get; set; }
            [DataMember]
            public string UtilizadorEdicao { get; set; }
        }
    }
}
