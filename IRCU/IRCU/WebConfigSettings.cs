﻿using ErrorHandling;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace IRCUWS
{

    public static class WebConfigSettings
    {
        public static bool Debug { get { return (new string[] { "1", "true", "sim", "yes" }).Contains((getValue("Debug") ?? "False").ToLowerInvariant()); } }
        public static string ApplicationName { get { return getValue("ApplicationName"); } }
        public static class ConnectionStrings
        {
            private static string getConnString(string name)
            {
                try
                {

                    if (ConfigurationManager.ConnectionStrings.Cast<ConnectionStringSettings>().Select(x=>x.Name).Contains(name))
                    {
                        return ConfigurationManager.ConnectionStrings[name].ConnectionString;
                    }
                    else return "";
                }
                catch (Exception ex)
                {
                    var e = new Exception(string.Format("A connection string {0} não existe no Web.config", name), ex);
                    throw e;
                }
            }
            public static string AcademSeguranca { get { return getConnString("academ_seguranca"); } }
            public static string RCU { get { return getConnString("RCU"); } }
            public static string ProxyDB { get { return getConnString("proxyDB"); } }
            public static string RCU_Dev_Test1 { get { return getConnString("RCU_Dev_Test1"); } }
            public static string RCU_Dev_Test2 { get { return getConnString("RCU_Dev_Test2"); } }
        }
        public static class AppSettings
        {
            public static class WebService
            {
                public static string User { get { return getValue("WebService.User"); } }
            }
            public static class IDM
            {
                public static string URL { get { return getValue("IDM.URL"); } }
                public static string Username { get { return getValue("IDM.Username"); } }
                public static string Password { get { return getValue("IDM.Password"); } }
            }
            public static class SharePoint
            {
                public static string URL { get { return getValue("SharePoint.URL"); } }
                public static string ListName { get { return getValue("SharePoint.ListName"); } }
                public static string Domain { get { return getValue("SharePoint.Domain"); } }
                public static string User { get { return getValue("SharePoint.User"); } }
                public static string Pass { get { return getValue("SharePoint.Pass"); } }
            }
            public static class Email
            {
                public static string SMTPServer
                {
                    get
                    {
                        return ConfigurationManager.AppSettings["Email.STMPServer"].ToString();
                    }
                }
                public static class ErrorLog
                {
                    public static string From
                    {
                        get
                        {
                            return getValue("Email.ErrorLog.From");
                        }
                    }

                    public static List<string> To
                    {
                        get
                        {
                            return getValue("Email.ErrorLog.To").Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        }
                    }

                    public static List<string> CC
                    {
                        get
                        {
                            return getValue("Email.ErrorLog.CC").Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        }
                    }

                    public static List<string> BCC
                    {
                        get
                        {
                            return getValue("Email.ErrorLog.BCC").Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        }
                    }
                }
            }
            public static class Errors
            {
                public static bool LogByEmail { get { return (new string[] { "1", "true", "sim", "yes" }).Contains((getValue("Errors.LogByEmail") ?? "False").ToLowerInvariant()); } }

                public static int LogLevel
                {
                    get
                    {
                        try
                        {
                            int level;

                            if (int.TryParse(getValue("Errors.LogLevel"), out level))
                            {
                                return level;
                            }
                        }
                        catch (Exception ex)
                        {
                            var e = new ArgumentException("Não foi possível interpretar o LogLevel no Web.config", ex);
                            ErrorHandler.LogError(e, Enums.Severity.Critical);
                        }


                        //Warning não deve ser muito verbose, mas manda os erros para trás
                        return 1;
                    }
                }
            }
            public static class Authentication
            {
                public static string[] AllowedAllAccessIPs { get { return UseAllowedAllAccessIPWhiteList ? (getValue("Authentication.AllowedAllAccessIPs") + ",::1,localhost,127.0.0.1").Split(new char[] { ',', ';', '-', ' ', '|' }) : new string[0]; } }
                public static bool UseAllowedAllAccessIPWhiteList { get { return (new string[] { "1", "true", "sim", "yes" }).Contains((getValue("Authentication.UseAllowedAllAccessIPWhiteList") ?? "False").ToLowerInvariant()); } }
                public static bool ChallengeBadAuth { get { return (new string[] { "1", "true", "sim", "yes" }).Contains((getValue("Authentication.ChallengeBadAuth") ?? "False").ToLowerInvariant()); } }
            }
            public static class LDAP
            {
                public static string BaseAddress { get { return getValue("LDAP.BaseAddress"); } }
            }
            public static class LDAPExchange
            {
                public static string BaseAddress { get { return getValue("LDAPExchange.BaseAddress"); } }
                public static string SearchAddressSuffix { get { return getValue("LDAPExchange.SearchAddressSuffix"); } }
                public static string SearchAddress { get { return string.Format("{0}/{1}", BaseAddress.TrimEnd(new char[] { '/' }), SearchAddressSuffix); } }
                public static string Filter { get { return getValue("LDAPExchange.Filter"); } }

                public static string Username { get { return getValue("LDAPExchange.Username"); } }
                public static string Password { get { return getValue("LDAPExchange.Password"); } }

            }
            public static class SiteUTAD
            {
                public static IEnumerable<string> FlagsToSend { get { return getValue("SiteUTAD.FlagsToSend").Split(new char[] { ',', ';', '-', ' ', '|' }).ToList(); } }
                public static string VisibilidadeUO { get { return getValue("VisibilidadeUO"); } }
                public static string VisibilidadeCat { get { return getValue("VisibilidadeCat"); } }
            }
            public static class RCU
            {
                public static bool ForcePublicEmail { get { return (new string[] { "1", "true", "sim", "yes" }).Contains((getValue("RCU.ForcePublicEmail") ?? "False").ToLowerInvariant()); } }
                public static bool ForcePublicExtensao { get { return (new string[] { "1", "true", "sim", "yes" }).Contains((getValue("RCU.ForcePublicExtensao") ?? "False").ToLowerInvariant()); } }
                public static bool ForcePublicUO { get { return (new string[] { "1", "true", "sim", "yes" }).Contains((getValue("RCU.ForcePublicUO") ?? "False").ToLowerInvariant()); } }
                public static bool ForcePublicPerfil { get { return (new string[] { "1", "true", "sim", "yes" }).Contains((getValue("RCU.ForcePublicPerfil") ?? "False").ToLowerInvariant()); } }
                public static bool ForcePublicCategoria { get { return (new string[] { "1", "true", "sim", "yes" }).Contains((getValue("RCU.ForcePublicCategoria") ?? "False").ToLowerInvariant()); } }
            }
        }


        private static string getValue(string key)
        {
            try
            {
                if (ConfigurationManager.AppSettings.Cast<string>().Contains(key))
                {
                    return ConfigurationManager.AppSettings[key];
                }
                else return "";
            }
            catch (Exception ex)
            {
                var e = new Exception(string.Format("A key {0} não existe no Web.Config", key), ex);
                throw e;
            }
        }
    }
}