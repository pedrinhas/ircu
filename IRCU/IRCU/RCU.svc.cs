﻿using IRCUWS.Secure;
using IRCUWS.Toolkit;
using IRCUAssemblies.Contactos;
using IRCUAssemblies.Contactos.SiteUTAD;
using IRCUAssemblies.Utilizadores;
using IRCUAssemblies.Toolkit;
using IRCUAcessoDB;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Threading;
using System.Web;
using IRCUAssemblies.WebpagePerfil;
using IRCUAssemblies.IDM;
using IRCUAssemblies.IDM.Helpers;
using ProxyAcessoDB;
using System.IO;
using ErrorHandling;
using IRCUAssemblies.LDAP;
using SharePointTools;

namespace IRCUWS
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class RCU : IRCU
    {
        private void HandleException(Exception ex, object obj = null)
        {
            if (ex is WebFaultException)
            {
                ErrorHandler.LogError(ex, obj, Enums.Severity.Info);
                if (WebConfigSettings.AppSettings.Authentication.ChallengeBadAuth)
                {
                    HttpContext.Current.Response.StatusCode = (int)((ex as WebFaultException).StatusCode);
                    if (WebConfigSettings.Debug) HttpContext.Current.Response.AddHeader("WWW-Authenticate", "Basic realm=intra.utad.pt");
                    HttpContext.Current.Response.End();
                }
            }
            else
            {
                ErrorHandler.LogError(ex, obj);
                HttpContext.Current.Response.StatusCode = 500;
                HttpContext.Current.Response.End();
            }
        }

        #region academ_seguranca

        public Guid AcademSeg_GetAluno4Account(string numero)
        {
            try
            {

                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                Guid res = RCUDB.AcademSeguranca.GetAluno4Account(WebConfigSettings.ConnectionStrings.AcademSeguranca, numero);

                return res;
            }
            catch (Exception e)
            {
                HandleException(e, numero);

                return Guid.Empty;
            }
        }

        public Guid AcademSeg_GetFuncionario4Account(string login, string email)
        {
            try
            {
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                Guid res = RCUDB.AcademSeguranca.GetFuncionario4Account(WebConfigSettings.ConnectionStrings.AcademSeguranca, login, email);

                return res;
            }
            catch (Exception e)
            {
                HandleException(e, new { Login = login, Email = email });

                return Guid.Empty;
            }
        }

        public Guid AcademSeg_GetIUPIAluno(string numero)
        {
            try
            {
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }


                Guid res = RCUDB.AcademSeguranca.GetIUPIAluno(WebConfigSettings.ConnectionStrings.AcademSeguranca, numero);

                return res;
            }
            catch (Exception e)
            {
                HandleException(e, numero);

                return Guid.Empty;
            }
        }

        public Guid AcademSeg_GetIUPIFuncionario(string login)
        {
            try
            {
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }


                Guid res = RCUDB.AcademSeguranca.GetIUPIFuncionario(WebConfigSettings.ConnectionStrings.AcademSeguranca, login);

                return res;
            }
            catch (Exception e)
            {
                HandleException(e, login);

                return Guid.Empty;
            }
        }

        #endregion

        #region RCU

        private static string FlagVisibilidadeEmail = "VisibilidadeEmail";
        private static string FlagVisibilidadeCat = "VisibilidadeCat";
        private static string FlagVisibilidadeUO = "VisibilidadeUO";

        private static int IDContactosExtensao = 12;

        public Nullable<Guid> RCU_Utilizador_IU(Utilizador.UtilizadorUpdate user)
        {
            try
            {
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif

                if (!Authenticated(authorizedRoles))
                {
                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                var res = RCUDB.RCU.Utilizador_IU(WebConfigSettings.ConnectionStrings.RCU, user);

                return res;
            }
            catch (Exception ex)
            {
                HandleException(ex, user);

                return null;
            }
        }
        public Utilizador RCU_Utilizador_S(Guid iupi)
        {
            try
            {
                //throw new Exception("okej");
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }
                var user = RCUDB.RCU.Utilizador_S(WebConfigSettings.ConnectionStrings.RCU, iupi);

                if (user != null && user.IUPI != Guid.Empty)
                {
                    return user;
                }
                else
                {
                    return null;
                    //throw new Exception(string.Format("O utilizador com o IUPI {0} não existe.", iupi.ToString()));
                }
            }
            catch (Exception ex)
            {
                HandleException(ex, iupi);

                return null;
            }
        }
        public Utilizador RCU_Utilizador_S_PorUsername(string username)
        {
            try
            {
                //throw new Exception("okej");
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }
                var iupi = RCUDB.RCU.Utilizador_GetIUPI(WebConfigSettings.ConnectionStrings.RCU, username);

                if (iupi.HasValue && iupi.Value != Guid.Empty)
                {
                    return RCUDB.RCU.Utilizador_S(WebConfigSettings.ConnectionStrings.RCU, iupi.Value);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex, username);

                return null;
            }
        }
        public Utilizador RCU_Utilizador_S_PorNMec(string nmec)
        {
            try
            {
                //throw new Exception("okej");
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }
                return RCUDB.RCU.Utilizador_PorNMec_S(WebConfigSettings.ConnectionStrings.RCU, nmec);
            }
            catch (Exception ex)
            {
                HandleException(ex, nmec);

                return null;
            }
        }
        public IEnumerable<Utilizador> RCU_Utilizador_LS()
        {
            try
            {
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif

                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                var res = RCUDB.RCU.Utilizador_LS(WebConfigSettings.ConnectionStrings.RCU);

                return res;
            }
            catch (Exception ex)
            {
                HandleException(ex);

                return null;
            }
        }

        public bool RCU_Utilizador_D(Guid iupi)
        {
            try
            {
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif

                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                RCUDB.RCU.Utilizador_D(WebConfigSettings.ConnectionStrings.RCU, iupi, WebConfigSettings.AppSettings.WebService.User);

                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex, iupi);

                return false;
            }
        }
        public bool RCU_Utilizador_R(Guid iupi)
        {
            try
            {
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif

                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                RCUDB.RCU.Utilizador_R(WebConfigSettings.ConnectionStrings.RCU, iupi, WebConfigSettings.AppSettings.WebService.User);

                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex, iupi);

                return false;
            }
        }
        public Nullable<Guid> RCU_Utilizador_GetIUPI(string username)
        {
            try
            {
                //throw new Exception("okej");
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                var res = RCUDB.RCU.Utilizador_GetIUPI(WebConfigSettings.ConnectionStrings.RCU, username);

                return res;
            }
            catch (Exception ex)
            {
                HandleException(ex, username);

                return null;
            }
        }

        public int RCU_Utilizador_Perfil_IU(Perfil.PerfilUpdate perfil)
        {
            try
            {
                //throw new Exception("okej");
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                var res = RCUDB.RCU.Utilizador_Perfil_IU(WebConfigSettings.ConnectionStrings.RCU, perfil);

                return res;
            }
            catch (Exception ex)
            {
                HandleException(ex, perfil);

                return -1;
            }
        }
        public bool RCU_Utilizador_Perfil_D(Guid iupi, int idPerfil)
        {
            try
            {
                //throw new Exception("okej");
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                RCUDB.RCU.Utilizador_Perfil_D(WebConfigSettings.ConnectionStrings.RCU, iupi, idPerfil, WebConfigSettings.AppSettings.WebService.User);

                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex, new { IUPI = iupi, IDPerfil = idPerfil });

                return false;
            }
        }
        public bool RCU_Utilizador_Perfil_R(Guid iupi, int idPerfil)
        {
            try
            {
                //throw new Exception("okej");
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                RCUDB.RCU.Utilizador_Perfil_R(WebConfigSettings.ConnectionStrings.RCU, iupi, idPerfil, WebConfigSettings.AppSettings.WebService.User);

                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex, new { IUPI = iupi, IDPerfil = idPerfil });

                return false;
            }
        }

        public IEnumerable<Contacto> RCU_Utilizador_Contactos_S(Guid iupi)
        {
            try
            {
                //throw new Exception("okej");
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                var data = RCUDB.RCU.Utilizador_Contactos_S(WebConfigSettings.ConnectionStrings.RCU, iupi);

                return data;
            }
            catch (Exception ex)
            {
                HandleException(ex, iupi);

                return null;
            }
        }
        public int RCU_Utilizador_Contactos_IU(IRCUAssemblies.Contactos.Contacto.ContactoUpdate contacto)
        {
            try
            {
                //throw new Exception("okej");
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                var res = RCUDB.RCU.Utilizador_Contactos_IU(WebConfigSettings.ConnectionStrings.RCU, contacto);

                return res;
            }
            catch (Exception ex)
            {
                HandleException(ex, contacto);

                return -1;
            }
        }
        public bool RCU_Utilizador_Contactos_D(Guid iupi, int idContacto)
        {
            try
            {
                //throw new Exception("okej");
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                RCUDB.RCU.Utilizador_Contactos_D(WebConfigSettings.ConnectionStrings.RCU, iupi, idContacto, WebConfigSettings.AppSettings.WebService.User);

                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex, new { IUPI = iupi, IDPerfil = idContacto });

                return false;
            }
        }
        public bool RCU_Utilizador_Contactos_R(Guid iupi, int idContacto)
        {
            try
            {
                //throw new Exception("okej");
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                RCUDB.RCU.Utilizador_Contactos_R(WebConfigSettings.ConnectionStrings.RCU, iupi, idContacto, WebConfigSettings.AppSettings.WebService.User);

                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex, new { IUPI = iupi, IDPerfil = idContacto });

                return false;
            }
        }

        //IMPLEMENTAR ISTO
        public IEnumerable<Contacto.TipoContacto> RCU_Utilizador_Contactos_Tipos_LS()
        {
            throw new NotImplementedException();
        }
        public IEnumerable<Contacto.TipoContacto> RCU_Utilizador_Contactos_Tipos_Grupos_LS()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Visibilidade> RCU_Utilizador_Visibilidade_LS()
        {
            try
            {
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif

                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                var res = RCUDB.RCU.Utilizador_Visibilidade_LS(WebConfigSettings.ConnectionStrings.RCU);

                return res;
            }
            catch (Exception ex)
            {
                HandleException(ex);

                return null;
            }
        }

        public int RCU_Utilizador_Ficheiro_I(Stream file, Guid iupi, string spURL, string spList, string spFolder, string filename, int tipo)
        {
            try
            {
                //throw new Exception("okej");
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                var idUtilizador = RCUDB.RCU.Utilizador_GetID(WebConfigSettings.ConnectionStrings.RCU, iupi);
                if (!idUtilizador.HasValue)
                {
                    throw new ArgumentException(string.Format("O utilizador com o IUPI {0} não existe", iupi));
                }

                var ficheiro = new FicheiroUtilizador()
                {
                    ID = -1,
                    Utilizador = new Utilizador() { ID = idUtilizador.Value },
                    Perfil = null,
                    SharePointURL = spURL,
                    SharePointList = spList,
                    SharePointFolder = spFolder,
                    Filename = filename,
                    Tipo = new FicheiroUtilizador.TipoFicheiroUtilizador() { ID = tipo },
                    Ativo = true
                };

                var sharepointRes = SharepointTools.UploadFile(spURL, spList, spFolder, file.ToArray(), filename, SharepointTools.ItemExistsStrategy.Overwrite);
                if(!string.IsNullOrWhiteSpace(sharepointRes))
                {
                    var res = RCUDB.RCU.Utilizador_Ficheiros_IU(WebConfigSettings.ConnectionStrings.RCU, ficheiro, WebConfigSettings.AppSettings.WebService.User);

                    return res;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex, new { IUPI = iupi, Filename = filename });

                return 0;
            }
        }
        public int RCU_Utilizador_Perfil_Ficheiro_I(Stream file, Guid iupi, int idPerfil, string spURL, string spList, string spFolder, string filename, int tipo)
        {
            try
            {
                //throw new Exception("okej");
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                var idUtilizador = RCUDB.RCU.Utilizador_GetID(WebConfigSettings.ConnectionStrings.RCU, iupi);
                if (!idUtilizador.HasValue)
                {
                    throw new ArgumentException(string.Format("O utilizador com o IUPI {0} não existe", iupi));
                }

                var ficheiro = new FicheiroUtilizador()
                {
                    ID = -1,
                    Utilizador = new Utilizador() { ID = idUtilizador.Value },
                    Perfil = new Perfil() { ID = idPerfil },
                    SharePointURL = spURL,
                    SharePointList = spList,
                    SharePointFolder = spFolder,
                    Filename = filename,
                    Tipo = new FicheiroUtilizador.TipoFicheiroUtilizador() { ID = tipo },
                    Ativo = true
                };

                var sharepointRes = SharepointTools.UploadFile(spURL, spList, spFolder, file.ToArray(), filename, SharepointTools.ItemExistsStrategy.Overwrite);
                if (!string.IsNullOrWhiteSpace(sharepointRes))
                {
                    var res = RCUDB.RCU.Utilizador_Ficheiros_IU(WebConfigSettings.ConnectionStrings.RCU, ficheiro, WebConfigSettings.AppSettings.WebService.User);

                    return res;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex, new { IUPI = iupi, IDPerfil = idPerfil, Filename = filename });

                return 0;
            }
        }
        #region Site UTAD

        public IEnumerable<User> GetContacts()
        {
            try
            {
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                var res = RCUDB.RCU.GetContactsSitePessoas(WebConfigSettings.ConnectionStrings.RCU, WebConfigSettings.AppSettings.SiteUTAD.FlagsToSend);

                return res;
            }
            catch (Exception ex)
            {
                HandleException(ex);

                return new List<User>();
            }
        }
        public IEnumerable<User> GetContactsPorVisibilidade(int visibilidade)
        {
            try
            {
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (visibilidade >= 3) authorizedRoles.Add(Roles.Public);

                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                var res = RCUDB.RCU.GetContactsSitePessoasPorVisibilidade(WebConfigSettings.ConnectionStrings.RCU, visibilidade, WebConfigSettings.AppSettings.SiteUTAD.FlagsToSend).OrderBy(x => x.Name);

                return res;
            }
            catch (Exception ex)
            {
                HandleException(ex);

                return new List<User>();
            }
        }

        #endregion

        #region WebsitePerfil

        public IEnumerable<Contacto.TipoContacto> RCU_WebsitePerfil_Utilizador_Contactos_Tipos_LS()
        {
            try
            {
                //throw new Exception("okej");
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                var data = RCUDB.RCU.Utilizador_Contactos_Tipos_LS(WebConfigSettings.ConnectionStrings.RCU);

                return data;
            }
            catch (Exception ex)
            {
                HandleException(ex);

                return null;
            }
        }
        public ContactosWebsiteUser RCU_WebsitePerfil_Utilizador_Contactos_S(string username)
        {
            try
            {
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                var res = new ContactosWebsiteUser();
                res.Username = username;

                var iupi = RCU_Utilizador_GetIUPI(username);
                if (iupi.HasValue)
                {
                    var user = RCU_Utilizador_S(iupi.Value);

                    res.ID = user.ID;
                    res.NomeUtilizador = user.Nome;
                    res.NomePersonalizado = user.NomePersonalizado;
                    res.Email = user.Email;

                    res.Visibilidade = user.Visibilidade;
                    res.Flags = user.Flags.Select(x => ContactosWebsiteFlag.ToContactosWebsiteFlag(x)).ToList();

                    foreach (var perfil in user.Perfil.Where(x => !x.DataSaidaObject.HasValue))
                    {
                        var p = new ContactosWebsitePerfil()
                            {
                                ID = perfil.ID,
                                Categoria = (perfil.Categoria ?? new Categoria()).Nome,
                                UnidadeOrganica = (perfil.Entidade ?? new Entidade()).Nome,
                                CodigoTipo = (perfil.Tipo ?? new Perfil.TipoPerfil()).Codigo,
                                NomeTipo = (perfil.Tipo ?? new Perfil.TipoPerfil()).Nome
                            };

                        p.Flags.AddRange(perfil.Flags.Select(x => ContactosWebsiteFlag.ToContactosWebsiteFlag(x)).ToList());

                        res.Perfil.Add(p);
                    }

                    var todosContactos = RCU_Utilizador_Contactos_S(iupi.Value);

                    if (todosContactos != null)
                    {
                        var tipos = RCU_WebsitePerfil_Utilizador_Contactos_Tipos_LS().Select(t => t.ID);

                        //if (todosContactos.Select(c => c.Tipo.ID).Contains(idPagPessoal))
                        //    res.PaginaPessoal = todosContactos.Single(c => c.Tipo.ID == idPagPessoal).Valor;
                        //if (todosContactos.Select(c => c.Tipo.ID).Contains(idExtensao))
                        //    res.Extensao = todosContactos.Single(c => c.Tipo.ID == idExtensao).Valor;

                        res.Contactos = todosContactos.Where(c => tipos.Contains(c.Tipo.ID)).ToList();
                    }
                }

                return res;
            }
            catch (Exception ex)
            {
                HandleException(ex, new { Username = username });

                return null;
            }
        }
        public bool RCU_WebsitePerfil_Utilizador_Contactos_Replace(ContactosWebsiteUser utilizadorFromWebsitePerfil)
        {
            try
            {
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif
                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                var iupi = RCUDB.RCU.Utilizador_GetIUPI(WebConfigSettings.ConnectionStrings.RCU, utilizadorFromWebsitePerfil.Username);

                if (iupi.HasValue)
                {
                    var user = RCUDB.RCU.Utilizador_S(WebConfigSettings.ConnectionStrings.RCU, iupi.Value);
                    var perfis = user.Perfil;

                    if (user.ID != utilizadorFromWebsitePerfil.ID)
                    {
                        throw new ArgumentException(string.Format("O utilizador {0} não tem o ID {1}.", utilizadorFromWebsitePerfil.Username, utilizadorFromWebsitePerfil.ID));
                    }

                    foreach (var perfilSite in utilizadorFromWebsitePerfil.Perfil)
                    {
                        if (!perfis.Select(x => x.ID).Contains(perfilSite.ID))
                        {
                            throw new ArgumentException(string.Format("O perfil {0} - {1} do utilizador {2} não tem o ID {3}.", perfilSite.NomeTipo, perfilSite.Categoria, utilizadorFromWebsitePerfil.Username, perfilSite.ID));
                        }
                    }
                }
                else
                {
                    throw new ArgumentException(string.Format("O utilizador {0} não existe.", utilizadorFromWebsitePerfil.Username));
                }

                if (WebConfigSettings.AppSettings.RCU.ForcePublicEmail)
                {
                    if (utilizadorFromWebsitePerfil.Flags.Select(f => f.Key).Contains(FlagVisibilidadeEmail))
                    {
                        utilizadorFromWebsitePerfil.Flags.Single(f => f.Key == FlagVisibilidadeEmail).Value = "3";
                    }
                    else
                    {
                        utilizadorFromWebsitePerfil.Flags.Add(new ContactosWebsiteFlag() { Key = FlagVisibilidadeEmail, Value = "3" });
                    }
                }
                if (WebConfigSettings.AppSettings.RCU.ForcePublicExtensao)
                {
                    foreach (var c in utilizadorFromWebsitePerfil.Contactos.Where(x => x.Tipo.ID == IDContactosExtensao))
                    {
                        c.Visibilidade = new Visibilidade() { ID = 3 };
                    }
                }


                foreach (var p in utilizadorFromWebsitePerfil.Perfil)
                {
                    if (WebConfigSettings.AppSettings.RCU.ForcePublicCategoria)
                    {
                        if (p.Flags.Select(f => f.Key).Contains(FlagVisibilidadeCat))
                        {
                            p.Flags.Single(f => f.Key == FlagVisibilidadeCat).Value = "3";
                        }
                        else
                        {
                            p.Flags.Add(new ContactosWebsiteFlag() { Key = FlagVisibilidadeCat, Value = "3" });
                        }
                    }
                    if (WebConfigSettings.AppSettings.RCU.ForcePublicUO)
                    {
                        if (p.Flags.Select(f => f.Key).Contains(FlagVisibilidadeUO))
                        {
                            p.Flags.Single(f => f.Key == FlagVisibilidadeUO).Value = "3";
                        }
                        else
                        {
                            p.Flags.Add(new ContactosWebsiteFlag() { Key = FlagVisibilidadeUO, Value = "3" });
                        }
                    }
                }

                foreach (var c in utilizadorFromWebsitePerfil.Contactos)
                {
                    c.PropertyID = null;
                }

                if (iupi.HasValue)
                {
                    ////Extensão vem como ligação na lista dos contactos
                    //if (!string.IsNullOrWhiteSpace(contactos.Extensao)) contactos.Contactos.Add(new Contacto()
                    //{
                    //    IUPI = iupi.Value,
                    //    Tipo = new Contacto.TipoContacto() { ID = 12 },
                    //    Valor = contactos.Extensao,
                    //    Visibilidade = new Visibilidade() { ID = 3 },
                    //    Ativo = true
                    //});
                    ////if (!string.IsNullOrWhiteSpace(contactos.PaginaPessoal)) contactos.Contactos.Add(new Contacto()
                    ////{
                    ////    IUPI = iupi.Value,
                    ////    Tipo = new Contacto.TipoContacto() { ID = 3 },
                    ////    Valor = contactos.PaginaPessoal,
                    ////    Visibilidade = new Visibilidade() { ID = 3 },
                    ////    Ativo = true
                    ////});

                    foreach (var c in utilizadorFromWebsitePerfil.Contactos)
                    {
                        c.IUPI = iupi.Value;
                        if (c.Visibilidade == null || c.Visibilidade.ID == 0) c.Visibilidade = new Visibilidade() { ID = 1 };
                    }

                    RCUDB.RCU.Utilizador_WebsitePerfil_Contactos_Replace(WebConfigSettings.ConnectionStrings.RCU, iupi.Value, utilizadorFromWebsitePerfil.Contactos, string.Format("{0} - {1} - {2}", WebConfigSettings.AppSettings.WebService.User, "Website Perfil", iupi.Value));

                    //ProxyDB.GIAF.Extensao_U(contactos.)

                    var user = RCU_Utilizador_S(iupi.Value);

                    //fita cola
                    if (utilizadorFromWebsitePerfil.Visibilidade == null) utilizadorFromWebsitePerfil.Visibilidade = new Visibilidade() { ID = 3 };

                    if (WebConfigSettings.AppSettings.RCU.ForcePublicPerfil)
                    {
                        user.Visibilidade = new Visibilidade() { ID = 3 };

                        foreach (var p in user.Perfil)
                        {
                            p.Visibilidade = new Visibilidade() { ID = 3 };
                        }
                    }

                    var userToUpdate = user.ToUtilizadorUpdate(string.Format("{0} - {1} - {2}", WebConfigSettings.AppSettings.WebService.User, "Website Perfil", iupi.Value.ToString().ToUpper()));
                    userToUpdate.NomePersonalizado = utilizadorFromWebsitePerfil.NomePersonalizado;
                    userToUpdate.Visibilidade = utilizadorFromWebsitePerfil.Visibilidade.ID;

                    RCU_Utilizador_IU(userToUpdate);

                    var exts = utilizadorFromWebsitePerfil.Contactos.Where(x => x.Tipo != null && x.Tipo.ID == 12);
                    var extensao = "";
                    if (exts.Count() > 0)
                    {
                        extensao = exts.First().Valor;
                    }
                    var gabs = utilizadorFromWebsitePerfil.Contactos.Where(x => x.Tipo != null && x.Tipo.ID == 16);
                    var gabinete = "";
                    if (gabs.Count() > 0)
                    {
                        gabinete = gabs.First().Valor;
                    }
                    if (!string.IsNullOrWhiteSpace(gabinete + extensao))
                    {
                        foreach (var nmec in user.Perfil.Select(x => x.NumeroMecanografico))
                        {
                            ProxyDB.GIAF.Dados_U(WebConfigSettings.ConnectionStrings.ProxyDB, nmec, extensao, "");
                            //ProxyDB.GIAF.Dados_U(WebConfigSettings.ConnectionStrings.ProxyDB, nmec, extensao, gabinete);
                        }
                    }
                    var allFlags = RCUDB.RCU.Flags_LS(WebConfigSettings.ConnectionStrings.RCU);
                    var flagsUser = new List<UtilizadorFlag>();
                    foreach (var fl in utilizadorFromWebsitePerfil.Flags)
                    {
                        var tmpFlag = new UtilizadorFlag()
                        {
                            IDUtilizador = utilizadorFromWebsitePerfil.ID,
                            IDPerfil = null,
                            Value = fl.Value,
                            Flag = allFlags.FirstOrDefault(x => x.Key == fl.Key)
                        };

                        flagsUser.Add(tmpFlag);
                    }

                    RCUDB.RCU.Utilizador_Flags_IU_TabelaEntrada(WebConfigSettings.ConnectionStrings.RCU, flagsUser.ToDBType(string.Format("{0} - {1} - {2}", WebConfigSettings.AppSettings.WebService.User, "Website Perfil", iupi.Value.ToString().ToUpper())));

                    foreach (var p in utilizadorFromWebsitePerfil.Perfil)
                    {
                        var flagsPerfil = new List<UtilizadorFlag>();
                        foreach (var fl in p.Flags)
                        {
                            var tmpFlag = new UtilizadorFlag()
                            {
                                IDUtilizador = utilizadorFromWebsitePerfil.ID,
                                IDPerfil = p.ID,
                                Value = fl.Value,
                                Flag = allFlags.FirstOrDefault(x => x.Key == fl.Key)
                            };

                            flagsPerfil.Add(tmpFlag);
                        }

                        RCUDB.RCU.Utilizador_Flags_IU_TabelaEntrada(WebConfigSettings.ConnectionStrings.RCU, flagsPerfil.ToDBType(string.Format("{0} - {1} - {2}", WebConfigSettings.AppSettings.WebService.User, "Website Perfil", iupi.Value.ToString().ToUpper())));
                    }

                    //modificar propriedades no IDM
                    var props = new Dictionary<string, string>();
                    props.Add("staff_v_displayname", utilizadorFromWebsitePerfil.NomePersonalizado);

                    if (utilizadorFromWebsitePerfil.Contactos.Where(x => x.Tipo.ID == IDContactosExtensao).Count() > 0)
                    {
                        props.Add("staff_v_extensao", utilizadorFromWebsitePerfil.Contactos.Single(x => x.Tipo.ID == IDContactosExtensao).Valor);
                    }

                    var propertiesReturnVal = SPMLOperations.ModifyProperties(utilizadorFromWebsitePerfil.Username, props);

                    if (propertiesReturnVal.Status == SPMLResult.SPMLResultStatus.Failure)
                    {
                        throw new Exception(string.Format("Ocorreu um erro ao atualizar as propriedades no IDM.{0}Erro {1}: {2}", Environment.NewLine, propertiesReturnVal.ErrorCode, propertiesReturnVal.ErrorMessage));
                    }

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                HandleException(ex, new { Username = utilizadorFromWebsitePerfil.NomeUtilizador });

                return false;
            }
        }

        #endregion

        #region Repositorio

        public Stream GetORCIDs()
        {
            try
            {
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Repositorio, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Repositorio };
#endif

                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                int idTipoORCID = 8;

                string pairTemplate = "<pair><displayed-value>{0} - {1}</displayed-value><stored-value>{0}</stored-value></pair>";
                string allTemplate = "<form-value-pairs><value-pairs value-pairs-name=\"SampleAuthorAuthority\" dc-term=\"contributor\">{0}</value-pairs></form-value-pairs>";

                string allPairs = "";

                //var contacts = RCUDB.RCU.Utilizador_Contactos_PorTipo_LS(WebConfigSettings.ConnectionStrings.RCU, idTipoORCID);
                var contacts = RCUDB.RCU.Utilizador_Contactos_PorTipo_LS(WebConfigSettings.ConnectionStrings.RCU, idTipoORCID);
                var users = RCUDB.RCU.Utilizador_LS(WebConfigSettings.ConnectionStrings.RCU, true); //para profs que tenham saido da UTAD

                foreach (var c in contacts.Where(x => !string.IsNullOrWhiteSpace(x.Valor)))
                {
                    string orcidID = c.Valor.Substring(c.Valor.LastIndexOf('/') + (char.IsDigit(c.Valor[0]) ? 0 : 1));
                    var u = users.Single(x=>x.IUPI == c.IUPI);

                    var nomeSplit = u.Nome.FirstCharToUpper().Split(new char[] {' '});
                    var formattedName = string.Format("{0}, {1}", nomeSplit.Last(), string.Join(" ", nomeSplit.Take(nomeSplit.Length - 1)));

                    allPairs = allPairs + string.Format(pairTemplate, orcidID, formattedName);
                }

                var bytes = Encoding.UTF8.GetBytes(string.Format(allTemplate, allPairs));

                var ms = new MemoryStream(bytes);

                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

                return ms;
                
            }
            catch (Exception ex)
            {
                HandleException(ex);

                return null;
            }

        }
        

        #endregion

        #endregion

        #region IDM

        public UtilizadorIDM IDM_Utilizador_S(string username)
        {
            try
            {
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif

                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                var res = SPMLOperations.SearchUser(username);

                return res;
            }
            catch (Exception ex)
            {
                HandleException(ex, username);

                return null;
            }
        }
        //Autenticação no IDM_Utilizador_S. Implementar aqui caso se mude o código
        public bool IDM_Utilizador_Exists(string username)
        {
            return !string.IsNullOrWhiteSpace(username) && IDM_Utilizador_S(username) != null;
        }

        public void IDM_Utilizador_I(string username)
        {
            try
            {
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif

                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                //!!!!!!
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                HandleException(ex, username);
            }
        }

        /// <summary>
        /// Altera a password do utilizador no IDM
        /// </summary>
        /// <param name="username">Nome de utilizador (username@utad.pt)</param>
        /// <param name="newPwd">Password a ser atribuida</param>
        /// <remarks>Este método é POST de modo a não aparecerem as passwords em plain text nos logs</remarks>
        public SPMLResult IDM_Utilizador_ModifyPassword(string username, string oldPwd, string newPwd)
        {
            try
            {
#if NOAUTH
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser, Roles.Public };
#else
                var authorizedRoles = new List<string>() { Roles.AuthenticatedUser };
#endif

                if (!Authenticated(authorizedRoles))
                {
                    HttpContext.Current.Response.StatusCode = 401;

                    throw new WebFaultException(HttpStatusCode.Unauthorized);
                }

                return SPMLOperations.ModifyPassword(username, oldPwd, newPwd);
            }
            catch (Exception ex)
            {
                HandleException(ex, username);

                return null;
            }
        }

        #endregion

        public bool Authenticated(IEnumerable<string> authorizedRoles)
        {
            try
            {
                if (WebConfigSettings.AppSettings.Authentication.AllowedAllAccessIPs.Contains(HttpContext.Current.Request.UserHostAddress)) return true;

                var authHeader = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];

                var roleNames = new List<String>();

                if ((authHeader != null) && (authHeader != string.Empty))
                {
                    var svcCredentials = System.Text.ASCIIEncoding.ASCII
                        .GetString(Convert.FromBase64String(authHeader.Substring(authHeader.IndexOf(' '))))
                        .Split(':');
                    var user = new
                    {
                        Name = svcCredentials[0],
                        Password = svcCredentials[1]
                    };

                    roleNames.AddRange(SecureDB.WS_DoLogin(user.Name, user.Password));
                }
                else
                {
                    //PUBLICO
                }

                //Caso não tenha autenticação, adiciona o role Public
                if (roleNames.Count == 0) roleNames.Add(Roles.Public);

                return roleNames.Intersect(authorizedRoles).Count() > 0;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Enums.Severity.Critical);

                return false;
            }
        }




    }
}
