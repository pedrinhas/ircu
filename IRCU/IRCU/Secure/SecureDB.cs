﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IRCUWS.Secure
{
    public static class SecureDB
    {
        public static List<string> WS_DoLogin(string user, string pass)
        {
            try
            {
                var roles = new List<string>();

                using (var con = new SqlConnection(WebConfigSettings.ConnectionStrings.RCU))
                using (var cmd = new SqlCommand("[stp_Sessao_WebService_DoLogin]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@username", System.Data.SqlDbType.NVarChar).Value = user;
                    cmd.Parameters.Add("@password", System.Data.SqlDbType.NVarChar).Value = pass;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        roles.Add(reader.GetString(3));
                    }

                    con.Close();
                }

                return roles;
            }
            catch (Exception ex)
            {
                var ex2 = new Exception(string.Format("Não foi possível autenticar o utilizador {0}", user), ex);
                throw ex2;
            }
        }

        public static bool WS_Password_Modify(string user, string pass, string alg = "SHA2_512")
        {
            try
            {
                using (var con = new SqlConnection(WebConfigSettings.ConnectionStrings.RCU))
                using (var cmd = new SqlCommand("[stp_Sessao_Utilizador_Password_Modify]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@username", System.Data.SqlDbType.NVarChar).Value = user;
                    cmd.Parameters.Add("@password", System.Data.SqlDbType.NVarChar).Value = pass;
                    cmd.Parameters.Add("@alg", System.Data.SqlDbType.NVarChar).Value = alg;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();

                    con.Close();

                    return true;
                }
            }
            catch (Exception ex)
            {
                var ex2 = new Exception(string.Format("Não foi possível mudar a password do utilizador {0}", user), ex);
                throw ex;
            }
        }
        public static string WS_Password_Reset(string user, string alg = "SHA2_512")
        {
            try
            {
                using (var con = new SqlConnection(WebConfigSettings.ConnectionStrings.RCU))
                using (var cmd = new SqlCommand("[stp_Sessao_Utilizador_Password_Reset]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@username", System.Data.SqlDbType.NVarChar).Value = user;
                    cmd.Parameters.Add("@alg", System.Data.SqlDbType.NVarChar).Value = alg;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var res = cmd.ExecuteScalar();

                    con.Close();

                    return res.ToString();
                }
            }
            catch (Exception ex)
            {
                var ex2 = new Exception(string.Format("Não foi possível mudar a password do utilizador {0}", user), ex);
                throw ex;
            }
        }
    }
}