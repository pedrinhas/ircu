﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IRCUWS.Secure
{
    public static class Roles
    {
        public const string Public = "Public";
        public const string AuthenticatedUser = "AuthenticatedUser";
        public const string Repositorio = "Repositorio";
    }
}