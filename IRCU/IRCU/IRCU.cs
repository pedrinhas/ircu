﻿using IRCUAssemblies.Contactos;
using IRCUAssemblies.Contactos.SiteUTAD;
using IRCUAssemblies.IDM;
using IRCUAssemblies.IDM.Helpers;
using IRCUAssemblies.Utilizadores;
using IRCUAssemblies.WebpagePerfil;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

namespace IRCUWS
{
    [ServiceContract]
    public interface IRCU
    {
        #region academ_seguranca

        [OperationContract]
        [WebInvoke(Method="GET", UriTemplate = "GetAluno4Account?numero={numero}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        Guid AcademSeg_GetAluno4Account(string numero);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetFuncionario4Account?login={login}&email={email}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        Guid AcademSeg_GetFuncionario4Account(string login, string email);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetIUPIAluno?numero={numero}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        Guid AcademSeg_GetIUPIAluno(string numero);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetIUPIFuncionario?login={login}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        Guid AcademSeg_GetIUPIFuncionario(string login);

        #endregion

        #region RCU

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "RCU_Utilizador_IU", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        Nullable<Guid> RCU_Utilizador_IU(Utilizador.UtilizadorUpdate user);
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RCU_Utilizador_S?iupi={iupi}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        Utilizador RCU_Utilizador_S(Guid iupi);
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RCU_Utilizador_S_PorUsername?username={username}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        Utilizador RCU_Utilizador_S_PorUsername(string username);
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RCU_Utilizador_S_PorNMec?nmec={nmec}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        Utilizador RCU_Utilizador_S_PorNMec(string nmec);
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RCU_Utilizador_LS", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        IEnumerable<Utilizador> RCU_Utilizador_LS();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RCU_Utilizador_D?iupi={iupi}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        bool RCU_Utilizador_D(Guid iupi);
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RCU_Utilizador_R?iupi={iupi}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        bool RCU_Utilizador_R(Guid iupi);
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RCU_Utilizador_GetIUPI?username={username}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        Nullable<Guid> RCU_Utilizador_GetIUPI(string username);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "RCU_Utilizador_Perfil_IU", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        int RCU_Utilizador_Perfil_IU(Perfil.PerfilUpdate perfil);
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RCU_Utilizador_Perfil_D?iupi={iupi}&idPerfil={idPerfil}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        bool RCU_Utilizador_Perfil_D(Guid iupi, int idPerfil);
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RCU_Utilizador_Perfil_R?iupi={iupi}&idPerfil={idPerfil}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        bool RCU_Utilizador_Perfil_R(Guid iupi, int idPerfil);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RCU_Utilizador_Contactos_S?iupi={iupi}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        IEnumerable<Contacto> RCU_Utilizador_Contactos_S(Guid iupi);
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "RCU_Utilizador_Contactos_IU", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        int RCU_Utilizador_Contactos_IU(IRCUAssemblies.Contactos.Contacto.ContactoUpdate contacto);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RCU_Utilizador_Contactos_Tipos_LS", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        IEnumerable<Contacto.TipoContacto> RCU_Utilizador_Contactos_Tipos_LS();
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RCU_Utilizador_Contactos_Tipos_Grupos_LS", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        IEnumerable<Contacto.TipoContacto> RCU_Utilizador_Contactos_Tipos_Grupos_LS();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RCU_Utilizador_Visibilidade_LS", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        IEnumerable<Visibilidade> RCU_Utilizador_Visibilidade_LS();



        //Estes 2 estão a funcionar corretamente
        //Estava a pensar guardar o ficheiro de identidade do Gestor de Contas no SharePoint, e adicioná-lo ao utilizador do RCU. No entanto, o utilizador ainda não existe no RCU. Ver como se vai fazer. Pode-se configurar o sincronizador para ler a pasta, procurar pelo NIF e adicionar o ficheiro ao user caso este não o tenha.
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "RCU_Utilizador_Ficheiro_I?iupi={iupi}&spURL={spURL}&spList={spList}&spFolder={spFolder}&filename={filename}&tipo={tipo}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        int RCU_Utilizador_Ficheiro_I(Stream file, Guid iupi, string spURL, string spList, string spFolder, string filename, int tipo);
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "RCU_Utilizador_Perfil_Ficheiro_I?iupi={iupi}&idp={idPerfil}&spURL={spURL}&spList={spList}&spFolder={spFolder}&filename={filename}&tipo={tipo}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        int RCU_Utilizador_Perfil_Ficheiro_I(Stream file, Guid iupi, int idPerfil, string spURL, string spList, string spFolder, string filename, int tipo);

        #region WebsitePerfil

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RCU_Utilizador_Contactos_Tipos_WebsitePerfil_LS", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        IEnumerable<Contacto.TipoContacto> RCU_WebsitePerfil_Utilizador_Contactos_Tipos_LS();
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RCU_Utilizador_Contactos_WebsitePerfil_S?username={username}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ContactosWebsiteUser RCU_WebsitePerfil_Utilizador_Contactos_S(string username);
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "RCU_Utilizador_Contactos_WebsitePerfil_Replace", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        bool RCU_WebsitePerfil_Utilizador_Contactos_Replace(ContactosWebsiteUser contactos);

        #endregion

        #region Repositorio

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetORCIDs", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        Stream GetORCIDs();

        #endregion

        #endregion

        #region IDM

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "IDM_Utilizador_S?username={username}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        UtilizadorIDM IDM_Utilizador_S(string username);
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "IDM_Utilizador_Exists?username={username}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        bool IDM_Utilizador_Exists(string username);
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "IDM_Utilizador_ModifyPassword", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        SPMLResult IDM_Utilizador_ModifyPassword(string username, string oldPwd, string newPwd);
        //[OperationContract]
        //[WebInvoke(Method = "POST", UriTemplate = "IDM_Utilizador_I", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        //void IDM_Utilizador_I(string username);

        #endregion

        #region public

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetContacts", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        IEnumerable<User> GetContacts();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetContactsPorVisibilidade?visibilidade={visibilidade}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        IEnumerable<User> GetContactsPorVisibilidade(int visibilidade);

        #endregion
    }
}
