﻿using ErrorHandling;
using IRCUAssemblies.IDM.Helpers;
using IRCUAssemblies.LDAP;
using SharePointTools;
using System;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;

namespace IRCUWS
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes();
        }

        private void RegisterRoutes()
        {
            // Edit the base address of Service1 by replacing the "Service1" string below
            // RouteTable.Routes.Add(new ServiceRoute("Service1", new WebServiceHostFactory(), typeof(Service1)));
            ErrorHandler.Initialize(WebConfigSettings.ApplicationName, WebConfigSettings.AppSettings.Email.SMTPServer, WebConfigSettings.AppSettings.Email.ErrorLog.From, WebConfigSettings.AppSettings.Email.ErrorLog.To, WebConfigSettings.AppSettings.Email.ErrorLog.CC, WebConfigSettings.AppSettings.Email.ErrorLog.BCC, WebConfigSettings.AppSettings.Errors.LogLevel, WebConfigSettings.AppSettings.Errors.LogByEmail);

            SPMLOperations.Initialize(WebConfigSettings.AppSettings.IDM.URL, WebConfigSettings.AppSettings.IDM.Username, WebConfigSettings.AppSettings.IDM.Password);
            LDAPTools.Initialize(WebConfigSettings.AppSettings.LDAP.BaseAddress);
            LDAPTools.Exchange.Initialize(WebConfigSettings.AppSettings.LDAPExchange.SearchAddress, WebConfigSettings.AppSettings.LDAPExchange.Filter, WebConfigSettings.AppSettings.LDAPExchange.Username, WebConfigSettings.AppSettings.LDAPExchange.Password);

            SharepointTools.Init(WebConfigSettings.AppSettings.SharePoint.User, WebConfigSettings.AppSettings.SharePoint.Pass, WebConfigSettings.AppSettings.SharePoint.Domain);



            RouteTable.Routes.Add(new ServiceRoute("", new WebServiceHostFactory(), typeof(RCU)));
        }
    }
}
