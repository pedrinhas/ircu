﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace IRCUWS.Toolkit
{
    public static class Tools
    {
        public static class Email
        {
            public static bool SendEmail(List<string> to, string subject, string body, List<string> cc = null, List<string> bcc = null)
            {
                try
                {
                    return IRCUAssemblies.Toolkit.Tools.Email.SendEmail(WebConfigSettings.AppSettings.Email.SMTPServer, WebConfigSettings.AppSettings.Email.ErrorLog.From, to, subject, body, cc, bcc);
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
    }
}