﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IRCUAcessoDB;
using ProxyAcessoDB;
using ProxyAcessoDB.Types;
using IRCUAssemblies.Utilizadores;

namespace RCUMaestroSynchronizer.Extensions
{
    public static class IRCUProxyExtensions
    {
        public static IEnumerable<NMecIUPIType> ToNMecIUPIDBType(this Utilizador u)
        {
            var res = new List<NMecIUPIType>();

            foreach (var p in u.Perfil)
            {
                res.Add(new NMecIUPIType()
                    {
                        Empnum = p.NumeroMecanografico,
                        IUPI = u.IUPI.Value.ToString()
                    });
            }

            return res;
        }

        public static IEnumerable<NMecIUPIType> ToNMecIUPIDBType(this IEnumerable<Utilizador> us)
        {
            return us.SelectMany(u => u.ToNMecIUPIDBType()).ToList();
        }
    }
}
