﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCUMaestroSynchronizer
{
    public static class AppConfigSettings
    {
        public static bool Debug { get { return (new string[] { "1", "true", "sim", "yes" }).Contains((ConfigurationManager.AppSettings["Debug"] ?? "False").ToLowerInvariant()); } }

        public static class ConnectionStrings
        {
            private static string getConnString(string name)
            {
                try
                {
                    return ConfigurationManager.ConnectionStrings[name].ConnectionString;
                }
                catch (Exception ex)
                {
                    var e = new Exception(string.Format("A connection string {0} não existe no Web.config", name), ex);
                    throw e;
                }
            }
            public static string Proxy { get { return getConnString("proxyDB"); } }
#if DEBUG
            public static string RCU { get { return getConnString(Debug ? "RCU_Dev" : "RCU"); } }
#else
            public static string RCU { get { return getConnString("RCU"); } }
#endif
            public static string RCU_Test1 { get { return getConnString("RCU_Dev_Test1"); } }
            public static string RCU_Test2 { get { return getConnString("RCU_Dev_Test2"); } }
        }
        public static class AppSettings
        {
            private static string getValue(string key)
            {
                try
                {
                    return ConfigurationManager.AppSettings[key];
                }
                catch (Exception ex)
                {
                    var e = new Exception(string.Format("A key {0} não existe no Web.Config", key), ex);
                    throw e;
                }
            }

            public static class Service
            {
                public static string User { get { return getValue("Service.User"); } }
            }

            public static class LDAP
            {
                public static string BaseAddress { get { return getValue("LDAP.BaseAddress"); } }
            }
            public static class LDAPExchange
            {
                public static string BaseAddress { get { return getValue("LDAPExchange.BaseAddress"); } }
                public static string SearchAddressSuffix { get { return getValue("LDAPExchange.SearchAddressSuffix"); } }
                public static string SearchAddress { get { return string.Format("{0}/{1}", BaseAddress.TrimEnd(new char[] { '/' }), SearchAddressSuffix); } }
                public static string Filter { get { return getValue("LDAPExchange.Filter"); } }

                public static string Username { get { return getValue("LDAPExchange.Username"); } }
                public static string Password { get { return getValue("LDAPExchange.Password"); } }
            }

            public static class IDM
            {
                public static string URL { get { return getValue("IDM.URL"); } }
                public static string Username { get { return getValue("IDM.Username"); } }
                public static string Password { get { return getValue("IDM.Password"); } }
            }
        }
    }
}
