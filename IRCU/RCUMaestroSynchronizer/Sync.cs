﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using MaestroStuff.Notifications;
using IRCUAssemblies.Utilizadores;
using IRCUAssemblies.Toolkit;
using IRCUAcessoDB;
using ProxyAcessoDB;
using IRCUAcessoDB.Types;
using System.Diagnostics;
using IRCUAssemblies.LDAP;
using IRCUAssemblies.IDM.Helpers;
using RCUMaestroSynchronizer.Extensions;

namespace RCUMaestroSynchronizer
{
    class Sync
    {
        private const string dateTimeFormat = "yyyy-MM-dd HH:mm:ss";
        private static bool forceSync = false;

        static void Main(string[] args)
        {
            try
            {
                if(args.Contains("-f") || args.Contains("--force"))
                {
                    forceSync = true;
                }

                LDAPTools.Initialize(AppConfigSettings.AppSettings.LDAP.BaseAddress);
                LDAPTools.Exchange.Initialize(AppConfigSettings.AppSettings.LDAPExchange.SearchAddress, AppConfigSettings.AppSettings.LDAPExchange.Filter, AppConfigSettings.AppSettings.LDAPExchange.Username, AppConfigSettings.AppSettings.LDAPExchange.Password);
                SPMLOperations.Initialize(AppConfigSettings.AppSettings.IDM.URL, AppConfigSettings.AppSettings.IDM.Username, AppConfigSettings.AppSettings.IDM.Password);

                var inicioMsg = "Início.";
                if (forceSync) inicioMsg += " A forçar a atualização. Todos os registos serão atualizados pelos dados do GIAF.";

                MaestroNotifier.ConsoleNotifier.WriteToConsole_Start(inicioMsg);

                //var res = IRCUAcessoDB.RCUDB.RCU.Utilizador_Contactos_LS(AppConfigSettings.ConnectionStrings.RCU_Dev);
                //var oi = IRCUAcessoDB.ProxyDB.GIAF.Carreiras_LS(AppConfigSettings.ConnectionStrings.Proxy);

                IEnumerable<Carreira> carreirasGIAF = null;
                IEnumerable<Carreira.GrupoCarreira> carreirasGruposGIAF = null;
                IEnumerable<Regime> regimesGIAF = null;
                IEnumerable<Entidade> entidadesGIAF = null;
                IEnumerable<Situacao> situacoesGIAF = null;
                IEnumerable<Categoria> categoriasGIAF = null;
                IEnumerable<Perfil.TipoPerfil> tiposGIAF = null;
                IEnumerable<Utilizador> funcionariosGIAF = null;

                Stopwatch sw = new Stopwatch();

                //var func = ProxyDB.GIAF.Funcionario_S(AppConfigSettings.ConnectionStrings.Proxy, "001714");

                using (var con = new SqlConnection(AppConfigSettings.ConnectionStrings.Proxy))
                {
                    carreirasGIAF = ProxyDB.GIAF.Carreiras_LS(con);
                    carreirasGruposGIAF = ProxyDB.GIAF.Carreiras_Grupos_LS(con);
                    regimesGIAF = ProxyDB.GIAF.Regimes_LS(con);
                    entidadesGIAF = ProxyDB.GIAF.Entidades_LS(con);
                    situacoesGIAF = ProxyDB.GIAF.Situacoes_LS(con);
                    categoriasGIAF = ProxyDB.GIAF.Categorias_LS(con);
                    funcionariosGIAF = ProxyDB.GIAF.Funcionario_LS(con);
                    tiposGIAF = ProxyDB.GIAF.TiposPerfil_LS(con);


                    //var a = funcionariosGIAF.Where(y => y.Perfil.Where(x => !x.OriginalEmail.Contains("@utad.pt")).Count() > 0).ToList();
                    //var str = "";
                    //foreach (var item in a)
                    //    foreach (var prf in item.Perfil.Where(x => !x.OriginalEmail.Contains("@utad.pt")))
                    //    {
                    //        str += string.Format("'{0}',{1}", prf.OriginalEmail, Environment.NewLine);
                    //    }
                }

                #region debug

                //List<string> debug_codigosCarreira = carreirasGIAF.Select(x => x.Codigo).ToList();
                //List<string> debug_codigosCarreiraCategorias = categoriasGIAF.Where(x => x.Carreira != null).Select(x => x.Carreira.Codigo).ToList();

                //string allCodCarreira = "";
                //debug_codigosCarreira.ForEach(x => allCodCarreira += Environment.NewLine + x);

                //string allCodCarreiraCategorias = "";
                //debug_codigosCarreiraCategorias.ForEach(x => allCodCarreiraCategorias += Environment.NewLine + x);

                #endregion

                using (var conRCU = new SqlConnection(AppConfigSettings.ConnectionStrings.RCU))
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(4, 20, 0)))
                {
                    conRCU.Open();

                    sw.Start();
                    //SincronizarCarreiras(con, carreirasGIAF, carreirasGruposGIAF, forceSync);
                    //SincronizarRegimes(con, regimesGIAF, forceSync);
                    //SincronizarEntidades(con, entidadesGIAF, forceSync);
                    //SincronizarSituacoes(con, situacoesGIAF, forceSync);
                    //SincronizarCategorias(con, categoriasGIAF, forceSync);
                    //SincronizarTiposPerfil(con, tiposGIAF, forceSync);
                    //SincronizarFuncionarios(con, funcionariosGIAF, forceSync);

                    SincronizarCarreirasTabelaParam(conRCU, carreirasGIAF, carreirasGruposGIAF, forceSync);
                    SincronizarRegimesTabelaParam(conRCU, regimesGIAF, forceSync);
                    SincronizarEntidadesTabelaParam(conRCU, entidadesGIAF, forceSync);
                    SincronizarSituacoesTabelaParam(conRCU, situacoesGIAF, forceSync);
                    SincronizarCategoriasTabelaParam(conRCU, categoriasGIAF, forceSync);
                    SincronizarTiposPerfilTabelaParam(conRCU, tiposGIAF, forceSync);
                    //SincronizarFuncionariosTabelaParam(con, funcionariosGIAF.Where(x => x.NIF == "193141540").ToList(), forceSync);
                    using (var conProxy = new SqlConnection(AppConfigSettings.ConnectionStrings.Proxy))
                    {
                        SincronizarFuncionariosTabelaParam(conRCU, conProxy, funcionariosGIAF, forceSync);
                    }

                    sw.Stop();
                    scope.Complete();
                    conRCU.Close();
                }

                MaestroNotifier.ConsoleNotifier.WriteToConsole_End(string.Format("Fim da sincronização ({0}). Sucesso.", TimeSpan.FromMilliseconds(sw.ElapsedMilliseconds)));
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Critical);
                MaestroNotifier.ConsoleNotifier.WriteToConsole_End("Fim da sincronização. Erro, ver logs.");
            }
        }

        /// <summary>
        /// Sincroniza os regimes.
        /// </summary>
        /// <param name="con">Ligação SQL ao RCU</param>
        /// <param name="regimesGIAF">Lista de regimes do GIAF</param>
        /// <returns></returns>
        private static bool SincronizarRegimes(SqlConnection con, IEnumerable<Regime> regimesGIAF, bool forceSync = false)
        {
            MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("SincronizarRegimes()");
            try
            {
                var regimesRCU = RCUDB.RCU.Utilizador_Regimes_LS(con);

                var regimesToUpdate = regimesGIAF.Where(x => forceSync || (!regimesRCU.Select(y => y.Codigo).Contains(x.Codigo) || x.DataModificadoObject > regimesRCU.Single(y => y.Codigo == x.Codigo).DataModificadoObject)).ToList();

                foreach (var regime in regimesToUpdate)
                {
                    if (regimesRCU.Select(x => x.Codigo).Contains(regime.Codigo))
                    {
                        regime.ID = regimesRCU.Single(x => x.Codigo == regime.Codigo).ID;
                    }

                    MaestroNotifier.ConsoleNotifier.WriteToConsole_Info(string.Format("A {0} o registo {1} - {2}", regime.ID != -1 ? "atualizar" : "criar", regime.Codigo, regime.Nome));

                    regime.ID = RCUDB.RCU.Utilizador_Regimes_IU(con, regime.ToRegimeUpdate(AppConfigSettings.AppSettings.Service.User, true));
                }
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                throw new Exception("Não foi possível sincronizar os regimes.");
            }

            return true;
        }
        /// <summary>
        /// Sincroniza os regimes.
        /// </summary>
        /// <param name="con">Ligação SQL ao RCU</param>
        /// <param name="regimesGIAF">Lista de regimes do GIAF</param>
        /// <returns></returns>
        private static bool SincronizarRegimesTabelaParam(SqlConnection con, IEnumerable<Regime> regimesGIAF, bool forceSync = false)
        {
            MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("SincronizarRegimesTabelaParam()");
            try
            {
                var regimesRCU = RCUDB.RCU.Utilizador_Regimes_LS(con);

                var regimesToUpdate = regimesGIAF.Where(x => forceSync || (!regimesRCU.Select(y => y.Codigo).Contains(x.Codigo) || x.DataModificadoObject > regimesRCU.Single(y => y.Codigo == x.Codigo).DataModificadoObject)).ToList();

                var output = "";

                foreach (var tipo in regimesToUpdate)
                {
                    if (regimesRCU.Select(x => x.Codigo).Contains(tipo.Codigo))
                    {
                        tipo.ID = regimesRCU.Single(x => x.Codigo == tipo.Codigo).ID;
                    }

                    output += string.Format("A processar o regime {0} - {1}{2}", tipo.Codigo, tipo.Nome, Environment.NewLine);
                }

                var regimesParam = regimesToUpdate.ToDBType(AppConfigSettings.AppSettings.Service.User, true);
                var res = RCUDB.RCU.Utilizador_Regimes_IU_TabelaEntrada(con, regimesParam);

                foreach (var r in res)
                {
                    output += string.Format("{0} o regime {1} - {2}{3}", r.Accao == IRCUAcessoDB.Returns.RCUDBReturnValue.AccaoEnum.Update ? "Atualizado" : "Criado", r.ID, r.Codigo, Environment.NewLine);
                }

                foreach (var l in output.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries))
                {
                    MaestroNotifier.ConsoleNotifier.WriteToConsole_Info(l);
                }
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                throw new Exception("Não foi possível sincronizar os regimes.", ex);
            }

            return true;
        }
        /// <summary>
        /// Sincroniza as entidades
        /// </summary>
        /// <param name="con">Ligação SQL ao RCU</param>
        /// <param name="entidadesGIAF">Lista de entidades do GIAF</param>
        /// <returns></returns>
        private static bool SincronizarEntidades(SqlConnection con, IEnumerable<Entidade> entidadesGIAF, bool forceSync = false)
        {
            MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("SincronizarEntidades()");
            try
            {
                var entidadesRCU = RCUDB.RCU.Entidades_LS(con);

                var entidadesToUpdate = entidadesGIAF.Where(x => forceSync || (!entidadesRCU.Select(y => y.Codigo).Contains(x.Codigo) || x.DataModificadoObject > entidadesRCU.Single(y => y.Codigo == x.Codigo).DataModificadoObject)).ToList();

                foreach (var entidade in entidadesToUpdate)
                {
                    if (entidadesRCU.Select(x => x.Codigo).Contains(entidade.Codigo))
                    {
                        entidade.ID = entidadesRCU.Single(x => x.Codigo == entidade.Codigo).ID;
                    }

                    MaestroNotifier.ConsoleNotifier.WriteToConsole_Info(string.Format("A {0} o registo {1} - {2}", entidade.ID != -1 ? "atualizar" : "criar", entidade.Codigo, entidade.Nome));

                    entidade.ID = RCUDB.RCU.Entidades_IU(con, entidade.ToEntidadeUpdate(AppConfigSettings.AppSettings.Service.User, true));
                }
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                throw new Exception("Não foi possível sincronizar as entidades.", ex);
            }

            return true;
        }
        /// <summary>
        /// Sincroniza as entidades
        /// </summary>
        /// <param name="con">Ligação SQL ao RCU</param>
        /// <param name="entidadesGIAF">Lista de entidades do GIAF</param>
        /// <returns></returns>
        private static bool SincronizarEntidadesTabelaParam(SqlConnection con, IEnumerable<Entidade> entidadesGIAF, bool forceSync = false)
        {
            MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("SincronizarEntidadesTabelaParam()");
            try
            {
                var entidadesRCU = RCUDB.RCU.Entidades_LS(con);

                var entidadesToUpdate = entidadesGIAF.Where(x => forceSync || (!entidadesRCU.Select(y => y.Codigo).Contains(x.Codigo) || x.DataModificadoObject > entidadesRCU.Single(y => y.Codigo == x.Codigo).DataModificadoObject)).ToList();

                var output = "";

                foreach (var tipo in entidadesToUpdate)
                {
                    if (entidadesRCU.Select(x => x.Codigo).Contains(tipo.Codigo))
                    {
                        tipo.ID = entidadesRCU.Single(x => x.Codigo == tipo.Codigo).ID;
                    }

                    output += string.Format("A processar a entidade {0} - {1}{2}", tipo.Codigo, tipo.Nome, Environment.NewLine);
                }

                var entidadesParam = entidadesToUpdate.ToDBType(AppConfigSettings.AppSettings.Service.User, true);
                var res = RCUDB.RCU.Entidades_IU_TabelaEntrada(con, entidadesParam);

                foreach (var r in res)
                {
                    output += string.Format("{0} a entidade {1} - {2}{3}", r.Accao == IRCUAcessoDB.Returns.RCUDBReturnValue.AccaoEnum.Update ? "Atualizada" : "Criada", r.ID, r.Codigo, Environment.NewLine);
                }

                foreach (var l in output.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries))
                {
                    MaestroNotifier.ConsoleNotifier.WriteToConsole_Info(l);
                }
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                throw new Exception("Não foi possível sincronizar as entidades.", ex);
            }

            return true;
        }
        /// <summary>
        /// Sincroniza as categorias.
        /// </summary>
        /// <param name="con">Ligação SQL ao RCU</param>
        /// <param name="categoriasGIAF">Lista de categorias do GIAF.</param>
        /// <returns></returns>
        private static bool SincronizarCategorias(SqlConnection con, IEnumerable<Categoria> categoriasGIAF, bool forceSync = false)
        {
            MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("SincronizarCategorias()");
            try
            {
                var categoriasRCU = RCUDB.RCU.Utilizador_Categorias_LS(con);
                var carreirasRCU = RCUDB.RCU.Utilizador_Carreiras_LS(con);

                var categoriasToUpdate = categoriasGIAF.Where(x => forceSync || (!categoriasRCU.Select(y => y.Codigo).Contains(x.Codigo) || x.DataModificadoObject > categoriasRCU.Single(y => y.Codigo == x.Codigo).DataModificadoObject)).ToList();

                foreach (var categoria in categoriasToUpdate)
                {
                    if (categoriasRCU.Select(x => x.Codigo).Contains(categoria.Codigo))
                    {
                        categoria.ID = categoriasRCU.Single(x => x.Codigo == categoria.Codigo).ID;
                    }
                    if (categoria.Carreira != null && carreirasRCU.Select(x => x.Codigo).Contains(categoria.Carreira.Codigo))
                    {
                        categoria.Carreira = carreirasRCU.Single(x => x.Codigo == categoria.Carreira.Codigo);
                    }
                    MaestroNotifier.ConsoleNotifier.WriteToConsole_Info(string.Format("A {0} o registo {1} - {2}", categoria.ID != -1 ? "atualizar" : "criar", categoria.Codigo, categoria.Nome));

                    categoria.ID = RCUDB.RCU.Utilizador_Categorias_IU(con, categoria.ToCategoriaUpdate(AppConfigSettings.AppSettings.Service.User, true));
                }
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                throw new Exception("Não foi possível sincronizar as categorias.", ex);
            }

            return true;
        }
        /// <summary>
        /// Sincroniza as categorias.
        /// </summary>
        /// <param name="con">Ligação SQL ao RCU</param>
        /// <param name="categoriasGIAF">Lista de categorias do GIAF.</param>
        /// <returns></returns>
        private static bool SincronizarCategoriasTabelaParam(SqlConnection con, IEnumerable<Categoria> categoriasGIAF, bool forceSync = false)
        {
            MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("SincronizarCategoriasTabelaParam()");
            try
            {
                var categoriasRCU = RCUDB.RCU.Utilizador_Categorias_LS(con);

                var categoriasToUpdate = categoriasGIAF.Where(x => forceSync || (!categoriasRCU.Select(y => y.Codigo).Contains(x.Codigo) || x.DataModificadoObject > categoriasRCU.Single(y => y.Codigo == x.Codigo).DataModificadoObject)).ToList();

                var output = "";

                foreach (var tipo in categoriasToUpdate)
                {
                    if (categoriasRCU.Select(x => x.Codigo).Contains(tipo.Codigo))
                    {
                        tipo.ID = categoriasRCU.Single(x => x.Codigo == tipo.Codigo).ID;
                    }

                    output += string.Format("A processar a categoria {0} - {1}{2}", tipo.Codigo, tipo.Nome, Environment.NewLine);
                }

                var categoriasParam = categoriasToUpdate.ToDBType(AppConfigSettings.AppSettings.Service.User, true);
                var res = RCUDB.RCU.Utilizador_Categorias_IU_TabelaEntrada(con, categoriasParam);

                foreach (var r in res)
                {
                    output += string.Format("{0} a categoria {1} - {2}{3}", r.Accao == IRCUAcessoDB.Returns.RCUDBReturnValue.AccaoEnum.Update ? "Atualizada" : "Criada", r.ID, r.Codigo, Environment.NewLine);
                }

                foreach (var l in output.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries))
                {
                    MaestroNotifier.ConsoleNotifier.WriteToConsole_Info(l);
                }
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                throw new Exception("Não foi possível sincronizar as categorias.", ex);
            }

            return true;
        }
        /// <summary>
        /// Sincroniza as carreiras, ofc.
        /// </summary>
        /// <param name="con">Ligação SQL ao RCU</param>
        /// <param name="carreirasGIAF">Lista de carreiras do GIAF</param>
        /// <param name="carreirasGruposGIAF">Lista de grupos de carreiras do GIAF</param>
        /// <returns></returns>
        private static bool SincronizarCarreiras(SqlConnection con, IEnumerable<Carreira> carreirasGIAF, IEnumerable<Carreira.GrupoCarreira> carreirasGruposGIAF, bool forceSync = false)
        {
            MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("SincronizarCarreiras()");
            try
            {
                var carreirasRCU = RCUDB.RCU.Utilizador_Carreiras_LS(con);
                var carreirasGruposRCU = RCUDB.RCU.Utilizador_Carreiras_Grupos_LS(con);

                var carreirasToUpdate = carreirasGIAF.Where(x => forceSync || (!carreirasRCU.Select(y => y.Codigo).Contains(x.Codigo) || x.DataModificadoObject > carreirasRCU.Single(y => y.Codigo == x.Codigo).DataModificadoObject)).ToList();

                var carreirasGruposToUpdate = carreirasGruposGIAF.Where(x => forceSync || (!carreirasGruposRCU.Select(y => y.Codigo).Contains(x.Codigo) || x.DataModificadoObject > carreirasGruposRCU.Single(y => y.Codigo == x.Codigo).DataModificadoObject)).ToList();

                foreach (var grupo in carreirasGruposToUpdate)
                {
                    if (carreirasGruposRCU.Select(x => x.Codigo).Contains(grupo.Codigo))
                    {
                        grupo.ID = carreirasGruposRCU.Single(x => x.Codigo == grupo.Codigo).ID;
                    }

                    MaestroNotifier.ConsoleNotifier.WriteToConsole_Info(string.Format("A {0} o registo {1} - {2}", grupo.ID != -1 ? "atualizar" : "criar", grupo.Codigo, grupo.Nome));

                    //o valor é atribuído ao ID duas vezes porque a primeira serve para mapear os dados de fora com os que já temos, e a segunda é para ficar com o ID caso tenham sido criados dados
                    grupo.ID = RCUDB.RCU.Utilizador_Carreiras_Grupos_IU(con, grupo.ToGrupoCarreiraUpdate(AppConfigSettings.AppSettings.Service.User, true));
                }


                foreach (var carreira in carreirasToUpdate)
                {
                    if (carreirasRCU.Select(x => x.Codigo).Contains(carreira.Codigo))
                    {
                        carreira.ID = carreirasRCU.Single(x => x.Codigo == carreira.Codigo).ID;
                    }
                    if (carreira.Grupo != null)
                    {
                        if (carreirasGruposRCU.Select(x => x.Codigo).Contains(carreira.Grupo.Codigo))
                        {
                            carreira.Grupo = carreirasGruposRCU.Single(x => x.Codigo == carreira.Grupo.Codigo);
                        }
                        else if (carreirasGruposToUpdate.Select(x => x.Codigo).Contains(carreira.Grupo.Codigo))
                        {
                            carreira.Grupo = carreirasGruposToUpdate.Single(x => x.Codigo == carreira.Grupo.Codigo);
                        }
                    }

                    MaestroNotifier.ConsoleNotifier.WriteToConsole_Info(string.Format("A {0} o registo {1} - {2}", carreira.ID != -1 ? "atualizar" : "criar", carreira.Codigo, carreira.Nome));

                    carreira.ID = RCUDB.RCU.Utilizador_Carreiras_IU(con, carreira.ToCarreiraUpdate(AppConfigSettings.AppSettings.Service.User, true));
                }
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                throw new Exception("Não foi possível sincronizar as carreiras.", ex);
            }

            return true;
        }
        /// <summary>
        /// Sincroniza as carreiras, ofc.
        /// </summary>
        /// <param name="con">Ligação SQL ao RCU</param>
        /// <param name="carreirasGIAF">Lista de carreiras do GIAF</param>
        /// <param name="carreirasGruposGIAF">Lista de grupos de carreiras do GIAF</param>
        /// <returns></returns>
        private static bool SincronizarCarreirasTabelaParam(SqlConnection con, IEnumerable<Carreira> carreirasGIAF, IEnumerable<Carreira.GrupoCarreira> carreirasGruposGIAF, bool forceSync = false)
        {
            MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("SincronizarCarreirasTabelaParam()");
            try
            {
                var carreirasRCU = RCUDB.RCU.Utilizador_Carreiras_LS(con);
                var carreirasGruposRCU = RCUDB.RCU.Utilizador_Carreiras_Grupos_LS(con);

                var carreirasToUpdate = carreirasGIAF.Where(x => forceSync || (!carreirasRCU.Select(y => y.Codigo).Contains(x.Codigo) || x.DataModificadoObject > carreirasRCU.Single(y => y.Codigo == x.Codigo).DataModificadoObject)).ToList();

                var carreirasGruposToUpdate = carreirasGruposGIAF.Where(x => forceSync || (!carreirasGruposRCU.Select(y => y.Codigo).Contains(x.Codigo) || x.DataModificadoObject > carreirasGruposRCU.Single(y => y.Codigo == x.Codigo).DataModificadoObject)).ToList();

                var output = "";

                foreach (var grupo in carreirasGruposToUpdate)
                {
                    if (carreirasGruposRCU.Select(x => x.Codigo).Contains(grupo.Codigo))
                    {
                        grupo.ID = carreirasGruposRCU.Single(x => x.Codigo == grupo.Codigo).ID;
                    }

                    output += string.Format("A processar o grupo de carreiras {0} - {1}{2}", grupo.Codigo, grupo.Nome, Environment.NewLine);
                }

                var gruposParam = carreirasGruposToUpdate.ToDBType(AppConfigSettings.AppSettings.Service.User, true);
                var resGrupos = RCUDB.RCU.Utilizador_Carreiras_Grupos_IU_TabelaEntrada(con, gruposParam);

                foreach (var g in resGrupos)
                {
                    carreirasGruposToUpdate.Single(x => x.Codigo == g.Codigo).ID = g.ID;
                    output += string.Format("{0} o grupo de carreiras {1} - {2}{3}", g.Accao == IRCUAcessoDB.Returns.RCUDBReturnValue.AccaoEnum.Update ? "Atualizado" : "Criado", g.ID, g.Codigo, Environment.NewLine);
                }

                foreach (var carreira in carreirasToUpdate)
                {
                    if (carreirasRCU.Select(x => x.Codigo).Contains(carreira.Codigo))
                    {
                        carreira.ID = carreirasRCU.Single(x => x.Codigo == carreira.Codigo).ID;
                    }
                    if (carreira.Grupo != null)
                    {
                        if (carreirasGruposRCU.Select(x => x.Codigo).Contains(carreira.Grupo.Codigo))
                        {
                            carreira.Grupo = carreirasGruposRCU.Single(x => x.Codigo == carreira.Grupo.Codigo);
                        }
                        else if (carreirasGruposToUpdate.Select(x => x.Codigo).Contains(carreira.Grupo.Codigo))
                        {
                            carreira.Grupo = carreirasGruposToUpdate.Single(x => x.Codigo == carreira.Grupo.Codigo);
                        }
                    }

                    output += string.Format("A processar a carreira {0} - {1}{2}", carreira.Codigo, carreira.Nome, Environment.NewLine);
                }

                var carreirasParam = carreirasToUpdate.ToDBType(AppConfigSettings.AppSettings.Service.User, true);
                var resCarreiras = RCUDB.RCU.Utilizador_Carreiras_IU_TabelaEntrada(con, carreirasParam);

                foreach (var c in resCarreiras)
                {
                    output += string.Format("{0} a carreira {1} - {2}{3}", c.Accao == IRCUAcessoDB.Returns.RCUDBReturnValue.AccaoEnum.Update ? "Atualizada" : "Criada", c.ID, c.Codigo, Environment.NewLine);
                }

                foreach (var l in output.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries))
                {
                    MaestroNotifier.ConsoleNotifier.WriteToConsole_Info(l);
                }
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                throw new Exception("Não foi possível sincronizar as carreiras.", ex);
            }

            return true;
        }
        /// <summary>
        /// Sincroniza as situacoes, ofc.
        /// </summary>
        /// <param name="con">Ligação SQL ao RCU</param>
        /// <param name="situacoesGIAF">Lista de situacoes do GIAF</param>
        /// <returns></returns>
        private static bool SincronizarSituacoes(SqlConnection con, IEnumerable<Situacao> situacoesGIAF, bool forceSync = false)
        {
            MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("SincronizarSituacoes()");
            try
            {
                var situacoesRCU = RCUDB.RCU.Utilizador_Situacoes_LS(con);

                var situacoesToUpdate = situacoesGIAF.Where(x => forceSync || (!situacoesRCU.Select(y => y.Codigo).Contains(x.Codigo) || x.DataModificadoObject > situacoesRCU.Single(y => y.Codigo == x.Codigo).DataModificadoObject)).ToList();

                foreach (var situacao in situacoesToUpdate)
                {
                    if (situacoesRCU.Select(x => x.Codigo).Contains(situacao.Codigo))
                    {
                        situacao.ID = situacoesRCU.Single(x => x.Codigo == situacao.Codigo).ID;
                    }

                    MaestroNotifier.ConsoleNotifier.WriteToConsole_Info(string.Format("A {0} o registo {1} - {2}", situacao.ID != -1 ? "atualizar" : "criar", situacao.Codigo, situacao.Nome));

                    situacao.ID = RCUDB.RCU.Utilizador_Situacoes_IU(con, situacao.ToSituacaoUpdate(AppConfigSettings.AppSettings.Service.User, true));
                }
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                throw new Exception("Não foi possível sincronizar as situações.", ex);
            }

            return true;
        }
        /// <summary>
        /// Sincroniza as situacoes, ofc.
        /// </summary>
        /// <param name="con">Ligação SQL ao RCU</param>
        /// <param name="situacoesGIAF">Lista de situacoes do GIAF</param>
        /// <returns></returns>
        private static bool SincronizarSituacoesTabelaParam(SqlConnection con, IEnumerable<Situacao> situacoesGIAF, bool forceSync = false)
        {
            MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("SincronizarSituacoesTabelaParam()");
            try
            {
                var situacoesRCU = RCUDB.RCU.Utilizador_Situacoes_LS(con);

                var situacoesToUpdate = situacoesGIAF.Where(x => forceSync || (!situacoesRCU.Select(y => y.Codigo).Contains(x.Codigo) || x.DataModificadoObject > situacoesRCU.Single(y => y.Codigo == x.Codigo).DataModificadoObject)).ToList();

                var output = "";

                foreach (var tipo in situacoesToUpdate)
                {
                    if (situacoesRCU.Select(x => x.Codigo).Contains(tipo.Codigo))
                    {
                        tipo.ID = situacoesRCU.Single(x => x.Codigo == tipo.Codigo).ID;
                    }

                    output += string.Format("A processar a situacao {0} - {1}{2}", tipo.Codigo, tipo.Nome, Environment.NewLine);
                }

                var situacoesParam = situacoesToUpdate.ToDBType(AppConfigSettings.AppSettings.Service.User, true);
                var res = RCUDB.RCU.Utilizador_Situacoes_IU_TabelaEntrada(con, situacoesParam);

                foreach (var r in res)
                {
                    output += string.Format("{0} a situacao {1} - {2}{3}", r.Accao == IRCUAcessoDB.Returns.RCUDBReturnValue.AccaoEnum.Update ? "Atualizada" : "Criada", r.ID, r.Codigo, Environment.NewLine);
                }

                foreach (var l in output.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries))
                {
                    MaestroNotifier.ConsoleNotifier.WriteToConsole_Info(l);
                }
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                throw new Exception("Não foi possível sincronizar as situacoes.", ex);
            }

            return true;
        }
        /// <summary>
        /// Sincroniza os tipos de perfil.
        /// </summary>
        /// <param name="con">Ligação SQL ao RCU</param>
        /// <param name="tiposGIAF">Lista de tipos de perfil do GIAF</param>
        /// <returns></returns>
        private static bool SincronizarTiposPerfil(SqlConnection con, IEnumerable<Perfil.TipoPerfil> tiposGIAF, bool forceSync = false)
        {
            MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("SincronizarTiposPerfil()");
            try
            {
                var tiposRCU = RCUDB.RCU.Utilizador_Perfil_Tipo_LS(con);

                var tiposToUpdate = tiposGIAF.Where(x => forceSync || (!tiposRCU.Select(y => y.Codigo).Contains(x.Codigo) || x.DataModificadoObject > tiposRCU.Single(y => y.Codigo == x.Codigo).DataModificadoObject)).ToList();

                foreach (var tipo in tiposToUpdate)
                {
                    if (tiposRCU.Select(x => x.Codigo).Contains(tipo.Codigo))
                    {
                        tipo.ID = tiposRCU.Single(x => x.Codigo == tipo.Codigo).ID;
                    }

                    MaestroNotifier.ConsoleNotifier.WriteToConsole_Info(string.Format("A {0} o registo {1} - {2}", tipo.ID != -1 ? "atualizar" : "criar", tipo.Codigo, tipo.Nome));

                    tipo.ID = RCUDB.RCU.Utilizador_Perfil_Tipo_IU(con, tipo.ToTipoPerfilUpdate(AppConfigSettings.AppSettings.Service.User, true));
                }
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                throw new Exception("Não foi possível sincronizar os tipos de perfil.", ex);
            }

            return true;
        }
        /// <summary>
        /// Sincroniza os tipos de perfil com uma tabela como parâmetro de entrada
        /// </summary>
        /// <param name="con">Ligação SQL ao RCU</param>
        /// <param name="tiposGIAF">Lista de tipos de perfil do GIAF</param>
        /// <returns></returns>
        private static bool SincronizarTiposPerfilTabelaParam(SqlConnection con, IEnumerable<Perfil.TipoPerfil> tiposGIAF, bool forceSync = false)
        {
            MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("SincronizarTiposPerfilTabelaParam()");
            try
            {
                var tiposRCU = RCUDB.RCU.Utilizador_Perfil_Tipo_LS(con);

                var tiposToUpdate = tiposGIAF.Where(x => forceSync || (!tiposRCU.Select(y => y.Codigo).Contains(x.Codigo) || x.DataModificadoObject > tiposRCU.Single(y => y.Codigo == x.Codigo).DataModificadoObject)).ToList();

                var output = "";

                foreach (var tipo in tiposToUpdate)
                {
                    if (tiposRCU.Select(x => x.Codigo).Contains(tipo.Codigo))
                    {
                        tipo.ID = tiposRCU.Single(x => x.Codigo == tipo.Codigo).ID;
                    }

                    output += string.Format("A processar o tipo de perfil {0} - {1}{2}", tipo.Codigo, tipo.Nome, Environment.NewLine);
                }

                var tiposPerfilParam = tiposToUpdate.ToDBType(AppConfigSettings.AppSettings.Service.User, true);
                var res = RCUDB.RCU.Utilizador_Perfil_Tipo_IU_TabelaEntrada(con, tiposPerfilParam);

                foreach (var r in res)
                {
                    output += string.Format("{0} o tipo de perfil {1} - {2}{3}", r.Accao == IRCUAcessoDB.Returns.RCUDBReturnValue.AccaoEnum.Update ? "Atualizado" : "Criado", r.ID, r.Codigo, Environment.NewLine);
                }

                foreach (var l in output.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries))
                {
                    MaestroNotifier.ConsoleNotifier.WriteToConsole_Info(l);
                }
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                throw new Exception("Não foi possível sincronizar os tipos de perfil.", ex);
            }

            return true;
        }
        /// <summary>
        /// Sincroniza os funcionários
        /// </summary>
        /// <param name="con">Ligação SQL ao RCU</param>
        /// <param name="funcionariosGIAF">Lista de funcionários do GIAF</param>
        /// <returns></returns>
        private static bool SincronizarFuncionarios(SqlConnection con, IEnumerable<Utilizador> funcionariosGIAF, bool forceSync = false)
        {
            MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("SincronizarFuncionarios()");
            try
            {
                var funcionariosRCU = RCUDB.RCU.Utilizador_LS(con);

                var carreirasRCU = RCUDB.RCU.Utilizador_Carreiras_LS(con);
                var categoriasRCU = RCUDB.RCU.Utilizador_Categorias_LS(con);
                var regimesRCU = RCUDB.RCU.Utilizador_Regimes_LS(con);
                var situacoesRCU = RCUDB.RCU.Utilizador_Situacoes_LS(con);
                var entidadesRCU = RCUDB.RCU.Entidades_LS(con);
                var tiposPerfilRCU = RCUDB.RCU.Utilizador_Perfil_Tipo_LS(con);

                var funcionariosToUpdate = funcionariosGIAF.Where(x => forceSync || (!funcionariosRCU.Select(y => y.NIF).Contains(x.NIF) || x.DataModificadoObject > funcionariosRCU.Single(y => y.NIF == x.NIF).DataModificadoObject || x.DataCriadoObject > funcionariosRCU.Select(z => z.DataCriadoObject).Max())).ToList();

                foreach (var funcionario in funcionariosToUpdate)
                {
                    if (funcionariosRCU.Select(x => x.NIF).Contains(funcionario.NIF))
                    {
                        var fRCU = funcionariosRCU.Single(x => x.NIF == funcionario.NIF);
                        funcionario.ID = fRCU.ID;
                        funcionario.IUPI = fRCU.IUPI;

                        foreach (var perfil in funcionario.Perfil)
                        {
                            if (fRCU.Perfil.Select(x => x.NumeroMecanografico).Contains(perfil.NumeroMecanografico))
                            {
                                var pRCU = fRCU.Perfil.Single(x => x.NumeroMecanografico == perfil.NumeroMecanografico);

                                perfil.ID = pRCU.ID;
                            }
                        }
                    }

                    MaestroNotifier.ConsoleNotifier.WriteToConsole_Info(string.Format("A {0} o funcionário {1} - {2}", funcionario.ID > 0 ? "atualizar" : "criar", funcionario.NIF, funcionario.Nome));

                    funcionario.IUPI = RCUDB.RCU.Utilizador_IU(con, funcionario.ToUtilizadorUpdate(AppConfigSettings.AppSettings.Service.User, true));

                    foreach (var perfil in funcionario.Perfil)
                    {
                        perfil.IUPI = funcionario.IUPI;

                        MaestroNotifier.ConsoleNotifier.WriteToConsole_Info(string.Format("A {0} o perfil {1} - {2} do funcionário {3}", perfil.ID > 0 ? "atualizar" : "criar", perfil.NumeroMecanografico, funcionario.Username, funcionario.Nome));

                        if (perfil.Carreira != null && carreirasRCU.Select(x => x.Codigo).Contains(perfil.Carreira.Codigo))
                        {
                            perfil.Carreira.ID = carreirasRCU.Single(x => x.Codigo == perfil.Carreira.Codigo).ID;
                        }

                        if (perfil.Categoria != null && categoriasRCU.Select(x => x.Codigo).Contains(perfil.Categoria.Codigo))
                        {
                            perfil.Categoria.ID = categoriasRCU.Single(x => x.Codigo == perfil.Categoria.Codigo).ID;
                        }

                        if (perfil.Entidade != null && entidadesRCU.Select(x => x.Codigo).Contains(perfil.Entidade.Codigo))
                        {
                            perfil.Entidade.ID = entidadesRCU.Single(x => x.Codigo == perfil.Entidade.Codigo).ID;
                        }

                        if (perfil.Regime != null && regimesRCU.Select(x => x.Codigo).Contains(perfil.Regime.Codigo))
                        {
                            perfil.Regime.ID = regimesRCU.Single(x => x.Codigo == perfil.Regime.Codigo).ID;
                        }

                        if (perfil.Situacao != null && situacoesRCU.Select(x => x.Codigo).Contains(perfil.Situacao.Codigo))
                        {
                            perfil.Situacao.ID = situacoesRCU.Single(x => x.Codigo == perfil.Situacao.Codigo).ID;
                        }

                        if (perfil.Tipo != null && tiposPerfilRCU.Select(x => x.Codigo).Contains(perfil.Tipo.Codigo))
                        {
                            perfil.Tipo.ID = tiposPerfilRCU.Single(x => x.Codigo == perfil.Tipo.Codigo).ID;
                        }

                        perfil.ID = RCUDB.RCU.Utilizador_Perfil_IU(con, perfil.ToPerfilUpdate(AppConfigSettings.AppSettings.Service.User, true));
                    }
                }
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                throw new Exception("Não foi possível sincronizar os funcionários.", ex);
            }

            return true;
        }
        /// <summary>
        /// Sincroniza os funcionários com uma tabela como parâmetro de entrada
        /// </summary>
        /// <param name="conRCU">Ligação SQL ao RCU</param>
        /// <param name="funcionariosGIAF">Lista de funcionários do GIAF</param>
        /// <returns></returns>
        private static bool SincronizarFuncionariosTabelaParam(SqlConnection conRCU, SqlConnection conProxy, IEnumerable<Utilizador> funcionariosGIAF, bool forceSync = false)
        {
            MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("SincronizarFuncionariosTabelaParam()");
            try
            {
                var funcionariosRCU = RCUDB.RCU.Utilizador_LS(conRCU, true);

                var carreirasRCU = RCUDB.RCU.Utilizador_Carreiras_LS(conRCU);
                var categoriasRCU = RCUDB.RCU.Utilizador_Categorias_LS(conRCU);
                var regimesRCU = RCUDB.RCU.Utilizador_Regimes_LS(conRCU);
                var situacoesRCU = RCUDB.RCU.Utilizador_Situacoes_LS(conRCU);
                var entidadesRCU = RCUDB.RCU.Entidades_LS(conRCU);
                var tiposPerfilRCU = RCUDB.RCU.Utilizador_Perfil_Tipo_LS(conRCU);

                var funcionariosToUpdate = funcionariosGIAF.Where(x => x.Ativo && (forceSync || (!funcionariosRCU.Select(y => y.NIF).Contains(x.NIF) || x.DataModificadoObject > funcionariosRCU.Single(y => y.NIF == x.NIF).DataModificadoObject || x.DataCriadoObject > funcionariosRCU.Select(z => z.DataCriadoObject).Max()))).ToList();
                var funcionariosToDelete = funcionariosRCU.Where(x => x.Ativo && !funcionariosGIAF.Select(f => f.NIF).Contains(x.NIF)).ToList();

                //No GIAF não existe nome personalizado, por põe-se o que estava no RCU
                foreach (var f in funcionariosToUpdate.Where(x => !string.IsNullOrWhiteSpace(x.Username)))
                {
                    f.NomePersonalizado = SPMLOperations.GetDisplayName(f.Username);
                }

                var perfisGIAF = funcionariosGIAF.SelectMany(f => f.Perfil).ToList();

                foreach (var f in funcionariosRCU)
                {
                    bool originalAtivo = f.Ativo;

                    foreach (var p in f.Perfil)
                    {
                        p.Ativo = perfisGIAF.Select(giaf => giaf.NumeroMecanografico).Contains(p.NumeroMecanografico);
                    }

                    f.Ativo = funcionariosGIAF.Select(fGIAF => fGIAF.NIF).Contains(f.NIF) && f.Perfil.Where(x=>x.Ativo).Count() > 0;

                    if (funcionariosToUpdate.Select(x => x.NIF).Contains(f.NIF))
                    {
                        funcionariosToUpdate.Single(x => x.NIF == f.NIF).Ativo = f.Ativo;
                    }
                    else if (originalAtivo != f.Ativo) funcionariosToUpdate.Add(f);
                }

                //var deixaVer = funcionariosToUpdate.Where(x => x.Username == "hparedes").ToList();
                //var deixaVerOsApagados = funcionariosToUpdate.Where(x => !x.Ativo).ToList();
                //var deixaVerOsPerfisApagados = funcionariosToUpdate.SelectMany(x => x.Perfil).Where(x => !x.Ativo).ToList();

                var output = "";

                foreach (var fGIAF in funcionariosToUpdate)
                {
                    if (!string.IsNullOrWhiteSpace(fGIAF.Username) && LDAPTools.Exchange.CheckUsernameExists(fGIAF.Username))
                    {
                        var aliasInfo = LDAPTools.Exchange.GetAlias(fGIAF.Username);

                        if (aliasInfo.HasAlias)
                        {
                            fGIAF.OriginalEmail = aliasInfo.OriginalEmail;
                            fGIAF.AliasedEmail = aliasInfo.AliasedEmail;
                        }
                    }

                    if (funcionariosRCU.Select(x => x.NIF).Contains(fGIAF.NIF))
                    {
                        var fRCU = funcionariosRCU.Single(x => x.NIF == fGIAF.NIF);
                        fGIAF.ID = fRCU.ID;
                        fGIAF.IUPI = fRCU.IUPI;
                        fGIAF.Visibilidade = fRCU.Visibilidade;

                        foreach (var perfil in fGIAF.Perfil)
                        {
                            if (fRCU.Perfil.Select(x => x.NumeroMecanografico).Contains(perfil.NumeroMecanografico))
                            {
                                var pRCU = fRCU.Perfil.Single(x => x.NumeroMecanografico == perfil.NumeroMecanografico);

                                perfil.ID = pRCU.ID;
                                perfil.Visibilidade = pRCU.Visibilidade;

                            }
                        }
                    }

                    output += string.Format("A processar o funcionário {0} - {1}{2}", fGIAF.NIF, fGIAF.Nome, Environment.NewLine);

                    //Inserir estes na mesma, mas apagá-los a seguir
                    if (string.IsNullOrWhiteSpace(fGIAF.Username)) funcionariosToDelete.Add(fGIAF);
                }

                var usersParam = funcionariosToUpdate.ToDBType(AppConfigSettings.AppSettings.Service.User, true);
                var resFunc = RCUDB.RCU.Utilizador_IU_TabelaEntrada(conRCU, usersParam);

                foreach (var f in resFunc)
                {
                    output += string.Format("{0} o funcionário {1} - {2}{3}", f.Accao == IRCUAcessoDB.Returns.RCUDBReturnValue.AccaoEnum.Update ? "Atualizado" : "Criado", f.NIF, funcionariosToUpdate.Single(upd => upd.NIF == f.NIF).Nome, Environment.NewLine);
                }

                var perfisParam = new List<UtilizadorPerfilType>();

                foreach (var fGIAF  in funcionariosToUpdate)
                {
                    fGIAF.IUPI = resFunc.Single(f => f.NIF == fGIAF.NIF).IUPI;

                    foreach (var perfil in fGIAF.Perfil)
                    {
                        perfil.IUPI = fGIAF.IUPI;

                        if (perfil.Carreira != null && carreirasRCU.Select(x => x.Codigo).Contains(perfil.Carreira.Codigo))
                        {
                            perfil.Carreira.ID = carreirasRCU.Single(x => x.Codigo == perfil.Carreira.Codigo).ID;
                        }

                        if (perfil.Categoria != null && categoriasRCU.Select(x => x.Codigo).Contains(perfil.Categoria.Codigo))
                        {
                            perfil.Categoria.ID = categoriasRCU.Single(x => x.Codigo == perfil.Categoria.Codigo).ID;
                        }

                        if (perfil.Entidade != null && entidadesRCU.Select(x => x.Codigo).Contains(perfil.Entidade.Codigo))
                        {
                            perfil.Entidade.ID = entidadesRCU.Single(x => x.Codigo == perfil.Entidade.Codigo).ID;
                        }

                        if (perfil.Regime != null && regimesRCU.Select(x => x.Codigo).Contains(perfil.Regime.Codigo))
                        {
                            perfil.Regime.ID = regimesRCU.Single(x => x.Codigo == perfil.Regime.Codigo).ID;
                        }

                        if (perfil.Situacao != null && situacoesRCU.Select(x => x.Codigo).Contains(perfil.Situacao.Codigo))
                        {
                            perfil.Situacao.ID = situacoesRCU.Single(x => x.Codigo == perfil.Situacao.Codigo).ID;
                        }

                        if (perfil.Tipo != null && tiposPerfilRCU.Select(x => x.Codigo).Contains(perfil.Tipo.Codigo))
                        {
                            perfil.Tipo.ID = tiposPerfilRCU.Single(x => x.Codigo == perfil.Tipo.Codigo).ID;
                        }

                        output += string.Format("A processar o perfil {0} - {1} do funcionário {2}{3}", perfil.NumeroMecanografico, fGIAF.Username ?? fGIAF.OriginalEmail, fGIAF.Nome, Environment.NewLine);
                        perfisParam.Add(perfil.ToDBType(AppConfigSettings.AppSettings.Service.User, true));
                    }
                }

                var resPerf = RCUDB.RCU.Utilizador_Perfil_IU_TabelaEntrada(conRCU, perfisParam);

                foreach (var p in resPerf)
                {
                    output += string.Format("{0} o perfil {1} - {2}{3}", p.Accao == IRCUAcessoDB.Returns.RCUDBReturnValue.AccaoEnum.Update ? "Atualizado" : "Criado", p.ID, p.NMec, Environment.NewLine);
                }

                if (!AppConfigSettings.Debug)
                {
                    ProxyDB.GIAF.IUPI_U_TabelaEntrada(conProxy, funcionariosToUpdate.ToNMecIUPIDBType());
                }

                //foreach (var p in perfisToDelete)
                //{
                //    output += string.Format("A eliminar o perfil {0} (IUPI: {1} - NMec: {2}). Não listado no acesso ao GIAF (Docentes com Erasmus, por ex.){3}", p.ID, p.IUPI, p.NumeroMecanografico, Environment.NewLine);
                //    RCUDB.RCU.Utilizador_Perfil_D(con, p.IUPI.Value, p.ID, AppConfigSettings.AppSettings.Service.User);
                //}

                //foreach (var f in funcionariosToDelete)
                //{
                //    output += string.Format("A eliminar o utilizador {0} (NIF: {1}). Não existe no GIAF (sem username, por ex.){2}", f.IUPI, f.NIF, Environment.NewLine);
                //    RCUDB.RCU.Utilizador_D(con, f.IUPI.Value, AppConfigSettings.AppSettings.Service.User);
                //}

                foreach (var l in output.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries))
                {
                    MaestroNotifier.ConsoleNotifier.WriteToConsole_Info(l);
                }
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                throw new Exception("Não foi possível sincronizar os funcionários.", ex);
            }

            return true;
        }
        private static bool SincronizarContactos()
        {
            MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("SincronizarContactos()");
            try
            {
                using (var con = new SqlConnection(AppConfigSettings.ConnectionStrings.Proxy))
                {
                    if (con.State != System.Data.ConnectionState.Open) con.Open();
                    using (var tran = con.BeginTransaction())
                    {
                        try
                        {
                            using (var cmd = new SqlCommand("syncEnt", con))
                            {
                                //WORK



                            }

                            tran.Commit();
                        }
                        catch (Exception ex)
                        {
                            tran.Rollback();
                            MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Error);

                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                return false;
            }

            return true;
        }
    }
}
