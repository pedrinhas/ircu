﻿using IRCUAcessoDB.Error;
using IRCUAssemblies.Utilizadores;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCUSharePointMaestroSynchronizer.SharePointAcessoDB
{
    public static class SharePointDB
    {
        public static void SyncUsersToGroup(string groupName, IEnumerable<SharePointGroupUser> users)
        {
            try
            {
                using (var con = new SqlConnection(AppConfigSettings.ConnectionStrings.SharePointIntra))
                using (var cmd = new SqlCommand("", con))
                {
                    var sb = new StringBuilder();

                    string usersIN = "";
                    foreach (var user in users)
                    {
                        usersIN += string.Format("'{0}', ", user.LoginName);
                    }
                    usersIN = usersIN.Trim(new char[] { ' ', ',' });

                    sb.AppendLine("begin transaction");
                    sb.AppendLine("begin try");
                    sb.AppendLine("declare @groupID int");
                    sb.AppendLine("SELECT @groupID=[ID] FROM [WSS_Content_b138837a72074aa9b3f2469dc535d4b3].[dbo].[Groups] where siteid = 'ea522518-4454-45cb-8139-cc92e654caa9' and title = 'Docentes'");
                    sb.AppendLine("if @groupID is not null");
                    sb.AppendLine("begin");
                    sb.AppendLine("delete from [dbo].[GroupMembership] where siteid = 'ea522518-4454-45cb-8139-cc92e654caa9' and GroupID = @groupID and MemberId <> 1 ");
                    sb.AppendLine("insert into [dbo].[GroupMembership] (SiteId, GroupId, MemberId)");
                    sb.AppendLine("select 'ea522518-4454-45cb-8139-cc92e654caa9', @groupID, tp_ID");
                    sb.AppendLine(string.Format("from UserInfo where tp_Login in ({0})", usersIN));

                    #region old
                    //sb.AppendLine("delete from [dbo].[GroupMembership_20180103] where siteid = 'ea522518-4454-45cb-8139-cc92e654caa9' and GroupID = @groupID and MemberId <> 1 ");
                    //sb.AppendLine("insert into [dbo].[GroupMembership_20180103] (SiteId, GroupId, MemberId)");
                    #endregion


                    sb.AppendLine("end");
                    sb.AppendLine("else");
                    sb.AppendLine("begin");
                    sb.AppendLine("RAISERROR('O grupo Docentes não existe', 16, 2)");
                    sb.AppendLine("end");

                    sb.AppendLine("commit");
                    sb.AppendLine("--rollback");
                    sb.AppendLine("end try");
                    sb.AppendLine("begin catch");
                    sb.AppendLine("rollback");
                    sb.AppendLine("select ERROR_NUMBER() AS ErrorNumber");
                    sb.AppendLine(",ERROR_SEVERITY() AS ErrorSeverity");
                    sb.AppendLine(",ERROR_STATE() AS ErrorState");
                    sb.AppendLine(",ERROR_PROCEDURE() AS ErrorProcedure");
                    sb.AppendLine(",ERROR_LINE() AS ErrorLine");
                    sb.AppendLine(",ERROR_MESSAGE() AS ErrorMessage;");
                    sb.AppendLine("end catch");

                    cmd.CommandText = sb.ToString();
                    cmd.CommandType = System.Data.CommandType.Text;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    if (reader != null && reader.FieldCount > 0 && !string.IsNullOrWhiteSpace(reader.GetName(0)))
                    {
                        throw new DBErrorException(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void SyncUsersToGroupDocentes(IEnumerable<SharePointGroupUser> users)
        {
            try
            {
                SyncUsersToGroup(AppConfigSettings.AppSettings.SharePoint.GroupDocentes, users);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
