﻿using IRCUAssemblies.LDAP;
using IRCUAssemblies.Utilizadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCUSharePointMaestroSynchronizer
{
    public static class Extensions
    {
        public static IEnumerable<SharePointGroupUser> ToSharePointUsers(this IEnumerable<Utilizador> users)
        {
            foreach (var u in users) yield return u.ToSharePointUser();
        }

        public static SharePointGroupUser ToSharePointUser(this Utilizador user)
        {
            try
            {
                var uPerfil = user.Perfil.Where(x => 
                    x.Tipo != null && 
                    new string[] { "01", "02" }.Contains(x.Tipo.Codigo) && 
                    x.EmFuncoes).First();

                if (uPerfil != null)
                {
                    return new SharePointGroupUser()
                    {
                        Email = user.Email,
                        LoginName = string.Format(@"INTRA\{0}", user.Username),
                        Name = user.Nome
                    };
                }
                else
                {
                    throw new Exception(string.Format("O utilizador [{0}] {1} não tem perfil de docente ou investigador. Listagem errada?", user.IUPI, user.Nome));
                }
            }
            catch (Exception ex)
            {
                //LOG
                return null;
            }
        }
        public static IEnumerable<IEnumerable<T>> Split<T>(this IEnumerable<T> things, int nSize = 100)
        {
            for (int i = 0; i < things.Count(); i += nSize)
            {
                yield return things.ToList().GetRange(i, Math.Min(nSize, things.Count() - i));
            }
        } 
    }
}
