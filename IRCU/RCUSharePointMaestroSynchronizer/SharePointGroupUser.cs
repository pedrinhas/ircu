﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RCUSharePointMaestroSynchronizer
{
    [Serializable]
    [XmlRoot(ElementName="User", Namespace="http://schemas.microsoft.com/sharepoint/soap/directory/")]
    public class SharePointGroupUser
    {
        [XmlAttribute("ID")]
        public int ID { get; set; }
        [XmlAttribute("Sid")]
        public string Sid { get; set; }
        [XmlAttribute("Name")]
        public string Name { get; set; }
        [XmlAttribute("LoginName")]
        public string LoginName { get; set; }
        [XmlAttribute("Email")]
        public string Email { get; set; }
        [XmlAttribute("Notes")]
        public string Notes { get; set; }
        [XmlAttribute("IsSiteAdmin")]
        public string IsSiteAdminXmlField { get; set; }
        public bool IsSiteAdmin { get { return Convert.ToBoolean(IsSiteAdminXmlField.ToLower()); } set { IsSiteAdminXmlField = value.ToString(); } }
        [XmlAttribute("IsDomainGroup")]
        public string IsDomainGroupXmlField { get; set; }
        public bool IsDomainGroup { get { return Convert.ToBoolean(IsDomainGroupXmlField.ToLower()); } set { IsDomainGroupXmlField = value.ToString(); } }

        [XmlAttribute("Flags")]
        public int Flags { get; set; }

        public static SharePointGroupUser FromXML(string xml)
        {
            using (TextReader reader = new StringReader(xml))
            {
                var serializer = new XmlSerializer(typeof(SharePointGroupUser));
                var r = (SharePointGroupUser)serializer.Deserialize(reader);

                return r;
            }
        }
    }
}
