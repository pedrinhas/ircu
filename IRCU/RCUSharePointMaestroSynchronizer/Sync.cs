﻿using IRCUAcessoDB;
using IRCUAssemblies.Utilizadores;
using MaestroStuff.Notifications;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

//"<User ID=\"2076\" Sid=\"S-1-5-21-101738387-955820755-3571844629-17550\" Name=\"Cláudio Pereira\" LoginName=\"INTRA\\cpereira\" Email=\"cpereira@utad.pt\" Notes=\"\" IsSiteAdmin=\"False\" IsDomainGroup=\"False\" Flags=\"0\" xmlns=\"http://schemas.microsoft.com/sharepoint/soap/directory/\" />"

namespace RCUSharePointMaestroSynchronizer
{
    public class Sync
    {
        private const string dateTimeFormat = "yyyy-MM-dd HH:mm:ss";

        static void Main(string[] args)
        {
            try
            {
                var inicioMsg = "Início.";

                MaestroNotifier.ConsoleNotifier.WriteToConsole_Start(inicioMsg);

                SynchronizeBD();

                MaestroNotifier.ConsoleNotifier.WriteToConsole_End("Fim da sincronização");
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex, MaestroStuff.Enums.Erro.Severidade.Critical);
                MaestroNotifier.ConsoleNotifier.WriteToConsole_End("Fim da sincronização. Erro, ver logs.");
            }
        }

        private static bool SynchronizeBD()
        {
            try
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("Método SynchronizeBD");

                var usersFromRCU = GetUsersFromRCU().ToSharePointUsers();

                if (usersFromRCU.Count() > 0)
                {
                    SharePointAcessoDB.SharePointDB.SyncUsersToGroupDocentes(usersFromRCU);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro no método SynchronizeBD", ex);
            }
        }
        private static bool SynchronizeSOAP()
        {
            try
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("Método SynchronizeSOAP");

                SharePointUserGroupService.UserGroupSoapClient client = new SharePointUserGroupService.UserGroupSoapClient();

                var NTLM = (client.Endpoint.Binding as System.ServiceModel.BasicHttpBinding).Security.Transport.ClientCredentialType == System.ServiceModel.HttpClientCredentialType.Ntlm;

                if(NTLM)
                {
                    client.ClientCredentials.UserName.UserName = AppConfigSettings.AppSettings.SharePoint.User;
                    client.ClientCredentials.UserName.Password = AppConfigSettings.AppSettings.SharePoint.Password;
                    client.ClientCredentials.Windows.ClientCredential.Domain = AppConfigSettings.AppSettings.SharePoint.Domain;
                    client.ClientCredentials.Windows.ClientCredential.UserName = AppConfigSettings.AppSettings.SharePoint.User;
                    client.ClientCredentials.Windows.ClientCredential.Password = AppConfigSettings.AppSettings.SharePoint.Password;
                }
                else
                {
                    client.ClientCredentials.Windows.ClientCredential.Domain = AppConfigSettings.AppSettings.SharePoint.Domain;
                    client.ClientCredentials.Windows.ClientCredential.UserName = AppConfigSettings.AppSettings.SharePoint.User;
                    client.ClientCredentials.Windows.ClientCredential.Password = AppConfigSettings.AppSettings.SharePoint.Password;
                }


                client.Open();

                var usersFromRCU = GetUsersFromRCU().ToSharePointUsers();
                if(usersFromRCU.Count() > 0)
                {
                    if (DeleteUsersFromSharePoint(client))
                    {
                        return AddUsersToSharePoint(client, usersFromRCU);
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro no método SynchronizeSOAP", ex);
            }
        }

        private static bool AddUsersToSharePoint(SharePointUserGroupService.UserGroupSoapClient client, IEnumerable<SharePointGroupUser> users)
        {
            try
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole_Info(string.Format("Método AddUsersToSharePoint ({0} utilizador{1})", users.Count(), users.Count() == 1 ? "" : "es"));

                foreach (var users100 in users.Split())
                {

                    #region Make XML init

                    var doc = new XmlDocument();

                    var declaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                    var root = doc.DocumentElement;
                    doc.InsertBefore(declaration, root);

                    var mainElement = doc.CreateElement(string.Empty, "Users", string.Empty);
                    doc.AppendChild(mainElement);

                    #endregion

                    //foreach (var user in users100.Where(x => !x.LoginName.Contains("emidi") && !x.LoginName.Contains("mota")))
                    foreach (var user in users100)
                    {
                        #region Add to XML

                        var userXML = doc.CreateElement(string.Empty, "User", string.Empty);

                        var attrLoginName = doc.CreateAttribute(string.Empty, "LoginName", string.Empty);
                        attrLoginName.Value = user.LoginName;
                        userXML.Attributes.Append(attrLoginName);
                        var attrName = doc.CreateAttribute(string.Empty, "Name", string.Empty);
                        attrName.Value = user.Name;
                        userXML.Attributes.Append(attrName);
                        var attrEmail = doc.CreateAttribute(string.Empty, "Email", string.Empty);
                        attrEmail.Value = user.Email;
                        userXML.Attributes.Append(attrEmail);

                        mainElement.AppendChild(userXML);

                        #endregion

                        //client.AddUserToGroup(AppConfigSettings.AppSettings.SharePoint.GroupDocentes, user.Name, user.LoginName, user.Email, "Sincronizado pelo RCU");
                    }

                    client.AddUserCollectionToGroup(AppConfigSettings.AppSettings.SharePoint.GroupDocentes, System.Xml.Linq.XElement.Load(new StringReader(doc.DocumentElement.OuterXml)));
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro no método AddUsersToSharePoint", ex);
            }
        }
        private static bool DeleteUsersFromSharePoint(SharePointUserGroupService.UserGroupSoapClient client)
        {
            try
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("DeleteUsersFromSharePoint");

                var groupUsers = client.GetUserCollectionFromGroup("Docentes");

                var doc = new XmlDocument();
                doc.LoadXml(groupUsers.ToString());

                var allUsers = new List<SharePointGroupUser>();
                var root = doc.GetElementsByTagName("GetUserCollectionFromGroup")[0];
                if (root != null)
                {
                    var array = root.ChildNodes.Cast<XmlNode>().Single(x => x.Name == "Users").ChildNodes.Cast<XmlNode>().Where(x => x.Name == "User").ToList();
                    foreach (var xmlUser in array)
                    {
                        allUsers.Add(SharePointGroupUser.FromXML(xmlUser.OuterXml));
                    }
                }

                var users = allUsers.Where(x => x.LoginName.ToLowerInvariant() != "intra\\administrator").ToList();

                //foreach (var user in users)
                //{
                //    client.RemoveUserFromGroup("Docentes", user.LoginName);
                //}

                foreach (var users100 in users.Split())
                {

                    #region Make XML init
                    /*
                         <Users>
                            <User LoginName=
                            "Domain\User1_Alias" 
                            Email="User1_E-mail" 
                            Name="User1_Display_Name" 
                            Notes="Notes"/>
                         </Users>
                         */

                    var docToDel = new XmlDocument();

                    var declaration = docToDel.CreateXmlDeclaration("1.0", "UTF-8", null);
                    var rootToDel = docToDel.DocumentElement;
                    docToDel.InsertBefore(declaration, rootToDel);

                    var mainElement = docToDel.CreateElement(string.Empty, "Users", string.Empty);
                    docToDel.AppendChild(mainElement);

                    #endregion

                    foreach (var user in users100)
                    {
                        #region Add to XML

                        var userXML = docToDel.CreateElement(string.Empty, "User", string.Empty);

                        var attrLoginName = docToDel.CreateAttribute(string.Empty, "LoginName", string.Empty);
                        attrLoginName.Value = user.LoginName;
                        userXML.Attributes.Append(attrLoginName);

                        mainElement.AppendChild(userXML);

                        #endregion

                        //client.AddUserToGroup(AppConfigSettings.AppSettings.SharePoint.GroupDocentes, user.Name, user.LoginName, user.Email, "Sincronizado pelo RCU");
                    }

                    client.RemoveUserCollectionFromGroup(AppConfigSettings.AppSettings.SharePoint.GroupDocentes, System.Xml.Linq.XElement.Load(new StringReader(docToDel.DocumentElement.OuterXml)));
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro no método DeleteUsersFromSharePoint", ex);
            }
        }

        private static IEnumerable<Utilizador> GetUsersFromRCU()
        {
            try
            {
                var users01 = RCUDB.RCU.Utilizador_PorTipoPerfil_S(AppConfigSettings.ConnectionStrings.RCU, "01").Where(x => x.EmFuncoes);
                var users02 = RCUDB.RCU.Utilizador_PorTipoPerfil_S(AppConfigSettings.ConnectionStrings.RCU, "02").Where(x => x.EmFuncoes);

                return users01.Union(users02);
            }
            catch (Exception ex)
            {
                //Tratar exceção
                return new List<Utilizador>();
            }
        }

        private static void Test()
        {
            try
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("Método Test");

                SharePointUserGroupService.UserGroupSoapClient client = new SharePointUserGroupService.UserGroupSoapClient();

                client.ClientCredentials.Windows.ClientCredential.Domain = AppConfigSettings.AppSettings.SharePoint.Domain;
                client.ClientCredentials.Windows.ClientCredential.UserName = AppConfigSettings.AppSettings.SharePoint.User;
                client.ClientCredentials.Windows.ClientCredential.Password = AppConfigSettings.AppSettings.SharePoint.Password;

                client.Open();

                var groupInfo = client.GetGroupInfo("Docentes");
                var groupUsers = client.GetUserCollectionFromGroup("Docentes");

                var doc = new XmlDocument();
                doc.LoadXml(groupUsers.ToString());

                var allUsers = new List<SharePointGroupUser>();
                var root = doc.GetElementsByTagName("GetUserCollectionFromGroup")[0];
                if (root != null)
                {
                    var array = root.ChildNodes.Cast<XmlNode>().Single(x => x.Name == "Users").ChildNodes.Cast<XmlNode>().Where(x => x.Name == "User").ToList();
                    foreach (var xmlUser in array)
                    {
                        allUsers.Add(SharePointGroupUser.FromXML(xmlUser.OuterXml));
                    }
                }

                var users = allUsers.Where(x => x.LoginName.ToLowerInvariant() != "intra\\administrator").ToList();

                foreach (var user in users)
                {
                    client.RemoveUserFromGroup("Docentes", user.LoginName);
                }

                client.AddUserToGroup("Docentes", "Cláudio Pereira", "intra\\cpereira", "cpereira@utad.pt", "Teste. Apagar.");
            }
            catch (Exception ex)
            {
                throw new Exception("Erro no método Test", ex);
            }
        }
    }
}
