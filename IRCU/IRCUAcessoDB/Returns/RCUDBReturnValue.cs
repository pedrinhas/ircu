﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAcessoDB.Returns
{
    public class RCUDBReturnValue
    {
        public RCUDBReturnValue(string accao)
        {
            Accao = accao.ToLowerInvariant() == "i" ? AccaoEnum.Insert : AccaoEnum.Update;
        }
        public AccaoEnum Accao { get; set; }

        public enum AccaoEnum { Insert, Update }
    }
}
