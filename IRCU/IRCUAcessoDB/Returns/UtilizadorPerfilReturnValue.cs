﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAcessoDB.Returns
{
    public class UtilizadorPerfilReturnValue : RCUDBReturnValue
    {
        public UtilizadorPerfilReturnValue(string nmec, int id, string accao) : base(accao)
        {
            NMec = nmec;
            ID = id;
        }
        public string NMec { get; set; }
        public int ID { get; set; }
    }
}