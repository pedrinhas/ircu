﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAcessoDB.Returns
{
    public class VisibilidadeReturnValue : RCUDBReturnValue
    {
        public VisibilidadeReturnValue(int id, string accao)
            : base(accao)
        {
            ID = id;
        }
        public int ID { get; set; }
    }
}
