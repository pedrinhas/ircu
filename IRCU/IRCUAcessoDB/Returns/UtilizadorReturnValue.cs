﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAcessoDB.Returns
{
    public class UtilizadorReturnValue : RCUDBReturnValue
    {
        public UtilizadorReturnValue(string nif, Guid iupi, string accao) : base(accao)
        {
            NIF = nif;
            IUPI = iupi;
            Perfis = new List<UtilizadorPerfilReturnValue>();
        }
        public UtilizadorReturnValue(string nif, Guid iupi, IEnumerable<UtilizadorPerfilReturnValue> perfis, string accao) : this(nif, iupi, accao)
        {
            Perfis.AddRange(perfis);
        }
        public string NIF { get; set; }
        public Guid IUPI { get; set; }
        public List<UtilizadorPerfilReturnValue> Perfis { get; set; }
    }
}
