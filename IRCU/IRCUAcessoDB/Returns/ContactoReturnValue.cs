﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAcessoDB.Returns
{
    public class ContactoReturnValue : RCUDBReturnValue
    {
        public ContactoReturnValue(Guid iupi, int? propertyID, string accao)
            : base(accao)
        {
            IUPI = iupi;
            PropertyID = propertyID;
        }
        public Guid IUPI { get; set; }
        public int? PropertyID { get; set; }
    }
}
