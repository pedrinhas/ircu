﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAcessoDB.Returns
{
    public class CarreiraReturnValue : RCUDBReturnValue
    {
        public CarreiraReturnValue(string codigo, int id, string accao) : base(accao)
        {
            Codigo = codigo;
            ID = id;
        }
        public string Codigo { get; set; }
        public int ID { get; set; }
    }
}
