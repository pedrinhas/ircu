﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAcessoDB.Returns
{
    public class FlagReturnValue : RCUDBReturnValue
    {
        public FlagReturnValue(int idUtilizador, int? idPerfil, int idFlag, string accao)
            : base(accao)
        {
            IDUtilizador = idUtilizador;
            IDPerfil = idPerfil;
            IDFlag = idFlag;
        }
        public int IDUtilizador { get; set; }
        public int? IDPerfil { get; set; }
        public int IDFlag { get; set; }
    }
}
