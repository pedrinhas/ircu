﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAcessoDB.Types
{
    public class ContactoType
    {
        public Guid IUPI { get; set; }
        public int? PropertyID { get; set; }
        public int IDTipo { get; set; }
        public string Value { get; set; }
        public int Visibilidade { get; set; }
        public bool Ativo { get; set; }
        public string UtilizadorEdicao { get; set; }

        public object[] ToDataRow()
        {
            return new object[] { IUPI, PropertyID, IDTipo, Value, Visibilidade, Ativo, UtilizadorEdicao };
        }
        public static DataTable CreateDataTable()
        {
            var dt = new DataTable();

            dt.Columns.Add("IUPI", typeof(Guid));
            dt.Columns.Add("PropertyID", typeof(int));
            dt.Columns.Add("IDTipo", typeof(int));
            dt.Columns.Add("Value", typeof(string));
            dt.Columns.Add("Visibilidade", typeof(int));
            dt.Columns.Add("Ativo", typeof(bool));
            dt.Columns.Add("UtilizadorEdicao", typeof(string));

            return dt;
        }
    }
}
