﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAcessoDB.Types
{
    public class CarreiraGrupoType
    {
        public int ID { get; set; }
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public bool Ativo { get; set; }
        public string UtilizadorEdicao { get; set; }

        public object[] ToDataRow()
        {
            return new object[] { ID, Codigo, Nome, Descricao, Ativo, UtilizadorEdicao };
        }
        public static DataTable CreateDataTable()
        {
            var dt = new DataTable();

            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("Codigo", typeof(string));
            dt.Columns.Add("Nome", typeof(string));
            dt.Columns.Add("Descricao", typeof(string));
            dt.Columns.Add("Ativo", typeof(bool));
            dt.Columns.Add("UtilizadorEdicao", typeof(string));

            return dt;
        }
    }
}
