﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAcessoDB.Types
{
    public class UtilizadorPerfilType
    {
        public int ID { get; set; }
        public Guid IUPI { get; set; }
        public string NMec { get; set; }
        public int? IDTipo { get; set; }
        public int? IDCategoria { get; set; }
        public int? IDEntidade { get; set; }
        public int? IDCarreira { get; set; }
        public int? IDSituacao { get; set; }
        public int? IDRegime { get; set; }
        public DateTime? DataEntrada { get; set; }
        public DateTime? DataSaida { get; set; }
        public bool Ativo { get; set; }
        public string UtilizadorEdicao { get; set; }

        public object[] ToDataRow()
        {
            return new object[] { ID, IUPI, NMec, IDTipo, IDCategoria, IDEntidade, IDCarreira, IDSituacao, IDRegime, DataEntrada, DataSaida, Ativo, UtilizadorEdicao };
        }
        public static DataTable CreateDataTable()
        {
            var dt = new DataTable();

            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("IUPI", typeof(Guid));
            dt.Columns.Add("NMec", typeof(string));
            dt.Columns.Add("IDTipo", typeof(int));
            dt.Columns.Add("IDCategoria", typeof(int));
            dt.Columns.Add("IDEntidade", typeof(int));
            dt.Columns.Add("IDCarreira", typeof(int));
            dt.Columns.Add("IDSituacao", typeof(int));
            dt.Columns.Add("IDRegime", typeof(int));
            dt.Columns.Add("DataEntrada", typeof(DateTime));
            dt.Columns.Add("DataSaida", typeof(DateTime));
            dt.Columns.Add("Ativo", typeof(bool));
            dt.Columns.Add("UtilizadorEdicao", typeof(string));

            return dt;
        }
    }

}


/*
 
[ID] [int] NULL,
[IUPI] [uniqueidentifier] NULL,
[Username] [nvarchar](50) NULL,
[Email] [nvarchar](60) NULL,
[NMec] [nvarchar](10) NULL,
[IDTipo] [int] NULL,
[IDCategoria] [int] NULL,
[IDEntidade] [int] NULL,
[IDCarreira] [int] NULL,
[IDSituacao] [int] NULL,
[IDRegime] [int] NULL,
[DataEntrada] [datetime] NULL,
[DataSaida] [datetime] NULL,
[UtilizadorEdicao] [nvarchar](max) NULL

*/