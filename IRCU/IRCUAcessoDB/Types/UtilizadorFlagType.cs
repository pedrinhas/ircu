﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAcessoDB.Types
{
    public class UtilizadorFlagType
    {
        public int ID { get; set; }
        public int IDUtilizador { get; set; }
        public int? IDPerfil { get; set; }
        public int IDFlag { get; set; }
        public string Value { get; set; }
        public bool Ativo { get; set; }
        public string UtilizadorEdicao { get; set; }

        public object[] ToDataRow()
        {
            return new object[] { ID, IDUtilizador, IDPerfil, IDFlag, Value, Ativo, UtilizadorEdicao };
        }
        public static DataTable CreateDataTable()
        {
            var dt = new DataTable();

            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("IDUtilizador", typeof(int));
            dt.Columns.Add("IDPerfil", typeof(int));
            dt.Columns.Add("IDFlag", typeof(int));
            dt.Columns.Add("Value", typeof(string));
            dt.Columns.Add("Ativo", typeof(bool));
            dt.Columns.Add("UtilizadorEdicao", typeof(string));

            return dt;
        }
    }
}
