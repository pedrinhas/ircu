﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAcessoDB.Types
{
    public class UtilizadorType
    {
        public Guid? IUPI { get; set; }
        public string Nome { get; set; }
        public string NomePersonalizado { get; set; }
        public string NIF { get; set; }
        public string Username { get; set; }
        public string OriginalEmail { get; set; }
        public string AliasedEmail { get; set; }
        public bool ContaNoGiaf { get; set; }
        public int Visibilidade { get; set; }
        public bool Ativo { get; set; }
        public string UtilizadorEdicao { get; set; }

        public object[] ToDataRow()
        {
            return new object[] { IUPI, Nome, NomePersonalizado, NIF, Username, OriginalEmail, AliasedEmail, ContaNoGiaf, Visibilidade, Ativo, UtilizadorEdicao };
        }
        public static DataTable CreateDataTable()
        {
            var dt = new DataTable();

            dt.Columns.Add("IUPI", typeof(Guid));
            dt.Columns.Add("Nome", typeof(string));
            dt.Columns.Add("NomePersonalizado", typeof(string));
            dt.Columns.Add("NIF", typeof(string));
            dt.Columns.Add("Username", typeof(string));
            dt.Columns.Add("OriginalEmail", typeof(string));
            dt.Columns.Add("AliasedEmail", typeof(string));
            dt.Columns.Add("ContaNoGiaf", typeof(bool));
            dt.Columns.Add("Visibilidade", typeof(int));
            dt.Columns.Add("Ativo", typeof(bool));
            dt.Columns.Add("UtilizadorEdicao", typeof(string));

            return dt;
        }
    }
}
