﻿using IRCUAssemblies.Utilizadores;
using IRCUAcessoDB.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCUAcessoDB
{
    public static class Extensions
    {
        #region SQL Types
        public static CarreiraGrupoType ToDBType(this Carreira.GrupoCarreira grupo, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            var res = new CarreiraGrupoType();

            res.ID = grupo.ID;
            res.Codigo = grupo.Codigo;
            res.Nome = grupo.Nome;
            res.Descricao = grupo.Descricao;
            res.Ativo = grupo.Ativo;
            res.UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, res.UtilizadorEdicao) : utilizadorEdicao;

            return res;
        }
        public static CarreiraType ToDBType(this Carreira carreira, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            var res = new CarreiraType();

            res.ID = carreira.ID;
            res.Codigo = carreira.Codigo;
            res.Nome = carreira.Nome;
            res.Descricao = carreira.Descricao;
            res.IDGrupo = carreira.Grupo != null ? carreira.Grupo.ID : -1;
            res.Ativo = carreira.Ativo;
            res.UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, res.UtilizadorEdicao) : utilizadorEdicao;

            return res;
        }
        public static CategoriaType ToDBType(this Categoria categoria, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            var res = new CategoriaType();

            res.ID = categoria.ID;
            res.Codigo = categoria.Codigo;
            res.Nome = categoria.Nome;
            res.Descricao = categoria.Descricao;
            res.CodCarreira = categoria.Carreira != null ? categoria.Carreira.Codigo : null;
            res.Ativo = categoria.Ativo;
            res.UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, res.UtilizadorEdicao) : utilizadorEdicao;

            return res;
        }
        public static EntidadeType ToDBType(this Entidade entidade, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            var res = new EntidadeType();

            res.ID = entidade.ID;
            res.Codigo = entidade.Codigo;
            res.Nome = entidade.Nome;
            res.Descricao = entidade.Descricao;
            res.Ativo = entidade.Ativo;
            res.UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, res.UtilizadorEdicao) : utilizadorEdicao;
            res.Sigla = entidade.Sigla;

            return res;
        }
        public static PerfilTipoType ToDBType(this Perfil.TipoPerfil tipoPerfil, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            var res = new PerfilTipoType();

            res.ID = tipoPerfil.ID;
            res.Codigo = tipoPerfil.Codigo;
            res.Nome = tipoPerfil.Nome;
            res.Descricao = tipoPerfil.Descricao;
            res.Ativo = tipoPerfil.Ativo;
            res.UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, res.UtilizadorEdicao) : utilizadorEdicao;

            return res;
        }
        public static RegimeType ToDBType(this Regime regime, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            var res = new RegimeType();

            res.ID = regime.ID;
            res.Codigo = regime.Codigo;
            res.Nome = regime.Nome;
            res.Descricao = regime.Descricao;
            res.Ativo = regime.Ativo;
            res.UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, res.UtilizadorEdicao) : utilizadorEdicao;

            return res;
        }
        public static SituacaoType ToDBType(this Situacao situacao, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            var res = new SituacaoType();

            res.ID = situacao.ID;
            res.Codigo = situacao.Codigo;
            res.Nome = situacao.Nome;
            res.Descricao = situacao.Descricao;
            res.PRC = situacao.PRC;
            res.Ativo = situacao.Ativo;
            res.UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, res.UtilizadorEdicao) : utilizadorEdicao;

            return res;
        }
        public static UtilizadorPerfilType ToDBType(this Perfil perfil, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            var res = new UtilizadorPerfilType();

            res.ID = perfil.ID;
            res.IUPI = perfil.IUPI.Value;
            res.NMec = perfil.NumeroMecanografico;
            res.IDTipo = perfil.Tipo != null ? perfil.Tipo.ID : (int?)null;
            res.IDCategoria = perfil.Categoria != null ? perfil.Categoria.ID : (int?)null;
            res.IDEntidade = perfil.Entidade != null ? perfil.Entidade.ID : (int?)null;
            res.IDCarreira = perfil.Carreira != null ? perfil.Carreira.ID : (int?)null;
            res.IDSituacao = perfil.Situacao != null ? perfil.Situacao.ID : (int?)null;
            res.IDRegime = perfil.Regime != null ? perfil.Regime.ID : (int?)null;
            res.DataEntrada = string.IsNullOrWhiteSpace(perfil.DataEntrada) ? (DateTime?)null : DateTime.Parse(perfil.DataEntrada);
            res.DataSaida = string.IsNullOrWhiteSpace(perfil.DataSaida) ? (DateTime?)null : DateTime.Parse(perfil.DataSaida);
            res.Ativo = perfil.Ativo;
            res.UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, res.UtilizadorEdicao) : utilizadorEdicao;

            return res;
        }
        public static UtilizadorType ToDBType(this Utilizador user, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            var res = new UtilizadorType();

            res.IUPI = user.IUPI;
            res.Nome = user.Nome;
            res.NomePersonalizado = user.NomePersonalizado;
            res.NIF = user.NIF;
            res.Username = user.Username;
            res.OriginalEmail = user.OriginalEmail;
            res.AliasedEmail = user.AliasedEmail;
            res.ContaNoGiaf = user.ContaNoGIAF;
            res.Visibilidade = (user.Visibilidade ?? new Visibilidade() { ID = 1 }).ID;
            res.Ativo = user.Ativo;
            res.UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, res.UtilizadorEdicao) : utilizadorEdicao;

            return res;
        }
        public static ContactoType ToDBType(this IRCUAssemblies.Contactos.Contacto contacto, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            var res = new ContactoType();

            res.IUPI = contacto.IUPI;
            res.PropertyID = contacto.PropertyID;
            res.IDTipo = contacto.Tipo.ID; //nunca devia ser null
            res.Value = contacto.Valor;
            res.Visibilidade = contacto.Visibilidade.ID;
            res.Ativo = contacto.Ativo;
            res.UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, res.UtilizadorEdicao) : utilizadorEdicao;

            return res;
        }
        public static UtilizadorFlagType ToDBType(this UtilizadorFlag flag, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            var res = new UtilizadorFlagType();

            res.IDUtilizador = flag.IDUtilizador;
            res.IDPerfil = flag.IDPerfil;
            res.IDFlag = flag.Flag.ID; //nunca devia ser null
            res.Value = flag.Value;
            res.Ativo = flag.Ativo;
            res.UtilizadorEdicao = keepOriginalUtilizadorModificado ? string.Format("{0} - {1}", utilizadorEdicao, res.UtilizadorEdicao) : utilizadorEdicao;

            return res;
        }

        public static List<UtilizadorFlagType> ToDBType(this IEnumerable<UtilizadorFlag> flags, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            return flags.Select(x => x.ToDBType(utilizadorEdicao, keepOriginalUtilizadorModificado)).ToList();
        }
        public static List<ContactoType> ToDBType(this IEnumerable<IRCUAssemblies.Contactos.Contacto> contactos, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            return contactos.Select(x => x.ToDBType(utilizadorEdicao, keepOriginalUtilizadorModificado)).ToList();
        }
        public static List<CarreiraGrupoType> ToDBType(this IEnumerable<Carreira.GrupoCarreira> grupos, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            return grupos.Select(x => x.ToDBType(utilizadorEdicao, keepOriginalUtilizadorModificado)).ToList();
        }
        public static List<CarreiraType> ToDBType(this IEnumerable<Carreira> carreiras, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            return carreiras.Select(x => x.ToDBType(utilizadorEdicao, keepOriginalUtilizadorModificado)).ToList();
        }
        public static List<CategoriaType> ToDBType(this IEnumerable<Categoria> categorias, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            return categorias.Select(x => x.ToDBType(utilizadorEdicao, keepOriginalUtilizadorModificado)).ToList();
        }
        public static List<EntidadeType> ToDBType(this IEnumerable<Entidade> entidades, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            return entidades.Select(x => x.ToDBType(utilizadorEdicao, keepOriginalUtilizadorModificado)).ToList();
        }
        public static List<PerfilTipoType> ToDBType(this IEnumerable<Perfil.TipoPerfil> tiposPerfil, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            return tiposPerfil.Select(x => x.ToDBType(utilizadorEdicao, keepOriginalUtilizadorModificado)).ToList();
        }
        public static List<RegimeType> ToDBType(this IEnumerable<Regime> regimes, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            return regimes.Select(x => x.ToDBType(utilizadorEdicao, keepOriginalUtilizadorModificado)).ToList();
        }
        public static List<SituacaoType> ToDBType(this IEnumerable<Situacao> situacoes, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            return situacoes.Select(x => x.ToDBType(utilizadorEdicao, keepOriginalUtilizadorModificado)).ToList();
        }
        public static List<UtilizadorPerfilType> ToDBType(this IEnumerable<Perfil> perfis, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            return perfis.Select(x => x.ToDBType(utilizadorEdicao, keepOriginalUtilizadorModificado)).ToList();
        }
        public static List<UtilizadorType> ToDBType(this IEnumerable<Utilizador> users, string utilizadorEdicao, bool keepOriginalUtilizadorModificado = false)
        {
            return users.Select(x => x.ToDBType(utilizadorEdicao, keepOriginalUtilizadorModificado)).ToList();
        }
        #endregion
    }
}
