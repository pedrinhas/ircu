﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IRCUAssemblies.Toolkit;

namespace IRCUAcessoDB.Error
{
    public class DBErrorException : Exception
    {
        public DBErrorException(IDataReader reader) : base()
        {
            DBErrorNumber = reader.GetInt32("ErrorNumber").Value;                 //nullable value must have cenas. o select ERROR_NUMBER() AS ErrorNumber deve vir sem nada - [stp_Utilizador_Perfil_IU]
            DBErrorSeverity = reader.GetInt32("ErrorSeverity").Value;
            DBErrorState = reader.GetInt32("ErrorState").Value;
            DBErrorProcedure = reader.GetString("ErrorProcedure");
            DBErrorLine = reader.GetInt32("ErrorLine").Value;
            DBErrorMessage = reader.GetString("ErrorMessage");

            DBErrorIdentifier = new ErrorIdentifier()
            {
                ColumnName = reader.GetName(1),
                Value = reader[1].ToString()
            };
        }


        public int DBErrorNumber { get; set; }
        public ErrorIdentifier DBErrorIdentifier { get; set; }
        public int DBErrorSeverity { get; set; }
        public int DBErrorState { get; set; }
        public string DBErrorProcedure { get; set; }
        public int DBErrorLine { get; set; }
        public string DBErrorMessage { get; set; }

        public class ErrorIdentifier
        {
            public string ColumnName { get; set; }
            public object Value { get; set; }
        }
    }
}
