﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IRCUAssemblies.Utilizadores;
using IRCUAssemblies.Toolkit;
using IRCUAssemblies.Contactos;
using IRCUAssemblies.Contactos.SiteUTAD;
using IRCUAssemblies.Errors;
using IRCUAcessoDB.Error;
using System.Transactions;
using IRCUAcessoDB.Types;
using IRCUAcessoDB.Returns;
using System.Data;
using IRCUAssemblies;

//Ver -- DataModificadoObject = reader.GetDateTime("dataModificado"), -- em todos os métodos S

namespace IRCUAcessoDB
{
    public static class RCUDB
    {
        public static class AcademSeguranca
        {
            public static Guid GetAluno4Account(string cString, string nmec)
            {
                Guid res = Guid.Empty;

                using (SqlConnection con = new SqlConnection(cString))
                {
                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var command = new SqlCommand();

                    command.Connection = con;
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = "[dbo].[stp_Get_Aluno4Account_S]";

                    command.Parameters.Add("@numero", System.Data.SqlDbType.VarChar).Value = nmec;

                    //if ("1" == "1")
                    if (command.ExecuteNonQuery() == 1)
                    {
                        res = GetIUPIAluno(cString, nmec);
                    }
                }

                return res;
            }
            public static Guid GetFuncionario4Account(string cString, string login, string email)
            {
                Guid res = Guid.Empty;

                using (SqlConnection con = new SqlConnection(cString))
                {
                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var command = new SqlCommand();

                    command.Connection = con;
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = "[dbo].[stp_Get_Funcionario4Account_S]";

                    command.Parameters.Add("@numero", System.Data.SqlDbType.VarChar).Value = login;
                    command.Parameters.Add("@email", System.Data.SqlDbType.VarChar).Value = email;

                    if (command.ExecuteNonQuery() == 1)
                    {
                        res = GetIUPIFuncionario(cString, email.Replace("@utad.pt", "") ?? string.Format("tmp_{0}", login));
                    }
                }

                return res;
            }
            public static Guid GetIUPIAluno(string cString, string numero)
            {
                Guid res = Guid.Empty;

                using (SqlConnection con = new SqlConnection(cString))
                {
                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var command = new SqlCommand();

                    command.Connection = con;
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = "[dbo].[SP_GetIUPIAluno]";

                    command.Parameters.Add("@numero", System.Data.SqlDbType.VarChar).Value = numero;

                    if (!Guid.TryParse(command.ExecuteScalar().ToString(), out res))
                    {
                        throw new BadParseValueException("Não foi possível interpretar a resposta da BD", typeof(Guid).ToString());
                    }
                }

                return res;
            }
            public static Guid GetIUPIFuncionario(string cString, string login)
            {
                Guid res = Guid.Empty;

                using (SqlConnection con = new SqlConnection(cString))
                {
                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var command = new SqlCommand();

                    command.Connection = con;
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = "[dbo].[SP_GetIUPIFuncionario]";

                    command.Parameters.Add("@login", System.Data.SqlDbType.VarChar).Value = login;

                    if (!Guid.TryParse(command.ExecuteScalar().ToString(), out res))
                    {
                        throw new BadParseValueException("Não foi possível interpretar a resposta da BD", typeof(Guid).ToString());
                    }
                }

                return res;
            }
        }

        public static class RCU
        {
            #region Aplicacoes



            #endregion

            #region Entidades - done
            public static void Entidades_D(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Entidades_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Entidades_D(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Entidades_D(con, id, utilizadorEdicao);
                }
            }
            public static void Entidades_R(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Entidades_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Entidades_R(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Entidades_R(con, id, utilizadorEdicao);
                }
            }
            public static Entidade Entidades_PorCodigo_S(SqlConnection con, string codigo, bool incluirApagados = false)
            {
                var entidade = (Entidade)null;

                using (var cmd = new SqlCommand("[stp_Entidades_PorCodigo_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@codigo", System.Data.SqlDbType.NVarChar).Value = codigo;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.NVarChar).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (entidade == null && reader.Read())
                    {
                        entidade = new Entidade()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Sigla = reader.GetString("sigla"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var idParent = reader.GetInt32("idParent");
                        if (idParent != null && idParent.HasValue)
                        {
                            entidade.Parent = new Entidade() { ID = idParent.Value };
                        }

                        var idTipo = reader.GetInt32("idTipoEntidade");
                        if (idTipo != null && idTipo.HasValue)
                        {
                            entidade.Tipo = new Entidade.TipoEntidade() { ID = idTipo.Value };
                        }

                        var dCriado = reader.GetDateTime("dataCriado");
                        entidade.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;
                    }

                    reader.Close();

                    if (entidade.Tipo != null)
                    {
                        entidade.Tipo = Entidades_Tipo_S(con, entidade.Tipo.ID, incluirApagados);
                    }
                    if (entidade.Parent != null)
                    {
                        if (entidade.ID == entidade.Parent.ID)
                        {
                            entidade.Parent = entidade;
                        }
                        else
                        {
                            entidade.Parent = Entidades_S(con, entidade.Parent.ID, incluirApagados);
                        }
                    }
                }

                return entidade;
            }
            public static Entidade Entidades_PorCodigo_S(string cString, string codigo, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Entidades_PorCodigo_S(con, codigo, incluirApagados);
                }
            }
            public static IEnumerable<Entidade> Entidades_PorParent_S(SqlConnection con, int idParent, bool incluirApagados = false)
            {
                var res = new List<Entidade>();

                using (var cmd = new SqlCommand("[stp_Entidades_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idParent", System.Data.SqlDbType.Int).Value = idParent;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.NVarChar).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var entidade = new Entidade()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Sigla = reader.GetString("sigla"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var _idParent = reader.GetInt32("idParent");
                        if (_idParent != null && _idParent.HasValue)
                        {
                            //Assim não faz pesquisas para as entidades que já estão na lista
                            if (res.Where(x => x.ID == _idParent.Value).Count() > 0)
                            {
                                entidade.Parent = res.Single(x => x.ID == _idParent.Value);
                            }
                            else
                            {
                                entidade.Parent = new Entidade() { ID = _idParent.Value };
                            }
                        }

                        var idTipo = reader.GetInt32("idTipoEntidade");
                        if (idTipo != null && idTipo.HasValue)
                        {
                            entidade.Tipo = new Entidade.TipoEntidade() { ID = idTipo.Value };
                        }

                        var dCriado = reader.GetDateTime("dataCriado");
                        entidade.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;
                    }

                    reader.Close();

                    foreach (var entidade in res.Where(x => x.Tipo != null))
                    {
                        entidade.Tipo = Entidades_Tipo_S(con, entidade.Tipo.ID, incluirApagados);
                    }
                    foreach (var entidade in res.Where(x => x.Parent != null && x.Parent.ID != x.ID))
                    {
                        if (entidade.ID == entidade.Parent.ID)
                        {
                            entidade.Parent = entidade;
                        }
                        else
                        {
                            entidade.Parent = Entidades_S(con, entidade.Parent.ID, incluirApagados);
                        }
                    }
                }

                return res;
            }
            public static IEnumerable<Entidade> Entidades_PorParent_S(string cString, int idParent, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Entidades_PorParent_S(con, idParent, incluirApagados);
                }
            }
            public static IEnumerable<Entidade> Entidades_PorTipo_S(SqlConnection con, int idTipo, bool incluirApagados = false)
            {
                var res = new List<Entidade>();

                using (var cmd = new SqlCommand("[stp_Entidades_PorTipo_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idTipo", System.Data.SqlDbType.Int).Value = idTipo;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.NVarChar).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var entidade = new Entidade()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Sigla = reader.GetString("sigla"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var _idParent = reader.GetInt32("idParent");
                        if (_idParent != null && _idParent.HasValue)
                        {
                            //Assim não faz pesquisas para as entidades que já estão na lista
                            if (res.Where(x => x.ID == _idParent.Value).Count() > 0)
                            {
                                entidade.Parent = res.Single(x => x.ID == _idParent.Value);
                            }
                            else
                            {
                                entidade.Parent = new Entidade() { ID = _idParent.Value };
                            }
                        }

                        var _idTipo = reader.GetInt32("idTipoEntidade");
                        if (_idTipo != null && _idTipo.HasValue)
                        {
                            entidade.Tipo = new Entidade.TipoEntidade() { ID = _idTipo.Value };
                        }

                        var dCriado = reader.GetDateTime("dataCriado");
                        entidade.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;
                    }

                    reader.Close();

                    foreach (var entidade in res.Where(x => x.Tipo != null))
                    {
                        entidade.Tipo = Entidades_Tipo_S(con, entidade.Tipo.ID, incluirApagados);
                    }
                    foreach (var entidade in res.Where(x => x.Parent != null && x.Parent.ID != x.ID))
                    {
                        if (entidade.ID == entidade.Parent.ID)
                        {
                            entidade.Parent = entidade;
                        }
                        else
                        {
                            entidade.Parent = Entidades_S(con, entidade.Parent.ID, incluirApagados);
                        }
                    }
                }

                return res;
            }
            public static IEnumerable<Entidade> Entidades_PorTipo_S(string cString, int idTipo, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Entidades_PorTipo_S(con, idTipo, incluirApagados);
                }
            }
            public static Entidade Entidades_S(SqlConnection con, int id, bool incluirApagados = false)
            {
                var entidade = (Entidade)null;
                using (var cmd = new SqlCommand("[stp_Entidades_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.NVarChar).Value = id;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.NVarChar).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (entidade == null && reader.Read())
                    {
                        entidade = new Entidade()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Sigla = reader.GetString("sigla"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        entidade.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;
                    }

                    var idParent = reader.GetInt32("idParent");

                    reader.Close();

                    if (idParent != null && idParent.HasValue)
                    {
                        entidade.Parent = Entidades_S(con, idParent.Value, incluirApagados);
                    }
                }

                return entidade;
            }
            public static Entidade Entidades_S(string cString, int id, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Entidades_S(con, id, incluirApagados);
                }
            }
            public static IEnumerable<Entidade> Entidades_LS(SqlConnection con, bool incluirApagados = false)
            {
                var res = new List<Entidade>();
                var tiposEntidade = RCUDB.RCU.Entidades_Tipo_LS(con, incluirApagados);

                using (var cmd = new SqlCommand("[stp_Entidades_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.NVarChar).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var entidade = new Entidade()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Sigla = reader.GetString("sigla"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var idParent = reader.GetInt32("idParent");
                        if (idParent != null && idParent.HasValue)
                        {
                            //Assim não faz pesquisas para as entidades que já estão na lista
                            if (res.Where(x => x.ID == idParent.Value).Count() > 0)
                            {
                                entidade.Parent = res.Single(x => x.ID == idParent.Value);
                            }
                            else
                            {
                                entidade.Parent = new Entidade() { ID = idParent.Value };
                            }
                        }

                        var _idTipo = reader.GetInt32("idTipoEntidade");
                        if (_idTipo != null && _idTipo.HasValue && tiposEntidade.Select(x => x.ID).Contains(_idTipo.Value))
                        {
                            entidade.Tipo = tiposEntidade.Single(x => x.ID == _idTipo);
                        }

                        var dCriado = reader.GetDateTime("dataCriado");
                        entidade.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(entidade);
                    }

                    reader.Close();

                    foreach (var entidade in res.Where(x => x.Parent != null && x.Parent.ID != x.ID))
                    {
                        if (entidade.ID == entidade.Parent.ID)
                        {
                            entidade.Parent = entidade;
                        }
                        else
                        {
                            entidade.Parent = res.Single(x => x.ID == entidade.Parent.ID);
                        }
                    }
                }

                return res;
            }
            public static IEnumerable<Entidade> Entidades_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Entidades_LS(con, incluirApagados);
                }
            }
            public static int Entidades_IU(SqlConnection con, Entidade.EntidadeUpdate entidade)
            {
                int res = -1;
                using (var cmd = new SqlCommand("[stp_Entidades_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = entidade.ID;
                    cmd.Parameters.Add("@codigo", System.Data.SqlDbType.NVarChar).Value = entidade.Codigo;
                    cmd.Parameters.Add("@sigla", System.Data.SqlDbType.NVarChar).Value = entidade.Sigla;
                    cmd.Parameters.Add("@nome", System.Data.SqlDbType.NVarChar).Value = entidade.Nome;
                    cmd.Parameters.Add("@descricao", System.Data.SqlDbType.NVarChar).Value = entidade.Descricao;
                    cmd.Parameters.Add("@idTipoEntidade", System.Data.SqlDbType.Int).Value = entidade.IDTipo;
                    cmd.Parameters.Add("@idParent", System.Data.SqlDbType.Int).Value = entidade.IDParent;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = entidade.Ativo;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = entidade.UtilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static int Entidades_IU(string cString, Entidade.EntidadeUpdate entidade)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Entidades_IU(con, entidade);
                }
            }
            public static List<EntidadeReturnValue> Entidades_IU_TabelaEntrada(SqlConnection con, IEnumerable<EntidadeType> entidades)
            {
                using (var cmd = new SqlCommand("[stp_Entidades_IU_TabelaEntrada]", con))
                {
                    var dt = EntidadeType.CreateDataTable();

                    foreach (var e in entidades)
                    {
                        dt.Rows.Add(e.ToDataRow());
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    var param = cmd.Parameters.AddWithValue("@entidades", dt);
                    param.SqlDbType = System.Data.SqlDbType.Structured;
                    param.TypeName = "dbo.EntidadeType";

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    var res = new List<EntidadeReturnValue>();

                    while (reader.Read())
                    {
                        res.Add(new EntidadeReturnValue(reader.GetString("Codigo"), reader.GetInt32("ID").Value, reader.GetString("Accao")));
                    }

                    reader.Close();

                    return res;
                }
            }
            public static List<EntidadeReturnValue> Entidades_IU_TabelaEntrada(string cString, IEnumerable<EntidadeType> entidades)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Entidades_IU_TabelaEntrada(con, entidades);
                }
            }

            public static void Entidades_Tipo_D(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Entidades_Tipo_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Entidades_Tipo_D(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Entidades_Tipo_D(con, id, utilizadorEdicao);
                }
            }
            public static void Entidades_Tipo_R(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Entidades_Tipo_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Entidades_Tipo_R(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Entidades_Tipo_R(con, id, utilizadorEdicao);
                }
            }
            public static Entidade.TipoEntidade Entidades_Tipo_S(SqlConnection con, int id, bool incluirApagados = false)
            {
                var tipo = (Entidade.TipoEntidade)null;

                using (var cmd = new SqlCommand("[stp_Entidades_Tipo_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (tipo == null && reader.Read())
                    {
                        tipo = new Entidade.TipoEntidade()
                        {
                            ID = reader.GetInt32("id").Value,
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            DataCriadoObject = reader.GetDateTime("dataCriado").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        tipo.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;
                    }

                    reader.Close();
                }

                return tipo;
            }
            public static Entidade.TipoEntidade Entidades_Tipo_S(string cString, int id, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Entidades_Tipo_S(con, id, incluirApagados);
                }
            }
            public static IEnumerable<Entidade.TipoEntidade> Entidades_Tipo_LS(SqlConnection con, bool incluirApagados = false)
            {
                var res = new List<Entidade.TipoEntidade>();

                using (var cmd = new SqlCommand("[stp_Entidades_Tipo_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var tipo = new Entidade.TipoEntidade()
                        {
                            ID = reader.GetInt32("id").Value,
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        tipo.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(tipo);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<Entidade.TipoEntidade> Entidades_Tipo_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Entidades_Tipo_LS(con, incluirApagados);
                }
            }
            public static int Entidades_Tipo_IU(SqlConnection con, Entidade.TipoEntidade.TipoEntidadeUpdate tipo)
            {
                int res = -1;
                using (var cmd = new SqlCommand("[stp_Entidades_Tipo_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = tipo.ID;
                    cmd.Parameters.Add("@nome", System.Data.SqlDbType.NVarChar).Value = tipo.Nome;
                    cmd.Parameters.Add("@descricao", System.Data.SqlDbType.NVarChar).Value = tipo.Descricao;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = tipo.Ativo;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = tipo.UtilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static int Entidades_Tipo_IU(string cString, Entidade.TipoEntidade.TipoEntidadeUpdate tipo)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Entidades_Tipo_IU(con, tipo);
                }
            }
            #endregion

            #region Perfl - done

            public static int Utilizador_Perfil_IU(SqlConnection con, Perfil.PerfilUpdate perfil)
            {
                int res = -1;

                //int id = RCU.Utilizador_GetID(con, perfil.IUPI.Value).Value;

                using (var cmd = new SqlCommand("[stp_Utilizador_Perfil_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = perfil.IUPI;
                    cmd.Parameters.Add("@username", System.Data.SqlDbType.NVarChar).Value = perfil.Username;
                    cmd.Parameters.Add("@originalEmail", System.Data.SqlDbType.NVarChar).Value = perfil.OriginalEmail;
                    //Inserir o aliasedEmail nos contactos, caso não exista.
                    cmd.Parameters.Add("@aliasedEmail", System.Data.SqlDbType.NVarChar).Value = perfil.AliasedEmail;
                    cmd.Parameters.Add("@nmec", System.Data.SqlDbType.NVarChar).Value = perfil.NumeroMecanografico;
                    cmd.Parameters.Add("@idPerfil", System.Data.SqlDbType.Int).Value = perfil.ID;
                    cmd.Parameters.Add("@idTipo", System.Data.SqlDbType.Int).Value = perfil.IDTipo; // TIPO DEVE PODER SER NULL
                    cmd.Parameters.Add("@idCategoria", System.Data.SqlDbType.Int).Value = perfil.IDCategoria;
                    cmd.Parameters.Add("@idEntidade", System.Data.SqlDbType.Int).Value = perfil.IDEntidade;
                    cmd.Parameters.Add("@idCarreira", System.Data.SqlDbType.Int).Value = perfil.IDCarreira;
                    cmd.Parameters.Add("@idSituacao", System.Data.SqlDbType.Int).Value = perfil.IDSituacao;
                    cmd.Parameters.Add("@idRegime", System.Data.SqlDbType.Int).Value = perfil.IDRegime;
                    cmd.Parameters.Add("@dataEntrada", System.Data.SqlDbType.DateTime).Value = perfil.DataEntrada;
                    cmd.Parameters.Add("@dataSaida", System.Data.SqlDbType.DateTime).Value = perfil.DataSaida;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = perfil.Ativo;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = perfil.UtilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static int Utilizador_Perfil_IU(string cString, Perfil.PerfilUpdate perfil)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Perfil_IU(con, perfil);
                }
            }
            public static List<UtilizadorPerfilReturnValue> Utilizador_Perfil_IU_TabelaEntrada(SqlConnection con, IEnumerable<UtilizadorPerfilType> perfis)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Perfil_IU_TabelaEntrada]", con))
                {
                    var dt = UtilizadorPerfilType.CreateDataTable();

                    foreach (var p in perfis)
                    {
                        dt.Rows.Add(p.ToDataRow());
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    var param = cmd.Parameters.AddWithValue("@perfis", dt);
                    param.SqlDbType = System.Data.SqlDbType.Structured;
                    param.TypeName = "dbo.UtilizadorPerfilType";

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    var res = new List<UtilizadorPerfilReturnValue>();

                    while(reader.Read())
                    {
                        res.Add(new UtilizadorPerfilReturnValue(reader.GetString("NMec"), reader.GetInt32("ID").Value, reader.GetString("Accao")));
                    }

                    reader.Close();

                    return res;
                }
            }
            public static List<UtilizadorPerfilReturnValue> Utilizador_Perfil_IU_TabelaEntrada(string cString, IEnumerable<UtilizadorPerfilType> perfis)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Perfil_IU_TabelaEntrada(con, perfis);
                }
            }
            public static void Utilizador_Perfil_D(SqlConnection con, Guid iupi, int idPerfil, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Perfil_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = iupi;
                    cmd.Parameters.Add("@idPerfil", System.Data.SqlDbType.Int).Value = idPerfil;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Perfil_D(string cString, Guid iupi, int idPerfil, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Perfil_D(con, iupi, idPerfil, utilizadorEdicao);
                }
            }
            public static void Utilizador_Perfil_R(SqlConnection con, Guid iupi, int idPerfil, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Perfil_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = iupi;
                    cmd.Parameters.Add("@idPerfil", System.Data.SqlDbType.Int).Value = idPerfil;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Perfil_R(string cString, Guid iupi, int idPerfil, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Perfil_R(con, iupi, idPerfil, utilizadorEdicao);
                }
            }

            public static void Utilizador_Perfil_Tipo_D(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Perfil_Tipo_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Perfil_Tipo_D(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Perfil_Tipo_D(con, id, utilizadorEdicao);
                }
            }
            public static void Utilizador_Perfil_Tipo_R(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Perfil_Tipo_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Perfil_Tipo_R(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Perfil_Tipo_R(con, id, utilizadorEdicao);
                }
            }
            public static Perfil.TipoPerfil Utilizador_Perfil_Tipo_S(SqlConnection con, int id, bool incluirApagados = false)
            {
                var res = (Perfil.TipoPerfil)null;

                using (var cmd = new SqlCommand("[stp_Utilizador_Perfil_Tipo_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        res = new Perfil.TipoPerfil()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Sigla = reader.GetString("sigla"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            DataCriadoObject = reader.GetDateTime("dataCriado").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        res.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;
                    }

                    reader.Close();
                }

                return res;
            }
            public static Perfil.TipoPerfil Utilizador_Perfil_Tipo_S(string cString, int id, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Perfil_Tipo_S(con, id, incluirApagados);
                }
            }
            public static IEnumerable<Perfil.TipoPerfil> Utilizador_Perfil_Tipo_LS(SqlConnection con, bool incluirApagados = false)
            {
                var res = new List<Perfil.TipoPerfil>();

                using (var cmd = new SqlCommand("[stp_Utilizador_Perfil_Tipo_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var tipo = new Perfil.TipoPerfil()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Sigla = reader.GetString("sigla"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            DataCriadoObject = reader.GetDateTime("dataCriado").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };


                        var dCriado = reader.GetDateTime("dataCriado");
                        tipo.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(tipo);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<Perfil.TipoPerfil> Utilizador_Perfil_Tipo_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Perfil_Tipo_LS(con, incluirApagados);
                }
            }
            public static int Utilizador_Perfil_Tipo_IU(SqlConnection con, Perfil.TipoPerfil.TipoPerfilUpdate tipo)
            {
                int res = -1;
                using (var cmd = new SqlCommand("[stp_Utilizador_Perfil_Tipo_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = tipo.ID;
                    cmd.Parameters.Add("@codigo", System.Data.SqlDbType.NVarChar).Value = tipo.Codigo;
                    cmd.Parameters.Add("@sigla", System.Data.SqlDbType.NVarChar).Value = tipo.Sigla;
                    cmd.Parameters.Add("@nome", System.Data.SqlDbType.NVarChar).Value = tipo.Nome;
                    cmd.Parameters.Add("@descricao", System.Data.SqlDbType.NVarChar).Value = tipo.Descricao;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = tipo.Ativo;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = tipo.UtilizadorEdicao;
                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {

                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static int Utilizador_Perfil_Tipo_IU(string cString, Perfil.TipoPerfil.TipoPerfilUpdate tipo)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Perfil_Tipo_IU(con, tipo);
                }
            }
            public static List<PerfilTipoReturnValue> Utilizador_Perfil_Tipo_IU_TabelaEntrada(SqlConnection con, IEnumerable<PerfilTipoType> tiposPerfil)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Perfil_Tipos_IU_TabelaEntrada]", con))
                {
                    var dt = PerfilTipoType.CreateDataTable();

                    foreach (var r in tiposPerfil)
                    {
                        dt.Rows.Add(r.ToDataRow());
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    var param = cmd.Parameters.AddWithValue("@perfil_tipos", dt);
                    param.SqlDbType = SqlDbType.Structured;
                    param.TypeName = "dbo.PerfilTipoType";

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    var res = new List<PerfilTipoReturnValue>();

                    while (reader.Read())
                    {
                        res.Add(new PerfilTipoReturnValue(reader.GetString("Codigo"), reader.GetInt32("ID").Value, reader.GetString("Accao")));
                    }

                    reader.Close();

                    return res;
                }
            }
            public static List<PerfilTipoReturnValue> Utilizador_Perfil_Tipo_IU_TabelaEntrada(string cString, IEnumerable<PerfilTipoType> tiposPerfil)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Perfil_Tipo_IU_TabelaEntrada(con, tiposPerfil);
                }
            }

            #endregion

            #region Utilizador - done

            public static IEnumerable<Utilizador> Utilizador_LS(SqlConnection con, bool incluirApagados = false)
            {
                var res = new List<Utilizador>();

                var visibilidades = Utilizador_Visibilidade_LS(con);
                var flags = Utilizador_Flags_LS(con);

                using (var cmd = new SqlCommand("[stp_Utilizador_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    var lastIUPI = Guid.Empty;

                    while (reader.Read())
                    {
                        //object[] values = new object[reader.FieldCount];
                        //int numColumns = reader.GetValues(values);

                        if (!reader.IsDBNull("iupi"))
                        {
                            Guid dbIupi = reader.GetGuid("iupi").Value;
                            bool newUser = true;

                            var user = new Utilizador()
                            {
                                IUPI = Guid.Empty
                            };

                            if (res.Where(x => x.IUPI == dbIupi).Count() > 0)
                            {
                                user = res.Where(x => x.IUPI == dbIupi).First();
                                newUser = false;
                            }
                            else
                            {
                                //não fazer nada
                            }

                            if (user.IUPI == Guid.Empty)
                            {
                                user.IUPI = dbIupi;
                                user.ID = reader.GetInt32("id").Value;
                                user.Nome = reader.GetString("nome");
                                user.NomePersonalizado = reader.GetString("nomePersonalizado");
                                user.NIF = reader.GetString("nif");

                                user.Username = reader.GetString("username");
                                user.Email = reader.GetString("email");
                                user.OriginalEmail = reader.GetString("originalEmail");
                                user.AliasedEmail = reader.GetString("aliasedEmail");

                                //user.DataEntradaObject = reader.GetDateTime("dataEntrada");
                                //user.DataSaidaObject = reader.GetDateTime("dataSaida");
                                user.Ativo = reader.GetBoolean("utilizadorAtivo").Value;
                                user.ContaNoGIAF = reader.GetBoolean("contaNoGIAF").Value;

                                user.Visibilidade = visibilidades.FirstOrDefault(x => x.ID == reader.GetInt32("visibilidade"));
                                user.Flags = flags.Where(x => x.IDUtilizador == user.ID && x.IDPerfil == null).ToList();

                                var dCriado = reader.GetDateTime("utilizadorDataCriado");
                                user.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                                user.UtilizadorCriado = reader.GetString("utilizadorUtilizadorCriado");
                                user.DataModificadoObject = reader.GetDateTime("utilizadorDataModificado");
                                user.UtilizadorModificado = reader.GetString("utilizadorUtilizadorModificado");
                            }

                            if (!reader.IsDBNull("idPerfil"))
                            {
                                var perfil = new Perfil();

                                perfil.IUPI = user.IUPI;

                                perfil.ID = reader.GetInt32("idPerfil").Value;
                                perfil.NumeroMecanografico = reader.GetString("nmec");
                                perfil.Visibilidade = visibilidades.FirstOrDefault(x => x.ID == reader.GetInt32("visibilidadePerfil"));

                                perfil.DataEntradaObject = reader.GetDateTime("dataEntrada");
                                perfil.DataSaidaObject = reader.GetDateTime("dataSaida");

                                perfil.Flags = flags.Where(x => x.IDUtilizador == user.ID && x.IDPerfil == perfil.ID).ToList();

                                if (!reader.IsDBNull("idPerfilTipo"))
                                {
                                    perfil.Tipo = new Perfil.TipoPerfil()
                                    {
                                        ID = reader.GetInt32("idPerfilTipo").Value,
                                        Nome = reader.GetString("perfilTipo"),
                                        Descricao = reader.GetString("perfilTipoDescricao"),
                                        Codigo = reader.GetString("perfilTipoCodigo")
                                    };
                                }
                                //if (!reader.IsDBNull(11)) //CARGO - chamar método para cargos
                                //{
                                //    perfil.Cargo = new Cargo()
                                //    {
                                //        ID = Convert.ToInt32(IRCUAssemblies.Toolkit.Tools.DB.GetStringFromReader(reader, 11) ?? "0"),
                                //        Codigo = (IRCUAssemblies.Toolkit.Tools.DB.GetStringFromReader(reader, 12) ?? "").ToString(),
                                //        Nome = (IRCUAssemblies.Toolkit.Tools.DB.GetStringFromReader(reader, 13) ?? "").ToString(),
                                //        Descricao = (IRCUAssemblies.Toolkit.Tools.DB.GetStringFromReader(reader, 14) ?? "").ToString()
                                //    };
                                //}
                                if (!reader.IsDBNull("idCarreira")) //CARREIRA
                                {
                                    perfil.Carreira = new Carreira()
                                    {
                                        ID = reader.GetInt32("idCarreira").Value,
                                        Codigo = reader.GetString("codCarreira"),
                                        Nome = reader.GetString("carreira"),
                                        Descricao = reader.GetString("carreiraDescricao")
                                    };
                                }
                                if (!reader.IsDBNull("idCategoria")) //CATEGORIA
                                {
                                    perfil.Categoria = new Categoria()
                                    {
                                        ID = reader.GetInt32("idCategoria").Value,
                                        Codigo = reader.GetString("codCategoria"),
                                        Nome = reader.GetString("categoria"),
                                        Descricao = reader.GetString("categoriaDescricao")
                                    };
                                }
                                if (!reader.IsDBNull("idRegime"))
                                {
                                    perfil.Regime = new Regime()
                                    {
                                        ID = reader.GetInt32("idRegime").Value,
                                        Codigo = reader.GetString("codRegime"),
                                        Nome = reader.GetString("regime"),
                                        Descricao = reader.GetString("regimeDescricao")
                                    };
                                }
                                if (!reader.IsDBNull("idRegime"))
                                {
                                    perfil.Regime = new Regime()
                                    {
                                        ID = reader.GetInt32("idRegime").Value,
                                        Codigo = reader.GetString("codRegime"),
                                        Nome = reader.GetString("regime"),
                                        Descricao = reader.GetString("regimeDescricao")
                                    };
                                }
                                if (!reader.IsDBNull("idEntidade")) //ENTIDADE
                                {
                                    //FAZER O MÉTODO PARA DEVOLVER ENTIDADE
                                    perfil.Entidade = new Entidade()
                                    {
                                        ID = reader.GetInt32("idEntidade").Value,
                                        Codigo = (reader.GetString("codEntidade") ?? "").ToString(),
                                        Nome = (reader.GetString("entidade") ?? "").ToString(),
                                        Descricao = (reader.GetString("entidadeDescricao") ?? "").ToString(),
                                        Sigla = (reader.GetString("sigla") ?? "").ToString(),
                                    };

                                    if (!reader.IsDBNull("idEntidadeTipo"))
                                    {
                                        perfil.Entidade.Tipo = new Entidade.TipoEntidade()
                                        {
                                            ID = reader.GetInt32("idEntidadeTipo").Value,
                                            Nome = (reader.GetString("entidadeTipo") ?? "").ToString(),
                                            Descricao = (reader.GetString("entidadeTipoDescricao") ?? "").ToString()
                                        };
                                    }
                                }
                                if (!reader.IsDBNull("perfilAtivo"))
                                {
                                    perfil.Ativo = reader.GetBoolean("perfilAtivo").Value;
                                }

                                var dCriado = reader.GetDateTime("perfilDataCriado");
                                perfil.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                                perfil.UtilizadorCriado = reader.GetString("perfilUtilizadorCriado");
                                perfil.DataModificadoObject = reader.GetDateTime("perfilDataModificado");
                                perfil.UtilizadorModificado = reader.GetString("perfilUtilizadorModificado");

                                user.Perfil.Add(perfil);
                            }


                            if (newUser) res.Add(user);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<Utilizador> Utilizador_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_LS(con, incluirApagados);
                }

            }
            public static IEnumerable<Utilizador> Utilizador_PorTipoPerfil_S(SqlConnection con, string codigoTipoPerfil)
            {
                var res = new List<Utilizador>();

                var visibilidades = Utilizador_Visibilidade_LS(con);
                var flags = Utilizador_Flags_LS(con);

                using (var cmd = new SqlCommand("[dbo].[stp_Utilizador_PorTipoPerfil_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@codigo", SqlDbType.NVarChar).Value = codigoTipoPerfil;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    var lastIUPI = Guid.Empty;

                    while (reader.Read())
                    {
                        //object[] values = new object[reader.FieldCount];
                        //int numColumns = reader.GetValues(values);

                        if (!reader.IsDBNull("iupi"))
                        {
                            Guid dbIupi = reader.GetGuid("iupi").Value;
                            bool newUser = true;

                            var user = new Utilizador()
                            {
                                IUPI = Guid.Empty
                            };

                            if (res.Where(x => x.IUPI == dbIupi).Count() > 0)
                            {
                                user = res.Where(x => x.IUPI == dbIupi).First();
                                newUser = false;
                            }
                            else
                            {
                                //não fazer nada
                            }

                            if (user.IUPI == Guid.Empty)
                            {
                                user.IUPI = dbIupi;
                                user.ID = reader.GetInt32("id").Value;
                                user.Nome = reader.GetString("nome");
                                user.NomePersonalizado = reader.GetString("nomePersonalizado");
                                user.NIF = reader.GetString("nif");

                                user.Username = reader.GetString("username");
                                user.Email = reader.GetString("email");
                                user.OriginalEmail = reader.GetString("originalEmail");
                                user.AliasedEmail = reader.GetString("aliasedEmail");

                                //user.DataEntradaObject = reader.GetDateTime("dataEntrada");
                                //user.DataSaidaObject = reader.GetDateTime("dataSaida");
                                user.Ativo = reader.GetBoolean("utilizadorAtivo").Value;
                                user.ContaNoGIAF = reader.GetBoolean("contaNoGIAF").Value;

                                user.Visibilidade = visibilidades.FirstOrDefault(x => x.ID == reader.GetInt32("visibilidade"));
                                user.Flags = flags.Where(x => x.IDUtilizador == user.ID && x.IDPerfil == null).ToList();

                                var dCriado = reader.GetDateTime("utilizadorDataCriado");
                                user.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                                user.UtilizadorCriado = reader.GetString("utilizadorUtilizadorCriado");
                                user.DataModificadoObject = reader.GetDateTime("utilizadorDataModificado");
                                user.UtilizadorModificado = reader.GetString("utilizadorUtilizadorModificado");
                            }

                            if (!reader.IsDBNull("idPerfil"))
                            {
                                var perfil = new Perfil();

                                perfil.IUPI = user.IUPI;

                                perfil.ID = reader.GetInt32("idPerfil").Value;
                                perfil.NumeroMecanografico = reader.GetString("nmec");
                                perfil.Visibilidade = visibilidades.FirstOrDefault(x => x.ID == reader.GetInt32("visibilidadePerfil"));

                                perfil.DataEntradaObject = reader.GetDateTime("dataEntrada");
                                perfil.DataSaidaObject = reader.GetDateTime("dataSaida");
                                
                                user.Flags = flags.Where(x => x.IDUtilizador == user.ID && x.IDPerfil == null).ToList();

                                if (!reader.IsDBNull("idPerfilTipo"))
                                {
                                    perfil.Tipo = new Perfil.TipoPerfil()
                                    {
                                        ID = reader.GetInt32("idPerfilTipo").Value,
                                        Nome = reader.GetString("perfilTipo"),
                                        Descricao = reader.GetString("perfilTipoDescricao"),
                                        Codigo = reader.GetString("perfilTipoCodigo")
                                    };
                                }
                                //if (!reader.IsDBNull(11)) //CARGO - chamar método para cargos
                                //{
                                //    perfil.Cargo = new Cargo()
                                //    {
                                //        ID = Convert.ToInt32(IRCUAssemblies.Toolkit.Tools.DB.GetStringFromReader(reader, 11) ?? "0"),
                                //        Codigo = (IRCUAssemblies.Toolkit.Tools.DB.GetStringFromReader(reader, 12) ?? "").ToString(),
                                //        Nome = (IRCUAssemblies.Toolkit.Tools.DB.GetStringFromReader(reader, 13) ?? "").ToString(),
                                //        Descricao = (IRCUAssemblies.Toolkit.Tools.DB.GetStringFromReader(reader, 14) ?? "").ToString()
                                //    };
                                //}
                                if (!reader.IsDBNull("idCarreira")) //CARREIRA
                                {
                                    perfil.Carreira = new Carreira()
                                    {
                                        ID = reader.GetInt32("idCarreira").Value,
                                        Codigo = reader.GetString("codCarreira"),
                                        Nome = reader.GetString("carreira"),
                                        Descricao = reader.GetString("carreiraDescricao")
                                    };
                                }
                                if (!reader.IsDBNull("idCategoria")) //CATEGORIA
                                {
                                    perfil.Categoria = new Categoria()
                                    {
                                        ID = reader.GetInt32("idCategoria").Value,
                                        Codigo = reader.GetString("codCategoria"),
                                        Nome = reader.GetString("categoria"),
                                        Descricao = reader.GetString("categoriaDescricao")
                                    };
                                }
                                if (!reader.IsDBNull("idRegime"))
                                {
                                    perfil.Regime = new Regime()
                                    {
                                        ID = reader.GetInt32("idRegime").Value,
                                        Codigo = reader.GetString("codRegime"),
                                        Nome = reader.GetString("regime"),
                                        Descricao = reader.GetString("regimeDescricao")
                                    };
                                }
                                if (!reader.IsDBNull("idEntidade")) //ENTIDADE
                                {
                                    //FAZER O MÉTODO PARA DEVOLVER ENTIDADE
                                    perfil.Entidade = new Entidade()
                                    {
                                        ID = reader.GetInt32("idEntidade").Value,
                                        Codigo = (reader.GetString("codEntidade") ?? "").ToString(),
                                        Nome = (reader.GetString("entidade") ?? "").ToString(),
                                        Descricao = (reader.GetString("entidadeDescricao") ?? "").ToString(),
                                        Sigla = (reader.GetString("sigla") ?? "").ToString()
                                    };

                                    if (!reader.IsDBNull("idEntidadeTipo"))
                                    {
                                        perfil.Entidade.Tipo = new Entidade.TipoEntidade()
                                        {
                                            ID = reader.GetInt32("idEntidadeTipo").Value,
                                            Nome = (reader.GetString("entidadeTipo") ?? "").ToString(),
                                            Descricao = (reader.GetString("entidadeTipoDescricao") ?? "").ToString()
                                        };
                                    }
                                }
                                if (!reader.IsDBNull("perfilAtivo"))
                                {
                                    perfil.Ativo = reader.GetBoolean("perfilAtivo").Value;
                                }

                                var dCriado = reader.GetDateTime("perfilDataCriado");
                                perfil.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                                perfil.UtilizadorCriado = reader.GetString("perfilUtilizadorCriado");
                                perfil.DataModificadoObject = reader.GetDateTime("perfilDataModificado");
                                perfil.UtilizadorModificado = reader.GetString("perfilUtilizadorModificado");

                                user.Perfil.Add(perfil);
                            }


                            if (newUser) res.Add(user);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<Utilizador> Utilizador_PorTipoPerfil_S(string cString, string codigoTipoPerfil)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_PorTipoPerfil_S(con, codigoTipoPerfil);
                }

            }
            public static Utilizador Utilizador_PorNMec_S(SqlConnection con, string nmec)
            {
                var user = new Utilizador()
                {
                    IUPI = Guid.Empty
                };

                var visibilidades = Utilizador_Visibilidade_LS(con);

                using (var cmd = new SqlCommand("[stp_Utilizador_PorNMec_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@nmec", System.Data.SqlDbType.NVarChar).Value = nmec;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        if (user.IUPI == Guid.Empty)
                        {
                            var dbIupi = reader.GetGuid("iupi");

                            if (dbIupi.HasValue)
                            {
                                user.IUPI = dbIupi.Value;
                            }
                            else
                            {
                                throw new Exception(string.Format("O utilizador com o NMec {0} não existe.", nmec));
                            }

                            user.ID = reader.GetInt32("id").Value;
                            user.Nome = reader.GetString("nome");
                            user.NomePersonalizado = reader.GetString("nomePersonalizado");
                            user.NIF = reader.GetString("nif");

                            user.Username = reader.GetString("username");
                            user.Email = reader.GetString("email");
                            user.OriginalEmail = reader.GetString("originalEmail");
                            user.AliasedEmail = reader.GetString("aliasedEmail");

                            user.Ativo = reader.GetBoolean("utilizadorAtivo").Value;
                            user.ContaNoGIAF = reader.GetBoolean("contaNoGIAF").Value;

                            user.Visibilidade = visibilidades.FirstOrDefault(x => x.ID == reader.GetInt32("visibilidade"));
                            user.Flags = Utilizador_Flags_PorUtilizador_S(con.ConnectionString, user.ID);

                            var dCriado = reader.GetDateTime("utilizadorDataCriado");
                            user.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                            user.UtilizadorCriado = reader.GetString("utilizadorUtilizadorCriado");
                            user.DataModificadoObject = reader.GetDateTime("utilizadorDataModificado");
                            user.UtilizadorModificado = reader.GetString("utilizadorUtilizadorModificado");
                        }
                        if (!reader.IsDBNull("idPerfil"))
                        {
                            var perfil = new Perfil();

                            perfil.IUPI = user.IUPI;

                            perfil.ID = reader.GetInt32("idPerfil").Value;
                            perfil.NumeroMecanografico = (reader.GetString("nmec") ?? "").ToString();
                            perfil.DataEntradaObject = reader.GetDateTime("dataEntrada");
                            perfil.DataSaidaObject = reader.GetDateTime("dataSaida");
                            perfil.Visibilidade = visibilidades.FirstOrDefault(x => x.ID == reader.GetInt32("visibilidadePerfil"));

                            perfil.Flags = Utilizador_Flags_PorPerfil_S(con.ConnectionString, user.ID, perfil.ID);

                            perfil.Tipo = new Perfil.TipoPerfil()
                            {
                                ID = reader.GetInt32("idPerfilTipo").Value,
                                Nome = (reader.GetString("perfilTipo") ?? "").ToString(),
                                Descricao = (reader.GetString("perfilTipoDescricao") ?? "").ToString(),
                                Codigo = reader.GetString("perfilTipoCodigo")
                            };
                            //if (!reader.IsDBNull(11)) //CARGO
                            //{
                            //    perfil.Cargo = new Cargo()
                            //    {
                            //        ID = Convert.ToInt32(reader.GetString("idCargo") ?? "0")
                            //    };
                            //}
                            if (!reader.IsDBNull("idCarreira")) //CARREIRA
                            {
                                perfil.Carreira = new Carreira()
                                {
                                    ID = reader.GetInt32("idCarreira").Value,
                                    Codigo = (reader.GetString("codCarreira") ?? "").ToString(),
                                    Nome = (reader.GetString("carreira") ?? "").ToString(),
                                    Descricao = (reader.GetString("carreiraDescricao") ?? "").ToString()
                                };
                            }
                            if (!reader.IsDBNull("idCategoria")) //CATEGORIA
                            {
                                perfil.Categoria = new Categoria()
                                {
                                    ID = reader.GetInt32("idCategoria").Value,
                                    Codigo = (reader.GetString("codCategoria") ?? "").ToString(),
                                    Nome = (reader.GetString("categoria") ?? "").ToString(),
                                    Descricao = (reader.GetString("categoriaDescricao") ?? "").ToString()
                                };
                            }
                            if (!reader.IsDBNull("idRegime"))
                            {
                                perfil.Regime = new Regime()
                                {
                                    ID = reader.GetInt32("idRegime").Value,
                                    Codigo = reader.GetString("codRegime"),
                                    Nome = reader.GetString("regime"),
                                    Descricao = reader.GetString("regimeDescricao")
                                };
                            }
                            if (!reader.IsDBNull("idEntidade")) //ENTIDADE
                            {
                                //FAZER O MÉTODO PARA DEVOLVER ENTIDADE
                                perfil.Entidade = new Entidade()
                                {
                                    ID = reader.GetInt32("idEntidade").Value,
                                    Codigo = (reader.GetString("codEntidade") ?? "").ToString(),
                                    Nome = (reader.GetString("entidade") ?? "").ToString(),
                                    Descricao = (reader.GetString("entidadeDescricao") ?? "").ToString(),
                                    Sigla = (reader.GetString("sigla") ?? "").ToString()
                                };

                                if (!reader.IsDBNull("idEntidadeTipo"))
                                {
                                    perfil.Entidade.Tipo = new Entidade.TipoEntidade()
                                    {
                                        ID = reader.GetInt32("idEntidadeTipo").Value,
                                        Nome = (reader.GetString("entidadeTipo") ?? "").ToString(),
                                        Descricao = (reader.GetString("entidadeTipoDescricao") ?? "").ToString()
                                    };
                                }
                            }
                            if (!reader.IsDBNull("perfilAtivo"))
                            {
                                perfil.Ativo = reader.GetBoolean("perfilAtivo").HasValue && reader.GetBoolean("perfilAtivo").Value;
                            }

                            var dCriadoPerfil = reader.GetDateTime("perfilDataCriado");
                            perfil.DataCriadoObject = dCriadoPerfil.HasValue ? dCriadoPerfil.Value : DateTime.MinValue;

                            perfil.UtilizadorCriado = reader.GetString("perfilUtilizadorCriado");
                            perfil.DataModificadoObject = reader.GetDateTime("perfilDataModificado");
                            perfil.UtilizadorModificado = reader.GetString("perfilUtilizadorModificado");

                            user.Perfil.Add(perfil);
                        }

                    }

                    reader.Close();
                }

                if (!user.IUPI.HasValue || user.IUPI.Value == Guid.Empty)
                {
                    return null;
                }

                using (var cmd = new SqlCommand("[dbo].[stp_Utilizador_Cargos_PorIUPI_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = user.IUPI;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var cargo = new Cargo()
                        {
                            ID = reader.GetInt32("idCargo").Value,
                            DataInicio = DateTime.Parse(reader.GetString("dataInicio")),
                            DataFim = reader.GetDateTime("dataFim"),
                            Ativo = reader.GetBoolean("cargoAtivo").HasValue && reader.GetBoolean("cargoAtivo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        cargo.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        cargo.Entidade = new Entidade()
                        {
                            ID = reader.GetInt32("idEntidade").Value,
                            Sigla = reader.GetString("siglaEntidade"),
                            Nome = reader.GetString("entidade"),
                            Descricao = reader.GetString("descricaoEntidade")
                        };

                        cargo.Tipo = new Cargo.TipoCargo() //incluir os campos com datacriado etc nestes objetos
                        {
                            ID = reader.GetInt32("idTipoCargo").Value,
                            Codigo = reader.GetString("codigoTipoCargo"),
                            Nome = reader.GetString("nomeTipoCargo"),
                            Descricao = reader.GetString("descricaoTipoCargo")
                        };
                    }

                    reader.Close();
                }

                if (user.IUPI != Guid.Empty)
                {
                    return user;
                }
                else
                {
                    throw new Exception(string.Format("O utilizador com o NMec {0} não existe.", nmec));
                }
            }
            public static Utilizador Utilizador_PorNMec_S(string cString, string nmec)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_PorNMec_S(con, nmec);
                }
            }
            public static Utilizador Utilizador_PorID_S(SqlConnection con, int id)
            {
                var user = new Utilizador()
                {
                    IUPI = Guid.Empty
                };

                var visibilidades = Utilizador_Visibilidade_LS(con);

                using (var cmd = new SqlCommand("[stp_Utilizador_PorNMec_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        if (user.IUPI == Guid.Empty)
                        {
                            var dbIupi = reader.GetGuid("iupi");

                            if (dbIupi.HasValue)
                            {
                                user.IUPI = dbIupi.Value;
                            }
                            else
                            {
                                throw new Exception(string.Format("O utilizador com o ID {0} não existe.", id));
                            }

                            user.ID = reader.GetInt32("id").Value;
                            user.Nome = reader.GetString("nome");
                            user.NomePersonalizado = reader.GetString("nomePersonalizado");
                            user.NIF = reader.GetString("nif");

                            user.Username = reader.GetString("username");
                            user.Email = reader.GetString("email");
                            user.OriginalEmail = reader.GetString("originalEmail");
                            user.AliasedEmail = reader.GetString("aliasedEmail");

                            user.Ativo = reader.GetBoolean("utilizadorAtivo").Value;
                            user.ContaNoGIAF = reader.GetBoolean("contaNoGIAF").Value;

                            user.Visibilidade = visibilidades.FirstOrDefault(x => x.ID == reader.GetInt32("visibilidade"));
                            user.Flags = Utilizador_Flags_PorUtilizador_S(con.ConnectionString, user.ID);

                            var dCriado = reader.GetDateTime("utilizadorDataCriado");
                            user.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                            user.UtilizadorCriado = reader.GetString("utilizadorUtilizadorCriado");
                            user.DataModificadoObject = reader.GetDateTime("utilizadorDataModificado");
                            user.UtilizadorModificado = reader.GetString("utilizadorUtilizadorModificado");
                        }
                        if (!reader.IsDBNull("idPerfil"))
                        {
                            var perfil = new Perfil();

                            perfil.IUPI = user.IUPI;

                            perfil.ID = reader.GetInt32("idPerfil").Value;
                            perfil.NumeroMecanografico = (reader.GetString("nmec") ?? "").ToString();
                            perfil.DataEntradaObject = reader.GetDateTime("dataEntrada");
                            perfil.DataSaidaObject = reader.GetDateTime("dataSaida");
                            perfil.Visibilidade = visibilidades.FirstOrDefault(x => x.ID == reader.GetInt32("visibilidadePerfil"));

                            perfil.Flags = Utilizador_Flags_PorPerfil_S(con.ConnectionString, user.ID, perfil.ID);

                            perfil.Tipo = new Perfil.TipoPerfil()
                            {
                                ID = reader.GetInt32("idPerfilTipo").Value,
                                Nome = (reader.GetString("perfilTipo") ?? "").ToString(),
                                Descricao = (reader.GetString("perfilTipoDescricao") ?? "").ToString(),
                                Codigo = reader.GetString("perfilTipoCodigo")
                            };
                            //if (!reader.IsDBNull(11)) //CARGO
                            //{
                            //    perfil.Cargo = new Cargo()
                            //    {
                            //        ID = Convert.ToInt32(reader.GetString("idCargo") ?? "0")
                            //    };
                            //}
                            if (!reader.IsDBNull("idCarreira")) //CARREIRA
                            {
                                perfil.Carreira = new Carreira()
                                {
                                    ID = reader.GetInt32("idCarreira").Value,
                                    Codigo = (reader.GetString("codCarreira") ?? "").ToString(),
                                    Nome = (reader.GetString("carreira") ?? "").ToString(),
                                    Descricao = (reader.GetString("carreiraDescricao") ?? "").ToString()
                                };
                            }
                            if (!reader.IsDBNull("idCategoria")) //CATEGORIA
                            {
                                perfil.Categoria = new Categoria()
                                {
                                    ID = reader.GetInt32("idCategoria").Value,
                                    Codigo = (reader.GetString("codCategoria") ?? "").ToString(),
                                    Nome = (reader.GetString("categoria") ?? "").ToString(),
                                    Descricao = (reader.GetString("categoriaDescricao") ?? "").ToString()
                                };
                            }
                            if (!reader.IsDBNull("idRegime"))
                            {
                                perfil.Regime = new Regime()
                                {
                                    ID = reader.GetInt32("idRegime").Value,
                                    Codigo = reader.GetString("codRegime"),
                                    Nome = reader.GetString("regime"),
                                    Descricao = reader.GetString("regimeDescricao")
                                };
                            }
                            if (!reader.IsDBNull("idEntidade")) //ENTIDADE
                            {
                                //FAZER O MÉTODO PARA DEVOLVER ENTIDADE
                                perfil.Entidade = new Entidade()
                                {
                                    ID = reader.GetInt32("idEntidade").Value,
                                    Codigo = (reader.GetString("codEntidade") ?? "").ToString(),
                                    Nome = (reader.GetString("entidade") ?? "").ToString(),
                                    Descricao = (reader.GetString("entidadeDescricao") ?? "").ToString(),
                                    Sigla = (reader.GetString("sigla") ?? "").ToString()
                                };

                                if (!reader.IsDBNull("idEntidadeTipo"))
                                {
                                    perfil.Entidade.Tipo = new Entidade.TipoEntidade()
                                    {
                                        ID = reader.GetInt32("idEntidadeTipo").Value,
                                        Nome = (reader.GetString("entidadeTipo") ?? "").ToString(),
                                        Descricao = (reader.GetString("entidadeTipoDescricao") ?? "").ToString()
                                    };
                                }
                            }
                            if (!reader.IsDBNull("perfilAtivo"))
                            {
                                perfil.Ativo = reader.GetBoolean("perfilAtivo").HasValue && reader.GetBoolean("perfilAtivo").Value;
                            }

                            var dCriadoPerfil = reader.GetDateTime("perfilDataCriado");
                            perfil.DataCriadoObject = dCriadoPerfil.HasValue ? dCriadoPerfil.Value : DateTime.MinValue;

                            perfil.UtilizadorCriado = reader.GetString("perfilUtilizadorCriado");
                            perfil.DataModificadoObject = reader.GetDateTime("perfilDataModificado");
                            perfil.UtilizadorModificado = reader.GetString("perfilUtilizadorModificado");

                            user.Perfil.Add(perfil);
                        }

                    }

                    reader.Close();
                }

                if (!user.IUPI.HasValue || user.IUPI.Value == Guid.Empty)
                {
                    return null;
                }

                using (var cmd = new SqlCommand("[dbo].[stp_Utilizador_Cargos_PorIUPI_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = user.IUPI;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var cargo = new Cargo()
                        {
                            ID = reader.GetInt32("idCargo").Value,
                            DataInicio = DateTime.Parse(reader.GetString("dataInicio")),
                            DataFim = reader.GetDateTime("dataFim"),
                            Ativo = reader.GetBoolean("cargoAtivo").HasValue && reader.GetBoolean("cargoAtivo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        cargo.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        cargo.Entidade = new Entidade()
                        {
                            ID = reader.GetInt32("idEntidade").Value,
                            Sigla = reader.GetString("siglaEntidade"),
                            Nome = reader.GetString("entidade"),
                            Descricao = reader.GetString("descricaoEntidade")
                        };

                        cargo.Tipo = new Cargo.TipoCargo() //incluir os campos com datacriado etc nestes objetos
                        {
                            ID = reader.GetInt32("idTipoCargo").Value,
                            Codigo = reader.GetString("codigoTipoCargo"),
                            Nome = reader.GetString("nomeTipoCargo"),
                            Descricao = reader.GetString("descricaoTipoCargo")
                        };
                    }

                    reader.Close();
                }

                if (user.IUPI != Guid.Empty)
                {
                    return user;
                }
                else
                {
                    throw new Exception(string.Format("O utilizador com o ID {0} não existe.", id));
                }
            }
            public static Utilizador Utilizador_PorID_S(string cString, int id)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_PorID_S(con, id);
                }
            }
            public static Utilizador Utilizador_S(SqlConnection con, Guid iupi, bool incluirApagados = false)
            {
                var user = new Utilizador()
                {
                    IUPI = Guid.Empty
                };

                var visibilidades = Utilizador_Visibilidade_LS(con);

                using (var cmd = new SqlCommand("[stp_Utilizador_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = iupi;
                    cmd.Parameters.Add("@incluirApagados", SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        if (user.IUPI == Guid.Empty)
                        {
                            var dbIupi = reader.GetGuid("iupi");

                            if (dbIupi.HasValue)
                            {
                                user.IUPI = dbIupi.Value;
                            }
                            else
                            {
                                throw new Exception(string.Format("O utilizador com o IUPI {0} não existe.", iupi.ToString()));
                            }

                            user.ID = reader.GetInt32("id").Value;
                            user.Nome = reader.GetString("nome");
                            user.NomePersonalizado = reader.GetString("nomePersonalizado");
                            user.NIF = reader.GetString("nif");

                            user.Username = reader.GetString("username");
                            user.Email = reader.GetString("email");
                            user.OriginalEmail = reader.GetString("originalEmail");
                            user.AliasedEmail = reader.GetString("aliasedEmail");

                            user.Ativo = reader.GetBoolean("utilizadorAtivo").Value;
                            user.ContaNoGIAF = reader.GetBoolean("contaNoGIAF").Value;

                            user.Visibilidade = visibilidades.FirstOrDefault(x => x.ID == reader.GetInt32("visibilidade"));
                            user.Flags = Utilizador_Flags_PorUtilizador_S(con.ConnectionString, user.ID);

                            var dCriado = reader.GetDateTime("utilizadorDataCriado");
                            user.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                            user.UtilizadorCriado = reader.GetString("utilizadorUtilizadorCriado");
                            user.DataModificadoObject = reader.GetDateTime("utilizadorDataModificado");
                            user.UtilizadorModificado = reader.GetString("utilizadorUtilizadorModificado");
                        }
                        if (!reader.IsDBNull("idPerfil"))
                        {
                            var perfil = new Perfil();

                            perfil.IUPI = user.IUPI;

                            perfil.ID = reader.GetInt32("idPerfil").Value;
                            perfil.NumeroMecanografico = (reader.GetString("nmec") ?? "").ToString();
                            perfil.DataEntradaObject = reader.GetDateTime("dataEntrada");
                            perfil.DataSaidaObject = reader.GetDateTime("dataSaida");
                            perfil.Visibilidade = visibilidades.FirstOrDefault(x => x.ID == reader.GetInt32("visibilidadePerfil"));

                            perfil.Flags = Utilizador_Flags_PorPerfil_S(con.ConnectionString, user.ID, perfil.ID);

                            perfil.Tipo = new Perfil.TipoPerfil()
                            {
                                ID = reader.GetInt32("idPerfilTipo").Value,
                                Nome = (reader.GetString("perfilTipo") ?? "").ToString(),
                                Descricao = (reader.GetString("perfilTipoDescricao") ?? "").ToString(),
                                Codigo = reader.GetString("perfilTipoCodigo")
                            };
                            //if (!reader.IsDBNull(11)) //CARGO
                            //{
                            //    perfil.Cargo = new Cargo()
                            //    {
                            //        ID = Convert.ToInt32(reader.GetString("idCargo") ?? "0")
                            //    };
                            //}
                            if (!reader.IsDBNull("idCarreira")) //CARREIRA
                            {
                                perfil.Carreira = new Carreira()
                                {
                                    ID = reader.GetInt32("idCarreira").Value,
                                    Codigo = (reader.GetString("codCarreira") ?? "").ToString(),
                                    Nome = (reader.GetString("carreira") ?? "").ToString(),
                                    Descricao = (reader.GetString("carreiraDescricao") ?? "").ToString()
                                };
                            }
                            if (!reader.IsDBNull("idCategoria")) //CATEGORIA
                            {
                                perfil.Categoria = new Categoria()
                                {
                                    ID = reader.GetInt32("idCategoria").Value,
                                    Codigo = (reader.GetString("codCategoria") ?? "").ToString(),
                                    Nome = (reader.GetString("categoria") ?? "").ToString(),
                                    Descricao = (reader.GetString("categoriaDescricao") ?? "").ToString()
                                };
                            }
                            if (!reader.IsDBNull("idRegime"))
                            {
                                perfil.Regime = new Regime()
                                {
                                    ID = reader.GetInt32("idRegime").Value,
                                    Codigo = reader.GetString("codRegime"),
                                    Nome = reader.GetString("regime"),
                                    Descricao = reader.GetString("regimeDescricao")
                                };
                            }
                            if (!reader.IsDBNull("idEntidade")) //ENTIDADE
                            {
                                //FAZER O MÉTODO PARA DEVOLVER ENTIDADE
                                perfil.Entidade = new Entidade()
                                {
                                    ID = reader.GetInt32("idEntidade").Value,
                                    Codigo = (reader.GetString("codEntidade") ?? "").ToString(),
                                    Nome = (reader.GetString("entidade") ?? "").ToString(),
                                    Descricao = (reader.GetString("entidadeDescricao") ?? "").ToString(),
                                    Sigla = (reader.GetString("sigla") ?? "").ToString()
                                };

                                if (!reader.IsDBNull("idEntidadeTipo"))
                                {
                                    perfil.Entidade.Tipo = new Entidade.TipoEntidade()
                                    {
                                        ID = reader.GetInt32("idEntidadeTipo").Value,
                                        Nome = (reader.GetString("entidadeTipo") ?? "").ToString(),
                                        Descricao = (reader.GetString("entidadeTipoDescricao") ?? "").ToString()
                                    };
                                }
                            }
                            if (!reader.IsDBNull("perfilAtivo"))
                            {
                                perfil.Ativo = reader.GetBoolean("perfilAtivo").HasValue && reader.GetBoolean("perfilAtivo").Value;
                            }

                            var dCriadoPerfil = reader.GetDateTime("perfilDataCriado");
                            perfil.DataCriadoObject = dCriadoPerfil.HasValue ? dCriadoPerfil.Value : DateTime.MinValue;

                            perfil.UtilizadorCriado = reader.GetString("perfilUtilizadorCriado");
                            perfil.DataModificadoObject = reader.GetDateTime("perfilDataModificado");
                            perfil.UtilizadorModificado = reader.GetString("perfilUtilizadorModificado");

                            user.Perfil.Add(perfil);
                        }

                    }

                    reader.Close();
                }

                if(!user.IUPI.HasValue || user.IUPI.Value == Guid.Empty)
                {
                    return null;
                }

                using (var cmd = new SqlCommand("[dbo].[stp_Utilizador_Cargos_PorIUPI_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = iupi;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var cargo = new Cargo()
                        {
                            ID = reader.GetInt32("idCargo").Value,
                            DataInicio = DateTime.Parse(reader.GetString("dataInicio")),
                            DataFim = reader.GetDateTime("dataFim"),
                            Ativo = reader.GetBoolean("cargoAtivo").HasValue && reader.GetBoolean("cargoAtivo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        cargo.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        cargo.Entidade = new Entidade()
                        {
                            ID = reader.GetInt32("idEntidade").Value,
                            Sigla = reader.GetString("siglaEntidade"),
                            Nome = reader.GetString("entidade"),
                            Descricao = reader.GetString("descricaoEntidade")
                        };

                        cargo.Tipo = new Cargo.TipoCargo() //incluir os campos com datacriado etc nestes objetos
                        {
                            ID = reader.GetInt32("idTipoCargo").Value,
                            Codigo = reader.GetString("codigoTipoCargo"),
                            Nome = reader.GetString("nomeTipoCargo"),
                            Descricao = reader.GetString("descricaoTipoCargo")
                        };
                    }

                    reader.Close();
                }

                if (user.IUPI != Guid.Empty)
                {
                    return user;
                }
                else
                {
                    throw new Exception(string.Format("O utilizador com o IUPI {0} não existe.", iupi.ToString()));
                }
            }
            public static Utilizador Utilizador_S(string cString, Guid iupi, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_S(con, iupi, incluirApagados);
                }
            }
            public static Nullable<Guid> Utilizador_GetIUPI(SqlConnection con, string username)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_GetIUPI]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@username", System.Data.SqlDbType.NVarChar).Value = username;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var resDB = (object)null;


                    var reader = cmd.ExecuteReader();

                    while (resDB == null && reader.Read())
                    {
                        if (!reader.IsDBNull("iupi"))
                        {
                            resDB = reader[0];
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    if (resDB == null) return null;

                    var res = Guid.Empty;

                    if (Guid.TryParse(resDB.ToString(), out res) && res != Guid.Empty)
                    {
                        return res;
                    }
                    else
                    {
                        if (res == Guid.Empty)
                            throw new Exception(string.Format("Não foi possível executar a operação sobre o utilizador com o username {0}", username));
                        else
                            throw new Exception(string.Format("Não foi possível executar a operação sobre o utilizador com o IUPI {0}", res.ToString()));
                    }
                }
            }
            public static Nullable<Guid> Utilizador_GetIUPI(string cString, string username)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_GetIUPI(con, username);
                }
            }
            public static Nullable<int> Utilizador_GetID(SqlConnection con, Guid iupi)
            {
                var res = -1;
                using (var cmd = new SqlCommand("[stp_Utilizador_GetID]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = iupi;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {
                        if (!reader.IsDBNull("id"))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static Nullable<int> Utilizador_GetID(string cString, Guid iupi)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_GetID(con, iupi);
                }
            }
            public static Nullable<Guid> Utilizador_IU(SqlConnection con, Utilizador.UtilizadorUpdate user)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = user.IUPI;
                    cmd.Parameters.Add("@nome", System.Data.SqlDbType.NVarChar).Value = user.Nome;
                    cmd.Parameters.Add("@nomePersonalizado", System.Data.SqlDbType.NVarChar).Value = string.IsNullOrWhiteSpace(user.NomePersonalizado) ? user.Nome : user.NomePersonalizado;
                    cmd.Parameters.Add("@nif", System.Data.SqlDbType.NVarChar).Value = user.NIF;
                    cmd.Parameters.Add("@username", System.Data.SqlDbType.NVarChar).Value = user.Username;
                    cmd.Parameters.Add("@originalEmail", System.Data.SqlDbType.NVarChar).Value = user.OriginalEmail;
                    cmd.Parameters.Add("@aliasedEmail", System.Data.SqlDbType.NVarChar).Value = user.AliasedEmail;
                    cmd.Parameters.Add("@contaNoGIAF", System.Data.SqlDbType.Bit).Value = user.ContaNoGIAF;
                    cmd.Parameters.Add("@visibilidade", System.Data.SqlDbType.Int).Value = user.Visibilidade;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = user.Ativo;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = user.UtilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var resDB = (object)null;


                    var reader = cmd.ExecuteReader();

                    while (resDB == null && reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            resDB = reader[0];
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    var res = Guid.Empty;

                    if (Guid.TryParse(resDB.ToString(), out res) && res != Guid.Empty)
                    {
                        return res;
                    }
                    else
                    {
                        if (res == Guid.Empty)
                            throw new Exception(string.Format("Não foi possível executar a operação sobre o utilizador com o NIF {0}", user.NIF));
                        else
                            throw new Exception(string.Format("Não foi possível executar a operação sobre o utilizador com o IUPI {0}", res.ToString()));
                    }
                }
            }
            public static Nullable<Guid> Utilizador_IU(string cString, Utilizador.UtilizadorUpdate user)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_IU(con, user);
                }
            }
            public static List<UtilizadorReturnValue> Utilizador_IU_TabelaEntrada(SqlConnection con, IEnumerable<UtilizadorType> users)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_IU_TabelaEntrada]", con))
                {
                    var dt = UtilizadorType.CreateDataTable();

                    foreach (var u in users)
                    {
                        dt.Rows.Add(u.ToDataRow());
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    var param = cmd.Parameters.AddWithValue("@users", dt);
                    param.SqlDbType = System.Data.SqlDbType.Structured;
                    param.TypeName = "dbo.UtilizadorType";

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    var res = new List<UtilizadorReturnValue>();

                    while (reader.Read())
                    {
                        res.Add(new UtilizadorReturnValue(reader.GetString("NIF"), reader.GetGuid("IUPI").Value, reader.GetString("Accao")));
                    }

                    reader.Close();

                    return res;
                }
            }
            public static List<UtilizadorReturnValue> Utilizador_IU_TabelaEntrada(string cString, IEnumerable<UtilizadorType> users)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_IU_TabelaEntrada(con, users);
                }
            }
            public static void Utilizador_D(SqlConnection con, Guid iupi, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = iupi;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_D(string cString, Guid iupi, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_D(con, iupi, utilizadorEdicao);
                }
            }
            public static void Utilizador_R(SqlConnection con, Guid iupi, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = iupi;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_R(string cString, Guid iupi, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_R(con, iupi, utilizadorEdicao);
                }
            }

            #region Ficheiros

            public static IEnumerable<FicheiroUtilizador> Utilizador_Ficheiros_LS(SqlConnection con, bool incluirApagados = false)
            {
                var res = new List<FicheiroUtilizador>();
                var utilizadores = RCUDB.RCU.Utilizador_LS(con, incluirApagados);
                var perfis = utilizadores.SelectMany(u => u.Perfil);
                var tipos = RCUDB.RCU.Utilizador_Ficheiros_Tipo_LS(con, incluirApagados);

                using (var cmd = new SqlCommand("[stp_Utilizador_Ficheiros_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var ficheiro = new FicheiroUtilizador()
                        {
                            ID = reader.GetInt32("id").Value,
                            //Utilizador = utilizadores.Single(x=>x.ID == reader.GetInt32("idUtilizador").Value),
                            //Perfil = reader.GetInt32("idPerfil").HasValue ? perfis.Single(x=>x.ID == reader.GetInt32("idPerfil").Value) : null,
                            SharePointURL = reader.GetString("sharepointURL"),
                            SharePointList = reader.GetString("sharepointList"),
                            SharePointFolder = reader.GetString("sharepointFolder"),
                            Filename = reader.GetString("filename"),
                            Tipo = tipos.Single(x => reader.GetInt32("idTipo").HasValue && x.ID == reader.GetInt32("idTipo").Value),
                            Ativo = reader.GetBoolean("ficheiroAtivo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var idUtilizador = reader.GetInt32("idUtilizador");
                        if (idUtilizador != null && utilizadores.Select(x => x.ID).Contains(idUtilizador.Value))
                        {
                            ficheiro.Utilizador = utilizadores.Single(x => x.ID == idUtilizador.Value);
                        }
                        else
                        {
                            //TRATAR ERRO
                            //Ao contrário do perfil, não é suposto o ficheiro não ter utilizador
                        }

                        var idPerfil = reader.GetInt32("idPerfil");
                        if (idPerfil != null && perfis.Select(x => x.ID).Contains(idPerfil.Value))
                        {
                            ficheiro.Perfil = perfis.Single(x => x.ID == idPerfil.Value);
                        }
                        else
                        {
                            //Redundante, mas não tem mal assegurar que o valor está correto
                            ficheiro.Perfil = null;
                        }

                        var dCriado = reader.GetDateTime("dataCriado");
                        ficheiro.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(ficheiro);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<FicheiroUtilizador> Utilizador_Ficheiros_LS(string cString, bool incluirApagados = false)
            {
                using(var con = new SqlConnection(cString))
                {
                    return Utilizador_Ficheiros_LS(con, incluirApagados);
                }
            }
            public static FicheiroUtilizador Utilizador_Ficheiros_S(SqlConnection con, int id, bool incluirApagados = false)
            {
                var res = new FicheiroUtilizador();
                var tipos = RCUDB.RCU.Utilizador_Ficheiros_Tipo_LS(con, incluirApagados);

                using (var cmd = new SqlCommand("[stp_Utilizador_Ficheiros_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res != null && reader.Read())
                    {
                        var ficheiro = new FicheiroUtilizador()
                        {
                            ID = reader.GetInt32("id").Value,
                            //Utilizador = utilizadores.Single(x=>x.ID == reader.GetInt32("idUtilizador").Value),
                            //Perfil = reader.GetInt32("idPerfil").HasValue ? perfis.Single(x=>x.ID == reader.GetInt32("idPerfil").Value) : null,
                            SharePointURL = reader.GetString("sharepointURL"),
                            SharePointList = reader.GetString("sharepointList"),
                            SharePointFolder = reader.GetString("sharepointFolder"),
                            Filename = reader.GetString("filename"),
                            Tipo = tipos.Single(x => reader.GetInt32("idTipo").HasValue && x.ID == reader.GetInt32("idTipo").Value),
                            Ativo = reader.GetBoolean("ficheiroAtivo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var idUtilizador = reader.GetInt32("idUtilizador");
                        if (idUtilizador != null)
                        {
                            //Passa-se a connectionstring para fazer uma nova conexão enquanto tem um reader aberto.
                            ficheiro.Utilizador = Utilizador_PorID_S(con.ConnectionString, idUtilizador.Value);
                        }
                        else
                        {
                            //TRATAR ERRO
                            //Ao contrário do perfil, não é suposto o ficheiro não ter utilizador
                        }

                        var idPerfil = reader.GetInt32("idPerfil");
                        if (idPerfil != null && ficheiro.Utilizador.Perfil.Select(x => x.ID).Contains(idPerfil.Value))
                        {
                            ficheiro.Perfil = ficheiro.Utilizador.Perfil.Single(x => x.ID == idPerfil.Value);
                        }
                        else
                        {
                            //Redundante, mas não tem mal assegurar que o valor está correto
                            ficheiro.Perfil = null;
                        }

                        var dCriado = reader.GetDateTime("dataCriado");
                        ficheiro.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res = ficheiro;
                    }

                    reader.Close();

                    return res;
                }
            }
            public static FicheiroUtilizador Utilizador_Ficheiros_S(string cString, int id, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Ficheiros_S(con, id, incluirApagados);
                }
            }
            public static FicheiroUtilizador Utilizador_Ficheiros_PorPerfil_S(SqlConnection con, int idPerfil, bool incluirApagados = false)
            {
                var res = new FicheiroUtilizador();
                var tipos = RCUDB.RCU.Utilizador_Ficheiros_Tipo_LS(con, incluirApagados);

                using (var cmd = new SqlCommand("[stp_Utilizador_Ficheiros_PorPerfil_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idPerfil", System.Data.SqlDbType.Int).Value = idPerfil;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res != null && reader.Read())
                    {
                        var ficheiro = new FicheiroUtilizador()
                        {
                            ID = reader.GetInt32("id").Value,
                            //Utilizador = utilizadores.Single(x=>x.ID == reader.GetInt32("idUtilizador").Value),
                            //Perfil = reader.GetInt32("idPerfil").HasValue ? perfis.Single(x=>x.ID == reader.GetInt32("idPerfil").Value) : null,
                            SharePointURL = reader.GetString("sharepointURL"),
                            SharePointList = reader.GetString("sharepointList"),
                            SharePointFolder = reader.GetString("sharepointFolder"),
                            Filename = reader.GetString("filename"),
                            Tipo = tipos.Single(x => reader.GetInt32("idTipo").HasValue && x.ID == reader.GetInt32("idTipo").Value),
                            Ativo = reader.GetBoolean("ficheiroAtivo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        ficheiro.Utilizador = Utilizador_PorID_S(con.ConnectionString, reader.GetInt32("idUtilizador").Value);
                        ficheiro.Perfil = ficheiro.Utilizador.Perfil.Single(x => x.ID == idPerfil);

                        if (ficheiro.Utilizador.Perfil.Select(x => x.ID).Contains(idPerfil))
                        {
                        }
                        else
                        {
                            //Redundante, mas não tem mal assegurar que o valor está correto
                            ficheiro.Perfil = null;
                        }

                        var dCriado = reader.GetDateTime("dataCriado");
                        ficheiro.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res = ficheiro;
                    }

                    reader.Close();

                    return res;
                }
            }
            public static FicheiroUtilizador Utilizador_Ficheiros_PorPerfil_S(string cString, int idPerfil, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Ficheiros_PorPerfil_S(con, idPerfil, incluirApagados);
                }
            }
            public static FicheiroUtilizador Utilizador_Ficheiros_PorTipo_S(SqlConnection con, int idTipo, bool incluirApagados = false)
            {
                var res = new FicheiroUtilizador();
                var tipos = RCUDB.RCU.Utilizador_Ficheiros_Tipo_LS(con, incluirApagados);

                using (var cmd = new SqlCommand("[stp_Utilizador_Ficheiros_PorTipo_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idTipo", System.Data.SqlDbType.Int).Value = idTipo;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res != null && reader.Read())
                    {
                        var ficheiro = new FicheiroUtilizador()
                        {
                            ID = reader.GetInt32("id").Value,
                            //Utilizador = utilizadores.Single(x=>x.ID == reader.GetInt32("idUtilizador").Value),
                            //Perfil = reader.GetInt32("idPerfil").HasValue ? perfis.Single(x=>x.ID == reader.GetInt32("idPerfil").Value) : null,
                            SharePointURL = reader.GetString("sharepointURL"),
                            SharePointList = reader.GetString("sharepointList"),
                            SharePointFolder = reader.GetString("sharepointFolder"),
                            Filename = reader.GetString("filename"),
                            Tipo = tipos.Single(x => reader.GetInt32("idTipo").HasValue && x.ID == reader.GetInt32("idTipo").Value),
                            Ativo = reader.GetBoolean("ficheiroAtivo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var idUtilizador = reader.GetInt32("idUtilizador");
                        if (idUtilizador != null)
                        {
                            //Passa-se a connectionstring para fazer uma nova conexão enquanto tem um reader aberto.
                            ficheiro.Utilizador = Utilizador_PorID_S(con.ConnectionString, idUtilizador.Value);
                        }
                        else
                        {
                            //TRATAR ERRO
                            //Ao contrário do perfil, não é suposto o ficheiro não ter utilizador
                        }

                        var idPerfil = reader.GetInt32("idPerfil");
                        if (idPerfil != null && ficheiro.Utilizador.Perfil.Select(x => x.ID).Contains(idPerfil.Value))
                        {
                            ficheiro.Perfil = ficheiro.Utilizador.Perfil.Single(x => x.ID == idPerfil.Value);
                        }
                        else
                        {
                            //Redundante, mas não tem mal assegurar que o valor está correto
                            ficheiro.Perfil = null;
                        }

                        var dCriado = reader.GetDateTime("dataCriado");
                        ficheiro.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res = ficheiro;
                    }

                    reader.Close();

                    return res;
                }
            }
            public static FicheiroUtilizador Utilizador_Ficheiros_PorTipo_S(string cString, int idTipo, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Ficheiros_PorTipo_S(con, idTipo, incluirApagados);
                }
            }
            public static FicheiroUtilizador Utilizador_Ficheiros_PorUtilizador_S(SqlConnection con, int idUtilizador, bool incluirApagados = false)
            {
                var res = new FicheiroUtilizador();
                var tipos = RCUDB.RCU.Utilizador_Ficheiros_Tipo_LS(con, incluirApagados);

                using (var cmd = new SqlCommand("[stp_Utilizador_Ficheiros_PorUtilizador_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idUtilizador", System.Data.SqlDbType.Int).Value = idUtilizador;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res != null && reader.Read())
                    {
                        var ficheiro = new FicheiroUtilizador()
                        {
                            ID = reader.GetInt32("id").Value,
                            //Utilizador = utilizadores.Single(x=>x.ID == reader.GetInt32("idUtilizador").Value),
                            //Perfil = reader.GetInt32("idPerfil").HasValue ? perfis.Single(x=>x.ID == reader.GetInt32("idPerfil").Value) : null,
                            SharePointURL = reader.GetString("sharepointURL"),
                            SharePointList = reader.GetString("sharepointList"),
                            SharePointFolder = reader.GetString("sharepointFolder"),
                            Filename = reader.GetString("filename"),
                            Tipo = tipos.Single(x => reader.GetInt32("idTipo").HasValue && x.ID == reader.GetInt32("idTipo").Value),
                            Ativo = reader.GetBoolean("ficheiroAtivo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        ficheiro.Utilizador = Utilizador_PorID_S(con.ConnectionString, idUtilizador);

                        var idPerfil = reader.GetInt32("idPerfil");
                        if (idPerfil != null && ficheiro.Utilizador.Perfil.Select(x => x.ID).Contains(idPerfil.Value))
                        {
                            ficheiro.Perfil = ficheiro.Utilizador.Perfil.Single(x => x.ID == idPerfil.Value);
                        }
                        else
                        {
                            //Redundante, mas não tem mal assegurar que o valor está correto
                            ficheiro.Perfil = null;
                        }

                        var dCriado = reader.GetDateTime("dataCriado");
                        ficheiro.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res = ficheiro;
                    }

                    reader.Close();

                    return res;
                }
            }
            public static FicheiroUtilizador Utilizador_Ficheiros_PorUtilizador_S(string cString, int idUtilizador, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Ficheiros_PorUtilizador_S(con, idUtilizador, incluirApagados);
                }
            }
            public static void Utilizador_Ficheiros_D(SqlConnection con, int idUtilizador, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Ficheiros_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idUtilizador", System.Data.SqlDbType.Int).Value = idUtilizador;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Ficheiros_D(string cString, int idUtilizador, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Ficheiros_D(con, idUtilizador, utilizadorEdicao);
                }
            }
            public static void Utilizador_Ficheiros_R(SqlConnection con, int idUtilizador, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idUtilizador", System.Data.SqlDbType.Int).Value = idUtilizador;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Ficheiros_R(string cString, int idUtilizador, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Ficheiros_R(con, idUtilizador, utilizadorEdicao);
                }
            }
            public static int Utilizador_Ficheiros_IU(SqlConnection con, FicheiroUtilizador ficheiro, string utilizadorEdicao)
            {
                if(ficheiro == null)
                {
                    throw new ArgumentException("O ficheiro não pode ser null");
                }
                else if(ficheiro.Utilizador == null)
                {
                    throw new ArgumentException("O utilizador associado ao ficheiro não pode ser null");
                }

                using (var cmd = new SqlCommand("[stp_Utilizador_Ficheiros_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = ficheiro.ID;
                    cmd.Parameters.Add("@idUtilizador", System.Data.SqlDbType.Int).Value = ficheiro.Utilizador.ID;
                    cmd.Parameters.Add("@idPerfil", System.Data.SqlDbType.Int).Value = ficheiro.Perfil != null ? ficheiro.Perfil.ID : (int?)null;
                    cmd.Parameters.Add("@sharepointURL", System.Data.SqlDbType.NVarChar).Value = ficheiro.SharePointURL;
                    cmd.Parameters.Add("@sharepointList", System.Data.SqlDbType.NVarChar).Value = ficheiro.SharePointList;
                    cmd.Parameters.Add("@sharepointFolder", System.Data.SqlDbType.NVarChar).Value = ficheiro.SharePointFolder;
                    cmd.Parameters.Add("@filename", System.Data.SqlDbType.NVarChar).Value = ficheiro.Filename;
                    cmd.Parameters.Add("@idTipo", System.Data.SqlDbType.Int).Value = ficheiro.Tipo.ID;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = ficheiro.Ativo;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var resDB = (object)null;


                    var reader = cmd.ExecuteReader();

                    while (resDB == null && reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            resDB = reader[0];
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    var res = -1;

                    if (int.TryParse(resDB.ToString(), out res) && res != -1)
                    {
                        return res;
                    }
                    else
                    {
                        if (res == -1)
                            throw new Exception(string.Format("Não foi possível executar a operação sobre o ficheiro {0}, na pasta {1} da biblioteca {2} no servidor SharePoint {3}", ficheiro.Filename, ficheiro.SharePointFolder, ficheiro.SharePointList, ficheiro.SharePointURL));
                        else
                            throw new Exception(string.Format("Não foi possível executar a operação sobre o ficheiro com o ID {0}", ficheiro.ID));
                    }
                }
            }
            public static int Utilizador_Ficheiros_IU(string cString, FicheiroUtilizador ficheiro, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Ficheiros_IU(con, ficheiro, utilizadorEdicao);
                }
            }

            public static void Utilizador_Ficheiros_Tipo_D(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Ficheiros_Tipos_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idTipo", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Ficheiros_Tipo_D(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Ficheiros_Tipo_D(con, id, utilizadorEdicao);
                }
            }
            public static void Utilizador_Ficheiros_Tipo_R(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Ficheiros_Tipos_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idTipo", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Ficheiros_Tipo_R(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Ficheiros_Tipo_R(con, id, utilizadorEdicao);
                }
            }
            public static FicheiroUtilizador.TipoFicheiroUtilizador Utilizador_Ficheiros_Tipo_S(SqlConnection con, int id, bool incluirApagados = false)
            {
                var tipo = (FicheiroUtilizador.TipoFicheiroUtilizador)null;

                using (var cmd = new SqlCommand("[stp_Utilizador_Ficheiros_Tipos_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (tipo == null && reader.Read())
                    {
                        tipo = new FicheiroUtilizador.TipoFicheiroUtilizador()
                        {
                            ID = reader.GetInt32("id").Value,
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            DataCriadoObject = reader.GetDateTime("dataCriado").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        tipo.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;
                    }

                    reader.Close();
                }

                return tipo;
            }
            public static FicheiroUtilizador.TipoFicheiroUtilizador Utilizador_Ficheiros_Tipo_S(string cString, int id, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Ficheiros_Tipo_S(con, id, incluirApagados);
                }
            }
            public static IEnumerable<FicheiroUtilizador.TipoFicheiroUtilizador> Utilizador_Ficheiros_Tipo_LS(SqlConnection con, bool incluirApagados = false)
            {
                var res = new List<FicheiroUtilizador.TipoFicheiroUtilizador>();

                using (var cmd = new SqlCommand("[stp_Utilizador_Ficheiros_Tipos_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var tipo = new FicheiroUtilizador.TipoFicheiroUtilizador()
                        {
                            ID = reader.GetInt32("id").Value,
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        tipo.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(tipo);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<FicheiroUtilizador.TipoFicheiroUtilizador> Utilizador_Ficheiros_Tipo_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Ficheiros_Tipo_LS(con, incluirApagados);
                }
            }
            public static int Utilizador_Ficheiros_Tipo_IU(SqlConnection con, FicheiroUtilizador.TipoFicheiroUtilizador.TipoFicheiroUtilizadorUpdate tipo)
            {
                int res = -1;
                using (var cmd = new SqlCommand("[stp_Utilizador_Ficheiros_Tipos_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idTipo", System.Data.SqlDbType.Int).Value = tipo.ID;
                    cmd.Parameters.Add("@nome", System.Data.SqlDbType.NVarChar).Value = tipo.Nome;
                    cmd.Parameters.Add("@descricao", System.Data.SqlDbType.NVarChar).Value = tipo.Descricao;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = tipo.Ativo;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = tipo.UtilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static int Utilizador_Ficheiros_Tipo_IU(string cString, FicheiroUtilizador.TipoFicheiroUtilizador.TipoFicheiroUtilizadorUpdate tipo)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Ficheiros_Tipo_IU(con, tipo);
                }
            }

            #endregion

            #endregion

            #region Contactos - done

            public static IEnumerable<Contacto> Utilizador_Contactos_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Contactos_LS(con, incluirApagados);
                }
            }
            public static IEnumerable<Contacto> Utilizador_Contactos_LS(SqlConnection con, bool incluirApagados = false)
            {
                var data = new List<Contacto>();
                var niveisAcesso = Utilizador_Visibilidade_LS(con, incluirApagados);

                using (var cmd = new SqlCommand("[stp_Utilizador_Contactos_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var c = new Contacto();

                        c.IUPI = reader.GetGuid("iupi").Value;

                        c.ID = reader.GetInt32("id").Value;
                        c.PropertyID = reader.GetInt32("propertyId").Value;
                        c.IDUtilizador = reader.GetInt32("idUtilizador").Value;
                        c.Valor = reader.GetString("propertyval");
                        c.Visibilidade = niveisAcesso.Single(x => x.ID == reader.GetInt32("visibilidadeContacto").Value);
                        c.UtilizadorCriado = reader.GetString("utilizadorCriado");
                        c.DataModificadoObject = reader.GetDateTime("dataModificado");
                        c.UtilizadorModificado = reader.GetString("utilizadorModificado");
                        c.Ativo = reader.GetBoolean("ativo").Value;

                        var dCriado = reader.GetDateTime("dataCriado");
                        c.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        var t = new Contacto.TipoContacto();
                        t.ID = reader.GetInt32("idTipo").Value;
                        t.Nome = reader.GetString("tipo");
                        t.tmp_oldText = reader.GetString("tmp_oldText");
                        c.Tipo = t;

                        var g = new Contacto.GrupoContacto();
                        g.ID = reader.GetInt32("idGrupo").Value;
                        g.Nome = reader.GetString("grupo");
                        t.Grupo = g;

                        data.Add(c);
                    }

                    reader.Close();

                    return data;
                }
            }
            public static IEnumerable<Contacto> Utilizador_Contactos_PorTipo_LS(string cString, int idTipo, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Contactos_PorTipo_LS(con, idTipo, incluirApagados);
                }
            }
            public static IEnumerable<Contacto> Utilizador_Contactos_PorTipo_LS(SqlConnection con, int idTipo, bool incluirApagados = false)
            {
                var data = new List<Contacto>();
                var niveisAcesso = Utilizador_Visibilidade_LS(con, incluirApagados);

                using (var cmd = new SqlCommand("[stp_Utilizador_Contactos_PorTipo_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idTipo", System.Data.SqlDbType.Int).Value = idTipo;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var c = new Contacto();

                        c.IUPI = reader.GetGuid("iupi").Value;

                        c.ID = reader.GetInt32("id").Value;
                        c.PropertyID = reader.GetInt32("propertyId").Value;
                        c.IDUtilizador = reader.GetInt32("idUtilizador").Value;
                        c.Valor = reader.GetString("propertyval");
                        c.Visibilidade = niveisAcesso.Single(x => x.ID == reader.GetInt32("visibilidadeContacto").Value);
                        c.UtilizadorCriado = reader.GetString("utilizadorCriado");
                        c.DataModificadoObject = reader.GetDateTime("dataModificado");
                        c.UtilizadorModificado = reader.GetString("utilizadorModificado");
                        c.Ativo = reader.GetBoolean("ativo").Value;

                        var dCriado = reader.GetDateTime("dataCriado");
                        c.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        var t = new Contacto.TipoContacto();
                        t.ID = reader.GetInt32("idTipo").Value;
                        t.Nome = reader.GetString("tipo");
                        t.tmp_oldText = reader.GetString("tmp_oldText");
                        c.Tipo = t;

                        var g = new Contacto.GrupoContacto();
                        g.ID = reader.GetInt32("idGrupo").Value;
                        g.Nome = reader.GetString("grupo");
                        t.Grupo = g;

                        data.Add(c);
                    }

                    reader.Close();

                    return data;
                }
            }
            public static IEnumerable<Contacto> Utilizador_Contactos_S(SqlConnection con, Guid iupi, bool incluirApagados = false)
            {
                var data = new List<Contacto>();
                var visibilidades = Utilizador_Visibilidade_LS(con, incluirApagados);

                using (var cmd = new SqlCommand("[stp_Utilizador_Contactos_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = iupi;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var c = new Contacto();

                        c.IUPI = iupi;

                        c.ID = reader.GetInt32("id").Value;
                        c.PropertyID = reader.GetInt32("propertyId").Value;
                        c.IDUtilizador = reader.GetInt32("idUtilizador").Value;
                        c.Valor = reader.GetString("propertyval");
                        c.Visibilidade = visibilidades.Single(x => x.ID == reader.GetInt32("visibilidadeContacto").Value);
                        c.UtilizadorCriado = reader.GetString("utilizadorCriado");
                        c.DataModificadoObject = reader.GetDateTime("dataModificado");
                        c.UtilizadorModificado = reader.GetString("utilizadorModificado");
                        c.Ativo = reader.GetBoolean("ativo").Value;

                        var dCriado = reader.GetDateTime("dataCriado");
                        c.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        var t = new Contacto.TipoContacto();
                        t.ID = reader.GetInt32("idTipo").Value;
                        t.Nome = reader.GetString("tipo");
                        c.Tipo = t;

                        var g = new Contacto.GrupoContacto();
                        g.ID = reader.GetInt32("idGrupo").Value;
                        g.Nome = reader.GetString("grupo");
                        t.Grupo = g;

                        data.Add(c);
                    }

                    reader.Close();

                    return data;
                }
            }
            public static IEnumerable<Contacto> Utilizador_Contactos_S(string cString, Guid iupi, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Contactos_S(con, iupi, incluirApagados);
                }
            }
            #region Utilizador_Contactos_IU_2
            //public static int Utilizador_Contactos_IU_2(SqlConnection con, Contacto.ContactoUpdate contacto)
            //{
            //    int res = -1;
            //    using (var cmd = new SqlCommand("[stp_Utilizador_Contactos_IU]", con))
            //    {
            //        cmd.CommandType = System.Data.CommandType.StoredProcedure;

            //        cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = contacto.IUPI;
            //        cmd.Parameters.Add("@idContacto", System.Data.SqlDbType.Int).Value = contacto.IDContacto;
            //        cmd.Parameters.Add("@idTipo", System.Data.SqlDbType.Int).Value = contacto.IDTipo;
            //        cmd.Parameters.Add("@val", System.Data.SqlDbType.NVarChar).Value = contacto.Valor;
            //        cmd.Parameters.Add("@public", System.Data.SqlDbType.Bit).Value = contacto.Publico;
            //        cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = contacto.UtilizadorEdicao;

            //        if (con.State != System.Data.ConnectionState.Open) con.Open();

            //        var reader = cmd.ExecuteReader();

            //        while (res == -1 && reader.Read())
            //        {
            //            if (string.IsNullOrWhiteSpace(reader.GetName(0)))
            //            {
            //                res = Convert.ToInt32(reader[0]);
            //            }
            //            else
            //            {
            //                throw new DBErrorException(reader);
            //            }
            //        }

            //        reader.Close();

            //        return res;
            //    }
            //}
            //public static int Utilizador_Contactos_IU_2(string cString, Contacto.ContactoUpdate contacto)
            //{
            //    using (var con = new SqlConnection(cString))
            //    {
            //        return Utilizador_Contactos_IU_2(con, contacto);
            //    }
            //}
            #endregion
            public static int Utilizador_Contactos_IU(SqlConnection con, Contacto.ContactoUpdate contacto)
            {
                int res = -1;

                using (var cmd = new SqlCommand("[stp_Utilizador_Contactos_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = contacto.IUPI;
                    cmd.Parameters.Add("@propertyID", System.Data.SqlDbType.Int).Value = contacto.PropertyID;
                    cmd.Parameters.Add("@idTipo", System.Data.SqlDbType.Int).Value = contacto.IDTipo;
                    cmd.Parameters.Add("@val", System.Data.SqlDbType.NVarChar).Value = contacto.Valor;
                    cmd.Parameters.Add("@visibilidade", System.Data.SqlDbType.Int).Value = contacto.Visibilidade;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = contacto.Ativo;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = contacto.UtilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static int Utilizador_Contactos_IU(string cString, Contacto.ContactoUpdate contacto)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Contactos_IU(con, contacto);
                }
            }
            public static IEnumerable<ContactoReturnValue> Utilizador_Contactos_IU_TabelaEntrada(SqlConnection con, IEnumerable<ContactoType> contactos)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Contactos_IU_TabelaEntrada]", con))
                {
                    var dt = ContactoType.CreateDataTable();

                    foreach (var r in contactos)
                    {
                        dt.Rows.Add(r.ToDataRow());
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    var param = cmd.Parameters.AddWithValue("@contactos", dt);
                    param.SqlDbType = SqlDbType.Structured;
                    param.TypeName = "dbo.ContactoType";

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    var res = new List<ContactoReturnValue>();

                    while (reader.Read())
                    {
                        res.Add(new ContactoReturnValue(reader.GetGuid("IUPI").Value, reader.GetInt32("PropertyID"), reader.GetString("Accao")));
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<ContactoReturnValue> Utilizador_Contactos_IU_TabelaEntrada(string cString, IEnumerable<ContactoType> contactos)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Contactos_IU_TabelaEntrada(con, contactos);
                }
            }

            public static void Utilizador_Contactos_D(SqlConnection con, Guid iupi, int idContacto, string utilizadorEdicao)
            {
                var user = new Utilizador()
                {
                    IUPI = Guid.Empty
                };

                using (var cmd = new SqlCommand("[stp_Utilizador_Contactos_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = iupi;
                    cmd.Parameters.Add("@idContacto", System.Data.SqlDbType.Int).Value = idContacto;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Contactos_D(string cString, Guid iupi, int idContacto, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Contactos_D(con, iupi, idContacto, utilizadorEdicao);
                }
            }

            public static void Utilizador_WebsitePerfil_Contactos_Replace(SqlConnection con, Guid iupi, IEnumerable<Contacto> contactos, string utilizadorEdicao)
            {
                using (var scope = new TransactionScope())
                {
                    int res = -1;

                    using (var cmd = new SqlCommand("[stp_WebsitePerfil_Contactos_Replace_D]", con))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = iupi;
                        cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                        if (con.State != System.Data.ConnectionState.Open) con.Open();

                        var reader = cmd.ExecuteReader();

                        while (res == -1 && reader.Read())
                        {
                            if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                            {
                                res = Convert.ToInt32(reader[0]);
                            }
                            else
                            {
                                throw new DBErrorException(reader);
                            }
                        }

                        reader.Close();
                    }

                    //for (int i = 0; i < contactos.Count(); i++)
                    //{
                    //    contactos.ElementAt(i).PropertyID = i + 1;
                    //}

                    Utilizador_Contactos_IU_TabelaEntrada(con, contactos.ToDBType(utilizadorEdicao));

                    scope.Complete();
                }
            }
            public static void Utilizador_WebsitePerfil_Contactos_Replace(string cString, Guid iupi, IEnumerable<Contacto> contactos, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_WebsitePerfil_Contactos_Replace(con, iupi, contactos, utilizadorEdicao);
                }
            }

            public static void Utilizador_Contactos_R(SqlConnection con, Guid iupi, int idContacto, string utilizadorEdicao)
            {
                var user = new Utilizador()
                {
                    IUPI = Guid.Empty
                };

                using (var cmd = new SqlCommand("[stp_Utilizador_Contactos_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = iupi;
                    cmd.Parameters.Add("@idContacto", System.Data.SqlDbType.Int).Value = idContacto;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Contactos_R(string cString, Guid iupi, int idContacto, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Contactos_R(con, iupi, idContacto, utilizadorEdicao);
                }
            }

            public static IEnumerable<Contacto.TipoContacto> Utilizador_Contactos_Tipos_LS(SqlConnection con, bool incluirApagados = false)
            {
                var data = new List<Contacto.TipoContacto>();

                using (var cmd = new SqlCommand("[stp_WebsitePerfil_Contactos_Tipos_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var t = new Contacto.TipoContacto();

                        t.ID = reader.GetInt32("id").Value;
                        t.Nome = reader.GetString("tipo");
                        t.Ativo = reader.GetBoolean("tipoAtivo").Value;

                        var g = new Contacto.GrupoContacto();

                        g.ID = reader.GetInt32("idGrupo").Value;
                        g.Nome = reader.GetString("grupo");
                        g.Ativo = reader.GetBoolean("grupoAtivo").Value;

                        t.Grupo = g;

                        data.Add(t);
                    }

                    reader.Close();

                    return data;
                }
            }
            public static IEnumerable<Contacto.TipoContacto> Utilizador_Contactos_Tipos_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Contactos_Tipos_LS(con, incluirApagados);
                }
            }

            public static IEnumerable<User> GetContactsSitePessoas(string cString, IEnumerable<string> flagsToSend = null)
            {
                return GetContactsSitePessoasPorVisibilidade(cString, 3, flagsToSend);
            }
            public static IEnumerable<User> GetContactsSitePessoasPorVisibilidade(string cString, int visibilidade, IEnumerable<string> flagsToSend = null)
            {
                var data = new List<User>();

                var users = Utilizador_LS(cString).Where(x => !string.IsNullOrWhiteSpace(x.Username) && x.EmFuncoes && x.Visibilidade != null && x.Visibilidade.ID >= visibilidade).ToList();
                var contactos = Utilizador_Contactos_LS(cString).Where(x => x.Visibilidade != null && x.Visibilidade.ID >= visibilidade).ToList();
                var flags = Utilizador_Flags_LS(cString).Where(f => flagsToSend.Contains(f.Flag.Key)).ToList();

                foreach (var u in users.Where(u => u.EmFuncoes))
                {
                    var user = new User()
                    {
                        Username = u.Username,
                        Name = string.IsNullOrWhiteSpace(u.NomePersonalizado) ? u.Nome.ToUpperInvariant() : u.NomePersonalizado.ToUpperInvariant(),
                        Visibility = u.Visibilidade.ID,
                        ID = u.ID
                    };

                    //Contactos duplicados
                    var contactosUser = contactos.Where(x => x.IDUtilizador == u.ID).ToList();

                    if (u.Flags.Select(f => f.Flag.ID).Contains(5))
                    {
                        contactosUser.Add(new Contacto()
                        {
                            ID = -1,
                            Tipo = new Contacto.TipoContacto()
                            {
                                Nome = "email",
                                Grupo = new Contacto.GrupoContacto()
                                {
                                    Nome = "Contactos"
                                }
                            },
                            Valor = u.Email,
                            Visibilidade = new Visibilidade()
                            {
                                ID = Convert.ToInt32(u.Flags.Where(f => f.Flag.ID == 5).First().Value)
                            }
                        });
                    }

                    var grupos = contactosUser.Select(x => x.Tipo.Grupo).Select(x => new ContactGroup() { Name = x.Nome });

                    foreach (var g in grupos)
                    {
                        if(user.ContactGroups != null && user.ContactGroups.Where(x=>x.Name == g.Name).Count() == 0)
                        {
                            g.Contacts = contactosUser.Where(x => x.Tipo.Grupo.Nome == g.Name).Select(x => new Contact() { Label = x.Tipo.tmp_oldText ?? x.Tipo.Nome, Value = x.Valor, Visibility = x.Visibilidade.ID }).ToList();
                            user.ContactGroups.Add(g);
                        }
                    }

                    foreach (var p in u.Perfil.Where(p => p.EmFuncoes))
                    {
                        var userProfile = new Profile();

                        userProfile.ID = p.ID;
                        if(p.Entidade != null)
                        {
                            var uo = new OrganicUnit() { Name = p.Entidade.Descricao };
                            if (uo != null && p.Categoria != null && !uo.Category.Select(x => x.Name).Contains(p.Categoria.Descricao)) uo.Category.Add(new Category() { Name = p.Categoria.Descricao });
                            userProfile.OU = uo;
                        }

                        user.Profiles.Add(userProfile);
                    }

                    if(flagsToSend != null)
                    {
                        var toAddUsr = flags.Where(f => f.IDUtilizador == u.ID && f.IDPerfil == null).Select(x => new SiteUTADFlag() { Key = x.Flag.Key, Value = x.Value });
                        if (toAddUsr.Count() > 0) user.Flags.AddRange(toAddUsr);
                    }

                    foreach (var p in user.Profiles)
                    {
                        var toAddPrfl = flags.Where(f => f.IDUtilizador == u.ID && f.IDPerfil == p.ID).Select(x => new SiteUTADFlag() { Key = x.Flag.Key, Value = x.Value });
                        if (toAddPrfl.Count() > 0) p.Flags.AddRange(toAddPrfl);
                    }

                    data.Add(user);
                }

                
                #region old
                //var dataFromDB = new List<RawFromDB>();
                //var data = new List<User>();

                //using (var con = new SqlConnection(cString))
                //using (var cmd = new SqlCommand("[stp_Contactos_PorVisibilidade_LS]", con))
                //{
                //    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                //    cmd.Parameters.Add("@visibilidade", SqlDbType.Int).Value = visibilidade;

                //    if (con.State != System.Data.ConnectionState.Open) con.Open();

                //    var reader = cmd.ExecuteReader();

                //    while (reader.Read())
                //    {
                //        var d = new RawFromDB()
                //        {
                //            id = reader.GetInt32("id").Value,
                //            idPerfil = reader.GetInt32("idPerfil"),
                //            nomedeutilizador = reader.GetString("username"),
                //            nome = reader.GetString("nome"),
                //            uo = reader.GetString("UO"),
                //            categoria = reader.GetString("categoria"),
                //            contactgroup = reader.GetString("contactGroup"),
                //            contactlabel = reader.GetString("label"),
                //            contactvalue = reader.GetString("value"),
                //            visibilidadeUtilizador = reader.GetInt32("visibilidadeUtilizador").Value,
                //            visibilidadeUtilizadorNome = reader.GetString("visibilidadeUtilizadorNome"),
                //            visibilidadePerfil = reader.GetInt32("visibilidadePerfil"),
                //            visibilidadePerfilNome = reader.GetString("visibilidadePerfilNome")
                //        };

                //        if (reader.GetInt32("visibilidadeContacto").HasValue)
                //        {
                //            d.visibilidadeContacto = reader.GetInt32("visibilidadeContacto").Value;
                //            d.visibilidadeContactoNome = reader.GetString("visibilidadeContactoNome");

                //        }

                //        dataFromDB.Add(d);
                //    }

                //    con.Close();
                //}

                ////ADICIONA OS USERNAMES, EMAIL
                //foreach (var c in dataFromDB.GroupBy(x => x.nomedeutilizador))
                //{
                //    try
                //    {
                //        var u = new User()
                //        {
                //            Username = c.Key,
                //            Name = c.First().nome,
                //            Visibility = c.First().visibilidadeUtilizador,
                //            ID = c.First().id
                //        };

                //        //um pouco de fita cola fica sempre bem
                //        bool emailSet = false;

                //        foreach (var line in c)
                //        {
                //            if (line.idPerfil.HasValue && !u.Profiles.Select(x => x.ID).Contains(line.idPerfil.Value))
                //            {
                //                var p = new Profile()
                //                {
                //                    ID = line.idPerfil.Value
                //                };

                //                if (!string.IsNullOrWhiteSpace(line.uo))
                //                {
                //                    var uo = new OrganicUnit() { Name = line.uo };
                //                    if (uo != null && !string.IsNullOrWhiteSpace(line.categoria) && !uo.Category.Select(x => x.Name).Contains(line.categoria)) uo.Category.Add(new Category() { Name = line.categoria });
                //                    p.OU = uo;
                //                }

                //                u.Profiles.Add(p);
                //            }

                //            if (!string.IsNullOrWhiteSpace(line.contactgroup))
                //            {
                //                var contactGroup = new ContactGroup() { Name = line.contactgroup };

                //                if (u.ContactGroups.Where(x => x.Name == line.contactgroup).Count() == 0) u.ContactGroups.Add(contactGroup);
                //                else contactGroup = u.ContactGroups.Single(x => x.Name == line.contactgroup);

                //                if(line.contactlabel.ToLowerInvariant() != "email" || !emailSet)
                //                {
                //                    contactGroup.Contacts.Add(new Contact() { Label = line.contactlabel, Value = line.contactvalue, Visibility = line.visibilidadeContacto });
                //                }
                //            }
                //        }
                    //if (flagsToSend != null)
                    //{
                    //    var flags = Utilizador_Flags_LS(cString).Where(f => flagsToSend.Contains(f.Flag.Key));

                    //    foreach (var u in data)
                    //    {
                    //        //;)
                    //        var toAddUsr = flags.Where(f => f.IDUtilizador == u.ID && f.IDPerfil == null).Select(x => new SiteUTADFlag() { Key = x.Flag.Key, Value = x.Value });
                    //        if (toAddUsr.Count() > 0) u.Flags.AddRange(toAddUsr);

                    //        foreach (var p in u.Profiles)
                    //        {
                    //            var toAddPrfl = flags.Where(f => f.IDUtilizador == u.ID && f.IDPerfil == p.ID).Select(x => new SiteUTADFlag() { Key = x.Flag.Key, Value = x.Value });
                    //            if (toAddPrfl.Count() > 0) p.Flags.AddRange(toAddPrfl);
                    //        }
                    //    }
                    //}
                //        data.Add(u);
                //    }
                //    catch (Exception ex)
                //    {
                //        var newEx = new Exception(string.Format("Erro no utilizador {0}", c.Key), ex);
                //        throw newEx;
                //    }
                //}
                #endregion


                return data;
            }
            #endregion

            #region Categorias - done

            public static void Utilizador_Categorias_D(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Categorias_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Categorias_D(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Categorias_D(con, id, utilizadorEdicao);
                }
            }
            public static void Utilizador_Categorias_R(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Categorias_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Categorias_R(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Categorias_R(con, id, utilizadorEdicao);
                }
            }
            public static Categoria Utilizador_Categorias_S(SqlConnection con, int id, bool incluirApagados = false)
            {
                var res = (Categoria)null;

                using (var cmd = new SqlCommand("[stp_Utilizador_Categorias_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == null && reader.Read())
                    {
                        res = new Categoria()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        res.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;
                    }

                    reader.Close();

                    return res;
                }
            }
            public static Categoria Utilizador_Categorias_S(string cString, int id, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Categorias_S(con, id, incluirApagados);
                }
            }
            public static IEnumerable<Categoria> Utilizador_Categorias_LS(SqlConnection con, bool incluirApagados = false)
            {
                var res = new List<Categoria>();
                var carreiras = RCUDB.RCU.Utilizador_Carreiras_LS(con, incluirApagados);

                using (var cmd = new SqlCommand("[stp_Utilizador_Categorias_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var cat = new Categoria()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var idCarreira = reader.GetInt32("idCarreira");
                        if (idCarreira != null && idCarreira.HasValue && carreiras.Select(x => x.ID).Contains(idCarreira.Value))
                        {
                            cat.Carreira = carreiras.Single(x => x.ID == idCarreira.Value);
                        }

                        var dCriado = reader.GetDateTime("dataCriado");
                        cat.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(cat);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<Categoria> Utilizador_Categorias_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Categorias_LS(con, incluirApagados);
                }
            }
            public static int Utilizador_Categorias_IU(SqlConnection con, Categoria.CategoriaUpdate categoria)
            {
                int res = -1;
                using (var cmd = new SqlCommand("[stp_Utilizador_Categorias_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = categoria.ID;
                    cmd.Parameters.Add("@codigo", System.Data.SqlDbType.NVarChar).Value = categoria.Codigo;
                    cmd.Parameters.Add("@nome", System.Data.SqlDbType.NVarChar).Value = categoria.Nome;
                    cmd.Parameters.Add("@descricao", System.Data.SqlDbType.NVarChar).Value = categoria.Descricao;
                    cmd.Parameters.Add("@idCarreira", System.Data.SqlDbType.Int).Value = categoria.IDCarreira;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = categoria.Ativo;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = categoria.UtilizadorEdicao;
                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static int Utilizador_Categorias_IU(string cString, Categoria.CategoriaUpdate categoria)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Categorias_IU(con, categoria);
                }
            }
            public static List<CategoriaReturnValue> Utilizador_Categorias_IU_TabelaEntrada(SqlConnection con, IEnumerable<CategoriaType> categorias)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Categorias_IU_TabelaEntrada]", con))
                {
                    var dt = CategoriaType.CreateDataTable();

                    foreach (var c in categorias)
                    {
                        dt.Rows.Add(c.ToDataRow());
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    var param = cmd.Parameters.AddWithValue("@categorias", dt);
                    param.SqlDbType = System.Data.SqlDbType.Structured;
                    param.TypeName = "dbo.CategoriaType";

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    var res = new List<CategoriaReturnValue>();

                    while (reader.Read())
                    {
                        res.Add(new CategoriaReturnValue(reader.GetString("Codigo"), reader.GetInt32("ID").Value, reader.GetString("Accao")));
                    }

                    reader.Close();

                    return res;
                }
            }
            public static List<CategoriaReturnValue> Utilizador_Categorias_IU_TabelaEntrada(string cString, IEnumerable<CategoriaType> categorias)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Categorias_IU_TabelaEntrada(con, categorias);
                }
            }

            #endregion

            #region Cargos - done

            public static void Utilizador_Cargos_D(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Cargos_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Cargos_D(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Cargos_D(con, id, utilizadorEdicao);
                }
            }
            public static void Utilizador_Cargos_R(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Cargos_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Cargos_R(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Cargos_R(con, id, utilizadorEdicao);
                }
            }
            public static IEnumerable<Cargo.CargoFromListagem> Utilizador_Cargos_PorEntidade_S(SqlConnection con, int idEntidade, bool incluirApagados = false)
            {
                var res = new List<Cargo.CargoFromListagem>();

                using (var cmd = new SqlCommand("[stp_Utilizador_Cargos_PorEntidade_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idEntidade", System.Data.SqlDbType.Int).Value = idEntidade;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.NVarChar).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var cargo = new Cargo.CargoFromListagem()
                        {
                            IDCargo = reader.GetInt32("idCargo").Value,
                            Email = reader.GetString("emailCargo"),
                            IDPerfil = reader.GetInt32("idPerfil").Value,
                            IDUtilizador = reader.GetInt32("idUtilizador").Value,
                            IUPI = reader.GetGuid("iupi").Value,
                            UtilizadorNome = reader.GetString("nome"),
                            UtilizadorUsername = reader.GetString("username"),
                            UtilizadorNumeroMecanografico = reader.GetString("nmec"),
                            UtilizadorEmail = reader.GetString("emailUser"),
                            IDEntidade = reader.GetInt32("idEntidade").Value,
                            EntidadeSigla = reader.GetString("siglaEntidade"),
                            EntidadeNome = reader.GetString("nomeEntidade"),
                            EntidadeDescricao = reader.GetString("descricaoEntidade"),
                            IDTipoCargo = reader.GetInt32("idTipoCargo").Value,
                            TipoCargoCodigo = reader.GetString("codigoTipo"),
                            TipoCargoNome = reader.GetString("nomeTipo"),
                            TipoCargoDescricao = reader.GetString("descricaoTipo"),
                            DataInicioObject = reader.GetDateTime("dataInicio").Value,
                            DataFimObject = reader.GetDateTime("dataFim"),
                            Ativo = reader.GetBoolean("cargoAtivo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        cargo.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(cargo);
                    }

                    reader.Close();
                }

                return res;
            }
            public static IEnumerable<Cargo.CargoFromListagem> Utilizador_Cargos_PorEntidade_S(string cString, int idEntidade, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Cargos_PorEntidade_S(con, idEntidade, incluirApagados);
                }
            }
            public static IEnumerable<Cargo.CargoFromListagem> Utilizador_Cargos_PorIUPI_S(SqlConnection con, Guid iupi, bool incluirApagados = false)
            {
                var res = new List<Cargo.CargoFromListagem>();

                using (var cmd = new SqlCommand("[stp_Utilizador_Cargos_PorIUPI_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@iupi", System.Data.SqlDbType.UniqueIdentifier).Value = iupi;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.NVarChar).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var cargo = new Cargo.CargoFromListagem()
                        {
                            IDCargo = reader.GetInt32("idCargo").Value,
                            Email = reader.GetString("emailCargo"),
                            IDPerfil = reader.GetInt32("idPerfil").Value,
                            IDUtilizador = reader.GetInt32("idUtilizador").Value,
                            IUPI = reader.GetGuid("iupi").Value,
                            UtilizadorNome = reader.GetString("nome"),
                            UtilizadorUsername = reader.GetString("username"),
                            UtilizadorNumeroMecanografico = reader.GetString("nmec"),
                            UtilizadorEmail = reader.GetString("emailUser"),
                            IDEntidade = reader.GetInt32("idEntidade").Value,
                            EntidadeSigla = reader.GetString("siglaEntidade"),
                            EntidadeNome = reader.GetString("nomeEntidade"),
                            EntidadeDescricao = reader.GetString("descricaoEntidade"),
                            IDTipoCargo = reader.GetInt32("idTipoCargo").Value,
                            TipoCargoCodigo = reader.GetString("codigoTipo"),
                            TipoCargoNome = reader.GetString("nomeTipo"),
                            TipoCargoDescricao = reader.GetString("descricaoTipo"),
                            DataInicioObject = reader.GetDateTime("dataInicio").Value,
                            DataFimObject = reader.GetDateTime("dataFim"),
                            Ativo = reader.GetBoolean("cargoAtivo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        cargo.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(cargo);
                    }

                    reader.Close();
                }

                return res;
            }
            public static IEnumerable<Cargo.CargoFromListagem> Utilizador_Cargos_PorIUPI_S(string cString, Guid iupi, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Cargos_PorIUPI_S(con, iupi, incluirApagados);
                }
            }
            public static IEnumerable<Cargo.CargoFromListagem> Utilizador_Cargos_PorTipoCargo_S(SqlConnection con, int idTipoCargo, bool incluirApagados = false)
            {
                var res = new List<Cargo.CargoFromListagem>();

                using (var cmd = new SqlCommand("[stp_Utilizador_Cargos_PorTipoCargo_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idTipoCargo", System.Data.SqlDbType.Int).Value = idTipoCargo;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.NVarChar).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var cargo = new Cargo.CargoFromListagem()
                        {
                            IDCargo = reader.GetInt32("idCargo").Value,
                            Email = reader.GetString("emailCargo"),
                            IDPerfil = reader.GetInt32("idPerfil").Value,
                            IDUtilizador = reader.GetInt32("idUtilizador").Value,
                            IUPI = reader.GetGuid("iupi").Value,
                            UtilizadorNome = reader.GetString("nome"),
                            UtilizadorUsername = reader.GetString("username"),
                            UtilizadorNumeroMecanografico = reader.GetString("nmec"),
                            UtilizadorEmail = reader.GetString("emailUser"),
                            IDEntidade = reader.GetInt32("idEntidade").Value,
                            EntidadeSigla = reader.GetString("siglaEntidade"),
                            EntidadeNome = reader.GetString("nomeEntidade"),
                            EntidadeDescricao = reader.GetString("descricaoEntidade"),
                            IDTipoCargo = reader.GetInt32("idTipoCargo").Value,
                            TipoCargoCodigo = reader.GetString("codigoTipo"),
                            TipoCargoNome = reader.GetString("nomeTipo"),
                            TipoCargoDescricao = reader.GetString("descricaoTipo"),
                            DataInicioObject = reader.GetDateTime("dataInicio").Value,
                            DataFimObject = reader.GetDateTime("dataFim"),
                            Ativo = reader.GetBoolean("cargoAtivo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        cargo.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(cargo);
                    }

                    reader.Close();
                }

                return res;
            }
            public static IEnumerable<Cargo.CargoFromListagem> Utilizador_Cargos_PorTipoCargo_S(string cString, int idTipoCargo, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Cargos_PorTipoCargo_S(con, idTipoCargo, incluirApagados);
                }
            }
            public static Cargo.CargoFromListagem Utilizador_Cargos_S(SqlConnection con, int id, bool incluirApagados = false)
            {
                var cargo = (Cargo.CargoFromListagem)null;
                using (var cmd = new SqlCommand("[stp_Utilizador_Cargos_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.NVarChar).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (cargo == null && reader.Read())
                    {
                        cargo = new Cargo.CargoFromListagem()
                        {
                            IDCargo = reader.GetInt32("idCargo").Value,
                            Email = reader.GetString("emailCargo"),
                            IDPerfil = reader.GetInt32("idPerfil").Value,
                            IDUtilizador = reader.GetInt32("idUtilizador").Value,
                            IUPI = reader.GetGuid("iupi").Value,
                            UtilizadorNome = reader.GetString("nome"),
                            UtilizadorUsername = reader.GetString("username"),
                            UtilizadorNumeroMecanografico = reader.GetString("nmec"),
                            UtilizadorEmail = reader.GetString("emailUser"),
                            IDEntidade = reader.GetInt32("idEntidade").Value,
                            EntidadeSigla = reader.GetString("siglaEntidade"),
                            EntidadeNome = reader.GetString("nomeEntidade"),
                            EntidadeDescricao = reader.GetString("descricaoEntidade"),
                            IDTipoCargo = reader.GetInt32("idTipoCargo").Value,
                            TipoCargoCodigo = reader.GetString("codigoTipo"),
                            TipoCargoNome = reader.GetString("nomeTipo"),
                            TipoCargoDescricao = reader.GetString("descricaoTipo"),
                            DataInicioObject = reader.GetDateTime("dataInicio").Value,
                            DataFimObject = reader.GetDateTime("dataFim"),
                            Ativo = reader.GetBoolean("cargoAtivo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        cargo.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;
                    }

                    reader.Close();
                }

                return cargo;
            }
            public static Cargo.CargoFromListagem Utilizador_Cargos_S(string cString, int id, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Cargos_S(con, id, incluirApagados);
                }
            }
            public static IEnumerable<Cargo.CargoFromListagem> Utilizador_Cargos_LS(SqlConnection con, bool incluirApagados = false)
            {
                var res = new List<Cargo.CargoFromListagem>();

                using (var cmd = new SqlCommand("[stp_Utilizador_Cargos_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.NVarChar).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var cargo = new Cargo.CargoFromListagem()
                        {
                            IDCargo = reader.GetInt32("idCargo").Value,
                            Email = reader.GetString("emailCargo"),
                            IDPerfil = reader.GetInt32("idPerfil").Value,
                            IDUtilizador = reader.GetInt32("idUtilizador").Value,
                            IUPI = reader.GetGuid("iupi").Value,
                            UtilizadorNome = reader.GetString("nome"),
                            UtilizadorUsername = reader.GetString("username"),
                            UtilizadorNumeroMecanografico = reader.GetString("nmec"),
                            UtilizadorEmail = reader.GetString("emailUser"),
                            IDEntidade = reader.GetInt32("idEntidade").Value,
                            EntidadeSigla = reader.GetString("siglaEntidade"),
                            EntidadeNome = reader.GetString("nomeEntidade"),
                            EntidadeDescricao = reader.GetString("descricaoEntidade"),
                            IDTipoCargo = reader.GetInt32("idTipoCargo").Value,
                            TipoCargoCodigo = reader.GetString("codigoTipo"),
                            TipoCargoNome = reader.GetString("nomeTipo"),
                            TipoCargoDescricao = reader.GetString("descricaoTipo"),
                            DataInicioObject = reader.GetDateTime("dataInicio").Value,
                            DataFimObject = reader.GetDateTime("dataFim"),
                            Ativo = reader.GetBoolean("cargoAtivo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };


                        var dCriado = reader.GetDateTime("dataCriado");
                        cargo.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(cargo);
                    }

                    reader.Close();
                }

                return res;
            }
            public static IEnumerable<Cargo.CargoFromListagem> Utilizador_Cargos_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Cargos_LS(con, incluirApagados);
                }
            }
            public static int Utilizador_Cargos_IU(SqlConnection con, Cargo.CargoUpdate cargo)
            {
                int res = -1;
                using (var cmd = new SqlCommand("[stp_Utilizador_Cargos_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = cargo.ID;
                    cmd.Parameters.Add("@idPerfil", System.Data.SqlDbType.Int).Value = cargo.IDPerfil;
                    cmd.Parameters.Add("@idEntidade", System.Data.SqlDbType.Int).Value = cargo.IDEntidade;
                    cmd.Parameters.Add("@idTipoCargo", System.Data.SqlDbType.Int).Value = cargo.IDTipo;
                    cmd.Parameters.Add("@dataInicio", System.Data.SqlDbType.DateTime).Value = cargo.DataInicio;
                    cmd.Parameters.Add("@email", System.Data.SqlDbType.NVarChar).Value = cargo.Email;
                    cmd.Parameters.Add("@dataFim", System.Data.SqlDbType.DateTime).Value = cargo.DataFim;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = cargo.Ativo;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = cargo.UtilizadorEdicao;
                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static int Utilizador_Cargos_IU(string cString, Cargo.CargoUpdate cargo)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Cargos_IU(con, cargo);
                }
            }


            public static void Utilizador_Cargos_Tipos_D(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Cargos_Tipos_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Cargos_Tipos_D(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Cargos_Tipos_D(con, id, utilizadorEdicao);
                }
            }
            public static void Utilizador_Cargos_Tipos_R(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Cargos_Tipos_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Cargos_Tipos_R(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Cargos_Tipos_R(con, id, utilizadorEdicao);
                }
            }
            public static Cargo.TipoCargo Utilizador_Cargos_Tipos_S(SqlConnection con, int id, bool incluirApagados = false)
            {
                var res = (Cargo.TipoCargo)null;
                using (var cmd = new SqlCommand("[stp_Utilizador_Cargos_Tipos_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        res = new Cargo.TipoCargo()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        res.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;
                    }

                    reader.Close();
                }

                return res;
            }
            public static Cargo.TipoCargo Utilizador_Cargos_Tipos_S(string cString, int id, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Cargos_Tipos_S(con, id, incluirApagados);
                }
            }
            public static IEnumerable<Cargo.TipoCargo> Utilizador_Cargos_Tipos_LS(SqlConnection con, bool incluirApagados = false)
            {
                var res = new List<Cargo.TipoCargo>();

                using (var cmd = new SqlCommand("[stp_Utilizador_Cargos_Tipos_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var cargo = new Cargo.TipoCargo()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        cargo.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(cargo);
                    }

                    reader.Close();
                }

                return res;
            }
            public static IEnumerable<Cargo.TipoCargo> Utilizador_Cargos_Tipos_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Cargos_Tipos_LS(con, incluirApagados);
                }

            }
            public static int Utilizador_Cargos_Tipos_IU(SqlConnection con, Cargo.TipoCargo.TipoCargoUpdate cargo)
            {
                int res = -1;
                using (var cmd = new SqlCommand("[stp_Utilizador_Cargos_Tipos_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    cmd.Parameters.Add("@idTipoCargo", System.Data.SqlDbType.Int).Value = cargo.ID;
                    cmd.Parameters.Add("@codigo", System.Data.SqlDbType.NVarChar).Value = cargo.Codigo;
                    cmd.Parameters.Add("@nome", System.Data.SqlDbType.NVarChar).Value = cargo.Nome;
                    cmd.Parameters.Add("@descricao", System.Data.SqlDbType.NVarChar).Value = cargo.Descricao;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = cargo.Ativo;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = cargo.UtilizadorEdicao;
                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static int Utilizador_Cargos_Tipos_IU(string cString, Cargo.TipoCargo.TipoCargoUpdate cargo)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Cargos_Tipos_IU(con, cargo);
                }
            }

            #endregion

            #region Carreiras - done

            public static void Utilizador_Carreiras_D(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Carreiras_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Carreiras_D(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Carreiras_D(con, id, utilizadorEdicao);
                }
            }
            public static void Utilizador_Carreiras_R(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Carreiras_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Carreiras_R(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Carreiras_R(con, id, utilizadorEdicao);
                }
            }
            public static Carreira Utilizador_Carreiras_S(SqlConnection con, int id, bool incluirApagados = false)
            {
                var res = (Carreira)null;
                using (var cmd = new SqlCommand("[stp_Utilizador_Carreiras_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        res = new Carreira()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        res.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        if (!reader.IsDBNull("idGrupo"))
                        {
                            var grupo = new Carreira.GrupoCarreira()
                            {
                                ID = reader.GetInt32("idGrupo").Value,
                                Codigo = reader.GetString("codigoGrupo"),
                                Nome = reader.GetString("nomeGrupo"),
                                Descricao = reader.GetString("descricaoGrupo"),
                                Ativo = reader.GetBoolean("ativoGrupo").Value,
                                UtilizadorCriado = reader.GetString("utilizadorCriadoGrupo"),
                                DataModificadoObject = reader.GetDateTime("dataModificadoGrupo"),
                                UtilizadorModificado = reader.GetString("utilizadorModificadoGrupo")
                            };

                            var dGrupoCriado = reader.GetDateTime("dataCriadoGrupo");
                            grupo.DataCriadoObject = dGrupoCriado != null && dGrupoCriado.HasValue ? dGrupoCriado.Value : DateTime.MinValue;

                            res.Grupo = grupo;
                        }

                    }

                    reader.Close();

                    return res;
                }
            }
            public static Carreira Utilizador_Carreiras_S(string cString, int id, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Carreiras_S(con, id, incluirApagados);
                }
            }
            public static IEnumerable<Carreira> Utilizador_Carreiras_LS(SqlConnection con, bool incluirApagados = false)
            {
                var res = new List<Carreira>();

                using (var cmd = new SqlCommand("[stp_Utilizador_Carreiras_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var carreira = new Carreira()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        carreira.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        if (!reader.IsDBNull("idGrupo"))
                        {
                            var grupo = new Carreira.GrupoCarreira()
                            {
                                ID = reader.GetInt32("idGrupo").Value,
                                Codigo = reader.GetString("codigoGrupo"),
                                Nome = reader.GetString("nomeGrupo"),
                                Descricao = reader.GetString("descricaoGrupo"),
                                Ativo = reader.GetBoolean("ativoGrupo").Value,
                                UtilizadorCriado = reader.GetString("utilizadorCriadoGrupo"),
                                DataModificadoObject = reader.GetDateTime("dataModificadoGrupo"),
                                UtilizadorModificado = reader.GetString("utilizadorModificadoGrupo")
                            };

                            var dGrupoCriado = reader.GetDateTime("dataCriadoGrupo");
                            grupo.DataCriadoObject = dGrupoCriado != null && dGrupoCriado.HasValue ? dGrupoCriado.Value : DateTime.MinValue;

                            carreira.Grupo = grupo;
                        }

                        res.Add(carreira);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<Carreira> Utilizador_Carreiras_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Carreiras_LS(con, incluirApagados);
                }
            }
            public static int Utilizador_Carreiras_IU(SqlConnection con, Carreira.CarreiraUpdate carreira)
            {
                int res = -1;
                using (var cmd = new SqlCommand("[stp_Utilizador_Carreiras_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = carreira.ID;
                    cmd.Parameters.Add("@codigo", System.Data.SqlDbType.NVarChar).Value = carreira.Codigo;
                    cmd.Parameters.Add("@nome", System.Data.SqlDbType.NVarChar).Value = carreira.Nome;
                    cmd.Parameters.Add("@descricao", System.Data.SqlDbType.NVarChar).Value = carreira.Descricao;
                    cmd.Parameters.Add("@idGrupo", System.Data.SqlDbType.Int).Value = carreira.IDGrupo;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = carreira.Ativo;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = carreira.UtilizadorEdicao;
                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static int Utilizador_Carreiras_IU(string cString, Carreira.CarreiraUpdate carreira)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Carreiras_IU(con, carreira);
                }
            }
            public static List<CarreiraReturnValue> Utilizador_Carreiras_IU_TabelaEntrada(SqlConnection con, IEnumerable<CarreiraType> carreiras)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Carreiras_IU_TabelaEntrada]", con))
                {
                    var dt = CarreiraType.CreateDataTable();

                    foreach (var r in carreiras)
                    {
                        dt.Rows.Add(r.ToDataRow());
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    var param = cmd.Parameters.AddWithValue("@carreiras", dt);
                    param.SqlDbType = SqlDbType.Structured;
                    param.TypeName = "dbo.CarreiraType";

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    var res = new List<CarreiraReturnValue>();

                    while (reader.Read())
                    {
                        res.Add(new CarreiraReturnValue(reader.GetString("Codigo"), reader.GetInt32("ID").Value, reader.GetString("Accao")));
                    }

                    reader.Close();

                    return res;
                }
            }
            public static List<CarreiraReturnValue> Utilizador_Carreiras_IU_TabelaEntrada(string cString, IEnumerable<CarreiraType> carreiras)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Carreiras_IU_TabelaEntrada(con, carreiras);
                }
            }


            public static void Utilizador_Carreiras_Grupos_D(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Carreiras_Grupos_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Carreiras_Grupos_D(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Carreiras_Grupos_D(con, id, utilizadorEdicao);
                }
            }
            public static void Utilizador_Carreiras_Grupos_R(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Carreiras_Grupos_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Carreiras_Grupos_R(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Carreiras_Grupos_R(con, id, utilizadorEdicao);
                }
            }
            public static Carreira.GrupoCarreira Utilizador_Carreiras_Grupos_S(SqlConnection con, int id, bool incluirApagados = false)
            {
                var res = (Carreira.GrupoCarreira)null;

                using (var cmd = new SqlCommand("[stp_Utilizador_Carreiras_Grupos_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        res = new Carreira.GrupoCarreira()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        res.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;
                    }

                    reader.Close();
                }

                return res;
            }
            public static Carreira.GrupoCarreira Utilizador_Carreiras_Grupos_S(string cString, int id, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Carreiras_Grupos_S(con, id, incluirApagados);
                }
            }
            public static IEnumerable<Carreira.GrupoCarreira> Utilizador_Carreiras_Grupos_LS(SqlConnection con, bool incluirApagados = false)
            {
                var res = new List<Carreira.GrupoCarreira>();

                using (var cmd = new SqlCommand("[stp_Utilizador_Carreiras_Grupos_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var carreira = new Carreira.GrupoCarreira()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            DataCriadoObject = reader.GetDateTime("dataCriado").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        carreira.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(carreira);
                    }

                    reader.Close();
                }

                return res;
            }
            public static IEnumerable<Carreira.GrupoCarreira> Utilizador_Carreiras_Grupos_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Carreiras_Grupos_LS(con, incluirApagados);
                }
            }
            public static int Utilizador_Carreiras_Grupos_IU(SqlConnection con, Carreira.GrupoCarreira.GrupoCarreiraUpdate grupoCarreira)
            {
                int res = -1;
                using (var cmd = new SqlCommand("[stp_Utilizador_Carreiras_Grupos_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = grupoCarreira.ID;
                    cmd.Parameters.Add("@codigo", System.Data.SqlDbType.NVarChar).Value = grupoCarreira.Codigo;
                    cmd.Parameters.Add("@nome", System.Data.SqlDbType.NVarChar).Value = grupoCarreira.Nome;
                    cmd.Parameters.Add("@descricao", System.Data.SqlDbType.NVarChar).Value = grupoCarreira.Descricao;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = grupoCarreira.Ativo;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = grupoCarreira.UtilizadorEdicao;
                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static int Utilizador_Carreiras_Grupos_IU(string cString, Carreira.GrupoCarreira.GrupoCarreiraUpdate grupoCarreira)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Carreiras_Grupos_IU(con, grupoCarreira);
                }
            }
            public static List<CarreiraGrupoReturnValue> Utilizador_Carreiras_Grupos_IU_TabelaEntrada(SqlConnection con, IEnumerable<CarreiraGrupoType> gruposCarreira)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Carreiras_Grupos_IU_TabelaEntrada]", con))
                {
                    var dt = CarreiraGrupoType.CreateDataTable();

                    foreach (var r in gruposCarreira)
                    {
                        dt.Rows.Add(r.ToDataRow());
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    var param = cmd.Parameters.AddWithValue("@carreiras_grupos", dt);
                    param.SqlDbType = SqlDbType.Structured;
                    param.TypeName = "dbo.CarreiraGrupoType";

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    var res = new List<CarreiraGrupoReturnValue>();

                    while (reader.Read())
                    {
                        res.Add(new CarreiraGrupoReturnValue(reader.GetString("Codigo"), reader.GetInt32("ID").Value, reader.GetString("Accao")));
                    }

                    reader.Close();

                    return res;
                }
            }
            public static List<CarreiraGrupoReturnValue> Utilizador_Carreiras_Grupos_IU_TabelaEntrada(string cString, IEnumerable<CarreiraGrupoType> gruposCarreira)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Carreiras_Grupos_IU_TabelaEntrada(con, gruposCarreira);
                }
            }


            #endregion

            #region Flags - done

            public static void Flags_D(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Flags_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Flags_D(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Flags_D(con, id, utilizadorEdicao);
                }
            }
            public static void Flags_R(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Flags_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Flags_R(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Flags_R(con, id, utilizadorEdicao);
                }
            }
            public static Flag Flags_PorKey_S(SqlConnection con, string key, bool incluirApagados = false)
            {
                var res = new Flag();

                using (var cmd = new SqlCommand("[stp_Flags_PorKey_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@key", System.Data.SqlDbType.Int).Value = key;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        res = new Flag()
                        {
                            ID = reader.GetInt32("id").Value,
                            Key = reader.GetString("key"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        res.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;
                    }

                    reader.Close();

                    return res;
                }
            }
            public static Flag Flags_PorKey_S(string cString, string key, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Flags_PorKey_S(con, key, incluirApagados);
                }
            }
            public static List<Flag> Flags_LS(SqlConnection con, bool incluirApagados = false)
            {
                var res = new List<Flag>();
                using (var cmd = new SqlCommand("[stp_Flags_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var flag = new Flag()
                        {
                            ID = reader.GetInt32("id").Value,
                            Key = reader.GetString("key"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        flag.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(flag);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static List<Flag> Flags_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Flags_LS(con, incluirApagados);
                }
            }
            public static int Flags_IU(SqlConnection con, Flag.FlagUpdate flag)
            {
                int res = -1;
                using (var cmd = new SqlCommand("[stp_Flags_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = flag.ID;
                    cmd.Parameters.Add("@key", System.Data.SqlDbType.NVarChar).Value = flag.Key;
                    cmd.Parameters.Add("@descricao", System.Data.SqlDbType.NVarChar).Value = flag.Descricao;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = 
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = flag.UtilizadorEdicao;
                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static int Flags_IU(string cString, Flag.FlagUpdate flag)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Flags_IU(con, flag);
                }
            }

            #region Defaults

            public static void Flags_Default_D(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Flags_Default_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Flags_Default_D(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Flags_Default_D(con, id, utilizadorEdicao);
                }
            }
            public static void Flags_Default_R(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Flags_Default_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Flags_Default_R(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Flags_Default_R(con, id, utilizadorEdicao);
                }
            }
            public static List<FlagDefault> Flags_Default_LS(SqlConnection con, bool incluirApagados = false)
            {
                var res = new List<FlagDefault>();
                using (var cmd = new SqlCommand("[stp_Flags_Default_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var flag = new FlagDefault()
                        {
                            ID = reader.GetInt32("id").Value,
                            IDFlag = reader.GetInt32("idFlag").Value,
                            IDTipoPerfil = reader.GetInt32("idTipoPerfil").Value,
                            ValorDefault = reader.GetString("valorDefault"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        flag.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(flag);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static List<FlagDefault> Flags_Default_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Flags_Default_LS(con, incluirApagados);
                }
            }
            public static int Flags_Default_IU(SqlConnection con, FlagDefault.FlagDefaultUpdate flag)
            {
                int res = -1;
                using (var cmd = new SqlCommand("[stp_Flags_Default_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = flag.ID;
                    cmd.Parameters.Add("@idFlag", System.Data.SqlDbType.Int).Value = flag.IDFlag;
                    cmd.Parameters.Add("@idTipePerfil", System.Data.SqlDbType.Int).Value = flag.IDTipoPerfil;
                    cmd.Parameters.Add("@valorDefault", System.Data.SqlDbType.NVarChar).Value = flag.ValorDefault;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = flag.UtilizadorEdicao;
                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static int Flags_Default_IU(string cString, FlagDefault.FlagDefaultUpdate flag)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Flags_Default_IU(con, flag);
                }
            }

            #endregion

            #endregion

            #region Flags Utilizador - done

            public static void Utilizador_Flags_D(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Flags_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Flags_D(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Flags_D(con, id, utilizadorEdicao);
                }
            }
            public static void Utilizador_Flags_R(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Flags_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Flags_R(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Flags_R(con, id, utilizadorEdicao);
                }
            }
            public static List<UtilizadorFlag> Utilizador_Flags_PorUtilizador_S(SqlConnection con, int idUtilizador, bool incluirApagados = false)
            {
                var res = new List<UtilizadorFlag>();

                using (var cmd = new SqlCommand("[stp_Utilizador_Flags_PorUtilizador_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idUtilizador", System.Data.SqlDbType.Int).Value = idUtilizador;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var uFlag = new UtilizadorFlag()
                        {
                            ID = reader.GetInt32("id").Value,
                            IDUtilizador = reader.GetInt32("idUtilizador").Value,
                            IDPerfil = reader.GetInt32("idPerfil"),
                            Value = reader.GetString("value"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        uFlag.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        var flag = new Flag()
                        {
                            ID = reader.GetInt32("idFlag").Value,
                            Key = reader.GetString("key"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("flagAtivo").Value,
                            UtilizadorCriado = reader.GetString("flagUtilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("flagDataModificado"),
                            UtilizadorModificado = reader.GetString("flagUtilizadorModificado")
                        };

                        var flagDataCriado = reader.GetDateTime("flagDataCriado");
                        flag.DataCriadoObject = flagDataCriado != null && flagDataCriado.HasValue ? flagDataCriado.Value : DateTime.MinValue;

                        uFlag.Flag = flag;

                        res.Add(uFlag);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static List<UtilizadorFlag> Utilizador_Flags_PorUtilizador_S(string cString, int idUtilizador, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Flags_PorUtilizador_S(con, idUtilizador, incluirApagados);
                }
            }
            public static List<UtilizadorFlag> Utilizador_Flags_PorPerfil_S(SqlConnection con, int idUtilizador, int idPerfil, bool incluirApagados = false)
            {
                var res = new List<UtilizadorFlag>();

                using (var cmd = new SqlCommand("[stp_Utilizador_Flags_PorPerfil_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idUtilizador", System.Data.SqlDbType.Int).Value = idUtilizador;
                    cmd.Parameters.Add("@idPerfil", System.Data.SqlDbType.Int).Value = idPerfil;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var uFlag = new UtilizadorFlag()
                        {
                            ID = reader.GetInt32("id").Value,
                            IDUtilizador = reader.GetInt32("idUtilizador").Value,
                            IDPerfil = reader.GetInt32("idPerfil").Value,
                            Value = reader.GetString("value"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        uFlag.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        var flag = new Flag()
                        {
                            ID = reader.GetInt32("idFlag").Value,
                            Key = reader.GetString("key"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("flagAtivo").Value,
                            UtilizadorCriado = reader.GetString("flagUtilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("flagDataModificado"),
                            UtilizadorModificado = reader.GetString("flagUtilizadorModificado")
                        };

                        var flagDataCriado = reader.GetDateTime("flagDataCriado");
                        flag.DataCriadoObject = flagDataCriado != null && flagDataCriado.HasValue ? flagDataCriado.Value : DateTime.MinValue;

                        uFlag.Flag = flag;

                        res.Add(uFlag);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static List<UtilizadorFlag> Utilizador_Flags_PorPerfil_S(string cString, int idUtilizador, int idPerfil, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Flags_PorPerfil_S(con, idUtilizador, idPerfil, incluirApagados);
                }
            }
            public static List<UtilizadorFlag> Utilizador_Flags_LS(SqlConnection con, bool incluirApagados = false)
            {
                var res = new List<UtilizadorFlag>();

                using (var cmd = new SqlCommand("[stp_Utilizador_Flags_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var uFlag = new UtilizadorFlag()
                        {
                            ID = reader.GetInt32("id").Value,
                            IDUtilizador = reader.GetInt32("idUtilizador").Value,
                            IDPerfil = reader.GetInt32("idPerfil"),
                            Value = reader.GetString("value"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        uFlag.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        var flag = new Flag()
                        {
                            ID = reader.GetInt32("idFlag").Value,
                            Key = reader.GetString("key"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("flagAtivo").Value,
                            UtilizadorCriado = reader.GetString("flagUtilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("flagDataModificado"),
                            UtilizadorModificado = reader.GetString("flagUtilizadorModificado")
                        };

                        var flagDataCriado = reader.GetDateTime("flagDataCriado");
                        flag.DataCriadoObject = flagDataCriado != null && flagDataCriado.HasValue ? flagDataCriado.Value : DateTime.MinValue;

                        uFlag.Flag = flag;

                        res.Add(uFlag);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static List<UtilizadorFlag> Utilizador_Flags_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Flags_LS(con, incluirApagados);
                }
            }
            public static int Utilizador_Flags_IU(SqlConnection con, UtilizadorFlag.UtilizadorFlagUpdate flag)
            {
                int res = -1;
                using (var cmd = new SqlCommand("[stp_Utilizador_Flags_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idUtilizador", System.Data.SqlDbType.Int).Value = flag.IDUtilizador;
                    cmd.Parameters.Add("@idPerfil", System.Data.SqlDbType.Int).Value = flag.IDPerfil;
                    cmd.Parameters.Add("@idFlag", System.Data.SqlDbType.Int).Value = flag.IDFlag;
                    cmd.Parameters.Add("@value", System.Data.SqlDbType.NVarChar).Value = flag.Value;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = flag.Ativo;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = flag.UtilizadorEdicao;
                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static int Utilizador_Flags_IU(string cString, UtilizadorFlag.UtilizadorFlagUpdate flag)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Flags_IU(con, flag);
                }
            }
            public static List<FlagReturnValue> Utilizador_Flags_IU_TabelaEntrada(SqlConnection con, IEnumerable<UtilizadorFlagType> flags)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Flags_IU_TabelaEntrada]", con))
                {
                    var dt = UtilizadorFlagType.CreateDataTable();

                    foreach (var c in flags)
                    {
                        dt.Rows.Add(c.ToDataRow());
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    var param = cmd.Parameters.AddWithValue("@flags", dt);
                    param.SqlDbType = System.Data.SqlDbType.Structured;
                    param.TypeName = "dbo.UtilizadorFlagType";

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    var res = new List<FlagReturnValue>();

                    while (reader.Read())
                    {
                        res.Add(new FlagReturnValue(reader.GetInt32("IDUtilizador").Value, reader.GetInt32("IDPerfil"), reader.GetInt32("IDFlag").Value, reader.GetString("Accao")));
                    }

                    reader.Close();

                    return res;
                }
            }
            public static List<FlagReturnValue> Utilizador_Flags_IU_TabelaEntrada(string cString, IEnumerable<UtilizadorFlagType> flags)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Flags_IU_TabelaEntrada(con, flags);
                }
            }

            #endregion

            #region Regimes - done

            public static void Utilizador_Regimes_D(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Regimes_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Regimes_D(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Regimes_D(con, id, utilizadorEdicao);
                }
            }
            public static void Utilizador_Regimes_R(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Regimes_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Regimes_R(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Regimes_R(con, id, utilizadorEdicao);
                }
            }
            public static Regime Utilizador_Regimes_S(SqlConnection con, int id, bool incluirApagados = false)
            {
                var res = (Regime)null;

                using (var cmd = new SqlCommand("[stp_Utilizador_Regimes_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        res = new Regime()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            DataCriadoObject = reader.GetDateTime("dataCriado").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        res.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;
                    }

                    reader.Close();
                }

                return res;
            }
            public static Regime Utilizador_Regimes_S(string cString, int id, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Regimes_S(con, id, incluirApagados);
                }
            }
            public static IEnumerable<Regime> Utilizador_Regimes_LS(SqlConnection con, bool incluirApagados = false)
            {
                var res = new List<Regime>();

                using (var cmd = new SqlCommand("[stp_Utilizador_Regimes_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var reg = new Regime()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        reg.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(reg);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<Regime> Utilizador_Regimes_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Regimes_LS(con, incluirApagados);
                }
            }
            public static int Utilizador_Regimes_IU(SqlConnection con, Regime.RegimeUpdate regime)
            {
                int res = -1;
                using (var cmd = new SqlCommand("[stp_Utilizador_Regimes_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = regime.ID;
                    cmd.Parameters.Add("@codigo", System.Data.SqlDbType.NVarChar).Value = regime.Codigo;
                    cmd.Parameters.Add("@nome", System.Data.SqlDbType.NVarChar).Value = regime.Nome;
                    cmd.Parameters.Add("@descricao", System.Data.SqlDbType.NVarChar).Value = regime.Descricao;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = regime.Ativo;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = regime.UtilizadorEdicao;
                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {

                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static int Utilizador_Regimes_IU(string cString, Regime.RegimeUpdate regime)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Regimes_IU(con, regime);
                }
            }
            public static List<RegimeReturnValue> Utilizador_Regimes_IU_TabelaEntrada(SqlConnection con, IEnumerable<RegimeType> regimes)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Regimes_IU_TabelaEntrada]", con))
                {
                    var dt = RegimeType.CreateDataTable();

                    foreach (var r in regimes)
                    {
                        dt.Rows.Add(r.ToDataRow());
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    var param = cmd.Parameters.AddWithValue("@regimes", dt);
                    param.SqlDbType = SqlDbType.Structured;
                    param.TypeName = "dbo.RegimeType";
                    
                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    var res = new List<RegimeReturnValue>();

                    while (reader.Read())
                    {
                        res.Add(new RegimeReturnValue(reader.GetString("Codigo"), reader.GetInt32("ID").Value, reader.GetString("Accao")));
                    }

                    reader.Close();

                    return res;
                }
            }
            public static List<RegimeReturnValue> Utilizador_Regimes_IU_TabelaEntrada(string cString, IEnumerable<RegimeType> regimes)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Regimes_IU_TabelaEntrada(con, regimes);
                }
            }

            #endregion

            #region Situacoes - done

            public static void Utilizador_Situacoes_D(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Situacoes_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Situacoes_D(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Situacoes_D(con, id, utilizadorEdicao);
               }
            }
            public static void Utilizador_Situacoes_R(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Situacoes_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Situacoes_R(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Situacoes_R(con, id, utilizadorEdicao);
                }
            }
            public static Situacao Utilizador_Situacoes_S(SqlConnection con, int id, bool incluirApagados = false)
            {
                var res = (Situacao)null;

                using (var cmd = new SqlCommand("[stp_Utilizador_Situacoes_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        res = new Situacao()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            PRC = reader.GetString("PRC"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            DataCriadoObject = reader.GetDateTime("dataCriado").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        res.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;
                    }

                    reader.Close();
                }

                return res;
            }
            public static Situacao Utilizador_Situacoes_S(string cString, int id, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Situacoes_S(con, id, incluirApagados);
                }
            }
            public static IEnumerable<Situacao> Utilizador_Situacoes_LS(SqlConnection con, bool incluirApagados = false)
            {
                var res = new List<Situacao>();

                using (var cmd = new SqlCommand("[stp_Utilizador_Situacoes_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var reg = new Situacao()
                        {
                            ID = reader.GetInt32("id").Value,
                            Codigo = reader.GetString("codigo"),
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            PRC = reader.GetString("PRC"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        reg.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        res.Add(reg);
                    }

                    reader.Close();

                    return res;
                }
            }
            public static IEnumerable<Situacao> Utilizador_Situacoes_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Situacoes_LS(con, incluirApagados);
                }
            }
            public static int Utilizador_Situacoes_IU(SqlConnection con, Situacao.SituacaoUpdate situacao)
            {
                int res = -1;
                using (var cmd = new SqlCommand("[stp_Utilizador_Situacoes_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = situacao.ID;
                    cmd.Parameters.Add("@codigo", System.Data.SqlDbType.NVarChar).Value = situacao.Codigo;
                    cmd.Parameters.Add("@nome", System.Data.SqlDbType.NVarChar).Value = situacao.Nome;
                    cmd.Parameters.Add("@descricao", System.Data.SqlDbType.NVarChar).Value = situacao.Descricao;
                    cmd.Parameters.Add("@PRC", System.Data.SqlDbType.NVarChar).Value = situacao.PRC;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = situacao.Ativo;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = situacao.UtilizadorEdicao;
                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static int Utilizador_Situacoes_IU(string cString, Situacao.SituacaoUpdate situacao)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Situacoes_IU(con, situacao);
                }
            }
            public static List<SituacoesReturnValue> Utilizador_Situacoes_IU_TabelaEntrada(SqlConnection con, IEnumerable<SituacaoType> situacoes)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Situacoes_IU_TabelaEntrada]", con))
                {
                    var dt = SituacaoType.CreateDataTable();

                    foreach (var s in situacoes)
                    {
                        dt.Rows.Add(s.ToDataRow());
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    var param = cmd.Parameters.AddWithValue("@situacoes", dt);
                    param.SqlDbType = SqlDbType.Structured;
                    param.TypeName = "dbo.SituacaoType";

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    var res = new List<SituacoesReturnValue>();

                    while (reader.Read())
                    {
                        res.Add(new SituacoesReturnValue(reader.GetString("Codigo"), reader.GetInt32("ID").Value, reader.GetString("Accao")));
                    }

                    reader.Close();

                    return res;
                }
            }
            public static List<SituacoesReturnValue> Utilizador_Situacoes_IU_TabelaEntrada(string cString, IEnumerable<SituacaoType> situacao)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Situacoes_IU_TabelaEntrada(con, situacao);
                }
            }

            #endregion

            #region Visibilidade - done



            public static IEnumerable<Visibilidade> Utilizador_Visibilidade_LS(SqlConnection con, bool incluirApagados = false)
            {
                var data = new List<Visibilidade>();

                using (var cmd = new SqlCommand("[stp_Utilizador_Visibilidade_LS]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var n = new Visibilidade();

                        n.ID = reader.GetInt32("id").Value;
                        n.Nome = reader.GetString("nome");
                        n.Descricao = reader.GetString("descricao");
                        n.UtilizadorCriado = reader.GetString("utilizadorCriado");
                        n.DataModificadoObject = reader.GetDateTime("dataModificado");
                        n.UtilizadorModificado = reader.GetString("utilizadorModificado");
                        n.Ativo = reader.GetBoolean("ativo").Value;

                        var dCriado = reader.GetDateTime("dataCriado");
                        n.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;

                        data.Add(n);
                    }

                    reader.Close();

                    return data;
                }
            }
            public static IEnumerable<Visibilidade> Utilizador_Visibilidade_LS(string cString, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Visibilidade_LS(con, incluirApagados);
                }
            }
            public static Visibilidade Utilizador_Visibilidade_S(SqlConnection con, int id, bool incluirApagados = false)
            {
                var res = (Visibilidade)null;

                using (var cmd = new SqlCommand("[stp_Utilizador_Visibilidade_S]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@incluirApagados", System.Data.SqlDbType.Bit).Value = incluirApagados;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == null && reader.Read())
                    {
                        res = new Visibilidade()
                        {
                            ID = reader.GetInt32("id").Value,
                            Nome = reader.GetString("nome"),
                            Descricao = reader.GetString("descricao"),
                            Ativo = reader.GetBoolean("ativo").Value,
                            UtilizadorCriado = reader.GetString("utilizadorCriado"),
                            DataModificadoObject = reader.GetDateTime("dataModificado"),
                            UtilizadorModificado = reader.GetString("utilizadorModificado")
                        };

                        var dCriado = reader.GetDateTime("dataCriado");
                        res.DataCriadoObject = dCriado != null && dCriado.HasValue ? dCriado.Value : DateTime.MinValue;
                    }

                    reader.Close();

                    return res;
                }
            }
            public static Visibilidade Utilizador_Visibilidade_S(string cString, int id, bool incluirApagados = false)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Visibilidade_S(con, id, incluirApagados);
                }
            }
            public static void Utilizador_Visibilidade_D(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[Utilizador_Visibilidade_D]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Visibilidade_D(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Visibilidade_D(con, id, utilizadorEdicao);
                }
            }
            public static void Utilizador_Visibilidade_R(SqlConnection con, int id, string utilizadorEdicao)
            {
                using (var cmd = new SqlCommand("[Utilizador_Visibilidade_R]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = utilizadorEdicao;

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
            public static void Utilizador_Visibilidade_R(string cString, int id, string utilizadorEdicao)
            {
                using (var con = new SqlConnection(cString))
                {
                    Utilizador_Visibilidade_R(con, id, utilizadorEdicao);
                }
            }
            public static int Utilizador_Visibilidade_IU(SqlConnection con, Visibilidade.VisibilidadeUpdate visibilidade)
            {
                int res = -1;
                using (var cmd = new SqlCommand("[stp_Utilizador_Visibilidade_IU]", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = visibilidade.ID;
                    cmd.Parameters.Add("@nome", System.Data.SqlDbType.NVarChar).Value = visibilidade.Nome;
                    cmd.Parameters.Add("@descricao", System.Data.SqlDbType.NVarChar).Value = visibilidade.Descricao;
                    cmd.Parameters.Add("@ativo", System.Data.SqlDbType.Bit).Value = visibilidade.Ativo;
                    cmd.Parameters.Add("@utilizadorEdicao", System.Data.SqlDbType.NVarChar).Value = visibilidade.UtilizadorEdicao;
                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    while (res == -1 && reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader.GetName(0)))
                        {
                            res = Convert.ToInt32(reader[0]);
                        }
                        else
                        {
                            throw new DBErrorException(reader);
                        }
                    }

                    reader.Close();

                    return res;
                }
            }
            public static int Utilizador_Visibilidade_IU(string cString, Visibilidade.VisibilidadeUpdate visibilidade)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Visibilidade_IU(con, visibilidade);
                }
            }
            public static List<VisibilidadeReturnValue> Utilizador_Visibilidade_IU_TabelaEntrada(SqlConnection con, IEnumerable<VisibilidadeType> visibilidades)
            {
                using (var cmd = new SqlCommand("[stp_Utilizador_Visibilidade_IU_TabelaEntrada]", con))
                {
                    var dt = CarreiraType.CreateDataTable();

                    foreach (var r in visibilidades)
                    {
                        dt.Rows.Add(r.ToDataRow());
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    var param = cmd.Parameters.AddWithValue("@visibilidades", dt);
                    param.SqlDbType = SqlDbType.Structured;
                    param.TypeName = "dbo.VisibilidadeType";

                    if (con.State != System.Data.ConnectionState.Open) con.Open();

                    var reader = cmd.ExecuteReader();

                    var res = new List<VisibilidadeReturnValue>();

                    while (reader.Read())
                    {
                        res.Add(new VisibilidadeReturnValue(reader.GetInt32("ID").Value, reader.GetString("Accao")));
                    }

                    reader.Close();

                    return res;
                }
            }
            public static List<VisibilidadeReturnValue> Utilizador_Visibilidade_IU_TabelaEntrada(string cString, IEnumerable<VisibilidadeType> visibilidades)
            {
                using (var con = new SqlConnection(cString))
                {
                    return Utilizador_Visibilidade_IU_TabelaEntrada(con, visibilidades);
                }
            }

            #endregion
        }
    }
}
