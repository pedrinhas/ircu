	drop trigger userModificadoDelete_Aplicacoes_AplicacaoGrupo
	go
	create trigger userModificadoDelete_Aplicacoes_AplicacaoGrupo
	on Aplicacoes_AplicacaoGrupo
	instead of delete
	as
		update Aplicacoes_AplicacaoGrupo
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where idAplicacaoGrupo in (select idAplicacaoGrupo from deleted)

		delete Aplicacoes_AplicacaoGrupo
		from DELETED D
		inner join Aplicacoes_AplicacaoGrupo T on T.idAplicacaoGrupo = D.idAplicacaoGrupo
	go

	drop trigger userModificadoUpdate_Aplicacoes_AplicacaoGrupo
	go
	create trigger userModificadoUpdate_Aplicacoes_AplicacaoGrupo
	on Aplicacoes_AplicacaoGrupo
	after update
	as
		UPDATE Aplicacoes_AplicacaoGrupo
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where idAplicacaoGrupo = t.idAplicacaoGrupo)
			from Aplicacoes_AplicacaoGrupo t
			where idAplicacaoGrupo in (select idAplicacaoGrupo from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

	drop trigger userModificadoDelete_Aplicacoes_Grupo
	go
	create trigger userModificadoDelete_Aplicacoes_Grupo
	on Aplicacoes_Grupo
	instead of delete
	as
		update Aplicacoes_Grupo
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where idGrupo in (select idGrupo from deleted)

		delete Aplicacoes_Grupo
		from DELETED D
		inner join Aplicacoes_Grupo T on T.idGrupo = D.idGrupo
	go

	drop trigger userModificadoUpdate_Aplicacoes_Grupo
	go
	create trigger userModificadoUpdate_Aplicacoes_Grupo
	on Aplicacoes_Grupo
	after update
	as
		UPDATE Aplicacoes_Grupo
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where idGrupo = t.idGrupo)
			from Aplicacoes_Grupo t
			where idGrupo in (select idGrupo from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		
	drop trigger userModificadoDelete_Sessao_Utilizador
	go
	create trigger userModificadoDelete_Sessao_Utilizador
	on Sessao_Utilizador
	instead of delete
	as
		update Sessao_Utilizador
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where idUtilizador in (select idUtilizador from deleted)

		delete Sessao_Utilizador
		from DELETED D
		inner join Sessao_Utilizador T on T.idUtilizador = D.idUtilizador
	go

	drop trigger userModificadoUpdate_Sessao_Utilizador
	go
	create trigger userModificadoUpdate_Sessao_Utilizador
	on Sessao_Utilizador
	after update
	as
		UPDATE Sessao_Utilizador
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where idUtilizador = t.idUtilizador)
			from Sessao_Utilizador t
			where idUtilizador in (select idUtilizador from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

	drop trigger userModificadoDelete_Menus_Permissao
	go
	create trigger userModificadoDelete_Menus_Permissao
	on Menus_Permissao
	instead of delete
	as
		update Menus_Permissao
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where idPermissaoMenu in (select idPermissaoMenu from deleted)

		delete Menus_Permissao
		from DELETED D
		inner join Menus_Permissao T on T.idPermissaoMenu = D.idPermissaoMenu
	go

	drop trigger userModificadoUpdate_Menus_Permissao
	go
	create trigger userModificadoUpdate_Menus_Permissao
	on Menus_Permissao
	after update
	as
		UPDATE Menus_Permissao
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where idPermissaoMenu = t.idPermissaoMenu)
			from Menus_Permissao t
			where idPermissaoMenu in (select idPermissaoMenu from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_Menus
		go
		create trigger userModificadoDelete_Menus
	on Menus
	instead of delete
	as
		update Menus
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where idMenu in (select idMenu from deleted)

		delete Menus
		from DELETED D
		inner join Menus T on T.idMenu = D.idMenu
	go

	drop trigger userModificadoUpdate_Menus
	go
	create trigger userModificadoUpdate_Menus
	on Menus
	after update
	as
		UPDATE Menus
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where idMenu = t.idMenu)
			from Menus t
			where idMenu in (select idMenu from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_Aplicacoes
		go
		create trigger userModificadoDelete_Aplicacoes
	on Aplicacoes
	instead of delete
	as
		update Aplicacoes
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where idAplicacao in (select idAplicacao from deleted)

		delete Aplicacoes
		from DELETED D
		inner join Aplicacoes T on T.idAplicacao = D.idAplicacao
	go

	drop trigger userModificadoUpdate_Aplicacoes
	go
	create trigger userModificadoUpdate_Aplicacoes
	on Aplicacoes
	after update
	as
		UPDATE Aplicacoes
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where idAplicacao = t.idAplicacao)
			from Aplicacoes t
			where idAplicacao in (select idAplicacao from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_Acesso_NivelAcesso
		go
		create trigger userModificadoDelete_Acesso_NivelAcesso
	on Acesso_NivelAcesso
	instead of delete
	as
		update Acesso_NivelAcesso
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where idNivelAcesso in (select idNivelAcesso from deleted)

		delete Acesso_NivelAcesso
		from DELETED D
		inner join Acesso_NivelAcesso T on T.idNivelAcesso = D.idNivelAcesso
	go

	drop trigger userModificadoUpdate_Acesso_NivelAcesso
	go
	create trigger userModificadoUpdate_Acesso_NivelAcesso
	on Acesso_NivelAcesso
	after update
	as
		UPDATE Acesso_NivelAcesso
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where idNivelAcesso = t.idNivelAcesso)
			from Acesso_NivelAcesso t
			where idNivelAcesso in (select idNivelAcesso from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_Acesso_Permissao
		go
		create trigger userModificadoDelete_Acesso_Permissao
	on Acesso_Permissao
	instead of delete
	as
		update Acesso_Permissao
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where idPermissao in (select idPermissao from deleted)

		delete Acesso_Permissao
		from DELETED D
		inner join Acesso_Permissao T on T.idPermissao = D.idPermissao
	go

	drop trigger userModificadoUpdate_Acesso_Permissao
	go
	create trigger userModificadoUpdate_Acesso_Permissao
	on Acesso_Permissao
	after update
	as
		UPDATE Acesso_Permissao
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where idPermissao = t.idPermissao)
			from Acesso_Permissao t
			where idPermissao in (select idPermissao from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

	
		drop trigger userModificadoDelete_Utilizadores
		go
		create trigger userModificadoDelete_Utilizadores
	on Utilizadores
	instead of delete
	as
		update Utilizadores
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where [id] in (select id from deleted)

		delete Utilizadores
		from DELETED D
		inner join Utilizadores T on T.id = D.id
	go

	drop trigger userModificadoUpdate_Utilizadores
	go
	create trigger userModificadoUpdate_Utilizadores
	on Utilizadores
	after update
	as
		UPDATE Utilizadores
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where id = t.id)
			from Utilizadores t
			where [id] in (select id from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_Utilizadores_Perfil_Tipo
		go
		create trigger userModificadoDelete_Utilizadores_Perfil_Tipo
	on Utilizadores_Perfil_Tipo
	instead of delete
	as
		update Utilizadores_Perfil_Tipo
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where [id] in (select id from deleted)

		delete Utilizadores_Perfil_Tipo
		from DELETED D
		inner join Utilizadores_Perfil_Tipo T on T.id = D.id
	go

	drop trigger userModificadoUpdate_Utilizadores_Perfil_Tipo
	go
	create trigger userModificadoUpdate_Utilizadores_Perfil_Tipo
	on Utilizadores_Perfil_Tipo
	after update
	as
		UPDATE Utilizadores_Perfil_Tipo
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where id = t.id)
			from Utilizadores_Perfil_Tipo t
			where [id] in (select id from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_Entidades_Tipo
		go
		create trigger userModificadoDelete_Entidades_Tipo
	on Entidades_Tipo
	instead of delete
	as
		update Entidades_Tipo
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where [id] in (select id from deleted)

		delete Entidades_Tipo
		from DELETED D
		inner join Entidades_Tipo T on T.id = D.id
	go

	drop trigger userModificadoUpdate_Entidades_Tipo
	go
	create trigger userModificadoUpdate_Entidades_Tipo
	on Entidades_Tipo
	after update
	as
		UPDATE Entidades_Tipo
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where id = t.id)
			from Entidades_Tipo t
			where [id] in (select id from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_Utilizadores_Cargos
		go
		create trigger userModificadoDelete_Utilizadores_Cargos
	on Utilizadores_Cargos
	instead of delete
	as
		update Utilizadores_Cargos
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where [id] in (select id from deleted)

		delete Utilizadores_Cargos
		from DELETED D
		inner join Utilizadores_Cargos T on T.id = D.id
	go

	drop trigger userModificadoUpdate_Utilizadores_Cargos
	go
	create trigger userModificadoUpdate_Utilizadores_Cargos
	on Utilizadores_Cargos
	after update
	as
		UPDATE Utilizadores_Cargos
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where id = t.id)
			from Utilizadores_Cargos t
			where [id] in (select id from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_Entidades
		go
		create trigger userModificadoDelete_Entidades
	on Entidades
	instead of delete
	as
		update Entidades
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where [id] in (select id from deleted)

		delete Entidades
		from DELETED D
		inner join Entidades T on T.id = D.id
	go

	drop trigger userModificadoUpdate_Entidades
	go
	create trigger userModificadoUpdate_Entidades
	on Entidades
	after update
	as
		UPDATE Entidades
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where id = t.id)
			from Entidades t
			where [id] in (select id from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_Sessao_UtilizadorExcepcao
		go
		create trigger userModificadoDelete_Sessao_UtilizadorExcepcao
	on Sessao_UtilizadorExcepcao
	instead of delete
	as
		update Sessao_UtilizadorExcepcao
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where [id] in (select id from deleted)

		delete Sessao_UtilizadorExcepcao
		from DELETED D
		inner join Sessao_UtilizadorExcepcao T on T.id = D.id
	go

	drop trigger userModificadoUpdate_Sessao_UtilizadorExcepcao
	go
	create trigger userModificadoUpdate_Sessao_UtilizadorExcepcao
	on Sessao_UtilizadorExcepcao
	after update
	as
		UPDATE Sessao_UtilizadorExcepcao
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where id = t.id)
			from Sessao_UtilizadorExcepcao t
			where [id] in (select id from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_Utilizadores_Categorias
		go
		create trigger userModificadoDelete_Utilizadores_Categorias
	on Utilizadores_Categorias
	instead of delete
	as
		update Utilizadores_Categorias
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where [id] in (select id from deleted)

		delete Utilizadores_Categorias
		from DELETED D
		inner join Utilizadores_Categorias T on T.id = D.id
	go

	drop trigger userModificadoUpdate_Utilizadores_Categorias
	go
	create trigger userModificadoUpdate_Utilizadores_Categorias
	on Utilizadores_Categorias
	after update
	as
		UPDATE Utilizadores_Categorias
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where id = t.id)
			from Utilizadores_Categorias t
			where [id] in (select id from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_Utilizadores_Carreiras
		go
		create trigger userModificadoDelete_Utilizadores_Carreiras
	on Utilizadores_Carreiras
	instead of delete
	as
		update Utilizadores_Carreiras
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where [id] in (select id from deleted)

		delete Utilizadores_Carreiras
		from DELETED D
		inner join Utilizadores_Carreiras T on T.id = D.id
	go

	drop trigger userModificadoUpdate_Utilizadores_Carreiras
	go
	create trigger userModificadoUpdate_Utilizadores_Carreiras
	on Utilizadores_Carreiras
	after update
	as
		UPDATE Utilizadores_Carreiras
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where id = t.id)
			from Utilizadores_Carreiras t
			where [id] in (select id from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_Utilizadores_Contactos_Tipos
		go
		create trigger userModificadoDelete_Utilizadores_Contactos_Tipos
	on Utilizadores_Contactos_Tipos
	instead of delete
	as
		update Utilizadores_Contactos_Tipos
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where [id] in (select id from deleted)

		delete Utilizadores_Contactos_Tipos
		from DELETED D
		inner join Utilizadores_Contactos_Tipos T on T.id = D.id
	go

	drop trigger userModificadoUpdate_Utilizadores_Contactos_Tipos
	go
	create trigger userModificadoUpdate_Utilizadores_Contactos_Tipos
	on Utilizadores_Contactos_Tipos
	after update
	as
		UPDATE Utilizadores_Contactos_Tipos
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where id = t.id)
			from Utilizadores_Contactos_Tipos t
			where [id] in (select id from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_Utilizadores_Contactos_Grupos
		go
		create trigger userModificadoDelete_Utilizadores_Contactos_Grupos
	on Utilizadores_Contactos_Grupos
	instead of delete
	as
		update Utilizadores_Contactos_Grupos
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where [id] in (select id from deleted)

		delete Utilizadores_Contactos_Grupos
		from DELETED D
		inner join Utilizadores_Contactos_Grupos T on T.id = D.id
	go

	drop trigger userModificadoUpdate_Utilizadores_Contactos_Grupos
	go
	create trigger userModificadoUpdate_Utilizadores_Contactos_Grupos
	on Utilizadores_Contactos_Grupos
	after update
	as
		UPDATE Utilizadores_Contactos_Grupos
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where id = t.id)
			from Utilizadores_Contactos_Grupos t
			where [id] in (select id from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_WebService_Acesso_Role
		go
		create trigger userModificadoDelete_WebService_Acesso_Role
	on WebService_Acesso_Role
	instead of delete
	as
		update WebService_Acesso_Role
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where [id] in (select id from deleted)

		delete WebService_Acesso_Role
		from DELETED D
		inner join WebService_Acesso_Role T on T.id = D.id
	go

	drop trigger userModificadoUpdate_WebService_Acesso_Role
	go
	create trigger userModificadoUpdate_WebService_Acesso_Role
	on WebService_Acesso_Role
	after update
	as
		UPDATE WebService_Acesso_Role
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where id = t.id)
			from WebService_Acesso_Role t
			where [id] in (select id from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_WebService_Acesso_UserRoleAss
		go
		create trigger userModificadoDelete_WebService_Acesso_UserRoleAss
	on WebService_Acesso_UserRoleAss
	instead of delete
	as
		update WebService_Acesso_UserRoleAss
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where [id] in (select id from deleted)

		delete WebService_Acesso_UserRoleAss
		from DELETED D
		inner join WebService_Acesso_UserRoleAss T on T.id = D.id
	go

	drop trigger userModificadoUpdate_WebService_Acesso_UserRoleAss
	go
	create trigger userModificadoUpdate_WebService_Acesso_UserRoleAss
	on WebService_Acesso_UserRoleAss
	after update
	as
		UPDATE WebService_Acesso_UserRoleAss
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where id = t.id)
			from WebService_Acesso_UserRoleAss t
			where [id] in (select id from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_Utilizadores_Contactos
		go
		create trigger userModificadoDelete_Utilizadores_Contactos
	on Utilizadores_Contactos
	instead of delete
	as
		update Utilizadores_Contactos
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where [id] in (select id from deleted)

		delete Utilizadores_Contactos
		from DELETED D
		inner join Utilizadores_Contactos T on T.id = D.id
	go

	drop trigger userModificadoUpdate_Utilizadores_Contactos
	go
	create trigger userModificadoUpdate_Utilizadores_Contactos
	on Utilizadores_Contactos
	after update
	as
		UPDATE Utilizadores_Contactos
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where id = t.id)
			from Utilizadores_Contactos t
			where [id] in (select id from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_Utilizadores_Situacoes
		go
		create trigger userModificadoDelete_Utilizadores_Situacoes
	on Utilizadores_Situacoes
	instead of delete
	as
		update Utilizadores_Situacoes
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where [id] in (select id from deleted)

		delete Utilizadores_Situacoes
		from DELETED D
		inner join Utilizadores_Situacoes T on T.id = D.id
	go

	drop trigger userModificadoUpdate_Utilizadores_Situacoes
	go
	create trigger userModificadoUpdate_Utilizadores_Situacoes
	on Utilizadores_Situacoes
	after update
	as
		UPDATE Utilizadores_Situacoes
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where id = t.id)
			from Utilizadores_Situacoes t
			where [id] in (select id from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_WebService_Acesso_User
		go
		create trigger userModificadoDelete_WebService_Acesso_User
	on WebService_Acesso_User
	instead of delete
	as
		update WebService_Acesso_User
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where [id] in (select id from deleted)

		delete WebService_Acesso_User
		from DELETED D
		inner join WebService_Acesso_User T on T.id = D.id
	go

	drop trigger userModificadoUpdate_WebService_Acesso_User
	go
	create trigger userModificadoUpdate_WebService_Acesso_User
	on WebService_Acesso_User
	after update
	as
		UPDATE WebService_Acesso_User
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where id = t.id)
			from WebService_Acesso_User t
			where [id] in (select id from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go

		drop trigger userModificadoDelete_Utilizadores_Perfil
		go
		create trigger userModificadoDelete_Utilizadores_Perfil
	on Utilizadores_Perfil
	instead of delete
	as
		update Utilizadores_Perfil
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where [id] in (select id from deleted)

		delete Utilizadores_Perfil
		from DELETED D
		inner join Utilizadores_Perfil T on T.id = D.id
	go

	drop trigger userModificadoUpdate_Utilizadores_Perfil
	go
	create trigger userModificadoUpdate_Utilizadores_Perfil
	on Utilizadores_Perfil
	after update
	as
		UPDATE Utilizadores_Perfil
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where id = t.id)
			from Utilizadores_Perfil t
			where [id] in (select id from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go
