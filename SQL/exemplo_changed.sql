 
SELECT [__$start_lsn] as idTransacaoInicio
      ,[__$end_lsn] as idTransacaoFim
      ,[__$seqval]
	  ,[__$operation] as operationType
      ,case [__$operation] WHEN 1 THEN 'DELETE' WHEN 2 THEN 'INSERT' WHEN 3 THEN 'UPDATE_BEFORE' WHEN 4 THEN 'UPDATE_AFTER' end as operationTypeText
      ,[__$update_mask]	  
	  ,( SELECT    CC.column_name + ', '
          FROM      cdc.captured_columns CC
                    INNER JOIN cdc.change_tables CT ON CC.[object_id] = CT.[object_id]
          WHERE     CT.capture_instance = 'dbo_Utilizadores'
                    AND sys.fn_cdc_is_bit_set(CC.column_ordinal,
                                              c.__$update_mask) = 1
        FOR
          XML PATH('')
        ) AS changedcolumns
      ,[id]
      ,[iupi]
      ,[nomedeutilizador]
      ,[nome]
      ,[nif]
      ,[ativo]
      ,[dataCriado]
      ,[utilizadorCriado]
      ,[dataModificado]
      ,[utilizadorModificado]
  FROM [RCU].[cdc].[dbo_Utilizadores_CT] c