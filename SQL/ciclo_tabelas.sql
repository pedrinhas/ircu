sp_MSforeachtable '


	if col_length(''?'', ''id'') is null
	begin
		print ''?''
	end

'

sp_msforeachtable
'

if col_length(''?'', ''utilizadorModificado'') is not null
begin

	--exec stp_Tools_DropDefaultValue ''?'', ''utilizadorCriado''
	--alter table ? add default SYSTEM_USER for utilizadorCriado
	

	if col_length(''?'', ''id'') is not null
	begin
		--exec stp_Tools_DropDefaultValue ''?'', ''dataModificado''
		--exec stp_Tools_DropDefaultValue ''?'', ''utilizadorModificado''
		
		declare @tbl nvarchar(255)
		declare @template nvarchar(max)

		set @template = ''
		drop trigger userModificadoDelete_{TBL}
		go
		create trigger userModificadoDelete_{TBL}
	on {TBL}
	instead of delete
	as
		update {TBL}
		set utilizadorModificado = SYSTEM_USER, dataModificado = getdate()
		where [id] in (select id from deleted)

		delete {TBL}
		from DELETED D
		inner join {TBL} T on T.id = D.id
	go

	drop trigger userModificadoUpdate_{TBL}
	go
	create trigger userModificadoUpdate_{TBL}
	on {TBL}
	after update
	as
		UPDATE {TBL}
			SET [dataModificado] = getdate(), utilizadorModificado = (select COALESCE(utilizadorModificado, SYSTEM_USER) from inserted where id = t.id)
			from {TBL} t
			where [id] in (select id from inserted)
			--tem de ser fazer um update quando os dados da tabela s�o alterados. assim s� o faz se o utilizadorModificado for null
			--no entando, quero conseguir alterar o utilizadorModificado, n�o podendo usar um update com SYSTEM_USER indiscriminado
	go''

		set @tbl = replace(replace(''?'', ''[dbo].['', ''''), '']'','''')

		declare @cmd nvarchar(max)
		set @cmd = replace(@template, ''{TBL}'', @tbl)

		print @cmd
		
		--alter table ? add default SYSTEM_USER for utilizadorModificado
		--alter table ? add default getdate() for dataModificado

		--execute(@cmd)

	end
	
end
'