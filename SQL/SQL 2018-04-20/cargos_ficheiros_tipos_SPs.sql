USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Cargos_Ficheiros_Tipos_D]    Script Date: 20/04/2018 11:53:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Utilizador_Cargos_Ficheiros_Tipos_D]
	@idTipoFicheiro int,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin tran
	begin try
		
		update [dbo].[Utilizadores_Cargos_Ficheiros_Tipos]
		set ativo = 0, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
		where id = @idTipoFicheiro

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@idTipoFicheiro as idTipoFicheiro
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	end








GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Cargos_Ficheiros_Tipos_IU]    Script Date: 20/04/2018 11:53:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Utilizador_Cargos_Ficheiros_Tipos_IU]
	@idTipoFicheiro int,
	@nome nvarchar(50),
	@descricao nvarchar(max) = null,
	@ativo bit = 1,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin tran
	begin try
		declare @exists bit
		set @exists = 0

		select @exists = coalesce(c.id, case when c.id is null then 0 when c.id is not null then 1 end) 
		from [dbo].[Utilizadores_Cargos_Ficheiros_Tipos] c
		where @idTipoFicheiro is not null and c.id = @idTipoFicheiro

		if @exists = 1 and @idTipoFicheiro is not null
		begin
			update [dbo].[Utilizadores_Cargos_Ficheiros_Tipos]
			set nome = @nome, descricao = @descricao, dataModificado = getdate(), ativo = @ativo, utilizadorModificado = @utilizadorEdicao
			where id = @idTipoFicheiro

			select @idTipoFicheiro
		end
		else
		begin
			insert into [dbo].[Utilizadores_Cargos_Ficheiros_Tipos] (nome, descricao, ativo, dataCriado, utilizadorCriado)
			values (@nome, @descricao, 1, getdate(), @utilizadorEdicao)

			select max(id) from [Utilizadores_Cargos_Ficheiros_Tipos]
		end

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@idTipoFicheiro as idTipoFicheiro
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	end










GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Cargos_Ficheiros_Tipos_LS]    Script Date: 20/04/2018 11:53:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Utilizador_Cargos_Ficheiros_Tipos_LS]
	@incluirApagados bit = 0
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [id]
      ,[nome]
      ,[descricao]
      ,[ativo]
      ,[dataCriado]
      ,[utilizadorCriado]
      ,[dataModificado]
      ,[utilizadorModificado]
  FROM [dbo].[Utilizadores_Cargos_Ficheiros_Tipos]
	  WHERE (ativo = 1 or ativo <> @incluirApagados)
end







GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Cargos_Ficheiros_Tipos_R]    Script Date: 20/04/2018 11:53:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Utilizador_Cargos_Ficheiros_Tipos_R]
	@idTipoFicheiro int,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin tran
	begin try
		
		update [dbo].[Utilizadores_Cargos_Ficheiros_Tipos]
		set ativo = 1, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
		where id = @idTipoFicheiro

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@idTipoFicheiro as idTipoFicheiro
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	end








GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Cargos_Ficheiros_Tipos_S]    Script Date: 20/04/2018 11:53:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Utilizador_Cargos_Ficheiros_Tipos_S]
	@id int,
	@incluirApagados bit = 0
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [id]
      ,[nome]
      ,[descricao]
      ,[ativo]
      ,[dataCriado]
      ,[utilizadorCriado]
      ,[dataModificado]
      ,[utilizadorModificado]
  FROM [dbo].[Utilizadores_Cargos_Ficheiros_Tipos]
  where id = @id
	  and (ativo = 1 or ativo <> @incluirApagados)
end







GO
