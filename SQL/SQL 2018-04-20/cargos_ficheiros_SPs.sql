USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizadores_Cargos_Ficheiros_D]    Script Date: 19/04/2018 15:09:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Utilizadores_Cargos_Ficheiros_D]
	@id int,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
	begin try
	begin tran
	
		SET NOCOUNT ON;

		UPDATE [dbo].[Utilizadores_Cargos_Ficheiros]
		SET Ativo = 0, utilizadorModificado = @utilizadorEdicao, dataModificado = getdate()
		WHERE id = @id

		commit

    end try
	begin catch
		rollback
	end catch

END








GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizadores_Cargos_Ficheiros_IU]    Script Date: 19/04/2018 15:09:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- exec [stp_Entidade_IU] 4

CREATE procEDURE [dbo].[stp_Utilizadores_Cargos_Ficheiros_IU]
	@id int,
	@idCargo int,
	@nomeFicheiro nvarchar(max),
	@idTipoFicheiro int,
	@urlFicheiro nvarchar(max),
	@ativo bit = 1,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exists bit
		set @exists = 0

		select @exists = coalesce(a.[id], case when a.[id] is null then 0 when a.[id] is not null then 1 end) 
		from [dbo].[Utilizadores_Cargos_Ficheiros] a
		where a.[id] = @id

		if @exists = 1 and @id is not null
		begin
			update [dbo].[Utilizadores_Cargos_Ficheiros]
			set idCargo = @idCargo, nomeFicheiro = @nomeFicheiro, idTipoFicheiro = @idTipoFicheiro, urlFicheiro = @urlFicheiro, ativo = @ativo, utilizadorModificado = @utilizadorEdicao
			where id = @id

			select @id
		end
		else
		begin

			insert into [dbo].[Utilizadores_Cargos_Ficheiros] (idCargo, nomeFicheiro, idTipoFicheiro, urlFicheiro, ativo, dataCriado, utilizadorCriado)
			values (@idCargo, @nomeFicheiro, @idTipoFicheiro, @urlFicheiro, 1, getdate(), @utilizadorEdicao)

			select max(id) from [dbo].[Utilizadores_Cargos_Ficheiros]
		end

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@id as idTipoFicheiro
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END










GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizadores_Cargos_Ficheiros_LS]    Script Date: 19/04/2018 15:09:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Utilizadores_Cargos_Ficheiros_LS]
	@incluirApagados bit = 0
AS
BEGIN
	SET NOCOUNT ON;

		SELECT cf.[id]
		  ,cf.[idCargo]
		  ,cf.[nomeFicheiro]
		  ,cf.[idTipoFicheiro]
		  ,cf.[urlFicheiro]
		  ,cf.[dataCriado]
		  ,cf.[utilizadorCriado]
		  ,cf.[dataModificado]
		  ,cf.[utilizadorModificado]
	  FROM [dbo].[Utilizadores_Cargos_Ficheiros] cf
	  where (cf.ativo = 1 or cf.ativo<> @incluirApagados)
END








GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizadores_Cargos_Ficheiros_R]    Script Date: 19/04/2018 15:09:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Utilizadores_Cargos_Ficheiros_R]
	@id int,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
	begin try
	begin tran
	
		SET NOCOUNT ON;

		UPDATE [dbo].[Utilizadores_Cargos_Ficheiros]
		SET Ativo = 1, utilizadorModificado = @utilizadorEdicao, dataModificado = getdate()
		WHERE id = @id

		commit

    end try
	begin catch
		rollback
	end catch

END








GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizadores_Cargos_Ficheiros_S]    Script Date: 19/04/2018 15:09:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Utilizadores_Cargos_Ficheiros_S]
	@id int,
	@incluirApagados bit = 0
AS
BEGIN
	SET NOCOUNT ON;

		SELECT cf.[id]
		  ,cf.[idCargo]
		  ,cf.[nomeFicheiro]
		  ,cf.[idTipoFicheiro]
		  ,cf.[urlFicheiro]
		  ,cf.[dataCriado]
		  ,cf.[utilizadorCriado]
		  ,cf.[dataModificado]
		  ,cf.[utilizadorModificado]
	  FROM [dbo].[Utilizadores_Cargos_Ficheiros] cf
	  WHERE cf.id = @id
	  AND (cf.ativo = 1 or cf.ativo<> @incluirApagados)
END








GO
