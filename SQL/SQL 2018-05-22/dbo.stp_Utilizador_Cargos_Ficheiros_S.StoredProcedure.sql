USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Cargos_Ficheiros_S]    Script Date: 22/05/2018 15:46:44 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Cargos_Ficheiros_S]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Cargos_Ficheiros_S]    Script Date: 22/05/2018 15:46:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Utilizador_Cargos_Ficheiros_S]
	@id int,
	@incluirApagados bit = 0
AS
BEGIN
	SET NOCOUNT ON;

		SELECT cf.[id]
		  ,cf.[idCargo]
		  ,cf.[sharepointURL]
		  ,cf.[sharepointList]
		  ,cf.[filename]
		  ,cf.[idTipo]
		  ,ct.[nome] as tipoFicheiro
		  ,ct.[ativo]
		  ,cf.[dataCriado]
		  ,cf.[utilizadorCriado]
		  ,cf.[dataModificado]
		  ,cf.[utilizadorModificado]
	  FROM [dbo].[Utilizadores_Cargos_Ficheiros] cf
	  left join [dbo].[Utilizadores_Cargos_Ficheiros_Tipos] ct on ct.id = cf.idTipo
	  WHERE cf.id = @id
	  AND (cf.ativo = 1 or cf.ativo<> @incluirApagados)
END











GO
