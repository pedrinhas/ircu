USE [RCU]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros] DROP CONSTRAINT [FK_Utilizadores_Cargos_Ficheiros_Utilizadores_Cargos_Ficheiros_Tipos]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros] DROP CONSTRAINT [FK_Utilizadores_Cargos_Ficheiros_Utilizadores_Cargos]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros] DROP CONSTRAINT [DF_Utilizadores_Cargos_Ficheiros_utilizadorCriado]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros] DROP CONSTRAINT [DF_Utilizadores_Cargos_Ficheiros_dataCriado]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros] DROP CONSTRAINT [DF_Utilizadores_Cargos_Ficheiros_ativo]
GO
/****** Object:  Table [dbo].[Utilizadores_Cargos_Ficheiros]    Script Date: 22/05/2018 15:46:44 PM ******/
DROP TABLE [dbo].[Utilizadores_Cargos_Ficheiros]
GO
/****** Object:  Table [dbo].[Utilizadores_Cargos_Ficheiros]    Script Date: 22/05/2018 15:46:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Utilizadores_Cargos_Ficheiros](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idCargo] [int] NOT NULL,
	[sharepointURL] [varchar](max) NOT NULL,
	[sharepointList] [varchar](max) NOT NULL,
	[filename] [varchar](64) NOT NULL,
	[idTipo] [int] NOT NULL,
	[ativo] [bit] NOT NULL,
	[dataCriado] [datetime] NOT NULL,
	[utilizadorCriado] [nvarchar](max) NOT NULL,
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Utilizadores_Cargos_Ficheiros] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros] ADD  CONSTRAINT [DF_Utilizadores_Cargos_Ficheiros_ativo]  DEFAULT ((1)) FOR [ativo]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros] ADD  CONSTRAINT [DF_Utilizadores_Cargos_Ficheiros_dataCriado]  DEFAULT (getdate()) FOR [dataCriado]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros] ADD  CONSTRAINT [DF_Utilizadores_Cargos_Ficheiros_utilizadorCriado]  DEFAULT (suser_sname()) FOR [utilizadorCriado]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Cargos_Ficheiros_Utilizadores_Cargos] FOREIGN KEY([idCargo])
REFERENCES [dbo].[Utilizadores_Cargos] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros] CHECK CONSTRAINT [FK_Utilizadores_Cargos_Ficheiros_Utilizadores_Cargos]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Cargos_Ficheiros_Utilizadores_Cargos_Ficheiros_Tipos] FOREIGN KEY([idTipo])
REFERENCES [dbo].[Utilizadores_Cargos_Ficheiros_Tipos] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros] CHECK CONSTRAINT [FK_Utilizadores_Cargos_Ficheiros_Utilizadores_Cargos_Ficheiros_Tipos]
GO
