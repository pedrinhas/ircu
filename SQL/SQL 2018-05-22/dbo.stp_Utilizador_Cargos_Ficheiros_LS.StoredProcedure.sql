USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Cargos_Ficheiros_LS]    Script Date: 22/05/2018 15:46:44 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Cargos_Ficheiros_LS]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Cargos_Ficheiros_LS]    Script Date: 22/05/2018 15:46:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Utilizador_Cargos_Ficheiros_LS]
	@incluirApagados bit = 0
AS
BEGIN
	SET NOCOUNT ON;

		SELECT cf.[id]
		  ,cf.[idCargo]
		  ,cf.[sharepointURL]
		  ,cf.[sharepointList]
		  ,cf.[filename]
		  ,cf.[idTipo]
		  ,ct.[nome] as tipoFicheiro
		  ,ct.[ativo]
		  ,cf.[dataCriado]
		  ,cf.[utilizadorCriado]
		  ,cf.[dataModificado]
		  ,cf.[utilizadorModificado]
	  FROM [dbo].[Utilizadores_Cargos_Ficheiros] cf
	  left join [dbo].[Utilizadores_Cargos_Ficheiros_Tipos] ct on ct.id = cf.idTipo
	  where (cf.ativo = 1 or cf.ativo<> @incluirApagados)
END











GO
