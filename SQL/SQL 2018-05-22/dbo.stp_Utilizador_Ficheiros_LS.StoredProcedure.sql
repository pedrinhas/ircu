USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_LS]    Script Date: 22/05/2018 15:46:44 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_LS]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_LS]    Script Date: 22/05/2018 15:46:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Utilizador_Ficheiros_LS]
	@incluirApagados bit = 0
AS
BEGIN
	SET NOCOUNT ON;

select uf.[id]
	  ,uf.[idUtilizador]
	  ,uf.[idPerfil]
	  ,uf.[sharepointURL]
	  ,uf.[sharepointList]
	  ,uf.[filename]
	  ,uf.[idTipo]
	  ,uft.[nome] as tipoFicheiro
	  ,uft.[ativo]
	  ,uf.[dataCriado]
	  ,uf.[utilizadorCriado]
	  ,uf.[dataModificado]
	  ,uf.[utilizadorModificado]
  FROM [dbo].[Utilizadores_Ficheiros] uf
  left join [dbo].[Utilizadores_Ficheiros_Tipos] uft on uft.id = uf.idTipo
  where (uf.ativo = 1 or uf.ativo<> @incluirApagados)
END











GO
