USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Cargos_Ficheiros_R]    Script Date: 22/05/2018 15:46:44 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Cargos_Ficheiros_R]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Cargos_Ficheiros_R]    Script Date: 22/05/2018 15:46:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Utilizador_Cargos_Ficheiros_R]
	@id int,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
	begin try
	begin tran
	
		SET NOCOUNT ON;

		UPDATE [dbo].[Utilizadores_Cargos_Ficheiros]
		SET Ativo = 1, utilizadorModificado = @utilizadorEdicao, dataModificado = getdate()
		WHERE id = @id

		commit

    end try
	begin catch
		rollback
	end catch

END










GO
