USE [RCU]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros_Tipos] DROP CONSTRAINT [DF_Utilizadores_Cargos_Ficheiros_Tipos_utilizadorCriado]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros_Tipos] DROP CONSTRAINT [DF_Utilizadores_Cargos_Ficheiros_Tipos_dataCriado]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros_Tipos] DROP CONSTRAINT [DF_Utilizadores_Cargos_Ficheiros_Tipos_ativo]
GO
/****** Object:  Table [dbo].[Utilizadores_Cargos_Ficheiros_Tipos]    Script Date: 22/05/2018 15:46:44 PM ******/
DROP TABLE [dbo].[Utilizadores_Cargos_Ficheiros_Tipos]
GO
/****** Object:  Table [dbo].[Utilizadores_Cargos_Ficheiros_Tipos]    Script Date: 22/05/2018 15:46:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores_Cargos_Ficheiros_Tipos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nome] [nvarchar](max) NOT NULL,
	[descricao] [nvarchar](max) NULL,
	[ativo] [bit] NOT NULL,
	[dataCriado] [datetime] NOT NULL,
	[utilizadorCriado] [nvarchar](max) NOT NULL,
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Utilizadores_Cargos_Ficheiros_Tipos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros_Tipos] ADD  CONSTRAINT [DF_Utilizadores_Cargos_Ficheiros_Tipos_ativo]  DEFAULT ((1)) FOR [ativo]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros_Tipos] ADD  CONSTRAINT [DF_Utilizadores_Cargos_Ficheiros_Tipos_dataCriado]  DEFAULT (getdate()) FOR [dataCriado]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Ficheiros_Tipos] ADD  CONSTRAINT [DF_Utilizadores_Cargos_Ficheiros_Tipos_utilizadorCriado]  DEFAULT (suser_sname()) FOR [utilizadorCriado]
GO
