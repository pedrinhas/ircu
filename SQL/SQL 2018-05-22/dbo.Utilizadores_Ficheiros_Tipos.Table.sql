USE [RCU]
GO
/****** Object:  Table [dbo].[Utilizadores_Ficheiros_Tipos]    Script Date: 22/05/2018 15:46:44 PM ******/
DROP TABLE [dbo].[Utilizadores_Ficheiros_Tipos]
GO
/****** Object:  Table [dbo].[Utilizadores_Ficheiros_Tipos]    Script Date: 22/05/2018 15:46:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores_Ficheiros_Tipos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nome] [nvarchar](max) NOT NULL,
	[descricao] [nvarchar](max) NULL,
	[ativo] [bit] NOT NULL CONSTRAINT [DF_Utilizadores_Ficheiros_Tipos_ativo]  DEFAULT ((1)),
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF_Utilizadores_Ficheiros_Tipos_dataCriado]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF_Utilizadores_Ficheiros_Tipos_utilizadorCriado]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Utilizadores_Ficheiros_Tipos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Utilizadores_Ficheiros_Tipos] ON 

INSERT [dbo].[Utilizadores_Ficheiros_Tipos] ([id], [nome], [descricao], [ativo], [dataCriado], [utilizadorCriado], [dataModificado], [utilizadorModificado]) VALUES (1, N'Impresso - Gestão de Identidades e Acessos', N'Ficheiro gerado no registo do utilizador através da plataforma de gestão de contas. Inclui os dados obtidos através do sistema de gestão financeira, e é assinado digitalmente com Cartão de Cidadão pelo visado.', 1, CAST(N'2018-05-22 15:01:04.233' AS DateTime), N'INTRA\cpereira', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Utilizadores_Ficheiros_Tipos] OFF
