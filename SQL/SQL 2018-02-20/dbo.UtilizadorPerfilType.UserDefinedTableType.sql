USE [RCU]
GO
/****** Object:  UserDefinedTableType [dbo].[UtilizadorPerfilType]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TYPE [dbo].[UtilizadorPerfilType]
GO
/****** Object:  UserDefinedTableType [dbo].[UtilizadorPerfilType]    Script Date: 20/02/2018 16:56:54 PM ******/
CREATE TYPE [dbo].[UtilizadorPerfilType] AS TABLE(
	[ID] [int] NULL,
	[IUPI] [uniqueidentifier] NULL,
	[NMec] [nvarchar](10) NULL,
	[IDTipo] [int] NULL,
	[IDCategoria] [int] NULL,
	[IDEntidade] [int] NULL,
	[IDCarreira] [int] NULL,
	[IDSituacao] [int] NULL,
	[IDRegime] [int] NULL,
	[DataEntrada] [datetime] NULL,
	[DataSaida] [datetime] NULL,
	[UtilizadorEdicao] [nvarchar](max) NULL
)
GO
