USE [RCU]
GO
ALTER TABLE [dbo].[Sessao_UtilizadorExcepcao] DROP CONSTRAINT [DF__Sessao_Ut__utili__48EFCE0F]
GO
ALTER TABLE [dbo].[Sessao_UtilizadorExcepcao] DROP CONSTRAINT [DF__Sessao_Ut__dataC__4F12BBB9]
GO
ALTER TABLE [dbo].[Sessao_UtilizadorExcepcao] DROP CONSTRAINT [DF__Sessao_Ut__ativo__0C06BB60]
GO
/****** Object:  Table [dbo].[Sessao_UtilizadorExcepcao]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[Sessao_UtilizadorExcepcao]
GO
/****** Object:  Table [dbo].[Sessao_UtilizadorExcepcao]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sessao_UtilizadorExcepcao](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[utilizador] [varchar](50) NOT NULL,
	[appId] [int] NOT NULL,
	[ativo] [bit] NOT NULL,
	[dataCriado] [datetime] NOT NULL,
	[utilizadorCriado] [nvarchar](max) NOT NULL,
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Sessao_UtilizadorExcepcao] ADD  CONSTRAINT [DF__Sessao_Ut__ativo__0C06BB60]  DEFAULT ((1)) FOR [ativo]
GO
ALTER TABLE [dbo].[Sessao_UtilizadorExcepcao] ADD  CONSTRAINT [DF__Sessao_Ut__dataC__4F12BBB9]  DEFAULT (getdate()) FOR [dataCriado]
GO
ALTER TABLE [dbo].[Sessao_UtilizadorExcepcao] ADD  CONSTRAINT [DF__Sessao_Ut__utili__48EFCE0F]  DEFAULT (suser_sname()) FOR [utilizadorCriado]
GO
