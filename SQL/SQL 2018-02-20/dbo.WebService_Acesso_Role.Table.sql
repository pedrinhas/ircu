USE [RCU]
GO
/****** Object:  Table [dbo].[WebService_Acesso_Role]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[WebService_Acesso_Role]
GO
/****** Object:  Table [dbo].[WebService_Acesso_Role]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebService_Acesso_Role](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nome] [nvarchar](50) NOT NULL,
	[ativo] [bit] NOT NULL CONSTRAINT [DF__WebServic__ativo__13A7DD28]  DEFAULT ((1)),
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF__WebServic__dataC__6D9742D9]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF__WebServic__utili__536D5C82]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_WebService_Acesso_Roles] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
