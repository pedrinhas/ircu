USE [RCU]
GO
/****** Object:  Table [dbo].[Utilizadores_Situacoes]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[Utilizadores_Situacoes]
GO
/****** Object:  Table [dbo].[Utilizadores_Situacoes]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores_Situacoes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [nvarchar](50) NOT NULL,
	[nome] [nvarchar](50) NOT NULL,
	[descricao] [nvarchar](max) NULL,
	[PRC] [nvarchar](max) NULL,
	[ativo] [bit] NOT NULL CONSTRAINT [DF__Utilizado__ativo__1590259A]  DEFAULT ((1)),
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF_Utilizadores_Situacoes_dataCriado]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF__Utilizado__utili__5649C92D]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Utilizadores_Situacoes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
