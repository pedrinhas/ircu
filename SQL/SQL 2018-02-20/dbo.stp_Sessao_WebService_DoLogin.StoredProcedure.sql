USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Sessao_WebService_DoLogin]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Sessao_WebService_DoLogin]
GO
/****** Object:  StoredProcedure [dbo].[stp_Sessao_WebService_DoLogin]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

-- exec [stp_Sessao_WebService_DoLogin] 'sis-dev', '$p4s7i10#'


CREATE procEDURE [dbo].[stp_Sessao_WebService_DoLogin]
	@username nvarchar(50),
	@password nvarchar(max)
AS
BEGIN
	begin tran
	begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;	

		declare @passwordHash binary(64)
		declare @salt uniqueidentifier
		declare @alg nvarchar(10)
		
		declare @authenticated bit
		
		set @alg = (SELECT Algoritmo FROM [dbo].[WebService_Acesso_User] WHERE Username = @username)
		set @salt = (SELECT Salt FROM [dbo].[WebService_Acesso_User] WHERE username = @username)
		set @passwordHash =  HASHBYTES(@alg, CONCAT(@password, cast (@salt as nvarchar(max))))

		SELECT @authenticated = (case when count(*) > 0 then 1 else 0 end)
		 FROM [dbo].[WebService_Acesso_User] u
		   left join [dbo].[WebService_Acesso_UserRoleAss] ass on u.id = ass.idUser
		   left join [dbo].[WebService_Acesso_Role] r on ass.idRole = r.id
		 WHERE u.Username = @username
		   AND u.[Password] = @passwordHash
		   AND u.Ativo = 1
		   and r.id <> 1

		if @authenticated = 1
			SELECT u.[id]
			  ,u.[Username]
			  ,r.id as idRole
			  ,r.[nome] as role
			 FROM [dbo].[WebService_Acesso_User] u
			   left join [dbo].[WebService_Acesso_UserRoleAss] ass on u.id = ass.idUser
			   left join [dbo].[WebService_Acesso_Role] r on ass.idRole = r.id
			 WHERE u.Username = @username
			   AND u.[Password] = @passwordHash
			   AND u.Ativo = 1
		else
			SELECT u.[id]
			  ,u.[Username]
			  ,r.id as idRole
			  ,r.[nome] as role
			 FROM [dbo].[WebService_Acesso_User] u
			   left join [dbo].[WebService_Acesso_UserRoleAss] ass on u.id = ass.idUser
			   left join [dbo].[WebService_Acesso_Role] r on ass.idRole = r.id
			 WHERE u.Username = 'noAuth'

		commit
	end try
	begin catch
		    select ERROR_NUMBER() AS ErrorNumber
			,@Username as Username
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END








GO
