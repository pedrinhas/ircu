USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Tools_Fix_Contactos_PropertyIDs]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Tools_Fix_Contactos_PropertyIDs]
GO
/****** Object:  StoredProcedure [dbo].[stp_Tools_Fix_Contactos_PropertyIDs]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[stp_Tools_Fix_Contactos_PropertyIDs]
as

update [Utilizadores_Contactos]
set [Utilizadores_Contactos].propertyid = fixed.propertyIDFixed
from(select contactos.id, ROW_NUMBER() OVER (PARTITION BY contactos.idUtilizador ORDER BY contactos.idUtilizador) - 1 AS propertyIDFixed
from [dbo].[Utilizadores_Contactos] contactos) fixed
where fixed.id = [Utilizadores_Contactos].id







GO
