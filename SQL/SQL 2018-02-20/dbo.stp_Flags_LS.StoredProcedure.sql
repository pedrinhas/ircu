USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Flags_LS]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Flags_LS]
GO
/****** Object:  StoredProcedure [dbo].[stp_Flags_LS]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[stp_Flags_LS]
	@incluirApagados bit = 0
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [id]
		  ,[key]
		  ,[descricao]
		  ,[dataCriado]
		  ,[ativo]
		  ,[utilizadorCriado]
		  ,[dataModificado]
		  ,[utilizadorModificado]
	  FROM [dbo].[Flags]
	  WHERE (ativo = 1 or ativo <> @incluirApagados)
END









GO
