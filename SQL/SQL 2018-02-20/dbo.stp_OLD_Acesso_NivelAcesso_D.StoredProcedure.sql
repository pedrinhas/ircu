USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_OLD_Acesso_NivelAcesso_D]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_OLD_Acesso_NivelAcesso_D]
GO
/****** Object:  StoredProcedure [dbo].[stp_OLD_Acesso_NivelAcesso_D]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- exec [stp_Acesso_NivelAcesso_IU] 0, 4, 'Nivel teste', 1, 'cpereira'

CREATE procEDURE [dbo].[stp_OLD_Acesso_NivelAcesso_D]
	@id int,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin try
	begin tran
	
		SET NOCOUNT ON;

		UPDATE [dbo].[Acesso_NivelAcesso]
		SET Ativo = 0, utilizadorModificado = @utilizadorEdicao, dataModificado = getdate()
		WHERE idNivelAcesso = @id

		commit

    end try
	begin catch
		rollback
	end catch


END








GO
