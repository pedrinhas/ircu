USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Flags_IU]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Flags_IU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Flags_IU]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


CREATE procEDURE [dbo].[stp_Utilizador_Flags_IU]
	  @idUtilizador int,
	  @idPerfil int null,
	  @idFlag int,
	  @value nvarchar(max),
	  @utilizadorEdicao nvarchar(max)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exists bit
		set @exists = 0

		select @exists = coalesce(c.id, case when c.id is null then 0 when c.id is not null then 1 end) 
		from [dbo].[Utilizadores_Flags] c
		where IDUtilizador = @idUtilizador AND (IDPerfil = @idPerfil OR IDPerfil is null) AND idFlag = @idFlag

		if @exists = 1 
		begin
			update [dbo].[Utilizadores_Flags]
			set idFlag = @idFlag, Value = @value, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
			where IDUtilizador = @idUtilizador AND (IDPerfil = @idPerfil OR IDPerfil is null) AND idFlag = @idFlag

			select id
			from [dbo].[Utilizadores_Flags] c
			where IDUtilizador = @idUtilizador AND (IDPerfil = @idPerfil OR IDPerfil is null) AND idFlag = @idFlag
		end
		else
		begin
			insert into Utilizadores_Flags (idUtilizador, idPerfil, idFlag, value, dataCriado, utilizadorCriado)
			values (@idUtilizador, @idPerfil, @idFlag, @value, getdate(), @utilizadorEdicao)

			select max(id) from [dbo].[Utilizadores_Flags]
		end

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@idUtilizador as idUtilizador
		,@idPerfil as idPerfil
		,@idFlag as idFlag
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	
END










GO
