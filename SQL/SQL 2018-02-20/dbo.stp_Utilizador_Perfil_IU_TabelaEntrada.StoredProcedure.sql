USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Perfil_IU_TabelaEntrada]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Perfil_IU_TabelaEntrada]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Perfil_IU_TabelaEntrada]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- alter date: <alter Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE procEDURE [dbo].[stp_Utilizador_Perfil_IU_TabelaEntrada]
	 @perfis [dbo].[UtilizadorPerfilType] readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	begin tran
	begin try
		declare @exist [dbo].[UtilizadorPerfilType],
				@new [dbo].[UtilizadorPerfilType]
		declare @result table (NMec nvarchar(10), ID int, Accao char)

		insert into @exist
		select *
		from @perfis p
		where p.id in (select id from [dbo].[Utilizadores_Perfil] p)

		insert into @new
		select *
		from @perfis p
		where p.id not in (select id from [dbo].[Utilizadores_Perfil] p)


		--existentes
		update PerfisExistentes
		set PerfisExistentes.idTipo = ExistentesParam.idTipo,
			PerfisExistentes.nmec = ExistentesParam.nmec,
			PerfisExistentes.idCategoria = ExistentesParam.idCategoria,
			PerfisExistentes.idEntidade = ExistentesParam.idEntidade,
			PerfisExistentes.idCarreira = ExistentesParam.idCarreira,
			PerfisExistentes.idSituacao = ExistentesParam.idSituacao,
			PerfisExistentes.idRegime = ExistentesParam.idRegime,
			PerfisExistentes.dataEntrada = ExistentesParam.dataEntrada,
			PerfisExistentes.dataSaida = ExistentesParam.dataSaida,
			PerfisExistentes.dataModificado = getDate(),
			PerfisExistentes.utilizadorModificado = ExistentesParam.utilizadorEdicao
		from [dbo].[Utilizadores_Perfil] as PerfisExistentes
		inner join @exist as ExistentesParam on PerfisExistentes.id = ExistentesParam.id

		insert into @result
		select nmec, id, 'U'
		from @exist

		--novos
		insert into [dbo].[Utilizadores_Perfil] (idUtilizador, nmec, idTipo, idCategoria, idEntidade, idCarreira, idSituacao, idRegime, dataEntrada, dataSaida, ativo, dataCriado, utilizadorCriado)
		select u.id, n.nmec, n.idtipo, n.idcategoria, n.identidade, n.idcarreira, n.idsituacao, n.idregime, n.dataentrada, n.datasaida, 1, getdate(), n.utilizadorEdicao
		from @new n
		inner join [dbo].[Utilizadores] u on u.iupi = n.iupi
		
		insert into @result
		select e.nmec, p.id, 'I'
		from @new as e
		inner join [dbo].[Utilizadores_Perfil] p on e.nmec = p.nmec


		DECLARE @devnull TABLE (iupi uniqueidentifier, idtipo int, accao char)

		declare @userIDs IDUtilizadorListType
		insert into @userIDs
		select distinct u.id, n.UtilizadorEdicao
		from @new n
		inner join [dbo].[Utilizadores] u on u.iupi = n.iupi

		insert into @devnull
		exec [stp_Flags_Default_SET_TabelaEntrada] @userIDs

		select * from @result

		--rollback
		commit
	end try
	begin catch

		select ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END
















GO
