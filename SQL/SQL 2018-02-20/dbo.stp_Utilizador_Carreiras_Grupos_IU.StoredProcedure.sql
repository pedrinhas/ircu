USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Carreiras_Grupos_IU]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Carreiras_Grupos_IU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Carreiras_Grupos_IU]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Utilizador_Carreiras_Grupos_IU]
	@id int,
	@codigo nvarchar(50),
	@nome nvarchar(50),
	@descricao nvarchar(max) = null,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin tran
	begin try
		declare @exists bit
		set @exists = 0

		select @exists = coalesce(c.id, case when c.id is null then 0 when c.id is not null then 1 end) 
		from [dbo].[Utilizadores_Carreiras_Grupos] c
		where @id is not null and c.id = @id

		if @exists = 1 and @id is not null
		begin
			update [dbo].[Utilizadores_Carreiras_Grupos]
			set codigo = @codigo, nome = @nome, descricao = @descricao, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
			where id = @id
		end
		else
		begin
			insert into [dbo].[Utilizadores_Carreiras_Grupos] (codigo, nome, descricao, ativo, dataCriado, utilizadorCriado)
			values (@codigo, @nome, @descricao, 1, getdate(), @utilizadorEdicao)

			select max(id) from [Utilizadores_Carreiras_Grupos]
		end

			commit
	end try
	begin catch
	rollback
		select ERROR_NUMBER() AS ErrorNumber
		,@id as idCarreiraGrupo
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
	end catch
	end










GO
