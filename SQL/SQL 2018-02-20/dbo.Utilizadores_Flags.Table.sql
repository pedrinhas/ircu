USE [RCU]
GO
ALTER TABLE [dbo].[Utilizadores_Flags] DROP CONSTRAINT [FK_Utilizadores_Flags_Utilizadores_Perfil]
GO
ALTER TABLE [dbo].[Utilizadores_Flags] DROP CONSTRAINT [FK_Utilizadores_Flags_Utilizadores]
GO
ALTER TABLE [dbo].[Utilizadores_Flags] DROP CONSTRAINT [FK_Utilizadores_Flags_Flags]
GO
/****** Object:  Table [dbo].[Utilizadores_Flags]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[Utilizadores_Flags]
GO
/****** Object:  Table [dbo].[Utilizadores_Flags]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores_Flags](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUtilizador] [int] NOT NULL,
	[idPerfil] [int] NULL,
	[idFlag] [int] NOT NULL,
	[value] [nvarchar](max) NULL,
	[ativo] [bit] NOT NULL,
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF__Flag__dataC__5A846E65]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF__Flag__utili__5555A4F4]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Utilizadores_Flags] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Utilizadores_Flags]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Flags_Flags] FOREIGN KEY([idFlag])
REFERENCES [dbo].[Flags] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Flags] CHECK CONSTRAINT [FK_Utilizadores_Flags_Flags]
GO
ALTER TABLE [dbo].[Utilizadores_Flags]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Flags_Utilizadores] FOREIGN KEY([idUtilizador])
REFERENCES [dbo].[Utilizadores] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Flags] CHECK CONSTRAINT [FK_Utilizadores_Flags_Utilizadores]
GO
ALTER TABLE [dbo].[Utilizadores_Flags]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Flags_Utilizadores_Perfil] FOREIGN KEY([idPerfil])
REFERENCES [dbo].[Utilizadores_Perfil] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Flags] CHECK CONSTRAINT [FK_Utilizadores_Flags_Utilizadores_Perfil]
GO
