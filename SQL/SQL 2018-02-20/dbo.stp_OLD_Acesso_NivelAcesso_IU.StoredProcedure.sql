USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_OLD_Acesso_NivelAcesso_IU]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_OLD_Acesso_NivelAcesso_IU]
GO
/****** Object:  StoredProcedure [dbo].[stp_OLD_Acesso_NivelAcesso_IU]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- exec [stp_Acesso_NivelAcesso_IU] 0, 4, 'Nivel teste', 1, 'cpereira'

CREATE procEDURE [dbo].[stp_OLD_Acesso_NivelAcesso_IU]
	@id int,
	@idAplicacao int,
	@nome nvarchar(50),
	@ativo bit, 
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exists bit
		set @exists = 0

		select @exists = coalesce(a.idNivelAcesso, case when a.idNivelAcesso is null then 0 when a.idNivelAcesso is not null then 1 end) 
		from [dbo].[Acesso_NivelAcesso] a
		where a.idNivelAcesso = @id

		if @exists = 1 and @id is not null
		begin
			update [dbo].[Acesso_NivelAcesso]
			set NomeNivel = @nome, ativo = @ativo, utilizadorModificado = @utilizadorEdicao
			where idAplicacao = @id

			select @id
		end
		else
		begin

			insert into [dbo].[Acesso_NivelAcesso] (idAplicacao, NomeNivel, Ativo, dataCriado, utilizadorCriado)
			values (@idAplicacao, @nome, 1, getdate(), @utilizadorEdicao)

			select max(idNivelAcesso) from [dbo].[Acesso_NivelAcesso]
		end

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@id as idNivelAcesso
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END









GO
