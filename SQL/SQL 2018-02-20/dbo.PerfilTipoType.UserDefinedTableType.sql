USE [RCU]
GO
/****** Object:  UserDefinedTableType [dbo].[PerfilTipoType]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TYPE [dbo].[PerfilTipoType]
GO
/****** Object:  UserDefinedTableType [dbo].[PerfilTipoType]    Script Date: 20/02/2018 16:56:54 PM ******/
CREATE TYPE [dbo].[PerfilTipoType] AS TABLE(
	[ID] [int] NULL,
	[Codigo] [nvarchar](50) NULL,
	[Sigla] [nvarchar](50) NULL,
	[Nome] [nvarchar](50) NULL,
	[Descricao] [nvarchar](max) NULL,
	[UtilizadorEdicao] [nvarchar](max) NOT NULL
)
GO
