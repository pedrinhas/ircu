USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Contactos_LS_Teste]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Contactos_LS_Teste]
GO
/****** Object:  StoredProcedure [dbo].[stp_Contactos_LS_Teste]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create procEDURE [dbo].[stp_Contactos_LS_Teste]
	@incluirApagados bit = 0
	
AS
BEGIN
	exec [stp_Contactos_PorVisibilidade_LS_Teste] 3, @incluirApagados
END





GO
