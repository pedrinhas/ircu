USE [RCU]
GO
/****** Object:  Table [dbo].[OLD_Utilizador_Categorias]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[OLD_Utilizador_Categorias]
GO
/****** Object:  Table [dbo].[OLD_Utilizador_Categorias]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OLD_Utilizador_Categorias](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [nvarchar](50) NOT NULL,
	[nome] [nvarchar](50) NOT NULL,
	[descricao] [nvarchar](max) NULL,
	[idCarreira] [int] NULL,
	[ativo] [bit] NOT NULL,
	[dataCriado] [datetime] NULL,
	[utilizadorCriado] [nvarchar](max) NULL,
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
