USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Regimes_S]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Regimes_S]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Regimes_S]    Script Date: 20/02/2018 16:56:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[stp_Utilizador_Regimes_S]
	@id int
	, @incluirApagados bit = 0
	as
	SET NOCOUNT ON;
SELECT [id]
      ,[codigo]
      ,[nome]
      ,[descricao]
      ,[ativo]
      ,[dataCriado]
      ,[utilizadorCriado]
      ,[dataModificado]
      ,[utilizadorModificado]
  FROM [dbo].[Utilizadores_Regimes] r
  where id = @id
  and  (r.ativo = 1 or r.ativo <> @incluirApagados)












GO
