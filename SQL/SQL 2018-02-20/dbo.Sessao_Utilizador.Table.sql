USE [RCU]
GO
/****** Object:  Index [IX_Sessao_Utilizador]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP INDEX [IX_Sessao_Utilizador] ON [dbo].[Sessao_Utilizador]
GO
/****** Object:  Table [dbo].[Sessao_Utilizador]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[Sessao_Utilizador]
GO
/****** Object:  Table [dbo].[Sessao_Utilizador]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sessao_Utilizador](
	[idUtilizador] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Nome] [nvarchar](max) NOT NULL,
	[Email] [nvarchar](max) NOT NULL,
	[PasswordHash] [nvarchar](131) NOT NULL,
	[Salt] [uniqueidentifier] NOT NULL,
	[Algoritmo] [nvarchar](10) NOT NULL,
	[Ativo] [bit] NOT NULL CONSTRAINT [DF_Sessao_Utilizador_Ativo]  DEFAULT ((1)),
	[dataCriado] [datetime] NOT NULL DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Sessao_Utilizador] PRIMARY KEY CLUSTERED 
(
	[idUtilizador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Sessao_Utilizador]    Script Date: 20/02/2018 16:56:54 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Sessao_Utilizador] ON [dbo].[Sessao_Utilizador]
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
