USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Contactos_LS]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Contactos_LS]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Contactos_LS]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[stp_Utilizador_Contactos_LS]
	@incluirApagados bit = 0
	as
	SET NOCOUNT ON;
SELECT c.[id]
      ,[propertyId]
	  ,c.idUtilizador
	  ,utilizador.iupi
      ,c.[idTipo]
	  ,tipo.tipo
	  ,tipo.idgrupo
	  ,grupo.grupo
      ,[propertyval]
	  ,perfil.visibilidade as visibilidadePerfil
      ,c.visibilidade as visibilidadeContacto
	  ,c.ativo
	  ,c.dataCriado
	  ,c.utilizadorCriado
	  ,c.dataModificado
	  ,c.utilizadorModificado
  FROM [RCU].[dbo].[Utilizadores_Contactos] c
  left join [dbo].[Utilizadores] utilizador on c.idUtilizador = utilizador.id
  left join [dbo].[Utilizadores_Contactos_Tipos] tipo on c.idTipo = tipo.id
  left join [dbo].[Utilizadores_Contactos_Tipos_Grupos] grupo on tipo.idGrupo = grupo.id
  left join [dbo].[Utilizadores_Perfil] perfil on utilizador.id = perfil.idUtilizador
  where (c.ativo = 1 or c.ativo <> @incluirApagados)
  and perfil.datasaida is null






GO
