USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_IU_TabelaEntrada]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_IU_TabelaEntrada]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_IU_TabelaEntrada]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		cpereira
-- Create date: 2017-11-21 14:32:24.870
-- Description:	Adiciona ou atualiza vários utilizadores
-- =============================================
CREATE procEDURE [dbo].[stp_Utilizador_IU_TabelaEntrada]
	  @users [dbo].[UtilizadorType] readonly
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	begin tran
	begin try
		declare @exist [dbo].[UtilizadorType],
				@new [dbo].[UtilizadorType]
		declare @result table(NIF nvarchar(20), IUPI uniqueidentifier, Accao char)


		insert into @exist
		select *
		from @users u
		where u.iupi is not null and u.iupi in (select IUPI from [dbo].[Utilizadores])

		insert into @new 
		select *
		from @users u
		where u.iupi is null or u.iupi not in (select IUPI from [dbo].[Utilizadores])

		update @new set iupi = newid() where iupi is null

		--select * from @exist
		--select * from @new

		--existentes
		update UtilizadoresExistentes
		set 
		UtilizadoresExistentes.nome = ExistentesParam.Nome,
		UtilizadoresExistentes.nomePersonalizado = ExistentesParam.NomePersonalizado,
		UtilizadoresExistentes.nif = ExistentesParam.NIF,
		UtilizadoresExistentes.username = ExistentesParam.Username,
		UtilizadoresExistentes.aliasedEmail = ExistentesParam.AliasedEmail,
		UtilizadoresExistentes.originalEmail = ExistentesParam.OriginalEmail,
		UtilizadoresExistentes.dataModificado = getdate(),
		UtilizadoresExistentes.contaNoGIAF = ExistentesParam.ContaNoGIAF,
		UtilizadoresExistentes.visibilidade = ExistentesParam.Visibilidade,
		UtilizadoresExistentes.utilizadorModificado = ExistentesParam.UtilizadorEdicao
		from [dbo].[Utilizadores] as UtilizadoresExistentes
		inner join @exist as ExistentesParam
		on UtilizadoresExistentes.iupi = ExistentesParam.iupi
		
		insert into @result
		select NIF, IUPI, 'U'
		from @exist

		--novos
		insert into [dbo].[Utilizadores] (iupi, nome, nomePersonalizado, nif, username, originalEmail, aliasedEmail, contaNoGIAF, visibilidade, ativo, dataCriado, utilizadorCriado)
		select IUPI, Nome, NomePersonalizado, NIF, Username, OriginalEmail, AliasedEmail, ContaNoGiaf, Visibilidade, 1, getdate(), UtilizadorEdicao
		from @new
		
		insert into @result
		select NIF, IUPI, 'I'
		from @new


		select * 
		from @result
		commit
		--rollback
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END










GO
