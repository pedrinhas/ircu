USE [RCU]
GO
ALTER TABLE [dbo].[Aplicacoes_AplicacaoGrupo] DROP CONSTRAINT [FK__Geral_Apl__idGru__164452B1]
GO
ALTER TABLE [dbo].[Aplicacoes_AplicacaoGrupo] DROP CONSTRAINT [FK__Geral_Apl__idApl__15502E78]
GO
ALTER TABLE [dbo].[Aplicacoes_AplicacaoGrupo] DROP CONSTRAINT [DF__Aplicacoe__utili__4707859D]
GO
ALTER TABLE [dbo].[Aplicacoes_AplicacaoGrupo] DROP CONSTRAINT [DF__Aplicacoe__dataC__36470DEF]
GO
ALTER TABLE [dbo].[Aplicacoes_AplicacaoGrupo] DROP CONSTRAINT [DF__Aplicacoe__ativo__0A1E72EE]
GO
/****** Object:  Table [dbo].[Aplicacoes_AplicacaoGrupo]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[Aplicacoes_AplicacaoGrupo]
GO
/****** Object:  Table [dbo].[Aplicacoes_AplicacaoGrupo]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Aplicacoes_AplicacaoGrupo](
	[idAplicacaoGrupo] [int] IDENTITY(1,1) NOT NULL,
	[idAplicacao] [int] NOT NULL,
	[idGrupo] [int] NOT NULL,
	[ativo] [bit] NOT NULL,
	[dataCriado] [datetime] NOT NULL,
	[utilizadorCriado] [nvarchar](max) NOT NULL,
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK__Geral_Ap__3214EC078D55F203] PRIMARY KEY CLUSTERED 
(
	[idAplicacaoGrupo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Aplicacoes_AplicacaoGrupo] ADD  CONSTRAINT [DF__Aplicacoe__ativo__0A1E72EE]  DEFAULT ((1)) FOR [ativo]
GO
ALTER TABLE [dbo].[Aplicacoes_AplicacaoGrupo] ADD  CONSTRAINT [DF__Aplicacoe__dataC__36470DEF]  DEFAULT (getdate()) FOR [dataCriado]
GO
ALTER TABLE [dbo].[Aplicacoes_AplicacaoGrupo] ADD  CONSTRAINT [DF__Aplicacoe__utili__4707859D]  DEFAULT (suser_sname()) FOR [utilizadorCriado]
GO
ALTER TABLE [dbo].[Aplicacoes_AplicacaoGrupo]  WITH CHECK ADD  CONSTRAINT [FK__Geral_Apl__idApl__15502E78] FOREIGN KEY([idAplicacao])
REFERENCES [dbo].[Aplicacoes] ([idAplicacao])
GO
ALTER TABLE [dbo].[Aplicacoes_AplicacaoGrupo] CHECK CONSTRAINT [FK__Geral_Apl__idApl__15502E78]
GO
ALTER TABLE [dbo].[Aplicacoes_AplicacaoGrupo]  WITH CHECK ADD  CONSTRAINT [FK__Geral_Apl__idGru__164452B1] FOREIGN KEY([idGrupo])
REFERENCES [dbo].[Aplicacoes_Grupo] ([idGrupo])
GO
ALTER TABLE [dbo].[Aplicacoes_AplicacaoGrupo] CHECK CONSTRAINT [FK__Geral_Apl__idGru__164452B1]
GO
