USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Situacoes_IU_TabelaEntrada]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Situacoes_IU_TabelaEntrada]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Situacoes_IU_TabelaEntrada]    Script Date: 20/02/2018 16:56:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


create procEDURE [dbo].[stp_Utilizador_Situacoes_IU_TabelaEntrada]
	  @Situacoes [dbo].[SituacaoType] readonly
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exist [dbo].[SituacaoType],
				@new [dbo].[SituacaoType]
		declare @result table(Codigo nvarchar(20), ID int, Accao char)

		insert into @exist
		select * 
		from @Situacoes r
		where r.id is not null and r.id in (select id from Utilizadores_Situacoes)

		insert into @new
		select * 
		from @Situacoes r
		where r.id is null or r.id not in (select id from Utilizadores_Situacoes)

		--existentes
		update SituacoesExistentes
		set SituacoesExistentes.codigo = ExistentesParam.codigo,
			SituacoesExistentes.nome = ExistentesParam.nome,
			SituacoesExistentes.descricao = ExistentesParam.descricao,
			SituacoesExistentes.PRC = ExistentesParam.PRC,
			SituacoesExistentes.dataModificado = getdate(),
			SituacoesExistentes.utilizadorModificado = ExistentesParam.UtilizadorEdicao
		from dbo.Utilizadores_Situacoes SituacoesExistentes
		inner join @exist as ExistentesParam on SituacoesExistentes.id = ExistentesParam.id

		insert into @result
		select Codigo, ID, 'U'
		from @exist

		--novos
		insert into dbo.Utilizadores_Situacoes (codigo, nome, descricao, PRC, ativo, dataCriado, utilizadorCriado)
		select n.Codigo, n.Nome, n.Descricao, n.PRC, 1, getdate(), n.UtilizadorEdicao
		from @new n

		insert into @result
		select Codigo, ID, 'I'
		from @new

		select * from @result

		commit
		--rollback
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	
END










GO
