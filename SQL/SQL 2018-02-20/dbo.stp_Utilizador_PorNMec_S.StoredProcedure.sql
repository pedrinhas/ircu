USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_PorNMec_S]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_PorNMec_S]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_PorNMec_S]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- alter date: <alter Date,,>
-- Description:	<Description,,>
-- =============================================

/*

stp_Utilizador_S '30BC8B13-1C3A-4942-8BE1-2035EBB3FE8E'
stp_Utilizador_S 'DC27A15B-0020-49FA-9B91-0ADE9B0B0F51'
[dbo].[stp_Utilizador_PorNMec_S] 'BO6367'
*/

CREATE procEDURE [dbo].[stp_Utilizador_PorNMec_S]
	@nmec nvarchar(10),
	@incluirApagados bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select	u.id, perfil.id as idPerfil, u.iupi, u.username as username, u.nome, u.nomePersonalizado, perfil.nmec, u.visibilidade, u.nif, u.contaNoGIAF, COALESCE(u.aliasedEmail, u.originalEmail) as email, u.originalEmail, u.aliasedEmail, perfil.visibilidade as visibilidadePerfil, perfil.dataEntrada, perfil.dataSaida,
			perfilTipo.id as idPerfilTipo, perfilTipo.codigo as perfilTipoCodigo, perfilTipo.nome as perfilTipo, perfilTipo.descricao as perfilTipoDescricao,
			--cargo.id as idCargo, cargoTipo.codigo as codCargo, cargoTipo.nome as cargo, cargoTipo.descricao as cargoDescricao, entidadeCargo.id as cargoIdEntidade, entidadeCargo.nome as cargoEntidade, entidadeCargo.descricao as cargoEntidadeDescricao,
			carreira.id as idCarreira, carreira.codigo as codCarreira, carreira.nome as carreira, carreira.descricao as carreiraDescricao,
			categoria.id as idCategoria, categoria.codigo as codCategoria, categoria.nome as categoria, categoria.descricao as categoriaDescricao,
			regime.id as idRegime, regime.codigo as codRegime, regime.nome as regime, regime.descricao as regimeDescricao,
			entidade.id as idEntidade, entidade.nome as entidade, entidade.descricao as entidadeDescricao,
			entidadeTipo.id as idEntidadeTipo, entidadeTipo.nome as entidadeTipo, entidadeTipo.descricao as entidadeTipoDescricao,
			u.dataCriado as utilizadorDataCriado, u.utilizadorCriado as utilizadorUtilizadorCriado, u.dataModificado as utilizadorDataModificado, u.utilizadorModificado as utilizadorUtilizadorModificado,
			perfil.dataCriado as perfilDataCriado, perfil.utilizadorCriado as perfilUtilizadorCriado, perfil.dataModificado as perfilDataModificado, perfil.utilizadorModificado as perfilUtilizadorModificado,
			perfil.ativo as perfilAtivo, u.ativo as utilizadorAtivo
	from [dbo].[Utilizadores] u
	left join [dbo].[Utilizadores_Perfil] perfil on u.id = perfil.idUtilizador
	left join [dbo].[Utilizadores_Perfil_Tipo] perfilTipo on perfilTipo.id = perfil.idTipo
	left join [dbo].[Utilizadores_Cargos] cargo on cargo.idPerfil = perfil.id
	left join [dbo].[Utilizadores_Cargos_Tipos] cargoTipo on cargoTipo.id = cargo.idTipoCargo
	left join [dbo].[Utilizadores_Carreiras] carreira on carreira.id = perfil.idCarreira
	left join [dbo].[Utilizadores_Categorias] categoria on categoria.id = perfil.idCategoria
	left join [dbo].Utilizadores_Regimes regime on perfil.idRegime = regime.id
	left join [dbo].[Entidades] entidade on entidade.id = perfil.idEntidade
	left join [dbo].[Entidades] entidadeCargo on entidade.id = cargo.idEntidade
	left join [dbo].[Entidades_Tipo] entidadeTipo on entidadeTipo.id = entidade.idTipoEntidade
	where perfil.nmec = @nmec AND
	(u.ativo = 1 or u.ativo <> @incluirApagados)
	--AND (perfil.ativo = 1 or perfil.ativo <> @incluirApagados)
	--AND (perfilTipo.ativo = 1 or perfilTipo.ativo <> @incluirApagados)
	--AND (carreira.ativo = 1 or carreira.ativo <> @incluirApagados)
	--AND (categoria.ativo = 1 or categoria.ativo <> @incluirApagados)
	--AND (entidade.ativo = 1 or entidade.ativo <> @incluirApagados)
	--AND (entidadeTipo.ativo = 1 or entidadeTipo.ativo <> @incluirApagados)
END









GO
