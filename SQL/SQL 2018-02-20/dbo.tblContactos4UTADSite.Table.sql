USE [RCU]
GO
/****** Object:  Table [dbo].[tblContactos4UTADSite]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[tblContactos4UTADSite]
GO
/****** Object:  Table [dbo].[tblContactos4UTADSite]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblContactos4UTADSite](
	[id] [int] NOT NULL,
	[username] [nvarchar](4000) NULL,
	[tipoContacto] [varchar](9) NOT NULL,
	[tipo] [nvarchar](50) NULL,
	[propertyval] [nvarchar](500) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
