USE [RCU]
GO
ALTER TABLE [dbo].[OLD_Acesso_NivelAcesso] DROP CONSTRAINT [FK_Acesso_NivelAcesso_Geral_Aplicacao]
GO
/****** Object:  Table [dbo].[OLD_Acesso_NivelAcesso]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[OLD_Acesso_NivelAcesso]
GO
/****** Object:  Table [dbo].[OLD_Acesso_NivelAcesso]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OLD_Acesso_NivelAcesso](
	[idNivelAcesso] [int] IDENTITY(1,1) NOT NULL,
	[idAplicacao] [int] NOT NULL,
	[NomeNivel] [nvarchar](50) NOT NULL,
	[ativo] [bit] NOT NULL CONSTRAINT [DF__Acesso_Ni__ativo__10CB707D]  DEFAULT ((1)),
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF__Acesso_Ni__dataC__308E3499]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF__Acesso_Ni__utili__51851410]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Acesso_NivelAcesso_1] PRIMARY KEY CLUSTERED 
(
	[idNivelAcesso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[OLD_Acesso_NivelAcesso]  WITH CHECK ADD  CONSTRAINT [FK_Acesso_NivelAcesso_Geral_Aplicacao] FOREIGN KEY([idAplicacao])
REFERENCES [dbo].[Aplicacoes] ([idAplicacao])
GO
ALTER TABLE [dbo].[OLD_Acesso_NivelAcesso] CHECK CONSTRAINT [FK_Acesso_NivelAcesso_Geral_Aplicacao]
GO
