USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Flags_S]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Flags_S]
GO
/****** Object:  StoredProcedure [dbo].[stp_Flags_S]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[stp_Flags_S]
	@id int
	, @incluirApagados bit = 0
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [id]
		  ,[key]
		  ,[descricao]
		  ,[dataCriado]
		  ,[ativo]
		  ,[utilizadorCriado]
		  ,[dataModificado]
		  ,[utilizadorModificado]
	  FROM [dbo].[Flags]
	  WHERE id = @id
	  AND (ativo = 1 or ativo <> @incluirApagados)
END









GO
