USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Entidades_Tipo_S]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Entidades_Tipo_S]
GO
/****** Object:  StoredProcedure [dbo].[stp_Entidades_Tipo_S]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Entidades_Tipo_S]
	@id int,
	@incluirApagados bit = 0
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [id]
		  ,[nome]
		  ,[descricao]
		  ,[ativo]
		  ,[dataCriado]
		  ,[utilizadorCriado]
		  ,[dataModificado]
		  ,[utilizadorModificado]
	  FROM [dbo].[Entidades_Tipo]
	  WHERE id = @id
	  AND (ativo = 1 or ativo <> @incluirApagados)
END








GO
