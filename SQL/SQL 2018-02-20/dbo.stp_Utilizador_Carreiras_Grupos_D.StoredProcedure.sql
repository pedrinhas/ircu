USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Carreiras_Grupos_D]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Carreiras_Grupos_D]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Carreiras_Grupos_D]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Utilizador_Carreiras_Grupos_D]
	@id int,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin tran
	begin try
		
		update [dbo].[Utilizadores_Cargos_Tipos]
		set ativo = 0, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
		where id = @id

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@id as idCarreiraGrupo
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	end









GO
