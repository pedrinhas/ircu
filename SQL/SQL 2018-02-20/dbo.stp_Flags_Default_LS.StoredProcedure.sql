USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Flags_Default_LS]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Flags_Default_LS]
GO
/****** Object:  StoredProcedure [dbo].[stp_Flags_Default_LS]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[stp_Flags_Default_LS]
	@incluirApagados bit = 0
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [id]
      ,[idFlag]
      ,[idTipoPerfil]
      ,[valorDefault]
      ,[ativo]
      ,[dataCriado]
      ,[utilizadorCriado]
      ,[dataModificado]
      ,[utilizadorModificado]
	  FROM [dbo].[Flags_Default]
	  WHERE (ativo = 1 or ativo <> @incluirApagados)
END










GO
