USE [RCU]
GO
/****** Object:  UserDefinedTableType [dbo].[CarreiraType]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TYPE [dbo].[CarreiraType]
GO
/****** Object:  UserDefinedTableType [dbo].[CarreiraType]    Script Date: 20/02/2018 16:56:54 PM ******/
CREATE TYPE [dbo].[CarreiraType] AS TABLE(
	[ID] [int] NULL,
	[Codigo] [nvarchar](50) NULL,
	[Nome] [nvarchar](50) NULL,
	[Descricao] [nvarchar](max) NULL,
	[IDGrupo] [int] NULL,
	[UtilizadorEdicao] [nvarchar](max) NOT NULL
)
GO
