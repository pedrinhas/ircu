USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Contactos_IU_2]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Contactos_IU_2]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Contactos_IU_2]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

-- stp_Utilizador_Contactos_IU '30BC8B13-1C3A-4942-8BE1-2035EBB3FE8E' , 0, '0'

CREATE procEDURE [dbo].[stp_Utilizador_Contactos_IU_2]
	  @iupi uniqueidentifier
	, @idContacto int
	, @idTipo int
	, @val nvarchar(255)
	, @visibilidade int
	, @utilizadorEdicao nvarchar(max)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @idUser int
	select @idUser = [id] 
	from [dbo].[Utilizadores] 
	where [iupi] = @iupi
	 
	if @idUser is not null
	begin

		begin tran
		begin try
			declare @exists bit
			set @exists = 0

			select @exists = coalesce(c.id, case when c.id is null then 0 when c.id is not null then 1 end) 
			from [dbo].[Utilizadores_Contactos] c
			left join [dbo].[Utilizadores] u on u.id = c.idUtilizador
			where @idContacto is not null and c.id = @idContacto

			if @exists = 1 and @idContacto is not null
			begin
				update [Utilizadores_Contactos]
				set idTipo = @idTipo, propertyval = @val, visibilidade = @visibilidade, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
				where id = @idContacto

				select @idContacto
			end
			else
				begin
				declare @propertyId int
				select @propertyId = count(*) from [Utilizadores_Contactos] where idUtilizador = @idUser

				insert into [dbo].[Utilizadores_Contactos] (idUtilizador, propertyId, idtipo, propertyval, visibilidade, ativo, dataCriado, utilizadorCriado)
				values (@idUser, @propertyId, @idTipo, @val, @visibilidade, 1, getdate(), @utilizadorEdicao)

				select max(id) from [Utilizadores_Contactos]
			end

			
			commit
		end try
		begin catch
			select ERROR_NUMBER() AS ErrorNumber
			,@iupi as IUPI
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
			rollback
		end catch
	end
END









GO
