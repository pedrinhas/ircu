USE [RCU]
GO
/****** Object:  UserDefinedTableType [dbo].[IDUtilizadorListType]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TYPE [dbo].[IDUtilizadorListType]
GO
/****** Object:  UserDefinedTableType [dbo].[IDUtilizadorListType]    Script Date: 20/02/2018 16:56:54 PM ******/
CREATE TYPE [dbo].[IDUtilizadorListType] AS TABLE(
	[IDUtilizador] [int] NULL,
	[UtilizadorEdicao] [nvarchar](max) NULL
)
GO
