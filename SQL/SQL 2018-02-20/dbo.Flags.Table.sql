USE [RCU]
GO
/****** Object:  Table [dbo].[Flags]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[Flags]
GO
/****** Object:  Table [dbo].[Flags]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Flags](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[key] [nvarchar](50) NOT NULL,
	[descricao] [nvarchar](max) NOT NULL,
	[ativo] [bit] NOT NULL CONSTRAINT [DF_Flags_ativo]  DEFAULT ((1)),
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF__Flag__52E34C9D]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF__Flag__4242D080]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Flags] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
