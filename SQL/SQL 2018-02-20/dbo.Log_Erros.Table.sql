USE [RCU]
GO
ALTER TABLE [dbo].[Log_Erros] DROP CONSTRAINT [DF__Log_Erros__utili__5832119F]
GO
ALTER TABLE [dbo].[Log_Erros] DROP CONSTRAINT [DF__Erros_Log__dataC__41B8C09B]
GO
/****** Object:  Table [dbo].[Log_Erros]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[Log_Erros]
GO
/****** Object:  Table [dbo].[Log_Erros]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log_Erros](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ip] [nvarchar](15) NULL,
	[url] [nvarchar](max) NULL,
	[statusHttp] [nvarchar](20) NULL,
	[requestHeaders] [nvarchar](max) NULL,
	[responseHeaders] [nvarchar](max) NULL,
	[exception] [nvarchar](max) NULL,
	[dataCriado] [datetime] NOT NULL,
	[utilizadorCriado] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Log_Erros] ADD  DEFAULT (getdate()) FOR [dataCriado]
GO
ALTER TABLE [dbo].[Log_Erros] ADD  DEFAULT (suser_sname()) FOR [utilizadorCriado]
GO
