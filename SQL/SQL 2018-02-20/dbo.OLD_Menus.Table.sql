USE [RCU]
GO
ALTER TABLE [dbo].[OLD_Menus] DROP CONSTRAINT [DF__Menus__utilizado__4EA8A765]
GO
ALTER TABLE [dbo].[OLD_Menus] DROP CONSTRAINT [DF__Menus__dataCriad__43A1090D]
GO
ALTER TABLE [dbo].[OLD_Menus] DROP CONSTRAINT [DF_Menus_Menu_Ativo]
GO
/****** Object:  Table [dbo].[OLD_Menus]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[OLD_Menus]
GO
/****** Object:  Table [dbo].[OLD_Menus]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OLD_Menus](
	[idMenu] [int] NOT NULL,
	[Texto] [nvarchar](max) NOT NULL,
	[Tooltip] [nvarchar](max) NOT NULL,
	[URL] [nvarchar](max) NOT NULL,
	[idAplicacao] [int] NOT NULL,
	[idMenuParent] [int] NULL,
	[Ativo] [bit] NOT NULL,
	[dataCriado] [datetime] NOT NULL,
	[utilizadorCriado] [nvarchar](max) NOT NULL,
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Menus_Menu] PRIMARY KEY CLUSTERED 
(
	[idMenu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[OLD_Menus] ADD  CONSTRAINT [DF_Menus_Menu_Ativo]  DEFAULT ((1)) FOR [Ativo]
GO
ALTER TABLE [dbo].[OLD_Menus] ADD  DEFAULT (getdate()) FOR [dataCriado]
GO
ALTER TABLE [dbo].[OLD_Menus] ADD  DEFAULT (suser_sname()) FOR [utilizadorCriado]
GO
