USE [RCU]
GO
/****** Object:  Table [dbo].[OLD_Utilizadores]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[OLD_Utilizadores]
GO
/****** Object:  Table [dbo].[OLD_Utilizadores]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OLD_Utilizadores](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[iupi] [uniqueidentifier] NULL,
	[nomedeutilizador] [nvarchar](50) NULL,
	[nome] [nvarchar](255) NULL,
	[nmec] [nvarchar](10) NULL,
	[email] [nvarchar](50) NULL,
	[nif] [nvarchar](20) NULL,
	[codUO] [nvarchar](10) NULL,
	[UO] [nvarchar](255) NULL,
	[codCategoria] [nvarchar](10) NULL,
	[categoria] [nvarchar](255) NULL,
	[codCarreira] [nvarchar](10) NULL,
	[carreira] [nvarchar](255) NULL,
	[ativo] [bit] NULL,
 CONSTRAINT [PK_t_utilizadores] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
