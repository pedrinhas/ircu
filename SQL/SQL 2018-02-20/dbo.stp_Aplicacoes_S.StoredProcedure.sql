USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Aplicacoes_S]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Aplicacoes_S]
GO
/****** Object:  StoredProcedure [dbo].[stp_Aplicacoes_S]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- exec [stp_Aplicacoes_Aplicacao_S] 4

CREATE procEDURE [dbo].[stp_Aplicacoes_S]
	@id int
AS
	SELECT a.*, g.idGrupo, g.nome as grupo
	from [dbo].[Aplicacoes] a
	left join [dbo].[Aplicacoes_AplicacaoGrupo] as ag on a.idAplicacao = ag.idAplicacao
	left join [dbo].[Aplicacoes_Grupo] as g on g.idGrupo = ag.idGrupo
	where a.idAplicacao = @id









GO
