USE [RCU]
GO
/****** Object:  UserDefinedTableType [dbo].[RegimeType]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TYPE [dbo].[RegimeType]
GO
/****** Object:  UserDefinedTableType [dbo].[RegimeType]    Script Date: 20/02/2018 16:56:54 PM ******/
CREATE TYPE [dbo].[RegimeType] AS TABLE(
	[ID] [int] NULL,
	[Codigo] [nvarchar](50) NULL,
	[Nome] [nvarchar](50) NULL,
	[Descricao] [nvarchar](max) NULL,
	[UtilizadorEdicao] [nvarchar](max) NOT NULL
)
GO
