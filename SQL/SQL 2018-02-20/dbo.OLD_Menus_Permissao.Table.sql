USE [RCU]
GO
ALTER TABLE [dbo].[OLD_Menus_Permissao] DROP CONSTRAINT [DF__Menus_Per__utili__4BCC3ABA]
GO
ALTER TABLE [dbo].[OLD_Menus_Permissao] DROP CONSTRAINT [DF__Menus_Per__dataC__477199F1]
GO
ALTER TABLE [dbo].[OLD_Menus_Permissao] DROP CONSTRAINT [DF__Menus_Per__ativo__0DEF03D2]
GO
/****** Object:  Table [dbo].[OLD_Menus_Permissao]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[OLD_Menus_Permissao]
GO
/****** Object:  Table [dbo].[OLD_Menus_Permissao]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OLD_Menus_Permissao](
	[idPermissaoMenu] [int] NOT NULL,
	[idMenu] [int] NOT NULL,
	[idNivelAcesso] [int] NOT NULL,
	[ativo] [bit] NOT NULL,
	[dataCriado] [datetime] NOT NULL,
	[utilizadorCriado] [nvarchar](max) NOT NULL,
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Menus_Permissao] PRIMARY KEY CLUSTERED 
(
	[idPermissaoMenu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[OLD_Menus_Permissao] ADD  CONSTRAINT [DF__Menus_Per__ativo__0DEF03D2]  DEFAULT ((1)) FOR [ativo]
GO
ALTER TABLE [dbo].[OLD_Menus_Permissao] ADD  CONSTRAINT [DF__Menus_Per__dataC__477199F1]  DEFAULT (getdate()) FOR [dataCriado]
GO
ALTER TABLE [dbo].[OLD_Menus_Permissao] ADD  CONSTRAINT [DF__Menus_Per__utili__4BCC3ABA]  DEFAULT (suser_sname()) FOR [utilizadorCriado]
GO
