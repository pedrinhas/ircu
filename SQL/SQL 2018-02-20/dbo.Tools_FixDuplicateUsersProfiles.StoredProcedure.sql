USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[Tools_FixDuplicateUsersProfiles]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[Tools_FixDuplicateUsersProfiles]
GO
/****** Object:  StoredProcedure [dbo].[Tools_FixDuplicateUsersProfiles]    Script Date: 20/02/2018 16:56:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Tools_FixDuplicateUsersProfiles]
as
begin try
	begin tran
		declare @idTMP TABLE (id int, iupi uniqueidentifier, oldID int)

		insert into @idTMP
		select n.id, n.iupi, u.id as oldID
		from Utilizadores_NEW n
		left join Utilizadores u on n.iupi = u.iupi
		where n.id <> u.id
		update Utilizadores_Perfil
		set idUtilizador = tmp.id
		from (select oldID, id from @idTMP) tmp
		where Utilizadores_Perfil.idUtilizador = tmp.oldID

		commit		
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch







GO
