USE [RCU]
GO
/****** Object:  Table [dbo].[tblCiencia]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[tblCiencia]
GO
/****** Object:  Table [dbo].[tblCiencia]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCiencia](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](50) NULL,
	[nmec] [nvarchar](50) NULL,
	[orcid] [nvarchar](50) NULL,
	[degois] [nvarchar](50) NULL,
 CONSTRAINT [PK_tblCiencia] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
