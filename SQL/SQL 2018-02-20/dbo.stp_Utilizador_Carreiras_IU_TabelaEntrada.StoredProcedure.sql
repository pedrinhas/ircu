USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Carreiras_IU_TabelaEntrada]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Carreiras_IU_TabelaEntrada]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Carreiras_IU_TabelaEntrada]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


create procEDURE [dbo].[stp_Utilizador_Carreiras_IU_TabelaEntrada]
	  @Carreiras [dbo].[CarreiraType] readonly
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exist [dbo].[CarreiraType],
				@new [dbo].[CarreiraType]
		declare @result table(Codigo nvarchar(20), ID int, Accao char)

		insert into @exist
		select * 
		from @Carreiras r
		where r.id is not null and r.id in (select id from Utilizadores_Carreiras)

		insert into @new
		select * 
		from @Carreiras r
		where r.id is null or r.id not in (select id from Utilizadores_Carreiras)

		--existentes
		update CarreirasExistentes
		set CarreirasExistentes.codigo = ExistentesParam.codigo,
			CarreirasExistentes.nome = ExistentesParam.nome,
			CarreirasExistentes.descricao = ExistentesParam.descricao,
			CarreirasExistentes.idGrupo = (case ExistentesParam.idGrupo when -1 then null else ExistentesParam.idGrupo end),
			CarreirasExistentes.dataModificado = getdate(),
			CarreirasExistentes.utilizadorModificado = ExistentesParam.UtilizadorEdicao
		from dbo.Utilizadores_Carreiras CarreirasExistentes
		inner join @exist as ExistentesParam on CarreirasExistentes.id = ExistentesParam.id

		insert into @result
		select Codigo, ID, 'U'
		from @exist

		--novos
		insert into dbo.Utilizadores_Carreiras (codigo, nome, descricao, idGrupo, ativo, dataCriado, utilizadorCriado)
		select n.Codigo, n.Nome, n.Descricao, n.IDGrupo, 1, getdate(), n.UtilizadorEdicao
		from @new n

		insert into @result
		select Codigo, ID, 'I'
		from @new

		select * from @result

		commit
		--rollback
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	
END










GO
