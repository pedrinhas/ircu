USE [RCU]
GO
ALTER TABLE [dbo].[Aplicacoes_Grupo] DROP CONSTRAINT [DF__Aplicacoe__utili__47FBA9D6]
GO
ALTER TABLE [dbo].[Aplicacoes_Grupo] DROP CONSTRAINT [DF__Aplicacoe__dataC__382F5661]
GO
ALTER TABLE [dbo].[Aplicacoes_Grupo] DROP CONSTRAINT [DF__Aplicacoe__ativo__0B129727]
GO
/****** Object:  Table [dbo].[Aplicacoes_Grupo]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[Aplicacoes_Grupo]
GO
/****** Object:  Table [dbo].[Aplicacoes_Grupo]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Aplicacoes_Grupo](
	[idGrupo] [int] IDENTITY(1,1) NOT NULL,
	[nome] [varchar](50) NOT NULL,
	[ativo] [bit] NOT NULL,
	[dataCriado] [datetime] NOT NULL,
	[utilizadorCriado] [nvarchar](max) NOT NULL,
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK__Geral_Gr__3214EC077FFB732F] PRIMARY KEY CLUSTERED 
(
	[idGrupo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Aplicacoes_Grupo] ADD  CONSTRAINT [DF__Aplicacoe__ativo__0B129727]  DEFAULT ((1)) FOR [ativo]
GO
ALTER TABLE [dbo].[Aplicacoes_Grupo] ADD  CONSTRAINT [DF__Aplicacoe__dataC__382F5661]  DEFAULT (getdate()) FOR [dataCriado]
GO
ALTER TABLE [dbo].[Aplicacoes_Grupo] ADD  CONSTRAINT [DF__Aplicacoe__utili__47FBA9D6]  DEFAULT (suser_sname()) FOR [utilizadorCriado]
GO
