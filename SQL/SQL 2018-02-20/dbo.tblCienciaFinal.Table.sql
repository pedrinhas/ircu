USE [RCU]
GO
/****** Object:  Table [dbo].[tblCienciaFinal]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[tblCienciaFinal]
GO
/****** Object:  Table [dbo].[tblCienciaFinal]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCienciaFinal](
	[id] [int] NOT NULL,
	[username] [nvarchar](50) NULL,
	[nmec] [nvarchar](50) NULL,
	[orcid] [nvarchar](50) NULL,
	[degois] [nvarchar](50) NULL
) ON [PRIMARY]

GO
