USE [RCU]
GO
/****** Object:  UserDefinedTableType [dbo].[EntidadeType]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TYPE [dbo].[EntidadeType]
GO
/****** Object:  UserDefinedTableType [dbo].[EntidadeType]    Script Date: 20/02/2018 16:56:54 PM ******/
CREATE TYPE [dbo].[EntidadeType] AS TABLE(
	[ID] [int] NULL,
	[Codigo] [nvarchar](50) NULL,
	[Sigla] [nvarchar](50) NULL,
	[Nome] [nvarchar](50) NULL,
	[Descricao] [nvarchar](max) NULL,
	[Email] [nvarchar](60) NULL,
	[UtilizadorEdicao] [nvarchar](max) NOT NULL
)
GO
