USE [RCU]
GO
/****** Object:  UserDefinedTableType [dbo].[VisibilidadeType]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TYPE [dbo].[VisibilidadeType]
GO
/****** Object:  UserDefinedTableType [dbo].[VisibilidadeType]    Script Date: 20/02/2018 16:56:54 PM ******/
CREATE TYPE [dbo].[VisibilidadeType] AS TABLE(
	[ID] [int] NULL,
	[Nome] [nvarchar](50) NULL,
	[Descricao] [nvarchar](255) NULL,
	[UtilizadorEdicao] [nvarchar](max) NOT NULL
)
GO
