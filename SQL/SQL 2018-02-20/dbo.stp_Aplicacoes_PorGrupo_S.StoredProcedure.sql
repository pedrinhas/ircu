USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Aplicacoes_PorGrupo_S]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Aplicacoes_PorGrupo_S]
GO
/****** Object:  StoredProcedure [dbo].[stp_Aplicacoes_PorGrupo_S]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Aplicacoes_PorGrupo_S]
	@nomeGrupo varchar(50)

AS
	SELECT a.Nome, a.Titulo, a.URL, a.Descricao
	from [dbo].[Aplicacoes_Grupo] as g
	left join [dbo].[Aplicacoes_AplicacaoGrupo] as ag on ag.idGrupo = g.idGrupo
	left join [dbo].[Aplicacoes_Aplicacao] as a on ag.idAplicacao = a.idAplicacao
	where g.nome=@nomeGrupo and a.Ativo=1









GO
