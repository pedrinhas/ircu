USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_D]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_D]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_D]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

/*

stp_Utilizador_U '30BC8B13-1C3A-4942-8BE1-2035EBB3FE8E', 'jborges', 'Borges', '345345'
stp_Utilizador_IU null, 'jborges', 'Borges', '345345'

*/

--APENAS CRIA O UTILIZADOR. TEM DE SE CHAMAR OUTRO SP PARA O PERFIL
CREATE procEDURE [dbo].[stp_Utilizador_D]
	  @iupi uniqueidentifier,
	  @utilizadorEdicao nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	begin tran
	begin try
		update [dbo].[Utilizadores]
		set ativo = 0, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
		where iupi = @iupi
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
			,@iupi as IUPI
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END








GO
