USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Cargos_Tipos_S]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Cargos_Tipos_S]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Cargos_Tipos_S]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Utilizador_Cargos_Tipos_S]
	@id int,
	@incluirApagados bit = 0
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [id]
      ,[codigo]
      ,[nome]
      ,[descricao]
      ,[ativo]
      ,[dataCriado]
      ,[utilizadorCriado]
      ,[dataModificado]
      ,[utilizadorModificado]
  FROM [dbo].[Utilizadores_Cargos_Tipos]
  where id = @id
	  and (ativo = 1 or ativo <> @incluirApagados)
end







GO
