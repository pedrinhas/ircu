USE [RCU]
GO
/****** Object:  Table [dbo].[BACKUP_Utilizadores_Flags]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[BACKUP_Utilizadores_Flags]
GO
/****** Object:  Table [dbo].[BACKUP_Utilizadores_Flags]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BACKUP_Utilizadores_Flags](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUtilizador] [int] NOT NULL,
	[idPerfil] [int] NULL,
	[key] [nvarchar](50) NOT NULL,
	[value] [nvarchar](5) NOT NULL
) ON [PRIMARY]

GO
