USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Flags_Default_R]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Flags_Default_R]
GO
/****** Object:  StoredProcedure [dbo].[stp_Flags_Default_R]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procEDURE [dbo].[stp_Flags_Default_R]
	@id int,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
	begin try
	begin tran
	
		SET NOCOUNT ON;

		UPDATE [dbo].[Flags_Default]
		SET Ativo = 1, utilizadorModificado = @utilizadorEdicao, dataModificado = getdate()
		WHERE id = @id

		commit

    end try
	begin catch
		rollback
	end catch
END


GO
