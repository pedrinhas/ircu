USE [RCU]
GO
/****** Object:  UserDefinedTableType [dbo].[CarreiraGrupoType]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TYPE [dbo].[CarreiraGrupoType]
GO
/****** Object:  UserDefinedTableType [dbo].[CarreiraGrupoType]    Script Date: 20/02/2018 16:56:54 PM ******/
CREATE TYPE [dbo].[CarreiraGrupoType] AS TABLE(
	[ID] [int] NULL,
	[Codigo] [nvarchar](50) NULL,
	[Nome] [nvarchar](50) NULL,
	[Descricao] [nvarchar](max) NULL,
	[UtilizadorEdicao] [nvarchar](max) NOT NULL
)
GO
