USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Contactos_R]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Contactos_R]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Contactos_R]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [stp_Utilizador_Contactos_R] '30BC8B13-1C3A-4942-8BE1-2035EBB3FE8E'


CREATE proc [dbo].[stp_Utilizador_Contactos_R]
	  @iupi uniqueidentifier
	, @idContacto int
	, @utilizadorEdicao nvarchar(max)
	as
	SET NOCOUNT ON;
	begin tran
	begin try
		update [dbo].[Utilizadores_Contactos] 
		set ativo = 1, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
		from (SELECT id from [dbo].[Utilizadores] WHERE iupi = @iupi) u
		where idUtilizador = u.id and [Utilizadores_Contactos].id = @idContacto
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
			,@iupi as IUPI
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch







GO
