USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Categorias_IU_TabelaEntrada]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Categorias_IU_TabelaEntrada]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Categorias_IU_TabelaEntrada]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


CREATE procEDURE [dbo].[stp_Utilizador_Categorias_IU_TabelaEntrada]
	  @categorias [dbo].[CategoriaType] readonly
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exist [dbo].[CategoriaType],
				@new [dbo].[CategoriaType]
		declare @result table(Codigo nvarchar(20), ID int, Accao char)

		insert into @exist
		select * 
		from @categorias r
		where r.id is not null and r.id in (select id from Utilizadores_Categorias)

		insert into @new
		select * 
		from @categorias r
		where r.id is null or r.id not in (select id from Utilizadores_Categorias)

		--existentes
		update CategoriasExistentes
		set CategoriasExistentes.codigo = ExistentesParam.codigo,
			CategoriasExistentes.nome = ExistentesParam.nome,
			CategoriasExistentes.descricao = ExistentesParam.descricao,
			CategoriasExistentes.idCarreira = carr.id,
			CategoriasExistentes.dataModificado = getdate(),
			CategoriasExistentes.utilizadorModificado = ExistentesParam.UtilizadorEdicao
		from dbo.Utilizadores_Categorias CategoriasExistentes
		inner join @exist as ExistentesParam on CategoriasExistentes.id = ExistentesParam.id
		left join [dbo].[Utilizadores_Carreiras] carr on ExistentesParam.CodCarreira = carr.codigo

		insert into @result
		select Codigo, ID, 'U'
		from @exist

		--novos
		insert into dbo.Utilizadores_Categorias (codigo, nome, descricao, idCarreira, ativo, dataCriado, utilizadorCriado)
		select n.Codigo, n.Nome, n.Descricao, carr.id, 1, getdate(), n.UtilizadorEdicao
		from @new n
		left join [dbo].[Utilizadores_Carreiras] carr on n.CodCarreira = carr.codigo

		insert into @result
		select Codigo, ID, 'I'
		from @new

		select * from @result

		commit
		--rollback
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	
END










GO
