USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Flags_Default_SET_TabelaEntrada]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_Flags_Default_SET_TabelaEntrada]
GO
/****** Object:  StoredProcedure [dbo].[stp_Flags_Default_SET_TabelaEntrada]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
declare @p1 IDUtilizadorListType

insert into @p1 
select id, SYSTEM_USER
from utilizadores

exec [stp_Flags_Default_SET_TabelaEntrada] @p1

*/


CREATE procedure [dbo].[stp_Flags_Default_SET_TabelaEntrada]
	@idUtilizadores as IDUtilizadorListType readonly
AS
BEGIN
	SET NOCOUNT ON;
	begin try
	begin tran
		
		--Flags de perfil
		insert into [dbo].[Utilizadores_Flags] (idUtilizador, idPerfil, idFlag, value, ativo, dataCriado, utilizadorCriado)
		select perfil.idUtilizador, perfil.id as idPerfil, idFlag as idFlagParaPerfis, defaults.valorDefault, 1 as ativo, getdate() as dataCriado, users.UtilizadorEdicao
		from [dbo].[Utilizadores_Perfil] perfil
		left join [dbo].[Flags_Default] defaults on perfil.idTipo = defaults.idTipoPerfil
		inner join @idUtilizadores users on users.IDUtilizador = perfil.idUtilizador
		where not exists (
			select *
			from [dbo].[Utilizadores_Flags] f
			where f.idPerfil = perfil.id
			and f.idFlag = defaults.idFlag
	
		)
		and perfil.idTipo is not null
		and defaults.idTipoPerfil is not null

		--Flags de utilizador
		insert into [dbo].[Utilizadores_Flags] (idUtilizador, idPerfil, idFlag, value, ativo, dataCriado, utilizadorCriado)
		select u.id, null as idPerfil, idFlag as idFlagParaUsers, defaults.valorDefault, 1 as ativo, getdate() as dataCriado, users.UtilizadorEdicao
		from [dbo].[Utilizadores] u
		left join [dbo].[Flags_Default] defaults on defaults.idTipoPerfil is null
		inner join @idUtilizadores users on users.IDUtilizador = u.id
		where not exists (
			select *
			from [dbo].[Utilizadores_Flags] f
			where (f.idPerfil is null and defaults.idTipoPerfil is null)
			and f.idFlag = defaults.idFlag
	
		)
		and defaults.idTipoPerfil is null


		commit
		--rollback
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END





--select * from utilizadores_perfil where idUtilizador = 2130

--select * from Utilizadores_Perfil_Tipo



GO
