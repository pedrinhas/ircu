USE [RCU]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] DROP CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Visibilidade]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] DROP CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Situacoes]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] DROP CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Regimes]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] DROP CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Perfil_Tipo]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] DROP CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Perfil]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] DROP CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Categorias]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] DROP CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Carreiras]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] DROP CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] DROP CONSTRAINT [FK_Utilizadores_Perfil_Entidades]
GO
/****** Object:  Index [IX_Utilizadores_Perfil_uniqueNMec]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP INDEX [IX_Utilizadores_Perfil_uniqueNMec] ON [dbo].[Utilizadores_Perfil]
GO
/****** Object:  Table [dbo].[Utilizadores_Perfil]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP TABLE [dbo].[Utilizadores_Perfil]
GO
/****** Object:  Table [dbo].[Utilizadores_Perfil]    Script Date: 20/02/2018 16:56:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores_Perfil](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUtilizador] [int] NOT NULL,
	[nmec] [nvarchar](10) NULL,
	[visibilidade] [int] NOT NULL CONSTRAINT [DF_Utilizadores_Perfil_nivelAcesso]  DEFAULT ((3)),
	[idTipo] [int] NULL,
	[idCategoria] [int] NULL,
	[idEntidade] [int] NULL,
	[idCarreira] [int] NULL,
	[idSituacao] [int] NULL,
	[idRegime] [int] NULL,
	[dataEntrada] [datetime] NULL,
	[dataSaida] [datetime] NULL,
	[ativo] [bit] NOT NULL,
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF__Utilizado__dataC__65F62111]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF__Utilizado__utili__592635D8]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Utilizadores_Perfil] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Utilizadores_Perfil_uniqueNMec]    Script Date: 20/02/2018 16:56:54 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Utilizadores_Perfil_uniqueNMec] ON [dbo].[Utilizadores_Perfil]
(
	[nmec] ASC
)
WHERE ([nmec] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Perfil_Entidades] FOREIGN KEY([idEntidade])
REFERENCES [dbo].[Entidades] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] CHECK CONSTRAINT [FK_Utilizadores_Perfil_Entidades]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores] FOREIGN KEY([idUtilizador])
REFERENCES [dbo].[Utilizadores] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] CHECK CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Carreiras] FOREIGN KEY([idCarreira])
REFERENCES [dbo].[Utilizadores_Carreiras] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] CHECK CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Carreiras]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Categorias] FOREIGN KEY([idCategoria])
REFERENCES [dbo].[Utilizadores_Categorias] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] CHECK CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Categorias]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Perfil] FOREIGN KEY([id])
REFERENCES [dbo].[Utilizadores_Perfil] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] CHECK CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Perfil]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Perfil_Tipo] FOREIGN KEY([idTipo])
REFERENCES [dbo].[Utilizadores_Perfil_Tipo] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] CHECK CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Perfil_Tipo]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Regimes] FOREIGN KEY([idRegime])
REFERENCES [dbo].[Utilizadores_Regimes] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] CHECK CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Regimes]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Situacoes] FOREIGN KEY([idSituacao])
REFERENCES [dbo].[Utilizadores_Situacoes] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] CHECK CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Situacoes]
GO
ALTER TABLE [dbo].[Utilizadores_Perfil]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Visibilidade] FOREIGN KEY([visibilidade])
REFERENCES [dbo].[Utilizadores_Visibilidade] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Perfil] CHECK CONSTRAINT [FK_Utilizadores_Perfil_Utilizadores_Visibilidade]
GO
