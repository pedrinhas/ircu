USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_WebsitePerfil_Contactos_Tipos_LS]    Script Date: 20/02/2018 16:56:54 PM ******/
DROP PROCEDURE [dbo].[stp_WebsitePerfil_Contactos_Tipos_LS]
GO
/****** Object:  StoredProcedure [dbo].[stp_WebsitePerfil_Contactos_Tipos_LS]    Script Date: 20/02/2018 16:56:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

-- [stp_Contactos_Tipos_LS] 1


CREATE procEDURE [dbo].[stp_WebsitePerfil_Contactos_Tipos_LS]
	@incluirApagados bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT t.id, t.tipo, g.id as idGrupo, g.grupo, t.ativo as tipoAtivo, g.ativo as grupoAtivo
	FROM [dbo].[Utilizadores_Contactos_Tipos] t
	left join [dbo].[Utilizadores_Contactos_Tipos_Grupos] g on t.idGrupo = g.id
	where (t.ativo = 1 or t.ativo <> @incluirApagados)
	and (g.ativo = 1 or g.ativo <> @incluirApagados)
	and t.id in ( 3, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16 )
END








GO
