USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_D]    Script Date: 29/05/2018 18:38:15 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_D]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_D]    Script Date: 29/05/2018 18:38:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Utilizador_Ficheiros_D]
	@id int,
	@utilizadorEdicao varchar(max)
AS
BEGIN
	begin try
	begin tran
	
		SET NOCOUNT ON;

		UPDATE [dbo].[Utilizadores_Ficheiros]
		SET Ativo = 0, utilizadorModificado = @utilizadorEdicao, dataModificado = getdate()
		WHERE id = @id

		commit

    end try
	begin catch
		rollback
	end catch

END











GO
