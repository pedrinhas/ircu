USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_IU]    Script Date: 29/05/2018 18:38:15 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_IU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_IU]    Script Date: 29/05/2018 18:38:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- exec [stp_Entidade_IU] 4

CREATE procEDURE [dbo].[stp_Utilizador_Ficheiros_IU]
	@id int,
	@idUtilizador int,
	@idPerfil int,
	@sharepointURL nvarchar(max),
	@sharepointList nvarchar(max),
	@sharepointFolder nvarchar(max),
	@filename nvarchar(64),
	@idTipo int,
	@ativo bit = 1,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exists bit
		set @exists = 0

		select @exists = coalesce(a.[id], case when a.[id] is null then 0 when a.[id] is not null then 1 end) 
		from [dbo].[Utilizadores_Ficheiros] a--
		where a.[id] = @id

		if @exists = 1 and @id is not null
		begin
			update [dbo].[Utilizadores_Ficheiros]
			set idUtilizador = @idUtilizador, idPerfil = @idPerfil, sharepointURL = @sharepointURL, sharepointList = @sharepointList, sharepointFolder = @sharepointFolder, filename = @filename, idTipo = @idTipo, ativo = @ativo, utilizadorModificado = @utilizadorEdicao
			where id = @id

			select @id
		end
		else
		begin

			insert into [dbo].[Utilizadores_Ficheiros] (idUtilizador, idPerfil, sharepointURL, sharepointList, sharepointFolder, filename, idTipo, ativo, dataCriado, utilizadorCriado)
			values (@idUtilizador, @idPerfil, @sharepointURL, @sharepointList, @sharepointFolder, @filename, @idTipo, 1, getdate(), @utilizadorEdicao)

			select max(id) from [dbo].[Utilizadores_Ficheiros]
		end

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@id as idFicheiro
		,@idUtilizador as idUtilizador
		,@idPerfil as idPerfil
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END














GO
