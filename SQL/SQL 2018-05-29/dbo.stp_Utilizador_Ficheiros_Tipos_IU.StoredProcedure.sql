USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_Tipos_IU]    Script Date: 29/05/2018 18:38:15 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_Tipos_IU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_Tipos_IU]    Script Date: 29/05/2018 18:38:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Utilizador_Ficheiros_Tipos_IU]
	@idTipo int,
	@nome varchar(50),
	@descricao varchar(max) = null,
	@ativo bit = 1,
	@utilizadorEdicao varchar(max)
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin tran
	begin try
		declare @exists bit
		set @exists = 0

		select @exists = coalesce(c.id, case when c.id is null then 0 when c.id is not null then 1 end) 
		from [dbo].[Utilizadores_Ficheiros_Tipos] c
		where @idTipo is not null and c.id = @idTipo

		if @exists = 1 and @idTipo is not null
		begin
			update [dbo].[Utilizadores_Ficheiros_Tipos]
			set nome = @nome, descricao = @descricao, dataModificado = getdate(), ativo = @ativo, utilizadorModificado = @utilizadorEdicao
			where id = @idTipo

			select @idTipo
		end
		else
		begin
			insert into [dbo].[Utilizadores_Ficheiros_Tipos] (nome, descricao, ativo, dataCriado, utilizadorCriado)
			values (@nome, @descricao, 1, getdate(), @utilizadorEdicao)

			select max(id) from [Utilizadores_Ficheiros_Tipos]
		end

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@idTipo as idTipo
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	end













GO
