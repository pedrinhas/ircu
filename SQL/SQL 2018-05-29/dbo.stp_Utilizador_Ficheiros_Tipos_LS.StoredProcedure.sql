USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_Tipos_LS]    Script Date: 29/05/2018 18:38:15 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_Tipos_LS]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_Tipos_LS]    Script Date: 29/05/2018 18:38:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Utilizador_Ficheiros_Tipos_LS]
	@incluirApagados bit = 0
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [id]
      ,[nome]
      ,[descricao]
      ,[ativo]
      ,[dataCriado]
      ,[utilizadorCriado]
      ,[dataModificado]
      ,[utilizadorModificado]
  FROM [dbo].[Utilizadores_Ficheiros_Tipos]
	  WHERE (ativo = 1 or ativo <> @incluirApagados)
end










GO
