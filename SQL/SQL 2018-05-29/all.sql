USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_Tipos_S]    Script Date: 29/05/2018 18:37:59 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_Tipos_S]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_Tipos_R]    Script Date: 29/05/2018 18:37:59 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_Tipos_R]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_Tipos_LS]    Script Date: 29/05/2018 18:37:59 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_Tipos_LS]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_Tipos_IU]    Script Date: 29/05/2018 18:37:59 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_Tipos_IU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_Tipos_D]    Script Date: 29/05/2018 18:37:59 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_Tipos_D]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_S]    Script Date: 29/05/2018 18:37:59 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_S]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_R]    Script Date: 29/05/2018 18:37:59 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_R]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_PorUtilizador_S]    Script Date: 29/05/2018 18:37:59 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_PorUtilizador_S]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_PorTipo_S]    Script Date: 29/05/2018 18:37:59 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_PorTipo_S]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_PorPerfil_S]    Script Date: 29/05/2018 18:37:59 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_PorPerfil_S]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_LS]    Script Date: 29/05/2018 18:37:59 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_LS]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_IU]    Script Date: 29/05/2018 18:37:59 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_IU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_D]    Script Date: 29/05/2018 18:37:59 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_D]
GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros] DROP CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores_Perfil]
GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros] DROP CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores_Ficheiros_Tipos]
GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros] DROP CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores]
GO
/****** Object:  Table [dbo].[Utilizadores_Ficheiros_Tipos]    Script Date: 29/05/2018 18:37:59 PM ******/
DROP TABLE [dbo].[Utilizadores_Ficheiros_Tipos]
GO
/****** Object:  Table [dbo].[Utilizadores_Ficheiros]    Script Date: 29/05/2018 18:37:59 PM ******/
DROP TABLE [dbo].[Utilizadores_Ficheiros]
GO
/****** Object:  Table [dbo].[Utilizadores_Ficheiros]    Script Date: 29/05/2018 18:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Utilizadores_Ficheiros](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUtilizador] [int] NOT NULL,
	[idPerfil] [int] NULL,
	[sharepointURL] [varchar](max) NOT NULL,
	[sharepointList] [varchar](max) NOT NULL,
	[sharepointFolder] [varchar](max) NOT NULL,
	[filename] [varchar](64) NOT NULL,
	[idTipo] [int] NOT NULL,
	[ativo] [bit] NOT NULL CONSTRAINT [DF_Utilizadores_Ficheiros_ativo]  DEFAULT ((1)),
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF_Utilizadores_Ficheiros_dataCriado]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF_Utilizadores_Ficheiros_utilizadorCriado]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Utilizadores_Ficheiros] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Utilizadores_Ficheiros_Tipos]    Script Date: 29/05/2018 18:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores_Ficheiros_Tipos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nome] [nvarchar](max) NOT NULL,
	[descricao] [nvarchar](max) NULL,
	[ativo] [bit] NOT NULL CONSTRAINT [DF_Utilizadores_Ficheiros_Tipos_ativo]  DEFAULT ((1)),
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF_Utilizadores_Ficheiros_Tipos_dataCriado]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF_Utilizadores_Ficheiros_Tipos_utilizadorCriado]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Utilizadores_Ficheiros_Tipos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores] FOREIGN KEY([idUtilizador])
REFERENCES [dbo].[Utilizadores] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros] CHECK CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores]
GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores_Ficheiros_Tipos] FOREIGN KEY([idTipo])
REFERENCES [dbo].[Utilizadores_Ficheiros_Tipos] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros] CHECK CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores_Ficheiros_Tipos]
GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores_Perfil] FOREIGN KEY([idPerfil])
REFERENCES [dbo].[Utilizadores_Perfil] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros] CHECK CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores_Perfil]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_D]    Script Date: 29/05/2018 18:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Utilizador_Ficheiros_D]
	@id int,
	@utilizadorEdicao varchar(max)
AS
BEGIN
	begin try
	begin tran
	
		SET NOCOUNT ON;

		UPDATE [dbo].[Utilizadores_Ficheiros]
		SET Ativo = 0, utilizadorModificado = @utilizadorEdicao, dataModificado = getdate()
		WHERE id = @id

		commit

    end try
	begin catch
		rollback
	end catch

END











GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_IU]    Script Date: 29/05/2018 18:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- exec [stp_Entidade_IU] 4

CREATE procEDURE [dbo].[stp_Utilizador_Ficheiros_IU]
	@id int,
	@idUtilizador int,
	@idPerfil int,
	@sharepointURL nvarchar(max),
	@sharepointList nvarchar(max),
	@sharepointFolder nvarchar(max),
	@filename nvarchar(64),
	@idTipo int,
	@ativo bit = 1,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exists bit
		set @exists = 0

		select @exists = coalesce(a.[id], case when a.[id] is null then 0 when a.[id] is not null then 1 end) 
		from [dbo].[Utilizadores_Ficheiros] a--
		where a.[id] = @id

		if @exists = 1 and @id is not null
		begin
			update [dbo].[Utilizadores_Ficheiros]
			set idUtilizador = @idUtilizador, idPerfil = @idPerfil, sharepointURL = @sharepointURL, sharepointList = @sharepointList, sharepointFolder = @sharepointFolder, filename = @filename, idTipo = @idTipo, ativo = @ativo, utilizadorModificado = @utilizadorEdicao
			where id = @id

			select @id
		end
		else
		begin

			insert into [dbo].[Utilizadores_Ficheiros] (idUtilizador, idPerfil, sharepointURL, sharepointList, sharepointFolder, filename, idTipo, ativo, dataCriado, utilizadorCriado)
			values (@idUtilizador, @idPerfil, @sharepointURL, @sharepointList, @sharepointFolder, @filename, @idTipo, 1, getdate(), @utilizadorEdicao)

			select max(id) from [dbo].[Utilizadores_Ficheiros]
		end

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@id as idFicheiro
		,@idUtilizador as idUtilizador
		,@idPerfil as idPerfil
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END














GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_LS]    Script Date: 29/05/2018 18:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Utilizador_Ficheiros_LS]
	@incluirApagados bit = 0
AS
BEGIN
	SET NOCOUNT ON;

select uf.[id]
	  ,uf.[idUtilizador]
	  ,uf.[idPerfil]
	  ,uf.[sharepointURL]
	  ,uf.[sharepointList]
	  ,uf.[sharepointFolder]
	  ,uf.[filename]
	  ,uf.[idTipo]
	  ,uft.[nome] as tipoFicheiro
	  ,uf.ativo as ficheiroAtivo
	  ,uft.[ativo] as tipoAtivo
	  ,uf.[dataCriado]
	  ,uf.[utilizadorCriado]
	  ,uf.[dataModificado]
	  ,uf.[utilizadorModificado]
  FROM [dbo].[Utilizadores_Ficheiros] uf
  left join [dbo].[Utilizadores_Ficheiros_Tipos] uft on uft.id = uf.idTipo
  where (uf.ativo = 1 or uf.ativo<> @incluirApagados)
END












GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_PorPerfil_S]    Script Date: 29/05/2018 18:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procEDURE [dbo].[stp_Utilizador_Ficheiros_PorPerfil_S]
	@idPerfil int,
	@incluirApagados bit = 0
AS
BEGIN
	SET NOCOUNT ON;

select uf.[id]
	  ,uf.[idUtilizador]
	  ,uf.[idPerfil]
	  ,uf.[sharepointURL]
	  ,uf.[sharepointList]
	  ,uf.[sharepointFolder]
	  ,uf.[filename]
	  ,uf.[idTipo]
	  ,uft.[nome] as tipoFicheiro
	  ,uft.[ativo]
	  ,uf.[dataCriado]
	  ,uf.[utilizadorCriado]
	  ,uf.[dataModificado]
	  ,uf.[utilizadorModificado]
  FROM [dbo].[Utilizadores_Ficheiros] uf
  left join [dbo].[Utilizadores_Ficheiros_Tipos] uft on uft.id = uf.idTipo
	  WHERE uf.idPerfil = @idPerfil
	  AND (uf.ativo = 1 or uf.ativo<> @incluirApagados)
END












GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_PorTipo_S]    Script Date: 29/05/2018 18:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procEDURE [dbo].[stp_Utilizador_Ficheiros_PorTipo_S]
	@idTipo int,
	@incluirApagados bit = 0
AS
BEGIN
	SET NOCOUNT ON;

select uf.[id]
	  ,uf.[idUtilizador]
	  ,uf.[idPerfil]
	  ,uf.[sharepointURL]
	  ,uf.[sharepointList]
	  ,uf.[sharepointFolder]
	  ,uf.[filename]
	  ,uf.[idTipo]
	  ,uft.[nome] as tipoFicheiro
	  ,uft.[ativo]
	  ,uf.[dataCriado]
	  ,uf.[utilizadorCriado]
	  ,uf.[dataModificado]
	  ,uf.[utilizadorModificado]
  FROM [dbo].[Utilizadores_Ficheiros] uf
  left join [dbo].[Utilizadores_Ficheiros_Tipos] uft on uft.id = uf.idTipo
	  WHERE uf.idTipo = @idTipo
	  AND (uf.ativo = 1 or uf.ativo<> @incluirApagados)
END












GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_PorUtilizador_S]    Script Date: 29/05/2018 18:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procEDURE [dbo].[stp_Utilizador_Ficheiros_PorUtilizador_S]
	@idUtilizador int,
	@incluirApagados bit = 0
AS
BEGIN
	SET NOCOUNT ON;

select uf.[id]
	  ,uf.[idUtilizador]
	  ,uf.[idPerfil]
	  ,uf.[sharepointURL]
	  ,uf.[sharepointList]
	  ,uf.[sharepointFolder]
	  ,uf.[filename]
	  ,uf.[idTipo]
	  ,uft.[nome] as tipoFicheiro
	  ,uft.[ativo]
	  ,uf.[dataCriado]
	  ,uf.[utilizadorCriado]
	  ,uf.[dataModificado]
	  ,uf.[utilizadorModificado]
  FROM [dbo].[Utilizadores_Ficheiros] uf
  left join [dbo].[Utilizadores_Ficheiros_Tipos] uft on uft.id = uf.idTipo
	  WHERE uf.idUtilizador = @idUtilizador
	  AND (uf.ativo = 1 or uf.ativo<> @incluirApagados)
END












GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_R]    Script Date: 29/05/2018 18:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Utilizador_Ficheiros_R]
	@id int,
	@utilizadorEdicao varchar(max)
AS
BEGIN
	begin try
	begin tran
	
		SET NOCOUNT ON;

		UPDATE [dbo].[Utilizadores_Ficheiros]
		SET Ativo = 1, utilizadorModificado = @utilizadorEdicao, dataModificado = getdate()
		WHERE id = @id

		commit

    end try
	begin catch
		rollback
	end catch

END











GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_S]    Script Date: 29/05/2018 18:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Utilizador_Ficheiros_S]
	@id int,
	@incluirApagados bit = 0
AS
BEGIN
	SET NOCOUNT ON;

select uf.[id]
	  ,uf.[idUtilizador]
	  ,uf.[idPerfil]
	  ,uf.[sharepointURL]
	  ,uf.[sharepointList]
	  ,uf.[sharepointFolder]
	  ,uf.[filename]
	  ,uf.[idTipo]
	  ,uft.[nome] as tipoFicheiro
	  ,uf.ativo as ficheiroAtivo
	  ,uft.[ativo] as tipoAtivo
	  ,uf.[dataCriado]
	  ,uf.[utilizadorCriado]
	  ,uf.[dataModificado]
	  ,uf.[utilizadorModificado]
  FROM [dbo].[Utilizadores_Ficheiros] uf
  left join [dbo].[Utilizadores_Ficheiros_Tipos] uft on uft.id = uf.idTipo
	  WHERE uf.id = @id
	  AND (uf.ativo = 1 or uf.ativo<> @incluirApagados)
END












GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_Tipos_D]    Script Date: 29/05/2018 18:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Utilizador_Ficheiros_Tipos_D]
	@idTipo int,
	@utilizadorEdicao varchar(max)
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin tran
	begin try
		
		update [dbo].[Utilizadores_Ficheiros_Tipos]
		set ativo = 0, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
		where id = @idTipo

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@idTipo as idTipo
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	end











GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_Tipos_IU]    Script Date: 29/05/2018 18:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Utilizador_Ficheiros_Tipos_IU]
	@idTipo int,
	@nome varchar(50),
	@descricao varchar(max) = null,
	@ativo bit = 1,
	@utilizadorEdicao varchar(max)
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin tran
	begin try
		declare @exists bit
		set @exists = 0

		select @exists = coalesce(c.id, case when c.id is null then 0 when c.id is not null then 1 end) 
		from [dbo].[Utilizadores_Ficheiros_Tipos] c
		where @idTipo is not null and c.id = @idTipo

		if @exists = 1 and @idTipo is not null
		begin
			update [dbo].[Utilizadores_Ficheiros_Tipos]
			set nome = @nome, descricao = @descricao, dataModificado = getdate(), ativo = @ativo, utilizadorModificado = @utilizadorEdicao
			where id = @idTipo

			select @idTipo
		end
		else
		begin
			insert into [dbo].[Utilizadores_Ficheiros_Tipos] (nome, descricao, ativo, dataCriado, utilizadorCriado)
			values (@nome, @descricao, 1, getdate(), @utilizadorEdicao)

			select max(id) from [Utilizadores_Ficheiros_Tipos]
		end

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@idTipo as idTipo
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	end













GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_Tipos_LS]    Script Date: 29/05/2018 18:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Utilizador_Ficheiros_Tipos_LS]
	@incluirApagados bit = 0
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [id]
      ,[nome]
      ,[descricao]
      ,[ativo]
      ,[dataCriado]
      ,[utilizadorCriado]
      ,[dataModificado]
      ,[utilizadorModificado]
  FROM [dbo].[Utilizadores_Ficheiros_Tipos]
	  WHERE (ativo = 1 or ativo <> @incluirApagados)
end










GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_Tipos_R]    Script Date: 29/05/2018 18:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Utilizador_Ficheiros_Tipos_R]
	@idTipo int,
	@utilizadorEdicao varchar(max)
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin tran
	begin try
		
		update [dbo].[Utilizadores_Ficheiros_Tipos]
		set ativo = 1, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
		where id = @idTipo

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@idTipo as idTipo
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	end











GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_Tipos_S]    Script Date: 29/05/2018 18:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Utilizador_Ficheiros_Tipos_S]
	@id int,
	@incluirApagados bit = 0
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [id]
      ,[nome]
      ,[descricao]
      ,[ativo]
      ,[dataCriado]
      ,[utilizadorCriado]
      ,[dataModificado]
      ,[utilizadorModificado]
  FROM [dbo].[Utilizadores_Ficheiros_Tipos]
  where id = @id
	  and (ativo = 1 or ativo <> @incluirApagados)
end










GO
