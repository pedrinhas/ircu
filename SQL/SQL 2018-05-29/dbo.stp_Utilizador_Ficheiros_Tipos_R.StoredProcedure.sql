USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_Tipos_R]    Script Date: 29/05/2018 18:38:15 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_Tipos_R]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_Tipos_R]    Script Date: 29/05/2018 18:38:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Utilizador_Ficheiros_Tipos_R]
	@idTipo int,
	@utilizadorEdicao varchar(max)
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin tran
	begin try
		
		update [dbo].[Utilizadores_Ficheiros_Tipos]
		set ativo = 1, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
		where id = @idTipo

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@idTipo as idTipo
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	end











GO
