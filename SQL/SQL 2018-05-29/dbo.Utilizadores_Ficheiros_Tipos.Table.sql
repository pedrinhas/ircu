USE [RCU]
GO
/****** Object:  Table [dbo].[Utilizadores_Ficheiros_Tipos]    Script Date: 29/05/2018 18:38:15 PM ******/
DROP TABLE [dbo].[Utilizadores_Ficheiros_Tipos]
GO
/****** Object:  Table [dbo].[Utilizadores_Ficheiros_Tipos]    Script Date: 29/05/2018 18:38:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores_Ficheiros_Tipos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nome] [nvarchar](max) NOT NULL,
	[descricao] [nvarchar](max) NULL,
	[ativo] [bit] NOT NULL CONSTRAINT [DF_Utilizadores_Ficheiros_Tipos_ativo]  DEFAULT ((1)),
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF_Utilizadores_Ficheiros_Tipos_dataCriado]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF_Utilizadores_Ficheiros_Tipos_utilizadorCriado]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Utilizadores_Ficheiros_Tipos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
