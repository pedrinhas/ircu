USE [RCU]
GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros] DROP CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores_Perfil]
GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros] DROP CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores_Ficheiros_Tipos]
GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros] DROP CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores]
GO
/****** Object:  Table [dbo].[Utilizadores_Ficheiros]    Script Date: 29/05/2018 18:38:15 PM ******/
DROP TABLE [dbo].[Utilizadores_Ficheiros]
GO
/****** Object:  Table [dbo].[Utilizadores_Ficheiros]    Script Date: 29/05/2018 18:38:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Utilizadores_Ficheiros](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUtilizador] [int] NOT NULL,
	[idPerfil] [int] NULL,
	[sharepointURL] [varchar](max) NOT NULL,
	[sharepointList] [varchar](max) NOT NULL,
	[sharepointFolder] [varchar](max) NOT NULL,
	[filename] [varchar](64) NOT NULL,
	[idTipo] [int] NOT NULL,
	[ativo] [bit] NOT NULL CONSTRAINT [DF_Utilizadores_Ficheiros_ativo]  DEFAULT ((1)),
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF_Utilizadores_Ficheiros_dataCriado]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF_Utilizadores_Ficheiros_utilizadorCriado]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Utilizadores_Ficheiros] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores] FOREIGN KEY([idUtilizador])
REFERENCES [dbo].[Utilizadores] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros] CHECK CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores]
GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores_Ficheiros_Tipos] FOREIGN KEY([idTipo])
REFERENCES [dbo].[Utilizadores_Ficheiros_Tipos] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros] CHECK CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores_Ficheiros_Tipos]
GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores_Perfil] FOREIGN KEY([idPerfil])
REFERENCES [dbo].[Utilizadores_Perfil] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Ficheiros] CHECK CONSTRAINT [FK_Utilizadores_Ficheiros_Utilizadores_Perfil]
GO
