USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_PorTipo_S]    Script Date: 29/05/2018 18:38:15 PM ******/
DROP PROCEDURE [dbo].[stp_Utilizador_Ficheiros_PorTipo_S]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Ficheiros_PorTipo_S]    Script Date: 29/05/2018 18:38:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procEDURE [dbo].[stp_Utilizador_Ficheiros_PorTipo_S]
	@idTipo int,
	@incluirApagados bit = 0
AS
BEGIN
	SET NOCOUNT ON;

select uf.[id]
	  ,uf.[idUtilizador]
	  ,uf.[idPerfil]
	  ,uf.[sharepointURL]
	  ,uf.[sharepointList]
	  ,uf.[sharepointFolder]
	  ,uf.[filename]
	  ,uf.[idTipo]
	  ,uft.[nome] as tipoFicheiro
	  ,uft.[ativo]
	  ,uf.[dataCriado]
	  ,uf.[utilizadorCriado]
	  ,uf.[dataModificado]
	  ,uf.[utilizadorModificado]
  FROM [dbo].[Utilizadores_Ficheiros] uf
  left join [dbo].[Utilizadores_Ficheiros_Tipos] uft on uft.id = uf.idTipo
	  WHERE uf.idTipo = @idTipo
	  AND (uf.ativo = 1 or uf.ativo<> @incluirApagados)
END












GO
