USE [RCU_Test2]
GO

CREATE TYPE [dbo].[UtilizadorType] AS TABLE(
	[IUPI] [uniqueidentifier] NULL,
	[Nome] [nvarchar](255) NULL,
	[NomePersonalizado] [nvarchar](255) NULL,
	[NIF] [nvarchar](20) NULL,
	[ContaNoGIAF] [bit] NULL,
	[UtilizadorEdicao] [nvarchar](max) NULL
)
GO

CREATE TYPE [dbo].[UtilizadorPerfilType] AS TABLE(
	[ID] [int] NULL,
	[IUPI] [uniqueidentifier] NULL,
	[Username] [nvarchar](50) NULL,
	[Email] [nvarchar](60) NULL,
	[NMec] [nvarchar](10) NULL,
	[IDTipo] [int] NULL,
	[IDCategoria] [int] NULL,
	[IDEntidade] [int] NULL,
	[IDCarreira] [int] NULL,
	[IDSituacao] [int] NULL,
	[IDRegime] [int] NULL,
	[DataEntrada] [datetime] NULL,
	[DataSaida] [datetime] NULL,
	[UtilizadorEdicao] [nvarchar](max) NULL
)
GO

CREATE TYPE [dbo].[CarreiraType] AS TABLE(
	ID int,
	Codigo nvarchar(50),
	Nome nvarchar(50),
	Descricao Nvarchar(max) null,
	IDGrupo int null,
	Ativo bit null,
	UtilizadorEdicao nvarchar(max) not null
)
GO

CREATE TYPE [dbo].[CarreiraGrupoType] AS TABLE(
	ID int,
	Codigo nvarchar(50),
	Nome nvarchar(50),
	Descricao Nvarchar(max) null,
	Ativo bit null,
	UtilizadorEdicao nvarchar(max) not null
)
GO

CREATE TYPE [dbo].[CategoriaType] AS TABLE(
	ID int,
	Codigo nvarchar(50),
	Nome nvarchar(50),
	Descricao Nvarchar(max) null,
	IDCarreira int null,
	Ativo bit null,
	UtilizadorEdicao nvarchar(max) not null
)
GO

CREATE TYPE [dbo].[RegimeType] AS TABLE(
	ID int,
	Codigo nvarchar(50),
	Nome nvarchar(50),
	Descricao Nvarchar(max) null,
	Ativo bit null,
	UtilizadorEdicao nvarchar(max) not null
)
GO

CREATE TYPE [dbo].[SituacaoType] AS TABLE(
	ID int,
	Codigo nvarchar(50),
	Nome nvarchar(50),
	Descricao Nvarchar(max) null,
	PRC nvarchar(max),
	Ativo bit null,
	UtilizadorEdicao nvarchar(max) not null
)
GO

CREATE TYPE [dbo].[PerfilTipoType] AS TABLE(
	ID int,
	Codigo nvarchar(50),
	Sigla nvarchar(50),
	Nome nvarchar(50),
	Descricao Nvarchar(max) null,
	Ativo bit null,
	UtilizadorEdicao nvarchar(max) not null
)
GO
