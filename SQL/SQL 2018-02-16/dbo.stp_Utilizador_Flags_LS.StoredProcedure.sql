USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Flags_LS]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[stp_Utilizador_Flags_LS]
	@incluirApagados bit = 0
	as
	SET NOCOUNT ON;
SELECT uf.[id]
      ,uf.[idUtilizador]
      ,uf.[idPerfil]
      ,uf.idFlag
	  ,f.[key]
	  ,f.descricao
      ,uf.[value]
      ,uf.[ativo]
	  ,f.ativo as flagAtivo
      ,uf.[dataCriado]
      ,uf.[utilizadorCriado]
      ,uf.[dataModificado]
      ,uf.[utilizadorModificado]
      ,f.[dataCriado] as flagDataCriado
      ,f.[utilizadorCriado] as flagUtilizadorCriado
      ,f.[dataModificado] as flagDataModificado
      ,f.[utilizadorModificado] as flagUtilizadorModificado
  FROM [dbo].[Utilizadores_Flags] uf
  left join [dbo].[Flags] f on uf.idFlag = f.id
  where (uf.ativo = 1 or uf.ativo <> @incluirApagados)












GO
