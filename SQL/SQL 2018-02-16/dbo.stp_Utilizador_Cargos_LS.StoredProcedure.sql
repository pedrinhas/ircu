USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Cargos_LS]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Utilizador_Cargos_LS]
	@incluirApagados bit = 0
AS
BEGIN
SELECT c.[id] as idCargo
      ,c.[idPerfil]
	  ,u.id as idUtilizador
	  ,u.iupi
	  ,u.nome
	  ,u.username
	  ,p.nmec
	  ,coalesce(u.originalEmail, u.aliasedEmail) as emailUser
	  ,c.email as emailCargo
      ,c.[idEntidade]
	  ,e.sigla as siglaEntidade
	  ,e.nome as nomeEntidade
	  ,e.descricao as descricaoEntidade
      ,c.[idTipoCargo] 
	  ,t.codigo as codigoTipo
	  ,t.nome as nomeTipo
	  ,t.descricao as descricaoTipo
      ,c.[dataInicio]
      ,c.[dataFim]
      ,c.[ativo] as cargoAtivo
      ,c.[dataCriado]
      ,c.[utilizadorCriado]
      ,c.[dataModificado]
      ,c.[utilizadorModificado]
  FROM [dbo].[Utilizadores_Cargos] c
  left join [dbo].[Utilizadores_Cargos_Tipos] t on c.idTipoCargo = t.id
  left join [dbo].[Utilizadores_Perfil] p on p.id = c.idPerfil 
  left join [dbo].[Utilizadores] u on u.id = p.idUtilizador
  left join [dbo].[Entidades] e on c.idEntidade = e.id
	where (c.ativo = 1 or c.ativo <> @incluirApagados)
	and  (t.ativo = 1 or t.ativo <> @incluirApagados)
	and  (p.ativo = 1 or p.ativo <> @incluirApagados)
	and  (u.ativo = 1 or u.ativo <> @incluirApagados)
	and  (e.ativo = 1 or e.ativo <> @incluirApagados)


	end








GO
