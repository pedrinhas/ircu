USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Entidades_PorCodigo_S]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Entidades_PorCodigo_S]
	@codigo nvarchar(50),
	@incluirApagados bit = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT e.[id]
      ,e.[codigo]
      ,e.[sigla]
      ,e.[nome]
      ,e.[descricao]
      ,e.[idTipoEntidade]
	  ,t.nome
	  ,t.descricao
      ,e.[idParent]
      ,e.[ativo]
      ,e.[dataCriado]
      ,e.[utilizadorCriado]
      ,e.[dataModificado]
      ,e.[utilizadorModificado]
  FROM [dbo].[Entidades] e
  left join [dbo].[Entidades_Tipo] t on e.idTipoEntidade = t.id AND (t.ativo = 1 or t.ativo <> @incluirApagados) --assim os cursos não desaparecem se o tipoEntidade não existir
  WHERE e.codigo = @codigo
  AND (e.ativo = 1 or e.ativo <> @incluirApagados)

END








GO
