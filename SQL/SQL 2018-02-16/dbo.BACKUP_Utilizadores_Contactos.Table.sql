USE [RCU]
GO
/****** Object:  Table [dbo].[BACKUP_Utilizadores_Contactos]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BACKUP_Utilizadores_Contactos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUtilizador] [int] NULL,
	[propertyId] [int] NULL,
	[idTipo] [int] NULL,
	[propertyval] [nvarchar](500) NULL,
	[visibilidade] [int] NULL,
	[ativo] [bit] NULL,
	[dataCriado] [datetime] NOT NULL,
	[utilizadorCriado] [nvarchar](max) NOT NULL,
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
