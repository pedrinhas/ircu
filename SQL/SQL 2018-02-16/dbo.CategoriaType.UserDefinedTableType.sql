USE [RCU]
GO
/****** Object:  UserDefinedTableType [dbo].[CategoriaType]    Script Date: 16/02/2018 16:41:49 PM ******/
CREATE TYPE [dbo].[CategoriaType] AS TABLE(
	[ID] [int] NULL,
	[Codigo] [nvarchar](50) NULL,
	[Nome] [nvarchar](50) NULL,
	[Descricao] [nvarchar](max) NULL,
	[CodCarreira] [nvarchar](max) NULL,
	[UtilizadorEdicao] [nvarchar](max) NOT NULL
)
GO
