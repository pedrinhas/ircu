USE [RCU]
GO
/****** Object:  Table [dbo].[Utilizadores_Contactos]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores_Contactos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUtilizador] [int] NULL,
	[propertyId] [int] NULL,
	[idTipo] [int] NULL,
	[propertyval] [nvarchar](500) NULL,
	[visibilidade] [int] NULL,
	[ativo] [bit] NULL,
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF__Utilizado__dataC__5A846E65]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF__Utilizado__utili__5555A4F4]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Utilizadores_Contactos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Utilizadores_Contactos]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Contactos_Utilizadores] FOREIGN KEY([idUtilizador])
REFERENCES [dbo].[Utilizadores] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Contactos] CHECK CONSTRAINT [FK_Utilizadores_Contactos_Utilizadores]
GO
ALTER TABLE [dbo].[Utilizadores_Contactos]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Contactos_Utilizadores_Contactos_Tipos] FOREIGN KEY([idTipo])
REFERENCES [dbo].[Utilizadores_Contactos_Tipos] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Contactos] CHECK CONSTRAINT [FK_Utilizadores_Contactos_Utilizadores_Contactos_Tipos]
GO
ALTER TABLE [dbo].[Utilizadores_Contactos]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Contactos_Utilizadores_Visibilidade] FOREIGN KEY([visibilidade])
REFERENCES [dbo].[Utilizadores_Visibilidade] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Contactos] CHECK CONSTRAINT [FK_Utilizadores_Contactos_Utilizadores_Visibilidade]
GO
