USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Carreiras_LS]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[stp_Utilizador_Carreiras_LS]
	@incluirApagados bit = 0
	as
	SET NOCOUNT ON;
SELECT c.[id]
      ,c.[codigo]
      ,c.[nome]
      ,c.[descricao]
      ,c.[ativo]
	  ,g.id as idGrupo
	  ,g.codigo as codigoGrupo
	  ,g.nome as nomeGrupo
	  ,g.descricao as descricaoGrupo
	  ,g.ativo as ativoGrupo
      ,c.[dataCriado]
      ,c.[utilizadorCriado]
      ,c.[dataModificado]
      ,c.[utilizadorModificado]
	  ,g.[dataCriado] as dataCriadoGrupo
      ,g.[utilizadorCriado] as utilizadorCriadoGrupo
      ,g.[dataModificado] as dataModificadoGrupo
      ,g.[utilizadorModificado] as utilizadorModificadoGrupo
  FROM [dbo].[Utilizadores_Carreiras] c
  left join [dbo].[Utilizadores_Carreiras_Grupos] g on g.id = c.idGrupo
  where (c.ativo = 1 or c.ativo <> @incluirApagados)
 











GO
