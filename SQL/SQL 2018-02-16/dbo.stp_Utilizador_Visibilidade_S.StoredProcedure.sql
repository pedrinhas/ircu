USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Visibilidade_S]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--[stp_Utilizadores_Visibilidade_LS]
create procEDURE [dbo].[stp_Utilizador_Visibilidade_S]
	@id int
	, @incluirApagados bit = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [id]
      ,[nome]
      ,[descricao]
	  ,[ativo]
      ,[dataCriado]
      ,[utilizadorCriado]
      ,[dataModificado]
      ,[utilizadorModificado]
  FROM [dbo].[Utilizadores_Visibilidade] n
  where (n.ativo = 1 or n.ativo <> @incluirApagados)
  and id = @id
  
END








GO
