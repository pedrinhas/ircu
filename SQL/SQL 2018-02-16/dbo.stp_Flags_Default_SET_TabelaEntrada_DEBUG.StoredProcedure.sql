USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Flags_Default_SET_TabelaEntrada_DEBUG]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--declare @p1 IDUtilizadorListType

--insert into @p1 
--select id, 'INTRA\cpereira' from Utilizadores

--exec [stp_Flags_Default_SET_TabelaEntrada_DEBUG] @p1

CREATE procedure [dbo].[stp_Flags_Default_SET_TabelaEntrada_DEBUG]
	@idUtilizadores as IDUtilizadorListType readonly
AS
BEGIN
	SET NOCOUNT ON;
	begin try
	begin tran
		insert into [dbo].[Utilizadores_Flags] (idUtilizador, idPerfil, idFlag, value, ativo, dataCriado, utilizadorCriado)
		select perfil.idUtilizador, perfil.id as idPerfil, idFlag, defaults.valorDefault, 1 as ativo, getdate() as dataCriado, users.UtilizadorEdicao
		from [dbo].[Utilizadores_Perfil] perfil
		left join [dbo].[Flags_Default] defaults on perfil.idTipo = defaults.idTipoPerfil
		inner join @idUtilizadores users on users.IDUtilizador = perfil.idUtilizador
		where not exists (
			select *
			from [dbo].[Utilizadores_Flags] f
			where f.idPerfil = perfil.id
			and f.idFlag = defaults.idFlag
	
		)
		and perfil.idTipo is not null

		commit
		--rollback
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END





--select * from utilizadores_perfil where idUtilizador = 2130

--select * from Utilizadores_Perfil_Tipo



GO
