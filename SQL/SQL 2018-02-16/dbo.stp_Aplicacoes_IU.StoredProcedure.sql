USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Aplicacoes_IU]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- exec [stp_Aplicacoes_Aplicacao_S] 4

CREATE procEDURE [dbo].[stp_Aplicacoes_IU]
	@id int,
	@nome nvarchar(50),
	@titulo nvarchar(50),
	@url nvarchar(500),
	@descricao nvarchar(500) = null,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exists bit
		set @exists = 0

		select @exists = coalesce(a.idAplicacao, case when a.idAplicacao is null then 0 when a.idAplicacao is not null then 1 end) 
		from [dbo].[Aplicacoes] a
		where a.idAplicacao = @id

		if @exists = 1 and @id is not null
		begin
			update [dbo].[Aplicacoes]
			set Nome = @nome, Titulo = @titulo, URL = @URL, Descricao = @descricao, utilizadorModificado = @utilizadorEdicao
			where idAplicacao = @id

			select @id
		end
		else
		begin

			insert into [dbo].[Aplicacoes] (Nome, Titulo, URL, Descricao, Ativo, dataCriado, utilizadorCriado)
			values (@nome, @titulo, @url, @descricao, 1, getdate(), @utilizadorEdicao)

			select max(idAplicacao) from [dbo].[Aplicacoes]
		end

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@id as idAplicacao
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END









GO
