USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Cargos_D]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Utilizador_Cargos_D]
	@idCargo int,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin tran
	begin try

		update [dbo].[Utilizadores_Cargos]
		set ativo = 0, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
		where id = @idCargo

		select @idCargo

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@idCargo as idCargo
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	end








GO
