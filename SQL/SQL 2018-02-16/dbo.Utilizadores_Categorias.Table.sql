USE [RCU]
GO
/****** Object:  Table [dbo].[Utilizadores_Categorias]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores_Categorias](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [nvarchar](50) NOT NULL,
	[nome] [nvarchar](50) NOT NULL,
	[descricao] [nvarchar](max) NULL,
	[idCarreira] [int] NULL,
	[ativo] [bit] NOT NULL CONSTRAINT [DF__Utilizado__ativo__0CFADF99]  DEFAULT ((1)),
	[dataCriado] [datetime] NULL,
	[utilizadorCriado] [nvarchar](max) NULL CONSTRAINT [DF__Utilizado__utili__49E3F248]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Categorias] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Utilizadores_Categorias]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Categorias_Utilizadores_Categorias] FOREIGN KEY([id])
REFERENCES [dbo].[Utilizadores_Categorias] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Categorias] CHECK CONSTRAINT [FK_Utilizadores_Categorias_Utilizadores_Categorias]
GO
