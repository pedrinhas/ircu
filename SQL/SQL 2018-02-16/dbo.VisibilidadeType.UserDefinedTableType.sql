USE [RCU]
GO
/****** Object:  UserDefinedTableType [dbo].[VisibilidadeType]    Script Date: 16/02/2018 16:41:49 PM ******/
CREATE TYPE [dbo].[VisibilidadeType] AS TABLE(
	[ID] [int] NULL,
	[Nome] [nvarchar](50) NULL,
	[Descricao] [nvarchar](255) NULL,
	[UtilizadorEdicao] [nvarchar](max) NOT NULL
)
GO
