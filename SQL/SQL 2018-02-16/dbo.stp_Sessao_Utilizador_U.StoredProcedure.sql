USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Sessao_Utilizador_U]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- stp_Sessao_Utilizador_I 'cpereira2', 'lol'

CREATE procEDURE [dbo].[stp_Sessao_Utilizador_U] 
	@username nvarchar(50),
	@nome nvarchar(max),
	@email nvarchar(max)
	, @utilizadorEdicao nvarchar(max)
AS
BEGIN
	begin tran
	begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;	

		UPDATE [Sessao_Utilizador]
		SET Nome = @nome, Email = @email, utilizadorModificado = @utilizadorEdicao, dataModificado = getdate()
		WHERE Username = @username

		commit
	end try
	begin catch
		    select ERROR_NUMBER() AS ErrorNumber
			,@Username as Username
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END








GO
