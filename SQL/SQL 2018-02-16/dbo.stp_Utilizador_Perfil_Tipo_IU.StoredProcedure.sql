USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Perfil_Tipo_IU]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


CREATE procEDURE [dbo].[stp_Utilizador_Perfil_Tipo_IU]
	  @id int,
	  @codigo nvarchar(50),
	  @sigla nvarchar(50) = null,
	  @nome nvarchar(50),
	  @descricao nvarchar(max) = null,
	  @utilizadorEdicao nvarchar(max)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exists bit
		set @exists = 0

		select @exists = coalesce(c.id, case when c.id is null then 0 when c.id is not null then 1 end) 
		from [dbo].[Utilizadores_Perfil_Tipo] c
		where @id is not null and c.id = @id

		if @exists = 1 and @id is not null
		begin
			update [dbo].[Utilizadores_Perfil_Tipo]
			set codigo = @codigo, sigla = @sigla, nome = @nome, descricao = @descricao, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
			where id = @id

			select @id
		end
		else
		begin
			insert into Utilizadores_Perfil_Tipo (codigo, sigla, nome, descricao, ativo, dataCriado, utilizadorCriado)
			values (@codigo, @sigla, @nome, @descricao, 1, getdate(), @utilizadorEdicao)

			select max(id) from [dbo].[Utilizadores_Perfil_Tipo]
		end

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@id as idCategoria
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	
END










GO
