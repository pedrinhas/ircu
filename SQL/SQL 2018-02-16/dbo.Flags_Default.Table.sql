USE [RCU]
GO
/****** Object:  Table [dbo].[Flags_Default]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Flags_Default](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idFlag] [int] NOT NULL,
	[idTipoPerfil] [int] NULL,
	[valorDefault] [nvarchar](max) NULL,
	[ativo] [bit] NOT NULL,
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF__Flag__def__52E34C9D]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF__Flag__def__4242D080]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Flag_Default] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Flags_Default]  WITH CHECK ADD  CONSTRAINT [FK_Flags_Default_Flags] FOREIGN KEY([idFlag])
REFERENCES [dbo].[Flags] ([id])
GO
ALTER TABLE [dbo].[Flags_Default] CHECK CONSTRAINT [FK_Flags_Default_Flags]
GO
ALTER TABLE [dbo].[Flags_Default]  WITH CHECK ADD  CONSTRAINT [FK_Flags_Default_Utilizadores_Perfil_Tipo] FOREIGN KEY([idTipoPerfil])
REFERENCES [dbo].[Utilizadores_Perfil_Tipo] ([id])
GO
ALTER TABLE [dbo].[Flags_Default] CHECK CONSTRAINT [FK_Flags_Default_Utilizadores_Perfil_Tipo]
GO
