USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Aplicacoes_R]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Aplicacoes_R]
	-- Add the parameters for the stored procedure here
	@id int
	, @utilizadorEdicao nvarchar(max)
AS
BEGIN
	begin try
	begin tran
	
		SET NOCOUNT ON;

		UPDATE [dbo].[Aplicacoes]
		SET Ativo = 1, utilizadorModificado = @utilizadorEdicao, dataModificado = getdate()
		WHERE idAplicacao = @id

		commit

    end try
	begin catch
		rollback
	end catch


END








GO
