USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Flags_IU]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procEDURE [dbo].[stp_Flags_IU]
	@id int,
	@key nvarchar(50),
	@descricao nvarchar(max) = null,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exists bit
		set @exists = 0

		select @exists = coalesce(a.[id], case when a.[id] is null then 0 when a.[id] is not null then 1 end) 
		from [dbo].[Flags] a
		where a.[id] = @id

		if @exists = 1 and @id is not null
		begin
			update [dbo].[Flags]
			set [key] = @key, descricao = @descricao, utilizadorModificado = @utilizadorEdicao
			where id = @id

			select @id
		end
		else
		begin

			insert into [dbo].[Flags] ([key], descricao, Ativo, dataCriado, utilizadorCriado)
			values (@key, @descricao, 1, getdate(), @utilizadorEdicao)

			select max(id) from [dbo].[Flags]
		end

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@id as idFlag
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END

GO
