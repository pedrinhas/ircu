USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Categorias_S]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- [stp_Utilizador_Categorias_S] 1
CREATE proc [dbo].[stp_Utilizador_Categorias_S]
	@id int
	, @incluirApagados bit = 0
	as
	SET NOCOUNT ON;
SELECT [id]
      ,[codigo]
      ,[nome]
      ,[descricao]
      ,[ativo]
      ,[dataCriado]
      ,[utilizadorCriado]
      ,[dataModificado]
      ,[utilizadorModificado]
  FROM [dbo].[Utilizadores_Categorias] c
  WHERE id = @id
  and (c.ativo = 1 or c.ativo <> @incluirApagados)











GO
