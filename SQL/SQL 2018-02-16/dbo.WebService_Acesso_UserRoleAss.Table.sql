USE [RCU]
GO
/****** Object:  Table [dbo].[WebService_Acesso_UserRoleAss]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebService_Acesso_UserRoleAss](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUser] [int] NOT NULL,
	[idRole] [int] NOT NULL,
	[ativo] [bit] NOT NULL CONSTRAINT [DF__WebServic__ativo__149C0161]  DEFAULT ((1)),
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF__WebServic__dataC__7167D3BD]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF__WebServic__utili__546180BB]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_WebService_Acesso_UserRoleAss] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[WebService_Acesso_UserRoleAss]  WITH CHECK ADD  CONSTRAINT [FK_WebService_Acesso_UserRoleAss_WebService_Acesso_Role] FOREIGN KEY([idRole])
REFERENCES [dbo].[WebService_Acesso_Role] ([id])
GO
ALTER TABLE [dbo].[WebService_Acesso_UserRoleAss] CHECK CONSTRAINT [FK_WebService_Acesso_UserRoleAss_WebService_Acesso_Role]
GO
ALTER TABLE [dbo].[WebService_Acesso_UserRoleAss]  WITH CHECK ADD  CONSTRAINT [FK_WebService_Acesso_UserRoleAss_WebService_Acesso_User] FOREIGN KEY([idUser])
REFERENCES [dbo].[WebService_Acesso_User] ([id])
GO
ALTER TABLE [dbo].[WebService_Acesso_UserRoleAss] CHECK CONSTRAINT [FK_WebService_Acesso_UserRoleAss_WebService_Acesso_User]
GO
