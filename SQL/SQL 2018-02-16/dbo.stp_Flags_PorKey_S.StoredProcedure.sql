USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Flags_PorKey_S]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- [stp_Flags_PorKey_S] 'VisibilidadeDadosDSD'

CREATE procedure [dbo].[stp_Flags_PorKey_S]
	@key nvarchar(50)
	, @incluirApagados bit = 0
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [id]
		  ,[key]
		  ,[descricao]
		  ,[dataCriado]
		  ,[ativo]
		  ,[utilizadorCriado]
		  ,[dataModificado]
		  ,[utilizadorModificado]
	  FROM [dbo].[Flags]
	  WHERE [key] = @key
	  AND (ativo = 1 or ativo <> @incluirApagados)
END









GO
