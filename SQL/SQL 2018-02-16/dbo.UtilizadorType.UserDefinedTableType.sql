USE [RCU]
GO
/****** Object:  UserDefinedTableType [dbo].[UtilizadorType]    Script Date: 16/02/2018 16:41:49 PM ******/
CREATE TYPE [dbo].[UtilizadorType] AS TABLE(
	[IUPI] [uniqueidentifier] NULL,
	[Nome] [nvarchar](255) NULL,
	[NomePersonalizado] [nvarchar](255) NULL,
	[NIF] [nvarchar](20) NULL,
	[Username] [nvarchar](50) NULL,
	[OriginalEmail] [nvarchar](60) NULL,
	[AliasedEmail] [nvarchar](60) NULL,
	[ContaNoGIAF] [bit] NULL,
	[Visibilidade] [int] NULL,
	[UtilizadorEdicao] [nvarchar](max) NULL
)
GO
