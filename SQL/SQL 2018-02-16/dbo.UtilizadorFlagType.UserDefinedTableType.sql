USE [RCU]
GO
/****** Object:  UserDefinedTableType [dbo].[UtilizadorFlagType]    Script Date: 16/02/2018 16:41:49 PM ******/
CREATE TYPE [dbo].[UtilizadorFlagType] AS TABLE(
	[ID] [int] NULL,
	[IDUtilizador] [int] NULL,
	[IDPerfil] [int] NULL,
	[IDFlag] [int] NULL,
	[Value] [nvarchar](max) NULL,
	[UtilizadorEdicao] [nvarchar](max) NOT NULL
)
GO
