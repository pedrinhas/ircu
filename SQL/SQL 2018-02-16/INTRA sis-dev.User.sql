USE [RCU]
GO
/****** Object:  User [INTRA\sis-dev]    Script Date: 16/02/2018 16:41:49 PM ******/
CREATE USER [INTRA\sis-dev] FOR LOGIN [INTRA\sis-dev] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_executor] ADD MEMBER [INTRA\sis-dev]
GO
ALTER ROLE [db_datareader] ADD MEMBER [INTRA\sis-dev]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [INTRA\sis-dev]
GO
ALTER ROLE [db_denydatareader] ADD MEMBER [INTRA\sis-dev]
GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [INTRA\sis-dev]
GO
