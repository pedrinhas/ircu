USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Contactos_IU]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

-- stp_Utilizador_Contactos_IU 'DC27A15B-0020-49FA-9B91-0ADE9B0B0F51' , 10, 'aiaiaiaia', 1, 'SYSTEM'

CREATE procEDURE [dbo].[stp_Utilizador_Contactos_IU]
	  @iupi uniqueidentifier
	, @propertyID int
	, @propertyVal int
	, @idTipo int
	, @val nvarchar(255)
	, @visibilidade int
	, @utilizadorEdicao nvarchar(max)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @idUser int
	select @idUser = [id] 
	from [dbo].[Utilizadores] 
	where [iupi] = @iupi
	 
	if @idUser is not null
	begin

		begin tran
		begin try
			declare @exists bit
			set @exists = 0

			select @exists = coalesce(c.id, case when c.id is null then 0 when c.id is not null then 1 end) 
			from [dbo].[Utilizadores_Contactos] c
			left join [dbo].[Utilizadores] u on u.id = c.idUtilizador
			where @idTipo is not null and c.idTipo = @idTipo
			AND u.id = @idUser
			AND c.propertyId = @propertyID

			if @exists = 1 and @idTipo is not null
			begin
				update [Utilizadores_Contactos]
				set propertyval = @val, visibilidade = @visibilidade, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
				where idTipo = @idTipo AND idUtilizador = @idUser

				select id
				from [Utilizadores_Contactos]
				where idUtilizador = @idUser
				AND idTIpo = @idTipo
			end
			else
				begin
				declare @propertyIdNew int
				select @propertyIdNew = count(*) from [Utilizadores_Contactos] where idUtilizador = @idUser

				insert into [dbo].[Utilizadores_Contactos] (idUtilizador, propertyId, idtipo, propertyval, visibilidade, ativo, dataCriado, utilizadorCriado)
				values (@idUser, @propertyIdNew, @idTipo, @val, @visibilidade, 1, getdate(), @utilizadorEdicao)

				select max(id) from [Utilizadores_Contactos]
			end

			
			commit
		end try
		begin catch
			select ERROR_NUMBER() AS ErrorNumber
			,@iupi as IUPI
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
			rollback
		end catch
	end
END









GO
