USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Sessao_WebService_User_Password_Modify]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec [stp_Sessao_WebService_User_Password_Modify] 'sis-dev', '$p4s7i10#', 'MD5'

CREATE procedure [dbo].[stp_Sessao_WebService_User_Password_Modify]
	@username nvarchar(max)
	, @newPassword nvarchar(max)
	, @alg nvarchar(10) = 'SHA2_512'
	, @utilizadorEdicao nvarchar(max)
as
	
	declare @newSalt uniqueidentifier,
		@pwdBytes binary(64)

	set @newSalt = newid()

	set @pwdBytes = HASHBYTES(@alg, CONCAT(@newpassword, cast (@newsalt as nvarchar(max))))

	select @pwdBytes

	update [dbo].[WebService_Acesso_User]
	set Password = @pwdBytes, Salt = @newSalt, algoritmo = @alg, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
	where username = @username







GO
