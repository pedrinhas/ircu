USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Contactos_LS]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Contactos_LS]
	@incluirApagados bit = 0
	
AS
BEGIN
	exec [stp_Contactos_PorVisibilidade_LS] 3, @incluirApagados
END





GO
