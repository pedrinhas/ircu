USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Flags_Default_SET]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- [stp_Flags_Default_SET] 1336, 'INTRA\cpereira'

CREATE procedure [dbo].[stp_Flags_Default_SET]
	@idUtilizador int
	, @utilizadorEdicao nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	begin try
	begin tran
		insert into [dbo].[Utilizadores_Flags] (idUtilizador, idPerfil, idFlag, value, ativo, dataCriado, utilizadorCriado)
		select idUtilizador, perfil.id as idPerfil, idFlag, defaults.valorDefault, 1 as ativo, getdate() as dataCriado, @utilizadorEdicao
		from [dbo].[Utilizadores_Perfil] perfil
		left join [dbo].[Flags_Default] defaults on perfil.idTipo = defaults.idTipoPerfil
		where perfil.idUtilizador = @idUtilizador
		AND not exists (
			select *
			from [dbo].[Utilizadores_Flags] f
			where f.idPerfil = perfil.id
			and f.idFlag = defaults.idFlag
	
		)

		commit
		--rollback
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@idUtilizador as idUtilizador
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END









--select * from Utilizadores_Flags where idUtilizador = 1336

GO
