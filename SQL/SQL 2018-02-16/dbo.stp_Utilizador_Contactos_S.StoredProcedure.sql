USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Contactos_S]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec stp_Utilizador_Contactos_S 'B295959E-956E-466F-944E-0128C2626223'
--exec [stp_Utilizador_Contactos_LS] 

CREATE proc [dbo].[stp_Utilizador_Contactos_S]
	@iupi uniqueidentifier,
	@incluirApagados bit = 0
	as
	SET NOCOUNT ON;
SELECT distinct c.[id]
      ,[propertyId]
	  ,c.idUtilizador
      ,c.[idTipo]
	  ,tipo.tipo
	  ,tipo.idgrupo
	  ,grupo.grupo
      ,[propertyval]
	  ,perfil.visibilidade as visibilidadePerfil
      ,c.visibilidade as visibilidadeContacto
	  ,c.ativo
	  ,c.dataCriado
	  ,c.utilizadorCriado
	  ,c.dataModificado
	  ,c.utilizadorModificado
  FROM [RCU].[dbo].[Utilizadores_Contactos] c
  left join [dbo].[Utilizadores] utilizador on c.idUtilizador = utilizador.id
  left join [dbo].[Utilizadores_Contactos_Tipos] tipo on c.idTipo = tipo.id
  left join [dbo].[Utilizadores_Contactos_Tipos_Grupos] grupo on tipo.idGrupo = grupo.id
  left join [dbo].[Utilizadores_Perfil] perfil on utilizador.id = perfil.idUtilizador
  where utilizador.iupi = @iupi AND (c.ativo = 1 or c.ativo <> @incluirApagados)
  and perfil.datasaida is null

  --ver porque o acrespi aparece 2x






GO
