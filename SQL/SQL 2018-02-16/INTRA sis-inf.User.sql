USE [RCU]
GO
/****** Object:  User [INTRA\sis-inf]    Script Date: 16/02/2018 16:41:49 PM ******/
CREATE USER [INTRA\sis-inf] FOR LOGIN [INTRA\sis-inf] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_executor] ADD MEMBER [INTRA\sis-inf]
GO
ALTER ROLE [db_datareader] ADD MEMBER [INTRA\sis-inf]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [INTRA\sis-inf]
GO
