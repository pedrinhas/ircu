USE [RCU]
GO
/****** Object:  Table [dbo].[Utilizadores_Carreiras_Grupos]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores_Carreiras_Grupos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [nvarchar](50) NOT NULL,
	[nome] [nvarchar](50) NOT NULL,
	[descricao] [nvarchar](max) NULL,
	[ativo] [bit] NOT NULL DEFAULT ((1)),
	[dataCriado] [datetime] NOT NULL DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
