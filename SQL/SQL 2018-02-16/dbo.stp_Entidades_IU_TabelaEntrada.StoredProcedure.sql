USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Entidades_IU_TabelaEntrada]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


CREATE procEDURE [dbo].[stp_Entidades_IU_TabelaEntrada]
	  @entidades [dbo].[EntidadeType] readonly
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exist [dbo].[EntidadeType],
				@new [dbo].[EntidadeType]
		declare @result table(Codigo nvarchar(20), ID int, Accao char)

		insert into @exist
		select * 
		from @entidades r
		where r.id is not null and r.id in (select id from Entidades)

		insert into @new
		select * 
		from @entidades r
		where r.id is null or r.id not in (select id from Entidades)

		--existentes
		update EntidadesExistentes
		set EntidadesExistentes.codigo = ExistentesParam.codigo,
			EntidadesExistentes.sigla = ExistentesParam.sigla,
			EntidadesExistentes.nome = ExistentesParam.nome,
			EntidadesExistentes.descricao = ExistentesParam.descricao,
			EntidadesExistentes.email = ExistentesParam.email,
			EntidadesExistentes.dataModificado = getdate(),
			EntidadesExistentes.utilizadorModificado = ExistentesParam.UtilizadorEdicao
		from dbo.Entidades EntidadesExistentes
		inner join @exist as ExistentesParam on EntidadesExistentes.id = ExistentesParam.id

		insert into @result
		select Codigo, ID, 'U'
		from @exist

		--novos
		insert into dbo.Entidades (codigo, sigla, nome, descricao, email, ativo, dataCriado, utilizadorCriado)
		select n.Codigo, n.Sigla, n.Nome, n.Descricao, n.Email, 1, getdate(), n.UtilizadorEdicao
		from @new n

		insert into @result
		select Codigo, ID, 'I'
		from @new

		select * from @result

		commit
		--rollback
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	
END










GO
