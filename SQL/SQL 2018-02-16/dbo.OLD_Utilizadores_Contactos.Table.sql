USE [RCU]
GO
/****** Object:  Table [dbo].[OLD_Utilizadores_Contactos]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OLD_Utilizadores_Contactos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[iupi] [uniqueidentifier] NULL,
	[email] [nvarchar](50) NULL,
	[propertyId] [int] NULL,
	[idTipo] [int] NULL,
	[propertyval] [nvarchar](500) NULL,
	[public] [bit] NULL
) ON [PRIMARY]

GO
