USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Entidades_Tipo_LS]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Entidades_Tipo_LS]
	@incluirApagados bit = 0
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [id]
		  ,[nome]
		  ,[descricao]
		  ,[ativo]
		  ,[dataCriado]
		  ,[utilizadorCriado]
		  ,[dataModificado]
		  ,[utilizadorModificado]
	  FROM [dbo].[Entidades_Tipo]
	  WHERE (ativo = 1 or ativo <> @incluirApagados)
END








GO
