USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_OLD_Acesso_Visibilidade_R]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- exec [stp_Acesso_Visibilidade_IU] 0, 4, 'Nivel teste', 1, 'cpereira'

CREATE procEDURE [dbo].[stp_OLD_Acesso_Visibilidade_R]
	@id int,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin try
	begin tran
	
		SET NOCOUNT ON;

		UPDATE [dbo].[Acesso_Visibilidade]
		SET Ativo = 1, utilizadorModificado = @utilizadorEdicao, dataModificado = getdate()
		WHERE idVisibilidade = @id

		commit

    end try
	begin catch
		rollback
	end catch


END









GO
