USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_OLD_Menus_MenuAplicacaoVisibilidade_S]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_OLD_Menus_MenuAplicacaoVisibilidade_S]
	-- Add the parameters for the stored procedure here
	@idAplicacao int
	, @idVisibilidade int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT   m.idMenu as ID
		   , m.Texto as Texto
		   , m.Tooltip as Tooltip
		   , m.URL as URL
		   , m.idMenuParent as IDParent
	FROM [dbo].[Menus_Menu] m
	LEFT JOIN [dbo].[Menus_Permissao] mp on m.idMenu = mp.idMenu
	WHERE [idAplicacao] = @idAplicacao
	AND mp.idVisibilidade = @idVisibilidade


END








GO
