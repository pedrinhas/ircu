USE [RCU]
GO
/****** Object:  Table [dbo].[Utilizadores_Contactos_Tipos_Grupos]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores_Contactos_Tipos_Grupos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[grupo] [nvarchar](255) NOT NULL,
	[tmp_oldText] [nvarchar](255) NULL,
	[ativo] [bit] NOT NULL CONSTRAINT [DF__Utilizado__ativo__0FD74C44]  DEFAULT ((1)),
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF__Utilizado__dataC__5E54FF49]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF__Utilizado__utili__4F9CCB9E]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Utilizadores_Contactos_Grupos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
