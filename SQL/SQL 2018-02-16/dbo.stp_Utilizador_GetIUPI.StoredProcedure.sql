USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_GetIUPI]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

--exec stp_Utilizador_GetIUPI 'egomes'

CREATE procEDURE [dbo].[stp_Utilizador_GetIUPI]
	@username nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select distinct u.iupi from [dbo].[Utilizadores] u
	left join [dbo].[Utilizadores_Perfil] p on u.id = p.idUtilizador
	where u.username = @username
END








GO
