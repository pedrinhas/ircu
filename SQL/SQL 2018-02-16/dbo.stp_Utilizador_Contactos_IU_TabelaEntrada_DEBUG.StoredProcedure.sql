USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Contactos_IU_TabelaEntrada_DEBUG]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

declare @p1 dbo.ContactoType
insert into @p1 values('30BC8B13-1C3A-4942-8BE1-2035EBB3FE8E',NULL,12,N'4903',1,N'IRCU - Website Perfil - 30bc8b13-1c3a-4942-8be1-2035ebb3fe8e')
insert into @p1 values('30BC8B13-1C3A-4942-8BE1-2035EBB3FE8E',NULL,3,N'http://www.utad.pt',1,N'IRCU - Website Perfil - 30bc8b13-1c3a-4942-8be1-2035ebb3fe8e')
insert into @p1 values('30BC8B13-1C3A-4942-8BE1-2035EBB3FE8E',NULL,7,N'http://www.twitter.com',1,N'IRCU - Website Perfil - 30bc8b13-1c3a-4942-8be1-2035ebb3fe8e')
insert into @p1 values('30BC8B13-1C3A-4942-8BE1-2035EBB3FE8E',NULL,6,N'https://www.facebook.com/jorgejsborges',1,N'IRCU - Website Perfil - 30bc8b13-1c3a-4942-8be1-2035ebb3fe8e')
insert into @p1 values('30BC8B13-1C3A-4942-8BE1-2035EBB3FE8E',NULL,5,N'https://www.linkedin.com/in/jorge-borges-25290b5/',1,N'IRCU - Website Perfil - 30bc8b13-1c3a-4942-8be1-2035ebb3fe8e')

exec [stp_Utilizador_Contactos_IU_TabelaEntrada_DEBUG] @contactos=@p1

*/

CREATE procEDURE [dbo].[stp_Utilizador_Contactos_IU_TabelaEntrada_DEBUG]
	  @contactos [dbo].[ContactoType] readonly
AS
BEGIN
	SET NOCOUNT ON;

	declare @exist [dbo].[ContactoType],
			@new [dbo].[ContactoType]
	declare @result table (IUPI uniqueidentifier, PropertyID int, Accao char)

	begin tran
	begin try
		insert into @exist
		select *
		from @contactos c
		where c.PropertyID is not null
					  
		insert into @new
		select *
		from @contactos c
		where c.PropertyID is null
		
		--existentes
		update ContactosExistentes
		set idUtilizador = u.id,
			idTipo = ExistentesParam.IDTipo,
			propertyval = ExistentesParam.Value,
			visibilidade = ExistentesParam.visibilidade,
			dataModificado = GETDATE(),
			utilizadorModificado = ExistentesParam.UtilizadorEdicao
		from [dbo].[Utilizadores_Contactos] ContactosExistentes
		inner join [dbo].[Utilizadores] u on u.id = ContactosExistentes.id
		inner join @exist as ExistentesParam on u.iupi = ExistentesParam.IUPI
		where ContactosExistentes.propertyId = ExistentesParam.PropertyID

		insert into @result
		select IUPI, PropertyID, 'U'
		from @exist

		--novos
		insert into [dbo].[Utilizadores_Contactos] (idUtilizador, propertyId, idTipo, propertyval, visibilidade, ativo, dataCriado, utilizadorCriado)
		select u.id, (select count(*) from [Utilizadores_Contactos] where idUtilizador = u.id), n.IDTipo, n.Value, n.visibilidade, 1, getdate(), n.UtilizadorEdicao
		from @new n
		inner join [dbo].[Utilizadores] u on u.iupi = n.iupi
		
		insert into @result
		select IUPI, PropertyID, 'I'
		from @new

		select * from @result

		commit
		--rollback
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END








GO
