USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Visibilidade_D]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--[stp_Utilizadores_Visibilidade_LS]
create procEDURE [dbo].[stp_Utilizador_Visibilidade_D]
	@id int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
/****** Script for SelectTopNRows command from SSMS  ******/
update [dbo].[Utilizadores_Visibilidade]
set ativo = 0
where id = @id
  
END








GO
