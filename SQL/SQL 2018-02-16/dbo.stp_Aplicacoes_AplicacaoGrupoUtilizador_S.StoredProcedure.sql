USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Aplicacoes_AplicacaoGrupoUtilizador_S]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Aplicacoes_AplicacaoGrupoUtilizador_S]
	@nomeGrupo varchar(50),
	@utilizador varchar(50)

AS
	SELECT a.Nome, a.Titulo, a.URL, a.Descricao
	from [dbo].[Aplicacoes_Grupo] as g
	left join [dbo].[Aplicacoes_AplicacaoGrupo] as ag on ag.idGrupo = g.idGrupo
	left join [dbo].[Aplicacoes] as a on ag.idAplicacao = a.idAplicacao
	where g.nome=@nomeGrupo and a.Ativo=1
	union select a.nome, a.titulo, a.URL, a.descricao
	from [dbo].[Sessao_UtilizadorExcepcao] as ue
	left join [dbo].[Aplicacoes] as a on ue.appId = a.idAplicacao
	where ue.utilizador=@utilizador and a.Ativo=1

	-- exec [stp_Aplicacoes_AplicacaoGrupoUtilizador_S] 'anonimo', 'pgoncalves'









GO
