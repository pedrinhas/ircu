USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Contactos_D]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec stp_Utilizador_Contactos_S '30BC8B13-1C3A-4942-8BE1-2035EBB3FE8E'


CREATE proc [dbo].[stp_Utilizador_Contactos_D]
	  @iupi uniqueidentifier
	, @idContacto int
	, @utilizadorEdicao nvarchar(max)
	as
	SET NOCOUNT ON;
	begin tran
	begin try
		update [dbo].[Utilizadores_Contactos] 
		set ativo = 0, utilizadorModificado = @utilizadorEdicao, dataModificado = getdate()
		from (SELECT id from [dbo].[Utilizadores] WHERE iupi = @iupi) u
		where idUtilizador = u.id and [Utilizadores_Contactos].id = @idContacto
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
			,@iupi as IUPI
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch







GO
