USE [RCU]
GO
/****** Object:  UserDefinedTableType [dbo].[ContactoType]    Script Date: 16/02/2018 16:41:49 PM ******/
CREATE TYPE [dbo].[ContactoType] AS TABLE(
	[IUPI] [uniqueidentifier] NULL,
	[PropertyID] [int] NULL,
	[IDTipo] [int] NULL,
	[Value] [nvarchar](255) NULL,
	[Visibilidade] [int] NULL,
	[UtilizadorEdicao] [nvarchar](max) NOT NULL
)
GO
