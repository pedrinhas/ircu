USE [RCU]
GO
/****** Object:  Table [dbo].[OLD_Acesso_Permissao]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OLD_Acesso_Permissao](
	[idPermissao] [int] IDENTITY(1,1) NOT NULL,
	[idUtilizador] [int] NOT NULL,
	[idNivelAcesso] [int] NOT NULL,
	[ativo] [bit] NOT NULL,
	[dataCriado] [datetime] NOT NULL,
	[utilizadorCriado] [nvarchar](max) NOT NULL,
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Acesso_Permissao] PRIMARY KEY CLUSTERED 
(
	[idPermissao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[OLD_Acesso_Permissao] ADD  CONSTRAINT [DF__Acesso_Pe__ativo__11BF94B6]  DEFAULT ((1)) FOR [ativo]
GO
ALTER TABLE [dbo].[OLD_Acesso_Permissao] ADD  CONSTRAINT [DF__Acesso_Pe__dataC__32767D0B]  DEFAULT (getdate()) FOR [dataCriado]
GO
ALTER TABLE [dbo].[OLD_Acesso_Permissao] ADD  CONSTRAINT [DF__Acesso_Pe__utili__52793849]  DEFAULT (suser_sname()) FOR [utilizadorCriado]
GO
ALTER TABLE [dbo].[OLD_Acesso_Permissao]  WITH CHECK ADD  CONSTRAINT [FK_Acesso_Permissao_Acesso_NivelAcesso] FOREIGN KEY([idNivelAcesso])
REFERENCES [dbo].[OLD_Acesso_NivelAcesso] ([idNivelAcesso])
GO
ALTER TABLE [dbo].[OLD_Acesso_Permissao] CHECK CONSTRAINT [FK_Acesso_Permissao_Acesso_NivelAcesso]
GO
ALTER TABLE [dbo].[OLD_Acesso_Permissao]  WITH CHECK ADD  CONSTRAINT [FK_Acesso_Permissao_Sessao_Utilizador] FOREIGN KEY([idUtilizador])
REFERENCES [dbo].[Sessao_Utilizador] ([idUtilizador])
GO
ALTER TABLE [dbo].[OLD_Acesso_Permissao] CHECK CONSTRAINT [FK_Acesso_Permissao_Sessao_Utilizador]
GO
