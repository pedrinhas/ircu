USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Visibilidade_IU]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

-- stp_Utilizador_Contactos_IU 'DC27A15B-0020-49FA-9B91-0ADE9B0B0F51' , 10, 'aiaiaiaia', 1, 'SYSTEM'

create procEDURE [dbo].[stp_Utilizador_Visibilidade_IU]
	  @id int
	, @nome nvarchar(50)
	, @descricao nvarchar(255)
	, @utilizadorEdicao nvarchar(max)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exists bit
		set @exists = 0

		select @exists = coalesce(n.id, case when n.id is null then 0 when n.id is not null then 1 end) 
		from [dbo].[Utilizadores_Visibilidade] n
		where n.id = @id

		if @exists = 1 
		begin
			update [dbo].[Utilizadores_Visibilidade]
			set nome = @nome, descricao = @descricao, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
			where id = @id

			select @id
		end
		else
			begin

			insert into [dbo].[Utilizadores_Visibilidade] (nome, descricao, dataCriado, utilizadorCriado)
			values (@nome, @descricao, getdate(), @utilizadorEdicao)

			select max(id) from [dbo].[Utilizadores_Visibilidade]
		end

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@id as idVisibilidade
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END









GO
