USE [RCU]
GO
/****** Object:  Table [dbo].[Utilizadores_Perfil_OLD]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores_Perfil_OLD](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUtilizador] [int] NOT NULL,
	[username] [nvarchar](50) NULL,
	[email] [nvarchar](60) NULL,
	[nmec] [nvarchar](10) NULL,
	[idTipo] [int] NULL,
	[idCategoria] [int] NULL,
	[idEntidade] [int] NULL,
	[idCarreira] [int] NULL,
	[idSituacao] [int] NULL,
	[idRegime] [int] NULL,
	[ativo] [bit] NOT NULL,
	[dataCriado] [datetime] NOT NULL,
	[utilizadorCriado] [nvarchar](max) NOT NULL,
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
