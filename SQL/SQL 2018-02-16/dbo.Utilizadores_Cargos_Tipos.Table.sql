USE [RCU]
GO
/****** Object:  Table [dbo].[Utilizadores_Cargos_Tipos]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores_Cargos_Tipos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [nvarchar](50) NOT NULL,
	[nome] [nvarchar](50) NOT NULL,
	[descricao] [nvarchar](max) NULL,
	[ativo] [bit] NOT NULL,
	[dataCriado] [datetime] NOT NULL,
	[utilizadorCriado] [nvarchar](max) NOT NULL,
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Utilizadores_Cargos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Tipos] ADD  CONSTRAINT [DF__Utilizado__ativo__08362A7C]  DEFAULT ((1)) FOR [ativo]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Tipos] ADD  CONSTRAINT [DF__Utilizado__dataC__2EA5EC27]  DEFAULT (getdate()) FOR [dataCriado]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos_Tipos] ADD  CONSTRAINT [DF__Utilizado__utili__451F3D2B]  DEFAULT (suser_sname()) FOR [utilizadorCriado]
GO
