USE [RCU]
GO
/****** Object:  Table [dbo].[Utilizadores_Contactos_Tipos]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores_Contactos_Tipos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tipo] [nvarchar](255) NOT NULL,
	[idGrupo] [int] NULL,
	[tmp_oldText] [nvarchar](50) NULL,
	[ativo] [bit] NOT NULL CONSTRAINT [DF__Utilizado__ativo__0EE3280B]  DEFAULT ((1)),
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF__Utilizado__dataC__6225902D]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF__Utilizado__utili__4DB4832C]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Config_TipoContacto] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Utilizadores_Contactos_Tipos]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Contactos_Tipos_Utilizadores_Contactos_Grupos] FOREIGN KEY([idGrupo])
REFERENCES [dbo].[Utilizadores_Contactos_Tipos_Grupos] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Contactos_Tipos] CHECK CONSTRAINT [FK_Utilizadores_Contactos_Tipos_Utilizadores_Contactos_Grupos]
GO
