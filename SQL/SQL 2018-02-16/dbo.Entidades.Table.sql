USE [RCU]
GO
/****** Object:  Table [dbo].[Entidades]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Entidades](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [nvarchar](50) NOT NULL,
	[sigla] [nvarchar](20) NULL,
	[nome] [nvarchar](50) NOT NULL,
	[descricao] [nvarchar](max) NULL,
	[email] [nvarchar](60) NULL,
	[idTipoEntidade] [int] NULL,
	[idParent] [int] NULL,
	[ativo] [bit] NOT NULL CONSTRAINT [DF__Entidades__ativo__092A4EB5]  DEFAULT ((1)),
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF__Entidades__dataC__3A179ED3]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF__Entidades__utili__46136164]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Entidades] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Entidades]  WITH CHECK ADD  CONSTRAINT [FK_Entidades_Entidades] FOREIGN KEY([idParent])
REFERENCES [dbo].[Entidades] ([id])
GO
ALTER TABLE [dbo].[Entidades] CHECK CONSTRAINT [FK_Entidades_Entidades]
GO
ALTER TABLE [dbo].[Entidades]  WITH CHECK ADD  CONSTRAINT [FK_Entidades_Entidades_Tipo] FOREIGN KEY([idTipoEntidade])
REFERENCES [dbo].[Entidades_Tipo] ([id])
GO
ALTER TABLE [dbo].[Entidades] CHECK CONSTRAINT [FK_Entidades_Entidades_Tipo]
GO
