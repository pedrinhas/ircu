USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Contactos_IU_TabelaEntrada]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procEDURE [dbo].[stp_Utilizador_Contactos_IU_TabelaEntrada]
	  @contactos [dbo].[ContactoType] readonly
AS
BEGIN
	SET NOCOUNT ON;

	declare @exist [dbo].[ContactoType],
			@new [dbo].[ContactoType]
	declare @result table (IUPI uniqueidentifier, PropertyID int, Accao char)

	begin tran
	begin try
		insert into @exist
		select *
		from @contactos c
		where exists (select 1
					  from [dbo].[Utilizadores_Contactos] cont
					  left join [dbo].[Utilizadores] ut on ut.id = cont.idUtilizador
					  where cont.idTipo = c.IDTipo
					  and ut.iupi = c.IUPI)
		AND c.PropertyID is not null
					  
		insert into @new
		select *
		from @contactos c
		where not exists (select 1
					  from [dbo].[Utilizadores_Contactos] cont
					  left join [dbo].[Utilizadores] ut on ut.id = cont.idUtilizador
					  where cont.idTipo = c.IDTipo
					  and ut.iupi = c.IUPI)
		AND c.PropertyID is null
		
		--existentes
		update ContactosExistentes
		set idUtilizador = u.id,
			idTipo = ExistentesParam.IDTipo,
			propertyval = ExistentesParam.Value,
			visibilidade = ExistentesParam.visibilidade,
			dataModificado = GETDATE(),
			utilizadorModificado = ExistentesParam.UtilizadorEdicao
		from [dbo].[Utilizadores_Contactos] ContactosExistentes
		inner join [dbo].[Utilizadores] u on u.id = ContactosExistentes.id
		inner join @exist as ExistentesParam on u.iupi = ExistentesParam.IUPI
		where ContactosExistentes.propertyId = ExistentesParam.PropertyID

		insert into @result
		select IUPI, PropertyID, 'U'
		from @exist

		--novos
		insert into [dbo].[Utilizadores_Contactos] (idUtilizador, propertyId, idTipo, propertyval, visibilidade, ativo, dataCriado, utilizadorCriado)
		select u.id, (select count(*) from [Utilizadores_Contactos] where idUtilizador = u.id), n.IDTipo, n.Value, n.visibilidade, 1, getdate(), n.UtilizadorEdicao
		from @new n
		inner join [dbo].[Utilizadores] u on u.iupi = n.iupi
		
		insert into @result
		select IUPI, PropertyID, 'I'
		from @new

		select * from @result

		commit
		--rollback
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END








GO
