USE [RCU]
GO
/****** Object:  UserDefinedTableType [dbo].[IDUtilizadorListType]    Script Date: 16/02/2018 16:41:49 PM ******/
CREATE TYPE [dbo].[IDUtilizadorListType] AS TABLE(
	[IDUtilizador] [int] NULL,
	[UtilizadorEdicao] [nvarchar](max) NULL
)
GO
