USE [RCU]
GO
/****** Object:  Table [dbo].[Utilizadores_Carreiras]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores_Carreiras](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [nvarchar](50) NOT NULL,
	[nome] [nvarchar](50) NOT NULL,
	[descricao] [nvarchar](max) NULL,
	[idGrupo] [int] NULL,
	[ativo] [bit] NOT NULL CONSTRAINT [DF_Utilizadores_Carreiras_ativo]  DEFAULT ((1)),
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF__Utilizado__dataC__56B3DD81]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF__Utilizado__utili__4CC05EF3]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Utilizadores_Carreiras] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Utilizadores_Carreiras]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Carreiras_Utilizadores_Carreiras_Grupos] FOREIGN KEY([idGrupo])
REFERENCES [dbo].[Utilizadores_Carreiras_Grupos] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Carreiras] CHECK CONSTRAINT [FK_Utilizadores_Carreiras_Utilizadores_Carreiras_Grupos]
GO
