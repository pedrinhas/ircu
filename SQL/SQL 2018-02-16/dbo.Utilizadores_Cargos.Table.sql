USE [RCU]
GO
/****** Object:  Table [dbo].[Utilizadores_Cargos]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores_Cargos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPerfil] [int] NOT NULL,
	[idEntidade] [int] NOT NULL,
	[idTipoCargo] [int] NOT NULL,
	[email] [nvarchar](60) NULL,
	[dataInicio] [datetime] NOT NULL,
	[dataFim] [datetime] NULL,
	[ativo] [bit] NOT NULL,
	[dataCriado] [datetime] NOT NULL,
	[utilizadorCriado] [nvarchar](max) NOT NULL,
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Utilizadores_Cargos_2] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Utilizadores_Cargos] ADD  CONSTRAINT [DF__Utilizado__ativo__12B3B8EF]  DEFAULT ((1)) FOR [ativo]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos] ADD  CONSTRAINT [DF_Utilizadores_Cargos_dataCriado]  DEFAULT (getdate()) FOR [dataCriado]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos] ADD  CONSTRAINT [DF_Utilizadores_Cargos_utilizadorCriado]  DEFAULT (suser_sname()) FOR [utilizadorCriado]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Cargos_Entidades] FOREIGN KEY([idEntidade])
REFERENCES [dbo].[Entidades] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Cargos] CHECK CONSTRAINT [FK_Utilizadores_Cargos_Entidades]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Cargos_Utilizadores_Cargos_Tipos] FOREIGN KEY([idTipoCargo])
REFERENCES [dbo].[Utilizadores_Cargos_Tipos] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Cargos] CHECK CONSTRAINT [FK_Utilizadores_Cargos_Utilizadores_Cargos_Tipos]
GO
ALTER TABLE [dbo].[Utilizadores_Cargos]  WITH CHECK ADD  CONSTRAINT [FK_Utilizadores_Cargos_Utilizadores_Perfil] FOREIGN KEY([idPerfil])
REFERENCES [dbo].[Utilizadores_Perfil] ([id])
GO
ALTER TABLE [dbo].[Utilizadores_Cargos] CHECK CONSTRAINT [FK_Utilizadores_Cargos_Utilizadores_Perfil]
GO
