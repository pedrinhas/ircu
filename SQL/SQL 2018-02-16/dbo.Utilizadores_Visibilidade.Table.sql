USE [RCU]
GO
/****** Object:  Table [dbo].[Utilizadores_Visibilidade]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores_Visibilidade](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nome] [nvarchar](50) NOT NULL,
	[descricao] [nvarchar](255) NULL,
	[ativo] [bit] NOT NULL CONSTRAINT [DF_Utilizadores_Contactos_NivelAcesso_ativo]  DEFAULT ((1)),
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF__NivelAcesso__dataC__6225902D]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF__NivelAcesso__utili__4DB4832C]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Utilizadores_Contactos_NivelAcesso] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
