USE [RCU]
GO
/****** Object:  Table [dbo].[Aplicacoes]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Aplicacoes](
	[idAplicacao] [int] IDENTITY(1,1) NOT NULL,
	[Nome] [varchar](50) NOT NULL,
	[Titulo] [varchar](50) NOT NULL,
	[URL] [varchar](500) NOT NULL,
	[Descricao] [varchar](500) NULL,
	[Ativo] [bit] NOT NULL,
	[dataCriado] [datetime] NOT NULL DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK__Geral_Ap__3214EC07F03126C0] PRIMARY KEY CLUSTERED 
(
	[idAplicacao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
