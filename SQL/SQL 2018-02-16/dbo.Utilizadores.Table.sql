USE [RCU]
GO
/****** Object:  Table [dbo].[Utilizadores]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizadores](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[iupi] [uniqueidentifier] NULL,
	[nome] [nvarchar](255) NULL,
	[nomePersonalizado] [nvarchar](255) NULL,
	[nif] [nvarchar](20) NULL,
	[username] [nvarchar](50) NULL,
	[originalEmail] [nvarchar](60) NULL,
	[aliasedEmail] [nvarchar](60) NULL,
	[contaNoGIAF] [bit] NOT NULL,
	[visibilidade] [int] NOT NULL,
	[ativo] [bit] NOT NULL,
	[dataCriado] [datetime] NOT NULL CONSTRAINT [DF__Utilizado__dataC__52E34C9D]  DEFAULT (getdate()),
	[utilizadorCriado] [nvarchar](max) NOT NULL CONSTRAINT [DF__Utilizado__utili__4242D080]  DEFAULT (suser_sname()),
	[dataModificado] [datetime] NULL,
	[utilizadorModificado] [nvarchar](max) NULL,
 CONSTRAINT [PK_Utilizadores] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
