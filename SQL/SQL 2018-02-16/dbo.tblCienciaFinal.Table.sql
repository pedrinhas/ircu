USE [RCU]
GO
/****** Object:  Table [dbo].[tblCienciaFinal]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCienciaFinal](
	[id] [int] NOT NULL,
	[username] [nvarchar](50) NULL,
	[nmec] [nvarchar](50) NULL,
	[orcid] [nvarchar](50) NULL,
	[degois] [nvarchar](50) NULL
) ON [PRIMARY]

GO
