USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Perfil_R]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

/*

[dbo].[stp_Utilizador_Perfil_D] '30BC8B13-1C3A-4942-8BE1-2035EBB3FE8E', 766
[dbo].[stp_Utilizador_Perfil_R] '30BC8B13-1C3A-4942-8BE1-2035EBB3FE8E', 766

*/

--APENAS CRIA O UTILIZADOR. TEM DE SE CHAMAR OUTRO SP PARA O PERFIL
CREATE procEDURE [dbo].[stp_Utilizador_Perfil_R]
	  @iupi uniqueidentifier
	, @idPerfil int
	, @utilizadorEdicao nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	begin tran
	begin try
		update [dbo].[Utilizadores_Perfil] 
		set ativo = 1, utilizadorModificado = @utilizadorEdicao, dataModificado = getdate()
		from (SELECT id from [dbo].[Utilizadores] WHERE iupi = @iupi) u
		where idUtilizador = u.id and [Utilizadores_Perfil].id = @idPerfil
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
			,@iupi as IUPI
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END








GO
