USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Visibilidade_IU_TabelaEntrada]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


CREATE procEDURE [dbo].[stp_Utilizador_Visibilidade_IU_TabelaEntrada]
	  @visibilidades [dbo].[VisibilidadeType] readonly
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exist [dbo].[VisibilidadeType],
				@new [dbo].[VisibilidadeType]
		declare @result table(ID int, Accao char)

		insert into @exist
		select * 
		from @visibilidades r
		where r.id is not null and r.id in (select id from Utilizadores_Visibilidade)

		insert into @new
		select * 
		from @visibilidades r
		where r.id is null or r.id not in (select id from Utilizadores_Visibilidade)

		--existentes
		update VisibilidadesExistentes
		set VisibilidadesExistentes.nome = ExistentesParam.nome,
			VisibilidadesExistentes.descricao = ExistentesParam.descricao,
			VisibilidadesExistentes.dataModificado = getdate(),
			VisibilidadesExistentes.utilizadorModificado = ExistentesParam.UtilizadorEdicao
		from dbo.Utilizadores_Visibilidade VisibilidadesExistentes
		inner join @exist as ExistentesParam on VisibilidadesExistentes.id = ExistentesParam.id

		insert into @result
		select ID, 'U'
		from @exist

		--novos
		insert into dbo.Utilizadores_Visibilidade (nome, descricao, ativo, dataCriado, utilizadorCriado)
		select n.Nome, n.Descricao, 1, getdate(), n.UtilizadorEdicao
		from @new n

		insert into @result
		select ID, 'I'
		from @new

		select * from @result

		commit
		--rollback
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	
END










GO
