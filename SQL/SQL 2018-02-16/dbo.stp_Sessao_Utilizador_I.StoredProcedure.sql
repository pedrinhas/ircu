USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Sessao_Utilizador_I]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- stp_Sessao_Utilizador_I 'cpereira2', 'lol', 'Cláudio Pereira', 'cpereira@utad.pt'

CREATE procEDURE [dbo].[stp_Sessao_Utilizador_I] 
	@username nvarchar(50),
	@password nvarchar(max),
	@nome nvarchar(max),
	@email nvarchar(max),
	@alg nvarchar(10) = 'SHA2_512'
	, @utilizadorEdicao nvarchar(max)
AS
BEGIN
	begin tran
	begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;	

		declare @passwordHash nvarchar(131)
		declare @salt uniqueidentifier

		set @salt=newid()
		set @passwordHash =  UPPER(master.dbo.fn_varbintohexstr(HASHBYTES(@alg, CONCAT(@password, cast (@salt as nvarchar(max))))))

		insert into [dbo].[Sessao_Utilizador] (Username, Nome, Email, PasswordHash, Salt, dataCriado, utilizadorCriado)
		values (@username, @nome, @email, @passwordHash, @salt, GETDATE(), @utilizadorEdicao)

		commit
	end try
	begin catch
		    select ERROR_NUMBER() AS ErrorNumber
			,@Username as Username
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END








GO
