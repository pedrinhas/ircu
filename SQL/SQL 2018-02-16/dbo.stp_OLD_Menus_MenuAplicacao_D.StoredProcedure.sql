USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_OLD_Menus_MenuAplicacao_D]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_OLD_Menus_MenuAplicacao_D]
	-- Add the parameters for the stored procedure here
	@idMenu int
	, @utilizadorEdicao nvarchar(max)
AS
BEGIN
	begin try
	begin tran
	
		SET NOCOUNT ON;

		UPDATE [dbo].[Menus]
		SET Ativo = 0, utilizadorModificado = @utilizadorEdicao, dataModificado = getdate()
		WHERE idMenu = @idMenu

		commit

    end try
	begin catch
		rollback
	end catch


END








GO
