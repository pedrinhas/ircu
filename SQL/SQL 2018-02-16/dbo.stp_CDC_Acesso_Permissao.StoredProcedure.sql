USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_CDC_Acesso_Permissao]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_CDC_Acesso_Permissao]
AS
BEGIN
 
SELECT [__$start_lsn] as idTransacaoInicio
      ,[__$seqval]
	  ,[__$operation] as operationType
      ,case [__$operation] WHEN 1 THEN 'DELETE' WHEN 2 THEN 'INSERT' WHEN 3 THEN 'UPDATE_BEFORE' WHEN 4 THEN 'UPDATE_AFTER' end as operationTypeText
      ,[__$update_mask]	  
	  ,( SELECT    CC.column_name + ', '
          FROM      cdc.captured_columns CC
                    INNER JOIN cdc.change_tables CT ON CC.[object_id] = CT.[object_id]
          WHERE     CT.capture_instance = 'dbo_Acesso_Permissao'
                    AND sys.fn_cdc_is_bit_set(CC.column_ordinal,
                                              c.__$update_mask) = 1
        FOR
          XML PATH('')
        ) AS changedcolumns
      ,[idPermissao]
      ,[idUtilizador]
      ,[idVisibilidade]
	  ,[ativo]
      ,[dataCriado]
      ,[utilizadorCriado]
      ,[dataModificado]
      ,[utilizadorModificado]
  FROM [RCU].[cdc].[dbo_Acesso_Permissao_CT] c
END









GO
