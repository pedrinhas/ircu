USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Carreiras_Grupos_IU_TabelaEntrada]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


create procEDURE [dbo].[stp_Utilizador_Carreiras_Grupos_IU_TabelaEntrada]
	  @Carreiras_Grupos [dbo].[CarreiraGrupoType] readonly
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exist [dbo].[CarreiraGrupoType],
				@new [dbo].[CarreiraGrupoType]
		declare @result table(Codigo nvarchar(20), ID int, Accao char)

		insert into @exist
		select * 
		from @Carreiras_Grupos r
		where r.id is not null and r.id in (select id from Utilizadores_Carreiras_Grupos)

		insert into @new
		select * 
		from @Carreiras_Grupos r
		where r.id is null or r.id not in (select id from Utilizadores_Carreiras_Grupos)

		--existentes
		update Carreiras_GruposExistentes
		set Carreiras_GruposExistentes.codigo = ExistentesParam.codigo,
			Carreiras_GruposExistentes.nome = ExistentesParam.nome,
			Carreiras_GruposExistentes.descricao = ExistentesParam.descricao,
			Carreiras_GruposExistentes.dataModificado = getdate(),
			Carreiras_GruposExistentes.utilizadorModificado = ExistentesParam.UtilizadorEdicao
		from dbo.Utilizadores_Carreiras_Grupos Carreiras_GruposExistentes
		inner join @exist as ExistentesParam on Carreiras_GruposExistentes.id = ExistentesParam.id

		insert into @result
		select Codigo, ID, 'U'
		from @exist

		--novos
		insert into dbo.Utilizadores_Carreiras_Grupos (codigo, nome, descricao, ativo, dataCriado, utilizadorCriado)
		select n.Codigo, n.Nome, n.Descricao, 1, getdate(), n.UtilizadorEdicao
		from @new n

		insert into @result
		select Codigo, ID, 'I'
		from @new

		select * from @result

		commit
		--rollback
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	
END










GO
