USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Tools_DropDefaultValue]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[stp_Tools_DropDefaultValue]
 @tabela varchar(max),
 @coluna varchar(max)
 as
 DECLARE @tableName VARCHAR(MAX) = @tabela
DECLARE @columnName VARCHAR(MAX) = @coluna
DECLARE @ConstraintName nvarchar(200)
SELECT @ConstraintName = Name 
FROM SYS.DEFAULT_CONSTRAINTS
WHERE PARENT_OBJECT_ID = OBJECT_ID(@tableName) 
AND PARENT_COLUMN_ID = (
    SELECT column_id FROM sys.columns
    WHERE NAME = @columnName AND object_id = OBJECT_ID(@tableName))
IF @ConstraintName IS NOT NULL
    EXEC('ALTER TABLE '+@tableName+' DROP CONSTRAINT ' + @ConstraintName)







GO
