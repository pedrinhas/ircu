USE [RCU]
GO
/****** Object:  UserDefinedTableType [dbo].[EntidadeType]    Script Date: 16/02/2018 16:41:49 PM ******/
CREATE TYPE [dbo].[EntidadeType] AS TABLE(
	[ID] [int] NULL,
	[Codigo] [nvarchar](50) NULL,
	[Sigla] [nvarchar](50) NULL,
	[Nome] [nvarchar](50) NULL,
	[Descricao] [nvarchar](max) NULL,
	[Email] [nvarchar](60) NULL,
	[UtilizadorEdicao] [nvarchar](max) NOT NULL
)
GO
