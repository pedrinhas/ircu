USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Categorias_R]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Utilizador_Categorias_R]
	@id int,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
	begin try
	begin tran
	
		SET NOCOUNT ON;

		UPDATE [dbo].[Utilizadores_Categorias]
		SET Ativo = 1, utilizadorModificado = @utilizadorEdicao, dataModificado = getdate()
		WHERE id = @id

		commit

    end try
	begin catch
		rollback
	end catch

END








GO
