USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Contactos_PorVisibilidade_LS_DEBUG]    Script Date: 16/02/2018 16:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- [dbo].[stp_Contactos_LS]
-- [dbo].[stp_Contactos_PorVisibilidade_LS_DEBUG] 0
CREATE procEDURE [dbo].[stp_Contactos_PorVisibilidade_LS_DEBUG]
	@visibilidade int,
	@incluirApagados bit = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @contactos table (id int, idPerfil int, username nvarchar(50), nome nvarchar(max), UO nvarchar(50), categoria nvarchar(50), contactGroup nvarchar(50), label nvarchar(50), value nvarchar(max), visibilidadeContacto int, visibilidadeContactoNome nvarchar(50), visibilidadeUtilizador int, visibilidadeUtilizadorNome nvarchar(50), visibilidadePerfil int, visibilidadePerfilNome nvarchar(50), ativo int)

	insert into @contactos
	SELECT 
	  u.id
	  ,p.id as idPerfil
      ,u.username 
      ,dbo.InitCap(u.[nome]) as nome
      --,u.[email]    
      ,dbo.InitCap(ent.nome) as UO
      ,dbo.InitCap(cat.nome) as categoria
	  ,isnull(grupoC.tmp_oldText, grupoc.grupo) as contactGroup
	  ,isnull(tipoC.tmp_oldText, tipoc.tipo) as label
	  ,c.propertyval as value
	  ,c.visibilidade as visibilidadeContacto
	  ,visContacto.nome as visibilidadeContactoNome
	  ,u.visibilidade as visibilidadeUtilizador
	  ,visUser.nome as visibilidadeUtilizadorNome
	  ,p.visibilidade as visibilidadePerfil
	  ,visPerfil.nome as visibilidadePerfilNome
	  ,c.ativo
  FROM [dbo].[Utilizadores] u 
  --left join tblContactos4UTADSite c on c.username=u.nomedeutilizador
  left join [dbo].[Utilizadores_Perfil] p on p.idutilizador = u.id
  left join [dbo].[Utilizadores_Carreiras] carr on p.idCarreira = carr.id
  left join [dbo].[Utilizadores_Categorias] cat on cat.id = p.idCategoria
  left join [dbo].[Entidades] ent on ent.id = p.idEntidade
  left join [dbo].[Utilizadores_Contactos] c on c.idUtilizador = u.id AND c.visibilidade >= 1 
  left join [dbo].[Utilizadores_Contactos_Tipos] tipoC on tipoC.id = c.idTipo
  left join [dbo].[Utilizadores_Contactos_Tipos_Grupos] grupoC on grupoC.id = tipoC.idGrupo
  left join [dbo].[Utilizadores_Visibilidade] visContacto on visContacto.id = c.visibilidade
  left join [dbo].[Utilizadores_Visibilidade] visUser on visUser.id = u.visibilidade
  left join [dbo].[Utilizadores_Visibilidade] visPerfil on visPerfil.id = u.visibilidade
  where carr.codigo not in ('00') and u.username is not null and u.username <>''
  and p.dataSaida is null
  and u.visibilidade >= 1
  and p.visibilidade >= 1
  and c.idTipo <> 14
  --and c.propertyval is not null

  insert into @contactos
  SELECT 
	  u.id
	  ,p.id as idPerfil
      ,u.username
      ,dbo.InitCap(u.[nome]) as nome
      --,u.[email]    
      ,dbo.InitCap(ent.nome) as UO
      ,dbo.InitCap(cat.nome) as categoria
	  ,'contactos' as contactGroup
	  ,'Email' as label
	  ,coalesce(u.aliasedEmail, u.originalEmail) as value
	  ,CONVERT(int, flagEmail.value) as visibilidadeContacto
	  ,visEmail.nome as visibilidadeContactoNome
	  ,u.visibilidade as visibilidadeUtilizador
	  ,visUser.nome as visibilidadeUtilizadorNome
	  ,p.visibilidade as visibilidadePerfil
	  ,visPerfil.nome as visibilidadePerfilNome
	  ,1 as ativo
  FROM [dbo].[Utilizadores] u 
  --left join tblContactos4UTADSite c on c.username=u.nomedeutilizador
  left join [dbo].[Utilizadores_Perfil] p on p.idutilizador = u.id
  left join [dbo].[Utilizadores_Carreiras] carr on p.idCarreira = carr.id
  left join [dbo].[Utilizadores_Categorias] cat on cat.id = p.idCategoria
  left join [dbo].[Entidades] ent on ent.id = p.idEntidade
  left join [dbo].[Utilizadores_Visibilidade] visUser on visUser.id = u.visibilidade
  left join [dbo].[Utilizadores_Visibilidade] visPerfil on visPerfil.id = u.visibilidade
  left join [dbo].[Utilizadores_Flags] flagEmail on flagEmail.idUtilizador = u.id and flagEmail.idFlag = 5
  left join [dbo].[Utilizadores_Visibilidade] visEmail on visEmail.id = CONVERT(int, flagEmail.value)
  where carr.codigo not in ('00') and u.username is not null and u.username <>''
  and p.dataSaida is null
  and u.visibilidade >= 1
  and p.visibilidade >= 1


  select * from @contactos
  order by nome collate Latin1_General_CI_AI
  --order by não liga ao collation


END

/****** Object:  StoredProcedure [dbo].[stp_Contactos_LS]    Script Date: 06/02/2018 15:04:32 PM ******/
SET ANSI_NULLS ON

--SELECT 
--	  u.id
--	  ,p.id as idPerfil
--      ,u.username
--      ,dbo.InitCap(u.[nome]) as nome
--      --,u.[email]    
--      ,dbo.InitCap(ent.nome) as UO
--      ,dbo.InitCap(cat.nome) as categoria
--	  ,'contactos' as contactGroup
--	  ,'Email' as label
--	  ,coalesce(u.aliasedEmail, u.originalEmail) as value
--	  ,CONVERT(int, flagEmail.value) as visibilidadeContacto
--	  ,visEmail.nome as visibilidadeContactoNome
--	  ,u.visibilidade as visibilidadeUtilizador
--	  ,visUser.nome as visibilidadeUtilizadorNome
--	  ,p.visibilidade as visibilidadePerfil
--	  ,visPerfil.nome as visibilidadePerfilNome
--	  ,1 as ativo
--  FROM [dbo].[Utilizadores] u 
--  --left join tblContactos4UTADSite c on c.username=u.nomedeutilizador
--  left join [dbo].[Utilizadores_Perfil] p on p.idutilizador = u.id
--  left join [dbo].[Utilizadores_Carreiras] carr on p.idCarreira = carr.id
--  left join [dbo].[Utilizadores_Categorias] cat on cat.id = p.idCategoria
--  left join [dbo].[Entidades] ent on ent.id = p.idEntidade
--  left join [dbo].[Utilizadores_Visibilidade] visUser on visUser.id = u.visibilidade
--  left join [dbo].[Utilizadores_Visibilidade] visPerfil on visPerfil.id = u.visibilidade
--  left join [dbo].[Utilizadores_Flags] flagEmail on flagEmail.idUtilizador = u.id and flagEmail.idFlag = 5
--  left join [dbo].[Utilizadores_Visibilidade] visEmail on visEmail.id = CONVERT(int, flagEmail.value)
--  where carr.codigo not in ('00') and u.username is not null and u.username <>''
--  and p.dataSaida is null
--  and u.visibilidade >= 1
--  and p.visibilidade >= 1
--  --and u.id 




GO
