USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Flags_IU_TabelaEntrada_DEBUG]    Script Date: 16/02/2018 16:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*

declare @p1 dbo.UtilizadorFlagType
insert into @p1 values(0,2305,NULL,5,N'1',N'IRCU - Website Perfil - 30BC8B13-1C3A-4942-8BE1-2035EBB3FE8E')

exec [stp_Utilizador_Flags_IU_TabelaEntrada_DEBUG] @flags=@p1

*/


CREATE procEDURE [dbo].[stp_Utilizador_Flags_IU_TabelaEntrada_DEBUG]
	  @flags [dbo].[UtilizadorFlagType] readonly
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exist [dbo].[UtilizadorFlagType],
				@new [dbo].[UtilizadorFlagType]
		declare @result table(IDUtilizador int, IDPerfil int, IDFlag int, Accao char)

		--insert into @exist
		select * 
		from @flags f
		where f.idFlag is not null and f.idFlag in (
			select t_f.idFlag from Utilizadores_Flags t_f
			inner join @flags fParam on (fParam.idUtilizador = t_f.idUtilizador AND fParam.idPerfil = t_f.idPerfil AND fParam.idFlag = t_f.idFlag)
									 or (fParam.idUtilizador = t_f.idUtilizador AND fParam.idPerfil is null AND fParam.idFlag = t_f.idFlag)
		)

		--insert into @new
		select * 
		from @flags f
		where f.idFlag is null or f.idFlag not in (
			select t_f.idFlag from Utilizadores_Flags t_f
			inner join @flags fParam on (fParam.idUtilizador = t_f.idUtilizador AND fParam.idPerfil = t_f.idPerfil AND fParam.idFlag = t_f.idFlag)
									 or (fParam.idUtilizador = t_f.idUtilizador AND fParam.idPerfil is null AND fParam.idFlag = t_f.idFlag)
		)

		--select * from @new
		--select * from @exist

		--existentes
		--update FlagsExistentes
		--set FlagsExistentes.idFlag = ExistentesParam.idFlag,
		--	FlagsExistentes.Value = ExistentesParam.Value,
		--	FlagsExistentes.dataModificado = getdate(),
		--	FlagsExistentes.utilizadorModificado = ExistentesParam.UtilizadorEdicao
		--from dbo.Utilizadores_Flags FlagsExistentes
		--inner join @exist as ExistentesParam on FlagsExistentes.IDUtilizador = ExistentesParam.IDUtilizador AND FlagsExistentes.IDPerfil = ExistentesParam.IDPerfil AND FlagsExistentes.idFlag = ExistentesParam.idFlag


		--insert into @result
		--select IDUtilizador, IDPerfil, idFlag, 'U'
		--from @exist

		----novos
		--insert into Utilizadores_Flags (idUtilizador, idPerfil, idFlag, value, ativo, dataCriado, utilizadorCriado)
		--select n.IDUtilizador, n.IDPerfil, n.idFlag, n.Value, 1, getdate(), n.UtilizadorEdicao
		--from @new n

		--insert into @result
		--select IDUtilizador, IDPerfil, idFlag, 'I'
		--from @new

		--select * from @result

		--commit
		rollback
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	
END












GO
