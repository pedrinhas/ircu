--use master
--go
--alter database RCU add filegroup [CDC]
--go

--alter database [RCU] add file (NAME = N'RCU_cdc', filename = N'c:\cdc file\rcu_cdc.ndf', SIZE = 2048MB, FILEGROWTH = 128MB)  TO FILEGROUP [CDC]

--use RCU
--go
--ALTER DATABASE [RCU] MODIFY FILEGROUP [CDC] DEFAULT
--GO

--exec sys.sp_cdc_enable_db
--go

--alter database RCU modify filegroup [PRIMARY] default

--use master
--go
--select [name], database_id, is_cdc_enabled
--from sys.databases
--where is_cdc_enabled = 1
--go

--use RCU
--go
--SELECT [name], is_tracked_by_cdc  
--FROM sys.tables 
--WHERE is_tracked_by_cdc=1
--GO   

EXEC sys.sp_cdc_enable_table   
@source_schema = N'dbo',
@source_name   = N'Utilizadores_Carreiras',
@role_name     = NULL,
@filegroup_name = N'CDC',
@supports_net_changes = 0
GO
/*

EXEC sys.sp_cdc_disable_table   
@source_schema = N'dbo',
@source_name   = N'Utilizadores_Cargos_Tipos',
@capture_instance     = 'dbo_Utilizadores_Cargos'
GO

*/

EXEC sys.sp_cdc_enable_table @source_schema = N'dbo', @source_name   = [name], @role_name     = NULL, @filegroup_name =  'CDC', @supports_net_changes = 0 GO

SELECT [name], is_tracked_by_cdc  
FROM sys.tables 
WHERE is_tracked_by_cdc=1
order by name
GO


SELECT 'EXEC sys.sp_cdc_enable_table @source_schema = N''dbo'', @source_name   = ' + [name] + ', @role_name     = NULL, @filegroup_name =  ''CDC'', @supports_net_changes = 0 GO'
FROM sys.tables 
WHERE is_tracked_by_cdc=1
order by name
GO


