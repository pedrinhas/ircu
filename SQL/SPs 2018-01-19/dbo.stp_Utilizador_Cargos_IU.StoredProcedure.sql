USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Cargos_IU]    Script Date: 19/01/2018 16:20:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Utilizador_Cargos_IU]
	@idCargo int,
	@idPerfil int,
	@idEntidade int,
	@idTipoCargo int,
	@email nvarchar(60),
	@dataInicio datetime,
	@dataFim datetime,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin tran
	begin try
		declare @exists bit
		set @exists = 0

		select @exists = coalesce(c.id, case when c.id is null then 0 when c.id is not null then 1 end) 
		from [dbo].[Utilizadores_Cargos] c
		where @idCargo is not null and c.id = @idCargo


		if @exists = 1 and @idCargo is not null
		begin
			update [dbo].[Utilizadores_Cargos]
			set idPerfil = @idPerfil, @idEntidade = idEntidade, idTipoCargo = @idTipoCargo, email = @email, dataInicio = @dataInicio, dataFim = @dataFim, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
			where id = @idCargo

			select @idCargo
		end
		else
		begin
			insert into [dbo].[Utilizadores_Cargos] (idPerfil, idEntidade, idTipoCargo, email, ativo, dataInicio, dataFim, dataCriado, utilizadorCriado)
			values (@idPerfil, @idEntidade, @idTipoCargo, @email, 1, @dataInicio, @dataFim, getdate(), @utilizadorEdicao)

			select max(id) from [Utilizadores_Cargos]
		end

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@idPerfil as idPerfil
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	end



GO
