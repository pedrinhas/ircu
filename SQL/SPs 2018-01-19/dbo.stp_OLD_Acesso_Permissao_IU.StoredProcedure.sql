USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_OLD_Acesso_Permissao_IU]    Script Date: 19/01/2018 16:20:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE procEDURE [dbo].[stp_OLD_Acesso_Permissao_IU]
	@id int,
	@idUtilizador int,
	@idNivelAcesso int,
	@ativo bit, 
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exists bit
		set @exists = 0

		select @exists = coalesce(a.idPermissao, case when a.idPermissao is null then 0 when a.idPermissao is not null then 1 end) 
		from [dbo].[Acesso_Permissao] a
		where a.idNivelAcesso = @id

		if @exists = 1 and @id is not null
		begin
			update [dbo].[Acesso_Permissao]
			set idUtilizador = @idUtilizador, idNivelAcesso = @idNivelAcesso, ativo = @ativo, utilizadorModificado = @utilizadorEdicao
			where idPermissao = @id

			select @id
		end
		else
		begin

			insert into [dbo].[Acesso_Permissao] (idUtilizador, idNivelAcesso, Ativo, dataCriado, utilizadorCriado)
			values (@idUtilizador, @idNivelAcesso, 1, getdate(), @utilizadorEdicao)

			select max(idNivelAcesso) from [dbo].[Acesso_NivelAcesso]
		end

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@id as idPermissao
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END




GO
