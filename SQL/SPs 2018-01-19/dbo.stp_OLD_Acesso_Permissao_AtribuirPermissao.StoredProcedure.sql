USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_OLD_Acesso_Permissao_AtribuirPermissao]    Script Date: 19/01/2018 16:20:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--[dbo].[stp_Acesso_Permissao_AtribuirPermissao] 'DC27A15B-0020-49FA-9B91-0ADE9B0B0F51', 2, 'cpereira'

CREATE procEDURE [dbo].[stp_OLD_Acesso_Permissao_AtribuirPermissao]
	@iupi uniqueidentifier,
	@idNivelAcesso int,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;

	declare @idUser int
	select @idUser = [id] 
	from [dbo].[Utilizadores] 
	where [iupi] = @iupi
	 
	if @idUser is not null
	begin
		begin tran
		begin try
			exec [dbo].[stp_Acesso_Permissao_IU] 0, @idUser, @idNivelAcesso, 1, @utilizadorEdicao

			commit
		end try
		begin catch
			select ERROR_NUMBER() AS ErrorNumber
			,@iupi as IUPI
			,@idNivelAcesso as idNivelAcesso
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
			--rollback
		end catch
	end
END



GO
