USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Perfil_IU]    Script Date: 19/01/2018 16:20:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- alter date: <alter Date,,>
-- Description:	<Description,,>
-- =============================================

/*

exec [stp_Utilizador_Perfil_IU] @iupi='0F65F7B2-4FCF-4FF9-AF7B-7BCB98EA5DA5',@idPerfil=-1,@idTipo=12,@idCategoria=36,@idEntidade=5,@idCarreira=9224,@idSituacao=1103,@idRegime=74,@utilizadorEdicao=N'RCUMaestroSynchronizer'

*/

CREATE procEDURE [dbo].[stp_Utilizador_Perfil_IU]
	  @iupi uniqueidentifier
	, @username nvarchar(50)
	, @email nvarchar(60) = null
	, @nmec nvarchar(10)
	, @idPerfil int
	, @idTipo int = null
	, @idCategoria int = null
	, @idEntidade int = null
	, @idCarreira int = null
	, @idSituacao int = null
	, @idRegime int = null
	, @dataEntrada datetime = null
	, @dataSaida datetime = null
	, @utilizadorEdicao nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	begin tran
	begin try
		declare @exists bit
		set @exists = 0

		select @exists = coalesce(p.id, case when p.id is null then 0 when p.id is not null then 1 end) 
		from [dbo].[Utilizadores_Perfil] p
		left join [dbo].[Utilizadores] u on u.id = p.idUtilizador
		where @idPerfil is not null and p.id = @idPerfil

		if @exists = 1 and @idPerfil is not null
		begin
			update [dbo].[Utilizadores_Perfil]
			set idTipo = @idTipo, username = @username, email = @email, nmec = @nmec, idCategoria = @idCategoria, idEntidade = @idEntidade, idCarreira = @idCarreira, idSituacao = @idSituacao, idRegime = @idRegime, dataEntrada = @dataEntrada, dataSaida = @dataSaida, dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
			where id = @idPerfil

			select @idPerfil
		end
		else
		begin
			declare @idUser int
			exec [dbo].[stp_Utilizador_GetID_OutputVar] @iupi, @idUser output

			--select @idUser = id
			--from [dbo].Utilizadores
			--where iupi = @iupi

			insert into [dbo].[Utilizadores_Perfil] (idUtilizador, username, email, nmec, idTipo, idCategoria, idEntidade, idCarreira, idSituacao, idRegime, dataEntrada, dataSaida, ativo, dataCriado, utilizadorCriado)
			values (@idUser, @username, @email, @nmec, @idTipo, @idCategoria, @idEntidade, @idCarreira, @idSituacao, @idRegime, @dataentrada, @dataSaida, 1, getdate(), @utilizadorEdicao)

			select max(id) from Utilizadores_Perfil
		end

			
		commit
	end try
	begin catch

		select ERROR_NUMBER() AS ErrorNumber
			,@iupi as IUPI
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END


GO
