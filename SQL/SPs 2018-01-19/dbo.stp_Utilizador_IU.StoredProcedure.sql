USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_IU]    Script Date: 19/01/2018 16:20:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

/*

stp_Utilizador_U '30BC8B13-1C3A-4942-8BE1-2035EBB3FE8E', 'jborges', 'Borges', '345345'
stp_Utilizador_IU null, 'jborges', 'Borges', '345345'

*/

--APENAS CRIA O UTILIZADOR. TEM DE SE CHAMAR OUTRO SP PARA O PERFIL
CREATE procEDURE [dbo].[stp_Utilizador_IU]
	  @iupi uniqueidentifier = null
	, @nome nvarchar(255)
	, @nomePersonalizado nvarchar(255)
	, @nif nvarchar(20)
	, @contaNoGIAF bit
	, @utilizadorEdicao nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	begin tran
	begin try
		declare @exists bit
		set @exists = 0
		select @exists = coalesce(id, case when id is null then 0 when id is not null then 1 end) 
		from [dbo].[Utilizadores]
		where @iupi is not null and iupi = @iupi

		if @exists = 1 and @iupi is not null
		begin
			update [dbo].[Utilizadores]
			set nome = @nome, nomePersonalizado = @nomePersonalizado, nif = @nif, dataModificado = getdate(), contaNoGIAF = @contaNoGIAF, utilizadorModificado = @utilizadorEdicao
			where iupi = @iupi

			select @iupi
		end
		else
		begin
			set @iupi = newid()

			insert into [dbo].[Utilizadores] (iupi, nome, nomePersonalizado, nif, contaNoGIAF, ativo, dataCriado, utilizadorCriado)
			values (@iupi, @nome, @nomePersonalizado, @nif, @contaNoGIAF, 1, getdate(), @utilizadorEdicao)
				
			select @iupi
		end
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
			,@iupi as IUPI
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END




GO
