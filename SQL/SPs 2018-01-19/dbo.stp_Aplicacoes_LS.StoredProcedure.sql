USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Aplicacoes_LS]    Script Date: 19/01/2018 16:20:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procEDURE [dbo].[stp_Aplicacoes_LS]
	@incluirApagados bit = 0
AS
	SELECT a.idAplicacao
	, a.nome
	, a.titulo
	, a.URL
	, a.descricao
	, g.idGrupo
	, g.nome
	, a.ativo
	, a.dataCriado
	, a.utilizadorCriado
	, a.dataModificado
	, a.utilizadorModificado
	from [dbo].[Aplicacoes] a
	left join [dbo].[Aplicacoes_AplicacaoGrupo] as ag on a.idAplicacao = ag.idAplicacao
	left join [dbo].[Aplicacoes_Grupo] as g on g.Idgrupo = ag.idGrupo
	where (a.ativo = 1 or a.ativo <> @incluirApagados)



GO
