USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Perfil_Tipos_IU_TabelaEntrada]    Script Date: 19/01/2018 16:20:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


create procEDURE [dbo].[stp_Utilizador_Perfil_Tipos_IU_TabelaEntrada]
	  @Perfil_Tipos [dbo].[PerfilTipoType] readonly
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exist [dbo].[PerfilTipoType],
				@new [dbo].[PerfilTipoType]
		declare @result table(Codigo nvarchar(20), ID int, Accao char)

		insert into @exist
		select * 
		from @Perfil_Tipos r
		where r.id is not null and r.id in (select id from Utilizadores_Perfil_Tipo)

		insert into @new
		select * 
		from @Perfil_Tipos r
		where r.id is null or r.id not in (select id from Utilizadores_Perfil_Tipo)

		--existentes
		update Perfil_TiposExistentes
		set Perfil_TiposExistentes.codigo = ExistentesParam.codigo,
			Perfil_TiposExistentes.sigla = ExistentesParam.sigla,
			Perfil_TiposExistentes.nome = ExistentesParam.nome,
			Perfil_TiposExistentes.descricao = ExistentesParam.descricao,
			Perfil_TiposExistentes.dataModificado = getdate(),
			Perfil_TiposExistentes.utilizadorModificado = ExistentesParam.UtilizadorEdicao
		from dbo.Utilizadores_Perfil_Tipo Perfil_TiposExistentes
		inner join @exist as ExistentesParam on Perfil_TiposExistentes.id = ExistentesParam.id

		insert into @result
		select Codigo, ID, 'U'
		from @exist

		--novos
		insert into dbo.Utilizadores_Perfil_Tipo (codigo, sigla, nome, descricao, ativo, dataCriado, utilizadorCriado)
		select n.Codigo, n.Sigla, n.Nome, n.Descricao, 1, getdate(), n.UtilizadorEdicao
		from @new n

		insert into @result
		select Codigo, ID, 'I'
		from @new

		select * from @result

		commit
		--rollback
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
	
END





GO
