USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Entidades_IU]    Script Date: 19/01/2018 16:20:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- exec [stp_Entidade_IU] 4

CREATE procEDURE [dbo].[stp_Entidades_IU]
	@id int,
	@codigo nvarchar(50),
	@sigla nvarchar(50) = null,
	@nome nvarchar(50),
	@descricao nvarchar(max) = null,
	@idTipoEntidade int = null,
	@idParent int = null,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin tran
	begin try
		declare @exists bit
		set @exists = 0

		select @exists = coalesce(a.[id], case when a.[id] is null then 0 when a.[id] is not null then 1 end) 
		from [dbo].[Entidades] a
		where a.[id] = @id

		if @exists = 1 and @id is not null
		begin
			update [dbo].[Entidades]
			set codigo = @codigo, sigla = @sigla, nome = @nome, descricao = @descricao, idTipoEntidade = @idTipoEntidade, idParent = @idParent, utilizadorModificado = @utilizadorEdicao
			where id = @id

			select @id
		end
		else
		begin

			insert into [dbo].[Entidades] (codigo, sigla, nome, descricao, idTipoEntidade, idParent, Ativo, dataCriado, utilizadorCriado)
			values (@codigo, @sigla, @nome, @descricao, @idTipoEntidade, @idParent, 1, getdate(), @utilizadorEdicao)

			select max(id) from [dbo].[Entidades]
		end

			
		commit
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@id as idEntidade
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END



GO
