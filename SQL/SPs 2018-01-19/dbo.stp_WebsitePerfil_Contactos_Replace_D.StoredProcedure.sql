USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_WebsitePerfil_Contactos_Replace_D]    Script Date: 19/01/2018 16:20:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

-- [stp_WebsitePerfil_Contactos_Replace_D] 'C74BA100-27F5-49AB-99C9-4814C7F3A8D1', 'cpereira'

CREATE procEDURE [dbo].[stp_WebsitePerfil_Contactos_Replace_D]
	@iupi uniqueidentifier,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;

	begin try
		begin tran
		
		declare @idUser int
		select @idUser = [id] 
		from [dbo].[Utilizadores] 
		where [iupi] = @iupi

		update [Utilizadores_Contactos]
		set dataModificado = getdate(), utilizadorModificado = @utilizadorEdicao
		where idUtilizador = @idUser

		DELETE FROM [dbo].[Utilizadores_Contactos]
		WHERE idUtilizador = @idUser

		commit

		select 1
	end try
	begin catch
		select ERROR_NUMBER() AS ErrorNumber
		,@idUser as idUtilizador
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
		rollback

		select 0
	end catch
END


GO
