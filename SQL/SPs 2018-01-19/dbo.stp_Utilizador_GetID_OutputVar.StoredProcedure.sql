USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_GetID_OutputVar]    Script Date: 19/01/2018 16:20:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

--exec [stp_Utilizador_GetID] '30BC8B13-1C3A-4942-8BE1-2035EBB3FE8E'

CREATE procEDURE [dbo].[stp_Utilizador_GetID_OutputVar]
	@iupi uniqueidentifier
	, @id int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select @id = max(u.id) from [dbo].[Utilizadores] u
	where u.iupi = @iupi
END


GO
