USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Sessao_DoLogin]    Script Date: 19/01/2018 16:20:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- [stp_Sessao_DoLogin] 'cpereira2', 'lel'

CREATE procEDURE [dbo].[stp_Sessao_DoLogin] 
	@username nvarchar(50),
	@password nvarchar(max)
AS
BEGIN
	begin tran
	begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;	

		declare @passwordHash nvarchar(131)
		declare @salt uniqueidentifier
		declare @alg nvarchar(10)

		set @alg = (SELECT Algoritmo FROM [dbo].[Sessao_Utilizador] WHERE Username = @username)
		set @salt = (SELECT Salt FROM [dbo].[Sessao_Utilizador] WHERE Username = @username)
		set @passwordHash =  UPPER(master.dbo.fn_varbintohexstr(HASHBYTES(@alg, CONCAT(@password, cast (@salt as nvarchar(max))))))

		SELECT [idUtilizador]
			  ,[Username]
			  ,[Nome]
			  ,[Email]
		  FROM [RCU].[dbo].[Sessao_Utilizador]
		 WHERE Username = @username
		   AND PasswordHash = @passwordHash
		   AND Ativo = 1

		commit
	end try
	begin catch
		    select ERROR_NUMBER() AS ErrorNumber
			,@Username as Username
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
		rollback
	end catch
END


GO
