USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Contactos_LS]    Script Date: 19/01/2018 16:20:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_Contactos_LS]
	@incluirApagados bit = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
      p.username
      ,dbo.InitCap(u.[nome]) as nome
      --,u.[email]    
      ,dbo.InitCap(ent.nome) as UO
      ,dbo.InitCap(cat.nome) as categoria
	  ,isnull(grupoC.tmp_oldText, grupoc.grupo) as contactGroup,isnull(tipoC.tmp_oldText, tipoc.tipo) as label,c.propertyval as value, c.ativo
  FROM [dbo].[Utilizadores] u 
  --left join tblContactos4UTADSite c on c.username=u.nomedeutilizador
  left join [dbo].[Utilizadores_Perfil] p on p.idutilizador = u.id
  left join [dbo].[Utilizadores_Carreiras] carr on p.idCarreira = carr.id
  left join [dbo].[Utilizadores_Categorias] cat on cat.id = p.idCategoria
  left join [dbo].[Entidades] ent on ent.id = p.idEntidade
  left join [dbo].[Utilizadores_Contactos] c on c.idUtilizador = u.id
  left join [dbo].[Utilizadores_Contactos_Tipos] tipoC on tipoC.id = c.idTipo
  left join [dbo].[Utilizadores_Contactos_Tipos_Grupos] grupoC on grupoC.id = tipoC.idGrupo
  where carr.codigo not in ('00') and p.username is not null and p.username <>''
  and c.propertyval is not null
  and p.dataSaida is null
  and (c.ativo = 1 or c.ativo <> @incluirApagados)
END
GO
