USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Categorias_LS]    Script Date: 19/01/2018 16:20:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[stp_Utilizador_Categorias_LS]
	@incluirApagados bit = 0
	as
	SET NOCOUNT ON;
SELECT [id]
      ,[codigo]
      ,[nome]
      ,[descricao]
      ,[ativo]
	  ,[idCarreira]
      ,[dataCriado]
      ,[utilizadorCriado]
      ,[dataModificado]
      ,[utilizadorModificado]
  FROM [dbo].[Utilizadores_Categorias] c
  where (c.ativo = 1 or c.ativo <> @incluirApagados)





GO
