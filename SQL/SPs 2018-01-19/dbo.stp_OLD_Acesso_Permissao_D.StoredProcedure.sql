USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_OLD_Acesso_Permissao_D]    Script Date: 19/01/2018 16:20:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_OLD_Acesso_Permissao_D]
	-- Add the parameters for the stored procedure here
	@id int
	, @utilizadorEdicao nvarchar(max)
AS
BEGIN
	begin try
	begin tran
	
		SET NOCOUNT ON;

		UPDATE [dbo].[Acesso_Permissao]
		SET Ativo = 0, utilizadorModificado = @utilizadorEdicao, dataModificado = getdate()
		WHERE idPermissao = @id

		commit

    end try
	begin catch
		rollback
	end catch


END


GO
