USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_OLD_Acesso_NivelAcesso_LS]    Script Date: 19/01/2018 16:20:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- exec [stp_Acesso_NivelAcesso_IU] 0, 4, 'Nivel teste', 1, 'cpereira'

CREATE procEDURE [dbo].[stp_OLD_Acesso_NivelAcesso_LS]
	@incluirApagados bit = 0
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [idNivelAcesso]
      ,[NomeNivel]
	  ,nivel.ativo as nivelAtivo
      ,app.[idAplicacao]
	  ,app.Nome as aplicacaoNome
      ,app.[ativo] as aplicacaoAtivo
      ,app.[dataCriado] as appDataCriado
      ,app.[utilizadorCriado] as appUtilizadorCriado
      ,app.[dataModificado] as appDataModificado
      ,app.[utilizadorModificado] as appUtilizadorModificado
      ,nivel.[dataCriado] as nivelDataCriado
      ,nivel.[utilizadorCriado] as nivelUtilizadorCriado
      ,nivel.[dataModificado] as nivelDataModificado
      ,nivel.[utilizadorModificado] as nivelUtilizadorModificado
  FROM [dbo].[Acesso_NivelAcesso] nivel
  left join [dbo].[Aplicacoes] app on app.idAplicacao = nivel.idAplicacao
  where (nivel.ativo = 1 or nivel.ativo <> @incluirApagados)
  AND (app.ativo = 1 or app.ativo <> @incluirApagados)
END



GO
