USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Sessao_Utilizador_Password_Reset]    Script Date: 19/01/2018 16:20:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[stp_Sessao_Utilizador_Password_Reset]
	@login nvarchar(max),
	@alg nvarchar(10) = 'SHA2_512'
	, @utilizadorEdicao nvarchar(max)
as
	declare @newSalt uniqueidentifier,
			@pwdBytes binary(64),
			@password nvarchar(max)

	set @newSalt = newid()

	exec [stp_Tools_GeneratePassword] @password OUTPUT

	exec stp_Sessao_Utilizador_Password_Modify @login, @password, @alg, @utilizadorEdicao

	select @password

	/*
	declare @pw nvarchar(16)

	exec stp_Sessao_WebService_User_ResetPassword 'sis-dev', @pw output

	select @pw
	
	*/

GO
