USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Contactos_S]    Script Date: 19/01/2018 16:20:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec stp_Utilizador_Contactos_S '30BC8B13-1C3A-4942-8BE1-2035EBB3FE8E'


CREATE proc [dbo].[stp_Utilizador_Contactos_S]
	@iupi uniqueidentifier,
	@incluirApagados bit = 0
	as
	SET NOCOUNT ON;
SELECT c.[id]
      ,[propertyId]
	  ,idUtilizador
      ,[idTipo]
	  ,tipo.tipo
	  ,tipo.idgrupo
	  ,grupo.grupo
      ,[propertyval]
      ,[public]
	  ,c.ativo
	  ,c.dataCriado
	  ,c.utilizadorCriado
	  ,c.dataModificado
	  ,c.utilizadorModificado
  FROM [RCU].[dbo].[Utilizadores_Contactos] c
  left join [dbo].[Utilizadores] utilizador on c.idUtilizador = utilizador.id
  left join [dbo].[Utilizadores_Contactos_Tipos] tipo on c.idTipo = tipo.id
  left join [dbo].[Utilizadores_Contactos_Tipos_Grupos] grupo on tipo.idGrupo = grupo.id
  where utilizador.iupi = @iupi AND (c.ativo = 1 or c.ativo <> @incluirApagados)



GO
