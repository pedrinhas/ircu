USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_Utilizador_Regimes_R]    Script Date: 19/01/2018 16:20:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[stp_Utilizador_Regimes_R]
	@id int,
	@utilizadorEdicao nvarchar(max)
AS
BEGIN
	begin try
	begin tran
	
		SET NOCOUNT ON;

		UPDATE [dbo].[Utilizadores_Regimes]
		SET Ativo = 1, utilizadorModificado = @utilizadorEdicao, dataModificado = getdate()
		WHERE id = @id

		commit

    end try
	begin catch
		rollback
	end catch

END


GO
