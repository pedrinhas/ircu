USE [RCU]
GO
/****** Object:  StoredProcedure [dbo].[stp_OLD_Menus_MenuAplicacao_I]    Script Date: 19/01/2018 16:20:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE procEDURE [dbo].[stp_OLD_Menus_MenuAplicacao_I]
	@idAplicacao int
	, @Texto nvarchar(max)
	, @Tooltip nvarchar(max)
	, @URL nvarchar(max)
	, @IDNivelAcesso int
	, @IDParent int
	, @utilizadorEdicao nvarchar(max)
AS
BEGIN
	begin try
	begin tran
	
		DECLARE	@insertedMenu table(idMenu int)
		DECLARE @idInserted int
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		INSERT INTO [dbo].[Menus] (Texto, Tooltip, URL, idAplicacao, idMenuParent, dataCriado, utilizadorCriado)
			OUTPUT INSERTED.idMenu
				INTO @insertedMenu
		VALUES (@Texto, @Tooltip, @URL, @idAplicacao, @IDParent, getdate(), @utilizadorEdicao)

		select @idInserted = idMenu from @insertedMenu

		INSERT INTO [dbo].[Menus_Permissao] (idMenu, idNivelAcesso, dataCriado, utilizadorCriado)
		VALUES (@idInserted, @IDNivelAcesso, getdate(), @utilizadorEdicao)

		commit

	end try
	begin catch

		select ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
		
		rollback
	end catch
END


GO
